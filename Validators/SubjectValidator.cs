﻿using FluentValidation;
using VireoIntegrativeProgram.Business.Subjects;

namespace VireoIntegrativeProgram.Validators
{
    public class SubjectValidator : AbstractValidator<SubjectDto>
    {
        public SubjectValidator()
        {
            RuleFor(x => x.Name)
                   .NotNull().NotEmpty()
                   .WithMessage("The name is required");

            RuleFor(x => x.Gender)
                   .NotNull().NotEmpty()
                   .WithMessage("Gender is required");

            RuleFor(x => x.Email).NotNull().NotEmpty()
                   .WithMessage("The email address is required").EmailAddress().WithMessage("Invalid Email Address");

            RuleFor(subject => subject.Phone).Matches("\\(?\\d{3}\\)?-? *\\d{3}-? *-?\\d{4}");

            RuleFor(subject => subject.PostalCode).Matches("^\\d{5}(?:[-\\s]\\d{ 4})?$");

            RuleFor(x => x.Notes).MaximumLength(1000).WithMessage("Notes supports 1000 characters");
        }

    }
}
