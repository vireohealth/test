﻿using FluentValidation;
using VireoIntegrativeProgram.Business.Surveys;

namespace VireoIntegrativeProgram.Validators
{
    public class PatientSurveyDetailsValidator : AbstractValidator<PatientSurveyDetailsDto>
    {
        public PatientSurveyDetailsValidator()
        {
            RuleFor(x => x.Name)
                   .NotNull().NotEmpty()
                   .WithMessage("Subject name is required");
         
        }

    }
}
