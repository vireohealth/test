﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Subjects;
using VireoIntegrativeProgram.Constants;

namespace VireoIntegrativeProgram.Validators
{
    public class SetPasswordValidator : AbstractValidator<ConfirmPasswordDto>
    {
        public SetPasswordValidator()
        {
            RuleFor(x => x.NewPassword)
                   .NotNull().NotEmpty()
                   .WithMessage("New password is required");
            RuleFor(x => x.ConfirmPassword)
                   .NotNull().NotEmpty()
                   .WithMessage("Confirm password is required");

            RuleFor(x => x.NewPassword)
              .Equal(x => x.ConfirmPassword).WithMessage(MessageConstants.ConfirmPasswordMessage);

            RuleFor(x => x.NewPassword).Length(8, 128).Matches(@"^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?\W).*$").WithMessage(MessageConstants.PasswordRequiredCharactersMessage);
        }
    }
}
