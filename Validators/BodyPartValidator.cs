﻿using FluentValidation;
using VireoIntegrativeProgram.Business.BodyParts;

namespace VireoIntegrativeProgram.Validators
{
    public class BodyPartValidator : AbstractValidator<BodyPartsDto>
    {
        public BodyPartValidator()
        {
            RuleFor(x => x.BodyPartName)
                  .NotNull().NotEmpty()
                  .WithMessage("Name is required");
            RuleFor(x => x.BodyPartName).MaximumLength(100).WithMessage("Name supports 100 characters");

            RuleFor(x => x.BodyPartType)
                 .NotNull().NotEmpty()
                 .WithMessage("Type is required");

            RuleFor(x => x.BodyPartImage)
                .NotNull().NotEmpty()
                .WithMessage("Image is required");
        }
    }
}
