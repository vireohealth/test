﻿using FluentValidation;
using VireoIntegrativeProgram.Business.Recommendations;

namespace VireoIntegrativeProgram.Validators
{
    public class RecommendationValidator: AbstractValidator<RecommendationDto>
    {
        public RecommendationValidator()
        {
            RuleFor(x => x.Name)
                   .NotNull().NotEmpty()
                   .WithMessage("Name is required");
            RuleFor(x => x.Name).MaximumLength(250).WithMessage("Name supports 250 characters");
        }
    }
}
