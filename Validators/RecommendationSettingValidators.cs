﻿using FluentValidation;
using VireoIntegrativeProgram.Business.RecommendationSettings;

namespace VireoIntegrativeProgram.Validators
{
    public class RecommendationSettingValidators : AbstractValidator<RecommendationSettingDto>
    {
        public RecommendationSettingValidators()
        {
            RuleFor(x => x.Recommendation.Name)
                 .NotNull().NotEmpty()
                 .WithMessage("Recommendation is required");

            RuleFor(x => x.PainEffectMoodStatus)
                .NotNull().NotEmpty()
                .WithMessage("Pain effect in mood is required");

            RuleFor(x => x.PainEffectSleepStatus)
               .NotNull().NotEmpty()
               .WithMessage("Pain effect in sleep is required");

        }
    }
}
