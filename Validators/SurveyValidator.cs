﻿using FluentValidation;
using VireoIntegrativeProgram.Business.Surveys;

namespace VireoIntegrativeProgram.Validators
{
    public class SurveyValidator : AbstractValidator<SurveyDto>
    {
        public SurveyValidator()
        {
            RuleFor(x => x.Name)
                   .NotNull().NotEmpty()
                   .WithMessage("Survey name is required");

            RuleFor(x => x.Product.Name)
                   .NotNull().NotEmpty()
                   .WithMessage("Product is required");

            RuleFor(x => x.SurveyType.Name)
                  .NotNull().NotEmpty()
                  .WithMessage("Survey type is required");

            RuleForEach(x => x.Patients)
                   .SetValidator(new PatientSurveyDetailsValidator());
        }

    }
}
