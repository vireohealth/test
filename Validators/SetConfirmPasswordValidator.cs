﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Subjects;

namespace VireoIntegrativeProgram.Validators
{
    public class SetConfirmPasswordValidator : AbstractValidator<EmailConfirmPasswordDto>
    {
        public SetConfirmPasswordValidator()
        {
            RuleFor(x => x.NewPassword)
                   .NotNull().NotEmpty()
                   .WithMessage("New password is required");
            RuleFor(x => x.ConfirmPassword)
                   .NotNull().NotEmpty()
                   .WithMessage("Confirm password is required");

            RuleFor(x => x.NewPassword)
              .Equal(x => x.ConfirmPassword);
        }
    }
}
