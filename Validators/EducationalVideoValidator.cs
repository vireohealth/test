﻿using FluentValidation;
using VireoIntegrativeProgram.Business.EducationalVideos;

namespace VireoIntegrativeProgram.Validators
{
    public class EducationalVideoValidator : AbstractValidator<EducationalVideoDto>
    {
        public EducationalVideoValidator()
        {
            RuleFor(x => x.Title)
                   .NotNull().NotEmpty()
                   .WithMessage("Title is required");

            RuleFor(x => x.VideoPath)
                   .NotNull().NotEmpty()
                   .WithMessage("Video path is required");


        }

    }
}
