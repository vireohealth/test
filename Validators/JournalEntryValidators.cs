﻿using FluentValidation;
using VireoIntegrativeProgram.Business.JournalEntries;

namespace VireoIntegrativeProgram.Validators
{
    public class JournalEntryValidators : AbstractValidator<JournalEntryDto>
    {
        public JournalEntryValidators()
        {
            RuleFor(x => x.Title)
                  .NotNull().NotEmpty()
                  .WithMessage("Title is required");
            RuleFor(x => x.Patient.Name)
                  .NotNull().NotEmpty()
                  .WithMessage("Subject is required");
            RuleFor(x => x.Time)
                 .NotNull().NotEmpty()
                 .WithMessage("Time is required");
            RuleFor(x => x.JournalDate)
                 .NotNull().NotEmpty()
                 .WithMessage("Please select journal entry date");
        }
    }
}
