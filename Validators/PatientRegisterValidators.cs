﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Subjects;
using VireoIntegrativeProgram.Constants;

namespace VireoIntegrativeProgram.Validators
{
    public class PatientRegisterValidators : AbstractValidator<PatientDto>
    {
        public PatientRegisterValidators()
        {
            RuleFor(x => x.Name)
                   .NotNull().NotEmpty()
                   .WithMessage("Name is required");

            RuleFor(x => x.Gender)
                   .NotNull().NotEmpty()
                   .WithMessage("Gender is required");

            RuleFor(x => x.City)
                  .NotNull().NotEmpty()
                  .WithMessage("City is required");

            RuleFor(x => x.Email).NotNull().NotEmpty()
                   .WithMessage("Email address is required").EmailAddress().WithMessage("Invalid Email Address");

            RuleFor(subject => subject.Phone).Matches("\\(?\\d{3}\\)?-? *\\d{3}-? *-?\\d{4}");

            RuleFor(x => x.Password)
                    .NotNull().NotEmpty()
                    .WithMessage("New password is required");

            RuleFor(x => x.ConfirmPassword)
                   .NotNull().NotEmpty()
                   .WithMessage("Confirm password is required");

            RuleFor(x => x.Password)
              .Equal(x => x.ConfirmPassword).WithMessage(MessageConstants.ConfirmPasswordMessage); 
            
            RuleFor(x => x.Password).Length(8, 128).Matches(@"^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?\W).*$").WithMessage(MessageConstants.PasswordRequiredCharactersMessage);
        
    }
    }
}

