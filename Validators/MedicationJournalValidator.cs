﻿using FluentValidation;
using VireoIntegrativeProgram.Business.MedicationJournal;

namespace VireoIntegrativeProgram.Validators
{
    public class MedicationJournalValidator : AbstractValidator<MedicationJournalDto>
    {
        public MedicationJournalValidator()
        {
            RuleFor(x => x.Product.Name)
                  .NotNull().NotEmpty()
                  .WithMessage("Product is required");
            RuleFor(x => x.Time)
                 .NotNull().NotEmpty()
                 .WithMessage("Time is required");
            RuleFor(x => x.MedicationJournalDate)
                 .NotNull().NotEmpty()
                 .WithMessage("Please select medication journal date");
            RuleFor(x => x.Patient.Name)
                 .NotNull().NotEmpty()
                 .WithMessage("Subject is required");
        }
    }
}
