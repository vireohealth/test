﻿using FluentValidation;
using VireoIntegrativeProgram.Business.Logs;

namespace VireoIntegrativeProgram.Validators
{
    public class LogValidator : AbstractValidator<LogDto>
    {
        public LogValidator()
        {
            RuleFor(x => x.PainType)
                  .NotNull().NotEmpty()
                  .WithMessage("Pain type is required");
            RuleFor(x => x.Sleep)
                  .NotNull().NotEmpty()
                  .WithMessage("Sleep is required");
            RuleFor(x => x.Treatments)
                 .NotNull().NotEmpty()
                 .WithMessage("Treatments is required");
            RuleFor(x => x.LevelOfPain)
                 .NotNull().NotEmpty()
                 .WithMessage("Please select level of pain");
        }
    }
}
