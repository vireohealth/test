﻿using FluentValidation;
using VireoIntegrativeProgram.Business.BodyParts;

namespace VireoIntegrativeProgram.Validators
{
    public class BodyPartUpdateValidator : AbstractValidator<BodyPartUpsertDto>
    {
        public BodyPartUpdateValidator()
        {
            RuleFor(x => x.BodyPartName)
                  .NotNull().NotEmpty()
                  .WithMessage("Name is required");
            RuleFor(x => x.BodyPartName).MaximumLength(100).WithMessage("Name supports 100 characters");

            RuleFor(x => x.BodyPartType)
                 .NotNull().NotEmpty()
                 .WithMessage("Type is required");
        }
    }
}
