﻿using FluentValidation;
using VireoIntegrativeProgram.Business.Dosing;

namespace VireoIntegrativeProgram.Validators
{
    public class DosingValidators : AbstractValidator<DosingDto>
    {
        public DosingValidators()
        {
            RuleFor(x => x.PEGScore)
                   .NotNull().NotEmpty()
                   .WithMessage("Score is required");

            RuleFor(x => x.Product.Name)
                   .NotNull().NotEmpty()
                   .WithMessage("Product is required");

            RuleFor(x => x.Recomendation.Name)
                  .NotNull().NotEmpty()
                  .WithMessage("Recomendation is required");

            RuleFor(x => x.TSQMSatisfaction)
                 .NotNull().NotEmpty()
                 .WithMessage("Satisfaction is required");

            RuleFor(x => x.TSQMSeverity)
                 .NotNull().NotEmpty()
                 .WithMessage("Severity is required");

            RuleFor(x => x.TSQMSideEffects)
                 .NotNull().NotEmpty()
                 .WithMessage("Side effect is required");
        }
    }
}
