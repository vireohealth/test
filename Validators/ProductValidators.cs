﻿using FluentValidation;
using VireoIntegrativeProgram.Business.Products;

namespace VireoIntegrativeProgram.Validators
{
    public class ProductValidators : AbstractValidator<ProductDto>
    {
        public ProductValidators()
        {
            RuleFor(x => x.Name)
                   .NotNull().NotEmpty()
                   .WithMessage("Name is required");
            RuleFor(x => x.Name).MaximumLength(100).WithMessage("Name supports 100 characters");
        }
    }
}
