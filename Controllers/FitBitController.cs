﻿using Fitbit.Api.Portable;
using Fitbit.Api.Portable.OAuth2;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.FitBitTokenManager;
using VireoIntegrativeProgram.Business.Subjects;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FitBitController : BaseController<FitBitController>
    {
        private readonly FitBitSettings settings;
        private readonly ITokenManager tokenManager;
        private readonly IRevokeTokenManager revokeTokenManager;
        private readonly ISubjectService subjectService;
        private readonly string[] Scopes = new string[] { "profile", "sleep", "activity", "heartrate" };
      

        public FitBitController(FitBitSettings settings, ITokenManager tokenManager, IRevokeTokenManager revokeTokenManager, ISubjectService subjectService, ILogger<FitBitController> logger) : base(logger)
        {
            this.tokenManager=tokenManager;
            this.revokeTokenManager = revokeTokenManager;
            this.settings = settings;
            this.subjectService = subjectService;
        }
        /// <summary>
        /// Authenticates and generates oAuth URL which needs to be called/triggered 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Authenticate")]
        public ActionResult Authenticate()
        {
            Logger.LogInformation("FitBit - Authenticate triggered response");
            var appCredentials = new FitbitAppCredentials()
            {
                ClientId = settings.FitbitClientId,
                ClientSecret = settings.FitbitClientSecret
            };
            var authenticator = new OAuth2Helper(appCredentials, settings.RedirectURL);
            string[] scopes = Scopes;
            FitBitUrlDto authUrlDto = new FitBitUrlDto();
            string authUrl = authenticator.GenerateAuthUrl(scopes, null);
            authUrlDto.Url = authUrl;
            return Ok(authUrlDto);
        }

        /// <summary>
        /// Generates access token, refresh token etc.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Callback")]
        public async Task<IActionResult> Callback(string code, string patientId)
        {
            Logger.LogInformation("FitBit - Callback triggered response");
            var appCredentials = new FitbitAppCredentials()
            {
                ClientId = settings.FitbitClientId,
                ClientSecret = settings.FitbitClientSecret
            };

            var authenticator = new OAuth2Helper(appCredentials, settings.RedirectURL);
            BaseResponse response = new BaseResponse();
            OAuth2AccessToken accessToken = await authenticator.ExchangeAuthCodeForAccessTokenAsync(code);
            var hasFitbitAccountAlreadyIntegrated = await subjectService.CheckFitBitAccountAlreadyIntegrated(patientId, accessToken.UserId);
            if(hasFitbitAccountAlreadyIntegrated)
            {
                Logger.LogInformation("FitBit - Callback already integrated response", patientId);
                response.Data = null;
                response.ResponseCode = HTTPConstants.BAD_REQUEST;
                response.ResponseMessage = MessageConstants.FitbitAlreadyIntegratedMessage;
                return BadRequest(response);
            }
            var subject = await subjectService.GetBySubjectId(patientId);
          
            if (subject != null)
            {
                subject.FitBitAccessToken = new AccessTokenDto
                {
                    ExpiresIn = accessToken.ExpiresIn,
                    RefreshToken = accessToken.RefreshToken,
                    Scope = accessToken.Scope,
                    Token = accessToken.Token,
                    TokenType = accessToken.TokenType,
                    UserId = accessToken.UserId,
                    UtcExpirationDate = accessToken.UtcExpirationDate
                };
                subjectService.UpdateAsync(subject.Id, subject);

                response.Data = accessToken;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.FitbitAccessTokenMessage;
                Logger.LogInformation("FitBit - Callback success response", accessToken);
                return Ok(response);
            }
            else
            {

                Logger.LogInformation("FitBit - Callback failure response", subject);
                response.Data = null;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SubjectNotFoundMessage;

            }

            return BadRequest(response);
        }

        /// <summary>
        /// Generates new access token using old tokens
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRefreshTokenAsync")]
        public async Task<IActionResult> GetRefreshTokenAsync([FromQuery] AccessTokenDto accessToken, string patientId)
        {
            Logger.LogInformation("FitBit - GetRefreshTokenAsync triggered response");
            var appCredentials = new FitbitAppCredentials()
            {
                ClientId = settings.FitbitClientId,
                ClientSecret = settings.FitbitClientSecret
            };
            OAuth2AccessToken oauthToken = new OAuth2AccessToken();
            oauthToken.Token = accessToken.Token;
            oauthToken.RefreshToken = accessToken.RefreshToken;
            oauthToken.Scope = accessToken.Scope;
            oauthToken.TokenType = accessToken.TokenType;
            oauthToken.UserId = accessToken.UserId;
            oauthToken.UtcExpirationDate = accessToken.UtcExpirationDate;
            oauthToken.ExpiresIn = accessToken.ExpiresIn;
            
            var client = new FitbitClient(appCredentials, oauthToken);
            var newAccessToken = await tokenManager.RefreshTokenAsync(client);
            var subject = await subjectService.GetBySubjectId(patientId);
            BaseResponse response = new BaseResponse();
            if (subject != null)
            {
                subject.FitBitAccessToken = new AccessTokenDto
                {
                    ExpiresIn = newAccessToken.ExpiresIn,
                    RefreshToken = newAccessToken.RefreshToken,
                    Scope = newAccessToken.Scope,
                    Token = newAccessToken.Token,
                    TokenType = newAccessToken.TokenType,
                    UserId = newAccessToken.UserId,
                    UtcExpirationDate = newAccessToken.UtcExpirationDate
                };
                subjectService.UpdateAsync(subject.Id, subject);

                response.Data = newAccessToken;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.FitbitRefreshAccessTokenMessage;
                Logger.LogInformation("FitBit - GetRefreshTokenAsync success response", newAccessToken);
                return Ok(response);
            }
            else
            {

                Logger.LogInformation("FitBit - GetRefreshTokenAsync failure response", subject);
                response.Data = null;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SubjectNotFoundMessage;

            }

            return BadRequest(response);
        }

        /// <summary>
        /// Revoking token
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRevokeTokenAsync")]
        public async Task<IActionResult> GetRevokeTokenAsync([FromQuery] AccessTokenDto accessToken, string patientId)
        {
            Logger.LogInformation("FitBit - GetRevokeTokenAsync triggered response");
            var appCredentials = new FitbitAppCredentials()
            {
                ClientId = settings.FitbitClientId,
                ClientSecret = settings.FitbitClientSecret
            };
            OAuth2AccessToken oauthToken = new OAuth2AccessToken();
            oauthToken.Token = accessToken.Token;
            oauthToken.RefreshToken = accessToken.RefreshToken;
            oauthToken.Scope = accessToken.Scope;
            oauthToken.TokenType = accessToken.TokenType;
            oauthToken.UserId = accessToken.UserId;
            oauthToken.UtcExpirationDate = accessToken.UtcExpirationDate;
            oauthToken.ExpiresIn = accessToken.ExpiresIn;

            var client = new FitbitClient(appCredentials, oauthToken);
            var revokeToken = await revokeTokenManager.RevokeTokenAsync(client);
            var subject = await subjectService.GetBySubjectId(patientId);
            BaseResponse response = new BaseResponse();
            if (subject != null)
            {
                subject.FitBitAccessToken = null;
                subjectService.UpdateAsync(subject.Id, subject);

                response.Data = revokeToken;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.FitbitRevokeAccessTokenMessage;
                Logger.LogInformation("FitBit - GetRevokeTokenAsync success response", revokeToken);
                return Ok(response);
            }
            else
            {

                Logger.LogInformation("FitBit - GetRefreshTokenAsync failure response", subject);
                response.Data = null;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SubjectNotFoundMessage;
            }

            return BadRequest(response);
        }

        /// <summary>
        /// Retrieves today's activity info
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetActivityAsync")]
        public async Task<IActionResult> GetActivityAsync([FromQuery] AccessTokenDto accessToken)
        {
            Logger.LogInformation("FitBit - GetActivityAsync triggered response");
            var appCredentials = new FitbitAppCredentials()
            {
                ClientId = settings.FitbitClientId,
                ClientSecret = settings.FitbitClientSecret
            };
            OAuth2AccessToken oauthToken = new OAuth2AccessToken();
            oauthToken.Token = accessToken.Token;
            oauthToken.RefreshToken = accessToken.RefreshToken;
            oauthToken.Scope = accessToken.Scope;
            oauthToken.TokenType = accessToken.TokenType;
            oauthToken.UserId = accessToken.UserId;
            oauthToken.UtcExpirationDate = accessToken.UtcExpirationDate;
            oauthToken.ExpiresIn = accessToken.ExpiresIn;
            BaseResponse response = new BaseResponse();
            var client = new FitbitClient(appCredentials, oauthToken);
            try
            {
                var activityInfo = await client.GetDayActivityAsync(System.DateTime.Today);
                Logger.LogInformation("FitBit - GetActivityAsync success response", activityInfo);
                response.Data = activityInfo;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.FitBitActivityStatus;
                return Ok(response);
            }
            catch(Exception ex)
            {
                Logger.LogInformation("FitBit - GetActivityAsync failure response", ex.Message);
                response.Data = ex.Message;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.FitBitIntegratedExpired;
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Retrieves today's heart rate info
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetHeartRateAsync")]
        public async Task<IActionResult> GetHeartRateAsync([FromQuery] AccessTokenDto accessToken)
        {
            Logger.LogInformation("FitBit - GetHeartRateAsync triggered response");
            var appCredentials = new FitbitAppCredentials()
            {
                ClientId = settings.FitbitClientId,
                ClientSecret = settings.FitbitClientSecret
            };
            OAuth2AccessToken oauthToken = new OAuth2AccessToken();
            oauthToken.Token = accessToken.Token;
            oauthToken.RefreshToken = accessToken.RefreshToken;
            oauthToken.Scope = accessToken.Scope;
            oauthToken.TokenType = accessToken.TokenType;
            oauthToken.UserId = accessToken.UserId;
            oauthToken.UtcExpirationDate = accessToken.UtcExpirationDate;
            oauthToken.ExpiresIn = accessToken.ExpiresIn;

            var client = new FitbitClient(appCredentials, oauthToken);
            BaseResponse response = new BaseResponse();
           
            try
            {
                var heartRateInfo = await client.GetHeartRateTimeSeries(System.DateTime.Today, Fitbit.Models.DateRangePeriod.OneDay);
                Logger.LogInformation("FitBit - GetHeartRateAsync success response", heartRateInfo);
                response.Data = heartRateInfo;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.FitBitHeartRateStatus;
                return Ok(response);
            }
            catch (Exception ex)
            {
                Logger.LogInformation("FitBit - GetHeartRateAsync failure response", ex.Message);
                response.Data = ex.Message;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.FitBitIntegratedExpired;
                return BadRequest(response);
            }
            
        }

        /// <summary>
        /// Retrieves today's sleep info
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSleepAsync")]
        public async Task<IActionResult> GetSleepAsync([FromQuery] AccessTokenDto accessToken)
        {
            Logger.LogInformation("FitBit - GetSleepAsync triggered response");
            var appCredentials = new FitbitAppCredentials()
            {
                ClientId = settings.FitbitClientId,
                ClientSecret = settings.FitbitClientSecret
            };
            OAuth2AccessToken oauthToken = new OAuth2AccessToken();
            oauthToken.Token = accessToken.Token;
            oauthToken.RefreshToken = accessToken.RefreshToken;
            oauthToken.Scope = accessToken.Scope;
            oauthToken.TokenType = accessToken.TokenType;
            oauthToken.UserId = accessToken.UserId;
            oauthToken.UtcExpirationDate = accessToken.UtcExpirationDate;
            oauthToken.ExpiresIn = accessToken.ExpiresIn;

            var client = new FitbitClient(appCredentials, oauthToken);
            BaseResponse response = new BaseResponse();
           
            try
            {
                var sleepInfo = await client.GetSleepAsync(System.DateTime.Today);
                Logger.LogInformation("FitBit - GetSleepAsync success response", sleepInfo);
                response.Data = sleepInfo;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.FitBitSleepStatus;
                return Ok(response);
            }
            catch (Exception ex)
            {
                Logger.LogInformation("FitBit - GetSleepAsync failure response", ex.Message);
                response.Data = ex.Message;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.FitBitIntegratedExpired;
                return BadRequest(response);
            }
        }

    }
}
