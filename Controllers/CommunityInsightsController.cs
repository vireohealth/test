﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Logs;
using VireoIntegrativeProgram.Business.Subjects;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CommunityInsightsController : BaseController<CommunityInsightsController>
    {
        private readonly ILogsService logService;
        private readonly ISubjectService subjectService;

        public CommunityInsightsController(ILogsService logService, ISubjectService subjectService, ILogger<CommunityInsightsController> logger) : base(logger)
        {
            this.logService = logService;
            this.subjectService = subjectService;
        }
        /// <summary>
        /// Gets top pain logged for other users
        /// </summary>
        /// <param name="dateCategory"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTopPainLoggedForOtherUsers")]
        public async Task<IActionResult> GetTopPainLoggedForOtherUsers(string dateCategory, string patientId)
        {
            Logger.LogInformation("Community Insights-GetTopPainLoggedForOtherUsers triggered");
            BaseResponse response = new BaseResponse();
            var patients = await subjectService.GetAllAsync();
            var insightDetails = await logService.GetTopPainLoggedOtherUsers(dateCategory, patientId, patients.Where(i=>i.Id != patientId).ToList());
            if (insightDetails != null)
            {
                response.Data = insightDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.TopPainForOtherUsersFetchSuccessMessage;
                Logger.LogInformation("Community Insights-GetTopPainLoggedForOtherUsers success response", insightDetails);
                return Ok(response);
            }
            else
            {
                response.Data = insightDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.TopPainForOtherUsersFetchFailureMessage;
                Logger.LogInformation("Community Insights-GetTopPainLoggedForOtherUsers failure response", insightDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        ///  Gets top treatments for other users
        /// </summary>
        /// <param name="dateCategory"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTopTreatmentsForOtherUsers")]
        public async Task<IActionResult> GetTopTreatmentsForOtherUsers(string dateCategory, string patientId)
        {
            Logger.LogInformation("Community Insights-GetTopTreatmentsForOtherUsers triggered");
            BaseResponse response = new BaseResponse();
            var patients = await subjectService.GetAllAsync();
            var insightDetails = await logService.GetTopTreatmentsForOtherUsers(dateCategory, patientId, patients.Where(i => i.Id != patientId).ToList());
            if (insightDetails != null)
            {
                response.Data = insightDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.TopTreatmentsForOtherUsersFetchSuccessMessage;
                Logger.LogInformation("Community Insights-GetTopTreatmentsForOtherUsers success response", insightDetails);
                return Ok(response);
            }
            else
            {
                response.Data = insightDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.TopTreatmentsForOtherUsersFetchFailureMessage;
                Logger.LogInformation("Community Insights-GetTopTreatmentsForOtherUsers failure response", insightDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Gets effect of pain in sleep for other users
        /// </summary>
        /// <param name="dateCategory"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPainEffectSleepForOtherUsers")]
        public async Task<IActionResult> GetPainEffectSleepForOtherUsers(string dateCategory, string patientId)
        {
            Logger.LogInformation("Community Insights-GetPainEffectSleepForOtherUsers triggered");
            BaseResponse response = new BaseResponse();
            var patients = await subjectService.GetAllAsync();
            var insightDetails = await logService.GetPainEffectSleepForOtherUsers(dateCategory, patientId, patients.Where(i => i.Id != patientId).ToList());
            if (insightDetails != null)
            {
                response.Data = insightDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.PainEffectSleepForOtherUsersFetchSuccessMessage;
                Logger.LogInformation("Community Insights-GetPainEffectSleepForOtherUsers success response", insightDetails);
                return Ok(response);
            }
            else
            {
                response.Data = insightDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.PainEffectSleepForOtherUsersFetchFailureMessage;
                Logger.LogInformation("Community Insights-GetPainEffectSleepForOtherUsers failure response", insightDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Gets soft-gel capsule feedback from other users
        /// </summary>
        /// <param name="dateCategory"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetFeedbackOtherUsers")]
        public async Task<IActionResult> GetFeedbackOtherUsers(string dateCategory, string patientId)
        {
            Logger.LogInformation("Community Insights-GetFeedbackOtherUsers triggered");
            BaseResponse response = new BaseResponse();
            var patients = await subjectService.GetAllAsync();
            var insightDetails = await logService.GetFeedbackOtherUsers(dateCategory, patientId, patients.Where(i => i.Id != patientId).ToList());
            if (insightDetails != null)
            {
                response.Data = insightDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.FeedbackOtherUsersFetchSuccessMessage;
                Logger.LogInformation("Community Insights-GetFeedbackOtherUsers success response", insightDetails);
                return Ok(response);
            }
            else
            {
                response.Data = insightDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.FeedbackOtherUsersFetchFailureMessage;
                Logger.LogInformation("Community Insights-GetFeedbackOtherUsers failure response", insightDetails);
                return BadRequest(response);
            }
        }
    }
}
