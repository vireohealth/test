﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Countries;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CountriesController : BaseController<CountriesController>
    {
        private const string DefaultCountryCode = "US";
        private readonly ICountryService countryService;
        public CountriesController(ICountryService countryService, ILogger<CountriesController> logger) : base(logger)
        {
            this.countryService = countryService;
        }

        /// <summary>
        /// Gets all states under country
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStates")]
        public async Task<IActionResult> GetStatesByCountryCodeAsync(string code = DefaultCountryCode)
        {
            Logger.LogInformation("States-GetStatesByCountryCodeAsync triggered.");
            BaseResponse response = new BaseResponse();
            var countries = await countryService.GetStatesByCountryCodeAsync(code);
            if (countries != null)
            {
                response.Data = countries;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.StateSuccessMessage;
                Logger.LogInformation("States-GetStatesByCountryCodeAsync success response", countries);
                return Ok(response);
            }
            else
            {
                response.Data = countries;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.StateNotFoundMessage;
            }
            Logger.LogInformation("States-GetStatesByCountryCodeAsync failure response", countries);
            return BadRequest(response);
        }
    }
}