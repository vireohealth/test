﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Logs;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class LogsController : BaseController<LogsController>
    {
        private readonly ILogsService logService;

        public LogsController(ILogsService logService, ILogger<LogsController> logger) : base(logger)
        {
            this.logService = logService;
        }

        /// <summary>
        /// Get logs by date and subjectId
        /// </summary>
        /// <param name="dateSelected"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        [Route("LogByDate")]
        public async Task<IActionResult> GetLogsByDate(DateTime dateSelected, string subjectId)
        {
            Logger.LogInformation("Logs-GetLogsByDate triggered");
            BaseResponse response = new BaseResponse();
            var logDetails = await logService.GetLogByDateAndSubjectId(dateSelected, subjectId);
            if(logDetails!=null)
            {
                response.Data = logDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.LogsFetchSuccessMessage;
                Logger.LogInformation("Logs-GetLogsByDate success response", logDetails);
                return Ok(response);
            }
            else
            {
                response.Data = logDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.LogsFetchFailureMessage;
                Logger.LogInformation("Logs-GetLogsByDate failure response", logDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Upsert log
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Upsert")]
        public IActionResult UpsertLogs(LogDto dto)
        {
            Logger.LogInformation("Upsert log triggered.");
            BaseResponse response = new BaseResponse();
            var log = logService.UpsertLog(dto);

            if (log == null)
            {
                var error = new Helpers.ErrorDto { Message = "Log failed while saving" };
                Logger.LogError("Upsert Logs failed", error);
                response.Data = log;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = error.Message;
                return BadRequest(response);
            }
            else
            {
                Logger.LogInformation("Upserting log response", log);
                response.Data = log;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.LogsSavedSuccessMessage;
                return Ok(response);
            }
        }

        /// <summary>
        /// Check feedback allowed or not for a medicine
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("CheckProductFeedback")]
        public async Task<IActionResult> CheckProductFeedback(string subjectId, string productId)
        {
            Logger.LogInformation("Logs-CheckProductFeedback triggered.");

            BaseResponse response = new BaseResponse();
            var logDetails = await logService.CheckFeedbackProvidedEarlier(subjectId, productId);
            if (logDetails == false)
            {
                response.Data = logDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.ProductFeedbackAllowedMessage;
                Logger.LogInformation("Logs-CheckProductFeedback success response", logDetails);
                return Ok(response);
            }
            else
            {
                response.Data = logDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.ProductFeedbackNotAllowedMessage;
                Logger.LogInformation("Logs-CheckProductFeedback failed response", logDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Check vireo product already logged or not
        /// </summary>
        /// <param name="dateSelected"></param>
        /// <param name="subjectId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("CheckAlreadyProductLogged")]
        public async Task<IActionResult> CheckAlreadyProductLogged(DateTime dateSelected, string subjectId, string productId)
        {
            Logger.LogInformation("Logs-CheckAlreadyProductLogged triggered.");

            BaseResponse response = new BaseResponse();
            var hasLogged = await logService.CheckAlreadyProductLogged(dateSelected,subjectId, productId);
            if (hasLogged)
            {
                response.Data = hasLogged;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.ProductFeedbackAllowedMessage;
                Logger.LogInformation("Logs-CheckAlreadyProductLogged success response", hasLogged);
                return Ok(response);
            }
            else
            {
                response.Data = hasLogged;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.ProductFeedbackNotAllowedMessage;
                Logger.LogInformation("Logs-CheckAlreadyProductLogged failed response", hasLogged);
                return BadRequest(response);
            }
        }
    }
}
