﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Subjects;
using VireoIntegrativeProgram.Business.Surveys;
using VireoIntegrativeProgram.Business.TempSubjects;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Helpers;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SubjectController : BaseController<SubjectController>
    {
        private const string Patient = "Patient";
        private readonly ISubjectService subjectService;
        private readonly ISurveyService surveyService;
        private readonly ITempSubjectService tempSubjectService;

        public SubjectController(ISubjectService userManagementService, ISurveyService surveyService, ITempSubjectService tempSubjectService, ILogger<SubjectController> logger) : base(logger)
        {
            this.subjectService = userManagementService;
            this.surveyService = surveyService;
            this.tempSubjectService = tempSubjectService;
        }

        /// <summary>
        /// Get All Paginated Subjects
        /// </summary>
        /// <param name="subjectParams"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("PaginatedSubjects")]
        public async Task<IActionResult> GetAllPaginationAsync([FromQuery] PaginationParams subjectParams)
        {
            Logger.LogInformation("Subjects - GetAllPaginationAsync triggered.");
            BaseResponse response = new BaseResponse();
            var subjects = await subjectService.GetAllPaginationAsync(subjectParams);

            Response.AddPaginationHeader(subjects.CurrentPage, subjects.PageSize,
               subjects.TotalCount, subjects.TotalPages);

            if (subjects != null)
            {
                response.Data = subjects;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SubjectsFetchMessage;
                Logger.LogInformation("Subjects - GetAllPaginationAsync success response", subjects);
                return Ok(response);
            }
            else
            {
                response.Data = subjects;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SubjectNotFoundMessage;
            }

            Logger.LogInformation("Subjects - GetAllPaginationAsync failure response", subjects);

            return BadRequest(response);
        }

        /// <summary>
        /// Get all subjects
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllAsync()
        {
            Logger.LogInformation("Subject list-GetAllAsync triggered.");
            BaseResponse response = new BaseResponse();
            var subjectList = await subjectService.GetAllAsync();
            if (subjectList != null)
            {
                response.Data = subjectList;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SubjectsFetchMessage;
                Logger.LogInformation("Subject list-GetAllAsync success response", subjectList);
                return Ok(response);
            }
            else
            {
                response.Data = subjectList;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SubjectListFetchFailureMessage;
            }
            Logger.LogInformation("Subject list-GetAllAsync failure response", subjectList);
            return BadRequest(response);
        }

        /// <summary>
        /// Checks whether the reset link is expired or not
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("CheckResetLinkExpired")]
        public async Task<IActionResult> CheckResetLinkExpired(string email)
        {
            Logger.LogInformation("Check reset link expired or not - CheckResetLinkExpired triggered.");

            BaseResponse response = new BaseResponse();
            var isResetLinkExpired = await subjectService.IsResetLinkExpired(email);
            if (isResetLinkExpired)
            {
                response.Data = isResetLinkExpired;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.CheckResetLinkExpiredMessage;
                Logger.LogInformation("Check reset link expired or not - CheckResetLinkExpired expired response", isResetLinkExpired);
                return BadRequest(response);
            }
            else
            {
                response.Data = isResetLinkExpired;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.CheckResetLinkAllowedMessage;
            }
            Logger.LogInformation("Check reset link expired or not - CheckResetLinkExpired not expired response", isResetLinkExpired);
            return Ok(response);
        }

        /// <summary>
        /// Reset Passsord
        /// </summary>
        /// <param name="resetPasswordDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ResetPassword")]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ConfirmPasswordDto resetPasswordDto)
        {
            Logger.LogInformation("Reset new password - triggered.");
            BaseResponse response = new BaseResponse();            
            // Check whether the last three passwords are matching with new password.
            var hasLastThreePasswordMatched = subjectService.CheckLastThreePasswordMatch(resetPasswordDto); 
            if (hasLastThreePasswordMatched)
            {
                response.Data = hasLastThreePasswordMatched;
                response.ResponseCode = HTTPConstants.BAD_REQUEST;
                response.ResponseMessage = MessageConstants.CheckLastThreePasswordMatchMessage;
                Logger.LogError("Reset new password  error- Last three password has been matched.", hasLastThreePasswordMatched);
                return BadRequest(response);
            }

            // Check password creation rules.          
            var isValidPassword = subjectService.ValidatePasswordRules(resetPasswordDto);
            if (!isValidPassword)
            {
                response.Data = isValidPassword;
                response.ResponseCode = HTTPConstants.BAD_REQUEST;
                response.ResponseMessage = MessageConstants.SetPasswordFailureMessage;
                Logger.LogError("Reset new password  error- Password rule verification failed.", isValidPassword);
                return BadRequest(response);
            }

            var validationResult = await subjectService.SetPassword(resetPasswordDto);
            if (!validationResult)
            {
                response.Data = validationResult;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SetPasswordFailureMessage;
                Logger.LogError("Reset new password  error.", validationResult);
                return NotFound(response);
            }
            response.Data = validationResult;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.SetPasswordSuccessMessage;
            Logger.LogInformation("Reset new password  success.");
            return Ok(response);
        }

        /// <summary>
        /// Unique check of subject name
        /// </summary>
        /// <param name="id"></param>
        /// <param name="subjectName"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("SubjectNameUniqueCheck")]
        public async Task<IActionResult> SubjectNameUniqueCheck(string id, string subjectName)
        {
            Logger.LogInformation("Subject name unique check-SubjectNameUniqueCheck triggered.");

            BaseResponse response = new BaseResponse();
            var subjectDetails = await subjectService.CheckUniqueSubjectName(id, subjectName);
            if (subjectDetails)
            {
                response.Data = subjectDetails;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.SubjectNameExistsMessage;
                Logger.LogInformation("Subject name unique check-SubjectNameUniqueCheck already exists response", subjectDetails);
                return BadRequest(response);
            }
            else
            {
                response.Data = subjectDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SubjectNameAvailableMessage;
            }
            Logger.LogInformation("Subject name unique check-SubjectNameUniqueCheck available response", subjectDetails);
            return Ok(response);
        }

        /// <summary>
        /// Adds subject
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Add")]
        public IActionResult AddSubject(SubjectDto dto)
        {
            Logger.LogInformation("Add subject - AddSubject triggered.");
            BaseResponse response = new BaseResponse();
            var subject = subjectService.AddSubject(dto);

            if (subject == null)
            {
                var error = new ErrorDto { Message = "Email already exist!" };
                response.Data = error;
                response.ResponseCode = HTTPConstants.BAD_REQUEST;
                response.ResponseMessage = MessageConstants.SubjectFailureMessage;
                Logger.LogError("Add subject - AddSubject Failed", error);
                return BadRequest(response);
            }
            else
            {
                response.Data = subject;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SubjectCreatedMessage;
                Logger.LogInformation("Add subject - AddSubject success response", subject);
                return Ok(response);
            }
        }

        /// <summary>
        /// Confirm password
        /// </summary>
        /// <param name="confirmPasswordDto"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("ConfirmPassword")]
        public IActionResult AddConfirmPassword(ConfirmPasswordDto confirmPasswordDto)
        {
            Logger.LogInformation("Set new password - triggered.");

            BaseResponse response = new BaseResponse();
            // Check whether the last three passwords are matching with new password.
            var hasLastThreePasswordMatched = subjectService.CheckLastThreePasswordMatch(confirmPasswordDto);           
            if (hasLastThreePasswordMatched)
            {
                response.Data = hasLastThreePasswordMatched;
                response.ResponseCode = HTTPConstants.BAD_REQUEST;
                response.ResponseMessage = MessageConstants.CheckLastThreePasswordMatchMessage;
                Logger.LogError("Set new password  error- Last three password has been matched.", hasLastThreePasswordMatched);
                return BadRequest(response);
            }

            // Check password creation rules.      
            var isValidPassword = subjectService.ValidatePasswordRules(confirmPasswordDto);
            if (!isValidPassword)
            {
                response.Data = isValidPassword;
                response.ResponseCode = HTTPConstants.BAD_REQUEST;
                response.ResponseMessage = MessageConstants.SetPasswordFailureMessage;
                Logger.LogError("Set new password  error- Password rule verification failed.", isValidPassword);
                return BadRequest(response);
            }

            if (confirmPasswordDto.PreviousPassword != null)
            {
                var hasCurrentPasswordMatched = subjectService.CheckCurrentPasswordMatch(confirmPasswordDto);
                if (hasCurrentPasswordMatched == false)
                {
                    response.Data = hasCurrentPasswordMatched;
                    response.ResponseCode = HTTPConstants.BAD_REQUEST;
                    response.ResponseMessage = MessageConstants.CheckCurrentPasswordMatchMessage;
                    Logger.LogError("Set new password  error- current password is not matching.", hasCurrentPasswordMatched);
                    return BadRequest(response);
                }
            }

            var validationResult = subjectService.SetPasswordForUser(confirmPasswordDto);

            if (!validationResult)
            {
                response.Data = validationResult;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SetPasswordFailureMessage;
                Logger.LogError("Set new password  error.", validationResult);
                return NotFound(response);
            }

            var subjectInfo = subjectService.GetBySubjectId(confirmPasswordDto.Uuid);
            if (subjectInfo.Result.UserType.ToLower() == Patient.ToLower())
            {
                var patientForSurveys = subjectInfo.Result.ToPatientSurvey();
                var findAndUpdatedToSurveys = surveyService.FindAndUpdateToSelectAllSurveys(patientForSurveys);
                if (findAndUpdatedToSurveys.Result)
                    Logger.LogInformation("Surveys updated with new patient info - success.");
                else
                    Logger.LogInformation("Surveys updated with new patient info - failure ");
            }
            response.Data = validationResult;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.SetPasswordSuccessMessage;
            Logger.LogInformation("Set new password  success.");
            return Ok(response);
        }

        /// <summary>
        /// Checks whether the user is activated
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("IsActivatedUser")]
        public IActionResult IsActivatedUser(string uuid)
        {
            Logger.LogInformation("Is activated triggered.", uuid);
            BaseResponse response = new BaseResponse();
            var user = subjectService.GetBySubjectId(uuid);

            if (user.Result != null && user.Result.IsActive)
            {
                response.Data = user.Result.IsActive;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.NotAccessibleMessage;
                Logger.LogError("Is activated error.", user.Result.IsActive);
                return NotFound(response);
            }
            response.Data = user.Result != null ? user.Result.IsActive : false;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.UserCanAccessMessage;
            Logger.LogInformation("Is activated success.", uuid);
            return Ok(response);
        }

        /// <summary>
        /// Updates the subject information
        /// </summary>
        /// <param name="id"></param>
        /// <param name="subjectIn"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(string id, [FromForm] SubjectDto subjectIn)
        {
            Logger.LogInformation("Update Subject-Update triggered.");
            BaseResponse response = new BaseResponse();
            var subject = await subjectService.GetBySubjectId(id);

            if (subject == null)
            {
                Logger.LogError("Update Subject-Update failed", subjectIn);
                response.Data = subjectIn;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SubjectNotFoundMessage;
                return NotFound(response);
            }
            if (subjectIn.ProfilePic != null)
            {
                var imageUploadResult = await subjectService.UploadProfilePic(subjectIn.ProfilePic);
                if (!imageUploadResult.IsUploaded)
                {
                    Logger.LogError("Update Subject - Upload image failed", imageUploadResult);
                    response.Data = imageUploadResult;
                    response.ResponseCode = HTTPConstants.NOT_FOUND;
                    response.ResponseMessage = imageUploadResult.Message;
                    return BadRequest(response);
                }

                subjectIn.ProfilePicName = imageUploadResult.ImageName;
            }
            else
            {
                subjectIn.ProfilePicName = subject.ProfilePicName;
            }

            subjectIn.CreatedOn = subject.CreatedOn;
            subjectIn.ActivatedOn = subject.ActivatedOn;
            subjectIn.ResetMailSentDate = subject.ResetMailSentDate;
            subjectIn.LastUpdatedOn = DateTime.UtcNow;
            subjectIn.Password = subject.Password;
            subjectIn.LastPasswordUpdatedDate = subject.LastPasswordUpdatedDate;
            subjectIn.IsMailExpired = subject.IsMailExpired;
            subjectService.UpdateAsync(id, subjectIn);
            response.Data = subjectIn;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.SubjectUpdatedMessage;
            Logger.LogError("Update Subject-Update success", subjectIn);
            return Ok(response);
        }

        /// <summary>
        /// Subject details by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Details")]
        public async Task<IActionResult> GetSubjectDetails(string id)
        {
            Logger.LogInformation("Subject details-GetSubjectDetails triggered.");

            BaseResponse response = new BaseResponse();
            var subjectDetails = await subjectService.GetBySubjectId(id);
            if (subjectDetails != null)
            {
                response.Data = subjectDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SubjectDetailsFetchSuccessMessage;
                Logger.LogInformation("Subject details-GetSubjectDetails success response", subjectDetails);
                return Ok(response);
            }
            else
            {
                response.Data = subjectDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SubjectDetailsFetchFailureMessage;
                Logger.LogInformation("Subject details-GetSubjectDetails failed response", subjectDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Unique check of subject email
        /// </summary>
        /// <param name="id"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("EmailUniqueCheck")]
        public async Task<IActionResult> EmailUniqueCheck(string id, string email)
        {
            Logger.LogInformation("Email unique check-EmailUniqueCheck triggered.");

            BaseResponse response = new BaseResponse();
            var subjectEmailDetails = await subjectService.CheckUniqueEmail(id, email);
            if (subjectEmailDetails == true)
            {
                response.Data = subjectEmailDetails;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.SubjectEmailExistsMessage;
                Logger.LogInformation("Email unique check-EmailUniqueCheck already exists response", subjectEmailDetails);
                return BadRequest(response);
            }
            else
            {
                response.Data = subjectEmailDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SubjectEmailAvailableMessage;
            }
            Logger.LogInformation("Email unique check-EmailUniqueCheck available response", subjectEmailDetails);
            return Ok(response);
        }

        /// <summary>
        /// Delete subject
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> DeleteSubject(string subjectId)
        {
            Logger.LogInformation("Subject-DeleteSubject triggered. subjectId:", subjectId);
            BaseResponse response = new BaseResponse();
            var subjectDetails = await subjectService.GetBySubjectId(subjectId);

            if (subjectDetails == null)
            {
                response.Data = subjectDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SubjectDeleteFailureMessage;
                Logger.LogError("Subject-DeleteSubject failed : subjectId =", subjectId);
                Logger.LogInformation("Subject-DeleteSubject failure response", response);
                return NotFound(response);
            }
            else
            {
                await subjectService.DeleteUserById(subjectDetails.Id);
                response.Data = "";
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SubjectDeleteSuccessMessage;
                Logger.LogInformation("Subject-DeleteSubject response", response);
            }
            return Ok(response);
        }

        /// <summary>
        /// Register patient
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("Register")]
        public IActionResult RegisterPatient(PatientDto dto)
        {
            Logger.LogInformation("Register patient - RegisterPatient triggered.");
            BaseResponse response = new BaseResponse();


            if (dto.Name.ToUpper() == dto.Password.ToUpper())
            {
                var error = new ErrorDto { Message = "Password is same as account name." };
                response.Data = error;
                response.ResponseCode = HTTPConstants.BAD_REQUEST;
                response.ResponseMessage = MessageConstants.PatientRegisterFailureMessage;
                Logger.LogError("Register patient - Password rule verification failed.Password is same as account name", error);
                return BadRequest(response);
            }

            var patient = subjectService.RegisterPatient(dto);   
            if (patient == null)
            {
                var error = new ErrorDto { Message = "Email already exist!" };
                response.Data = error;
                response.ResponseCode = HTTPConstants.BAD_REQUEST;
                response.ResponseMessage = MessageConstants.PatientRegisterFailureMessage;
                Logger.LogError("Register patient - RegisterPatient Failed", error);
                return BadRequest(response);
            }
            else
            {
                tempSubjectService.UpdateTempUser(dto.Email);
                var subjectInfo = subjectService.GetBySubjectByMail(dto.Email);
                if (subjectInfo.Result.UserType.ToLower() == Patient.ToLower())
                {
                    var patientForSurveys = subjectInfo.Result.ToPatientSurvey();
                    var findAndUpdatedToSurveys = surveyService.FindAndUpdateToSelectAllSurveys(patientForSurveys);
                    if (findAndUpdatedToSurveys.Result)
                        Logger.LogInformation("Surveys updated with new patient info - success.");
                    else
                        Logger.LogInformation("Surveys updated with new patient info - failure ");
                }
                response.Data = patient;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.PatientRegisterCreatedMessage;
                Logger.LogInformation("Register patient - RegisterPatient success response", patient);
                return Ok(response);
            }
        }

        /// <summary>
        /// Removes the profile pic of subject
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="imageName"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteProfilePic")]
        public async Task<IActionResult> DeleteProfilePic(string subjectId, string imageName)
        {
            Logger.LogInformation("DeleteProfilePic - Delete profile pic triggered. subjectId:", subjectId);
            BaseResponse response = new BaseResponse();
            var subject = await subjectService.GetBySubjectId(subjectId);

            if (subject == null)
            {
                response.Data = subject;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.ProfilePicDeleteFailureMessage;
                Logger.LogError("ProfilePic - Delete profile pic failed : subjectId =", subjectId);
                return NotFound(response);
            }
            else
            {
                subjectService.DeleteProfilePic(imageName);
                await subjectService.RemoveProfileImage(subject.Id);
                response.Data = "";
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.ProfilePicDeleteSuccessMessage;
                Logger.LogError("DeleteProfilePic - Delete profile pic success : subjectId =", subjectId);
                return Ok(response);
            }
        }



    }
}
