﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.UnitOfMeasurements;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UnitOfMeasurementController : BaseController<UnitOfMeasurementController>
    {
        private readonly IUnitOfMeasurementService unitOfMeasurementService;

        public UnitOfMeasurementController(IUnitOfMeasurementService unitMeasureService, ILogger<UnitOfMeasurementController> logger) : base(logger)
        {
            this.unitOfMeasurementService = unitMeasureService;
        }

        /// <summary>
        /// List of Unit Of Measures
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllAsync()
        {
            Logger.LogInformation("Unit of measure-GetAllAsync triggered.");

            BaseResponse response = new BaseResponse();
            var unitOfMeasure = await unitOfMeasurementService.GetAllAsync();
            if (unitOfMeasure != null)
            {
                response.Data = unitOfMeasure;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.UnitOfMeasureSuccessMessage;
                Logger.LogInformation("Unit of measure-GetAllAsync success response", unitOfMeasure);
                return Ok(response);
            }
            else
            {
                response.Data = unitOfMeasure;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.UnitOfMeasureFailureMessage;
                Logger.LogInformation("Unit of measure-GetAllAsync failure response", unitOfMeasure);
                return BadRequest(response);
            }
        }
    }
}
