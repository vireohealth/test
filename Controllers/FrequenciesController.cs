﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Frequencies;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FrequenciesController : BaseController<FrequenciesController>
    {
        private readonly IFrequencyService frequencyService;

        public FrequenciesController(IFrequencyService frequencyService, ILogger<FrequenciesController> logger) : base(logger)
        {
            this.frequencyService = frequencyService;
        }

        /// <summary>
        /// Get all frequency
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllAsync()
        {
            Logger.LogInformation("Frequencies - GetAllAsync triggered.");
            BaseResponse response = new BaseResponse();
            var frequencies = await frequencyService.GetAllAsync();
            if (frequencies != null)
            {
                response.Data = frequencies;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.FrequenciesFetchMessage;
                Logger.LogInformation("Frequencies - GetAllAsync success response", frequencies);
                return Ok(response);
            }
            else
            {
                response.Data = frequencies;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.FrequenciesFetchFailureMessage;
            }
            Logger.LogInformation("Frequencies - GetAllAsync failure response", frequencies);
            return BadRequest(response);
        }
    }
}
