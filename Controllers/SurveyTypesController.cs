﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.SurveyTypes;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SurveyTypesController : BaseController<SurveyTypesController>
    {
        private readonly ISurveyTypesService surveyTypeService;

        public SurveyTypesController(ISurveyTypesService surveyTypeService, ILogger<SurveyTypesController> logger) : base(logger)
        {
            this.surveyTypeService = surveyTypeService;
        }

        /// <summary>
        /// Get all survey types
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllAsync()
        {
            Logger.LogInformation("Survey types list-GetAllAsync triggered.");
            BaseResponse response = new BaseResponse();
            var surveyTypeDetails = await surveyTypeService.GetAllAsync();
            if (surveyTypeDetails != null)
            {
                response.Data = surveyTypeDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyTypesFetchSuccessMessage;
                Logger.LogInformation("Survey types list-GetAllAsync success response", surveyTypeDetails);
                return Ok(response);
            }
            else
            {
                response.Data = surveyTypeDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyTypesFetchFailureMessage;
            }
            Logger.LogInformation("Survey types list-GetAllAsync failure response", surveyTypeDetails);
            return BadRequest(response);
        }
    }
}
