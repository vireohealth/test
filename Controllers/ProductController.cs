﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Products;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Helpers;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProductController : BaseController<ProductController>
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService, ILogger<ProductController> logger) : base(logger)
        {
            this.productService = productService;
        }

        /// <summary>
        /// Get all product
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllAsync()
        {
            Logger.LogInformation("Products - GetAllAsync triggered.");
            BaseResponse response = new BaseResponse();
            var products = await productService.GetAllAsync();
            if (products != null)
            {
                response.Data = products;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.ProductsFetchMessage;
                Logger.LogInformation("Products - GetAllAsync success response", products);
                return Ok(response);
            }
            else
            {
                response.Data = products;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.ProductsFetchFailureMessage;
            }
            Logger.LogInformation("Products - GetAllAsync failure response", products);
            return BadRequest(response);
        }

        /// <summary>
        /// Add product
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Add")]
        public IActionResult AddProduct(ProductDto dto)
        {
            Logger.LogInformation("Add product - AddProduct triggered.");
            BaseResponse response = new BaseResponse();
            var product = productService.AddProduct(dto);

            if (product == null)
            {
                var error = new ErrorDto { Message = "Product already exist!" };
                response.Data = error;
                response.ResponseCode = HTTPConstants.BAD_REQUEST;
                response.ResponseMessage = MessageConstants.SubjectFailureMessage;
                Logger.LogError("Add product - AddProduct Failed", error);
                return BadRequest(response);
            }
            else
            {
                response.Data = product;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SubjectCreatedMessage;
                Logger.LogInformation("Add product - AddProduct success response", product);
                return Ok(response);
            }
        }

        /// <summary>
        /// Product unique check
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("NameUniqueCheck")]
        public async Task<IActionResult> NameUniqueCheck(string id, string name)
        {
            Logger.LogInformation("Name unique check-NameUniqueCheck triggered.");

            BaseResponse response = new BaseResponse();
            var productDetails = await productService.CheckUniqueName(id, name);
            if (productDetails)
            {
                response.Data = productDetails;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.ProductNameExistsMessage;
                Logger.LogInformation("Name unique check-NameUniqueCheck already exists response", productDetails);
                return BadRequest(response);
            }
            else
            {
                response.Data = productDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.ProductNameAvailableMessage;
            }
            Logger.LogInformation("Name unique check-EmailUniqNameUniqueCheckueCheck available response", productDetails);
            return Ok(response);
        }

        /// <summary>
        /// Updates medication/product
        /// </summary>
        /// <param name="id"></param>
        /// <param name="productIn"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(string id, ProductDto productIn)
        {
            Logger.LogInformation("Product - Update triggered.");
            BaseResponse response = new BaseResponse();
            var product = await productService.GetProductById(id);

            if (product == null)
            {
                Logger.LogError("Product - Update failed", productIn);
                response.Data = productIn;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.ProductNotFoundMessage;
                return NotFound(response);
            }
            
            productIn.CreatedOn = product.CreatedOn;
            productIn.LastUpdatedOn = DateTime.UtcNow;
            productService.UpdateAsync(id, productIn);
            response.Data = productIn;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.ProductUpdatedMessage;
            Logger.LogError("Product - Update success", productIn);
            return Ok(response);
        }

        /// <summary>
        /// Get medication/product detail information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Details")]
        public async Task<IActionResult> GetProductDetails(string id)
        {
            Logger.LogInformation("Product details-GetProductDetails triggered.");

            BaseResponse response = new BaseResponse();
            var productDetails = await productService.GetProductById(id);
            if (productDetails != null)
            {
                response.Data = productDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.ProductDetailsFetchSuccessMessage;
                Logger.LogInformation("Product details-GetProductDetails success response", productDetails);
                return Ok(response);
            }
            else
            {
                response.Data = productDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.ProductDetailsFetchFailureMessage;
                Logger.LogInformation("Product details-GetProductDetails failed response", productDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Delete medication/product
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> DeleteProduct(string productId)
        {
            Logger.LogInformation("Product-DeleteProduct triggered. ProductId:", productId);
            BaseResponse response = new BaseResponse();
            var productDetails = await productService.GetProductById(productId);

            if (productDetails == null)
            {
                response.Data = productDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.ProductDeleteFailureMessage;
                Logger.LogError("Product-DeleteProduct failed : ProductId =", productId);
                return NotFound(response);
            }
            else
            {
                productService.RemoveAsync(productDetails.Id);
                response.Data = "";
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.ProductDeleteSuccessMessage;
                Logger.LogError("Product-DeleteProduct success : ProductId =", productId);
                return Ok(response);
            }
        }

        /// <summary>
        /// Get paginated products
        /// </summary>
        /// <param name="productParams"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("PaginatedProducts")]
        public async Task<IActionResult> GetAllPaginationAsync([FromQuery] PaginationParams productParams)
        {
            Logger.LogInformation("Product - GetAllPaginationAsync triggered.");
            BaseResponse response = new BaseResponse();
            var productDetails = await productService.GetAllPaginationAsync(productParams);

            Response.AddPaginationHeader(productDetails.CurrentPage, productDetails.PageSize,
               productDetails.TotalCount, productDetails.TotalPages);

            if (productDetails != null)
            {
                response.Data = productDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.ProductsFetchMessage;
                Logger.LogInformation("Product - GetAllPaginationAsync success response", productDetails);
                return Ok(response);
            }
            else
            {
                response.Data = productDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.ProductNotFoundMessage;
            }

            Logger.LogInformation("Product - GetAllPaginationAsync failure response", productDetails);

            return BadRequest(response);
        }
    }
}
