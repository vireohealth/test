﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.MedicationJournal;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MedicationJournalController :  BaseController<MedicationJournalController>
    {
        private readonly IMedicationJournalService medicationJournalService;
        private readonly AppSettings appSettings;

        public MedicationJournalController(IMedicationJournalService medicationJournalService, ILogger<MedicationJournalController> logger) : base(logger)
        {
            this.medicationJournalService = medicationJournalService;
        }

        /// <summary>
        /// Add medication journal entry
        /// </summary>
        /// <param name="medicationJournal"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Add")]
        public IActionResult AddJournalEntry(MedicationJournalDto medicationJournal)
        {
            Logger.LogInformation("Add medication journal triggered.");
            BaseResponse response = new BaseResponse();
            var medicationJournalDetails = medicationJournalService.AddMedicationJournal(medicationJournal);

            if (medicationJournalDetails == null)
            {
                var error = new Helpers.ErrorDto { Message = "Medication Journal  already exist!" };
                Logger.LogError("Add medication journal failed", error);
                response.Data = medicationJournalDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = error.Message;
                return BadRequest(response);
            }
            else
            {
                Logger.LogInformation("Adding medication journal response", medicationJournalDetails);
                response.Data = medicationJournalDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.MedicationJournalAddedSuccessMessage;
                return Ok(response);
            }
        }

        /// <summary>
        /// Get all paginated medication journals
        /// </summary>
        /// <param name="medicationJournal"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("PaginatedMedicationJournals")]
        public async Task<IActionResult> GetAllPaginationAsync([FromQuery] PaginationParams medicationJournal,string subjectId)
        {
            Logger.LogInformation("Medication journal entries - GetAllPaginationAsync triggered.");
            BaseResponse response = new BaseResponse();
            var journalEntries = await medicationJournalService.GetAllPaginationAsync(medicationJournal,subjectId);

            Response.AddPaginationHeader(journalEntries.CurrentPage, journalEntries.PageSize,
               journalEntries.TotalCount, journalEntries.TotalPages);

            if (journalEntries != null)
            {
                response.Data = journalEntries;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.MedicationJournalEntriesFetchMessage;
                Logger.LogInformation("Medication journal entries - GetAllPaginationAsync success response", journalEntries);
                return Ok(response);
            }
            else
            {
                response.Data = journalEntries;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.MedicationJournalEntryNotFoundMessage;
            }

            Logger.LogInformation("Medication journal entries - GetAllPaginationAsync failure response", journalEntries);

            return BadRequest(response);
        }

        /// <summary>
        /// Gets all medication journals
        /// </summary>
        /// <param name="medicationJournal"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllAsync(DateTime medicationJournal,string subjectId)
        {
            Logger.LogInformation("Journal entries - GetAllAsync triggered.");
            BaseResponse response = new BaseResponse();
            var medicationJournals = await medicationJournalService.GetAllAsync(medicationJournal,subjectId);
            if (medicationJournals != null)
            {
                response.Data = medicationJournals;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.MedicationJournalEntriesFetchMessage;
                Logger.LogInformation("Journal entries - GetAllAsync success response", medicationJournals);
                return Ok(response);
            }
            else
            {
                response.Data = medicationJournals;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.MedicationJournalEntryNotFoundMessage;
            }
            Logger.LogInformation("Journal entries - GetAllAsync failure response", medicationJournals);
            return BadRequest(response);
        }

        /// <summary>
        /// Get details of medication journal
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Details")]
        public async Task<IActionResult> GetMedicationJournalDetails(string id)
        {
            Logger.LogInformation("Medication journal entry details-GetMedicationJournalDetails triggered.");

            BaseResponse response = new BaseResponse();
            var medicationJournalDetails = await medicationJournalService.GetMedicationJournalById(id);
            if (medicationJournalDetails != null)
            {
                response.Data = medicationJournalDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.MedicationJournalDetailsFetchSuccessMessage;
                Logger.LogInformation("Medication journal details-GetMedicationJournalDetails success response", medicationJournalDetails);
                return Ok(response);
            }
            else
            {
                response.Data = medicationJournalDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.MedicationJournalDetailsFetchFailureMessage;
                Logger.LogInformation("Medication journal details-GetMedicationJournalDetails failed response", medicationJournalDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Updates medication journal
        /// </summary>
        /// <param name="id"></param>
        /// <param name="medicationJournal"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(string id, MedicationJournalDto medicationJournal)
        {
            Logger.LogInformation("Medication journal - Update triggered.");
            BaseResponse response = new BaseResponse();
            var medicationjournalDetails = await medicationJournalService.GetMedicationJournalById(id);

            if (medicationjournalDetails == null)
            {
                Logger.LogError("Medication journal - Update failed", medicationJournal);
                response.Data = medicationJournal;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.MedicationJournalIdNotFoundMessage;
                return NotFound(response);
            }

            medicationJournal.CreatedOn = medicationjournalDetails.CreatedOn;
            medicationJournal.LastUpdatedOn = DateTime.UtcNow;
            medicationJournalService.UpdateMedicationJournal(id, medicationJournal);
            response.Data = medicationJournal;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.MedicationJournalUpdatedMessage;
            Logger.LogError("Medication journal - Update success", medicationJournal);
            return Ok(response);
        }

        /// <summary>
        /// Unique check of journal entry
        /// </summary>
        /// <param name="id"></param>
        /// <param name="productName"></param>
        /// <param name="time"></param>
        /// <param name="medicationJournalDate"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("UniqueCheck")]
        public async Task<IActionResult> UniqueMedicationJournalCheck(string id, string productName, string time, DateTime medicationJournalDate, string subjectId)
        {
            Logger.LogInformation("Medication journal entry unique check-UniqueMedicationJournalCheck triggered.");

            BaseResponse response = new BaseResponse();
            var journalEntryDetails = await medicationJournalService.CheckUniqueMedicationJournal(id, productName, time, medicationJournalDate, subjectId);
            if (journalEntryDetails)
            {
                response.Data = journalEntryDetails;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.MedicationJournalExistsMessage;
                Logger.LogInformation("Medication journal entry unique check-UniqueMedicationJournalCheck already exists response", journalEntryDetails);
                return BadRequest(response);
            }
            else
            {
                response.Data = journalEntryDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.MedicationJournalAvailableMessage;
            }
            Logger.LogInformation("Medication journal entry unique check-UniqueMedicationJournalCheck available response", journalEntryDetails);
            return Ok(response);
        }

        /// <summary>
        /// Unique check of medication journal entry on a particular time
        /// </summary>
        /// <param name="id"></param>
        /// <param name="time"></param>
        /// <param name="medicationJournalDate"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("TimeUniqueCheck")]
        public async Task<IActionResult> UniqueMedicationJournalTimeCheck(string id, string time, DateTime medicationJournalDate, string subjectId)
        {
            Logger.LogInformation("Medication journal entry unique check for time-UniqueMedicationJournalTimeCheck triggered.");

            BaseResponse response = new BaseResponse();
            var journalEntryDetails = await medicationJournalService.CheckUniqueMedicationJournalTime(id, time, medicationJournalDate, subjectId);
            if (journalEntryDetails)
            {
                response.Data = journalEntryDetails;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.MedicationJournalExistsMessage;
                Logger.LogInformation("Medication journal entry unique check for time-UniqueMedicationJournalTimeCheck already exists response", journalEntryDetails);
                return BadRequest(response);
            }
            else
            {
                response.Data = journalEntryDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.MedicationJournalAvailableMessage;
            }
            Logger.LogInformation("Medication journal entry unique check for time-UniqueMedicationJournalTimeCheck available response", journalEntryDetails);
            return Ok(response);
        }
    }
}
