﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Menus;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MenusController : BaseController<MenusController>
    {
        private readonly IMenuService menuService;

        public MenusController(IMenuService menuService, ILogger<MenusController> logger) : base(logger)
        {
            this.menuService = menuService;
        }

        /// <summary>
        /// Gets menus 
        /// </summary>
        /// <param name="userType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllAsync(string userType)
        {
            Logger.LogInformation("Menus-GetAllAsync by user type triggered.");
            BaseResponse response = new BaseResponse();
            var menus = await menuService.GetAllAsync(userType);
            if (menus != null)
            {
                response.Data = menus;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.MenuGetSuccessMessage;
                Logger.LogInformation("Menus-GetAllAsync by user type success response", menus);
                return Ok(response);
            }
            else
            {
                response.Data = menus;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.MenuGetFailureMessage;
            }
            Logger.LogInformation("Menus-GetAllAsync by user type failure response", menus);
            return BadRequest(response);
        }

    }
}
