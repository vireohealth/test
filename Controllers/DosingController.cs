﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Dosing;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DosingController : BaseController<DosingController>
    {
        private readonly IDosingService dosingService;

        public DosingController(IDosingService dosingService, ILogger<DosingController> logger) : base(logger)
        {
            this.dosingService = dosingService;
        }

        /// <summary>
        /// Add dosing
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Add")]
        public IActionResult AddDosing(List<DosingDto> dto)
        {
            Logger.LogInformation("Add Dosing triggered.");
            BaseResponse response = new BaseResponse();
            var dosingDetails = dosingService.AddDosing(dto);

            if (dosingDetails == null)
            {
                var error = new Helpers.ErrorDto { Message = "Dosing already exist!" };
                Logger.LogError("Add dosing failed", error);
                response.Data = dosingDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = error.Message;
                return BadRequest(response);
            }
            else
            {
                Logger.LogInformation("Adding dosing response", dosingDetails);
                response.Data = dosingDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.DosingAddedSuccessMessage;
                return Ok(response);
            }
        }

        /// <summary>
        /// Get all dosing
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllAsync()
        {
            Logger.LogInformation("Dosing - GetAllAsync triggered.");
            BaseResponse response = new BaseResponse();
            var dosingDetails = await dosingService.GetAllAsync();
            if (dosingDetails != null)
            {
                response.Data = dosingDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.DosingFetchSuccessMessage;
                Logger.LogInformation("Dosing - GetAllAsync success response", dosingDetails);
                return Ok(response);
            }
            else
            {
                response.Data = dosingDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.DosingFetchFailureMessage;
            }
            Logger.LogInformation("Dosing - GetAllAsync failure response", dosingDetails);
            return BadRequest(response);
        }

        /// <summary>
        /// Unique check
        /// </summary>
        /// <param name="dosing"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UniqueCheck")]
        public async Task<IActionResult> UniqueCheck(DosingUniqueDto dosing)
        {
            Logger.LogInformation("Dosing unique check-UniqueCheck triggered.");

            BaseResponse response = new BaseResponse();
            var dosingDetails = await dosingService.UniqueCheckDosing(dosing);
            if (dosingDetails)
            {
                response.Data = dosingDetails;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.DosingUniqueCheckConflictMessage;
                Logger.LogInformation("Dosing unique check-UniqueCheck already exists response", dosingDetails);
                return BadRequest(response);
            }
            else
            {
                response.Data = dosingDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.DosingUniqueCheckAvailableMessage;
            }
            Logger.LogInformation("Dosing unique check-UniqueCheck available response", dosingDetails);
            return Ok(response);
        }

        /// <summary>
        /// Deletes the dosing item
        /// </summary>
        /// <param name="dosingId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> DeleteDosing(string dosingId)
        {
            Logger.LogInformation("Dosing-Delete dosing triggered. DosingId:", dosingId);
            BaseResponse response = new BaseResponse();
            var dosing = await dosingService.GetDosingById(dosingId);

            if (dosing == null)
            {
                response.Data = dosing;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.DosingDeleteFailureMessage;
                Logger.LogError("Dosing-Delete dosing failed : DosingId =", dosingId);
                return NotFound(response);
            }
            else
            {
                dosingService.RemoveAsync(dosing.Id);
                response.Data = "";
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.DosingDeleteSuccessMessage;
                Logger.LogError("Dosing-Delete dosing success : DosingId =", dosingId);
                return Ok(response);
            }
        }
    }
}
