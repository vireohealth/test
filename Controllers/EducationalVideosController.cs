﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.EducationalVideos;
using VireoIntegrativeProgram.Business.FormControls;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EducationalVideosController : BaseController<EducationalVideosController>
    {
        private readonly IEducationalVideoService educationalVideoService;
        public EducationalVideosController(IEducationalVideoService educationalVideoService, ILogger<EducationalVideosController> logger) : base(logger)
        {
            this.educationalVideoService = educationalVideoService;
        }

        /// <summary>
        /// Gets all educational videos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllAsync()
        {
            Logger.LogInformation("Education Videos - GetAllAsync triggered.");
            BaseResponse response = new BaseResponse();
            var educationalVideos = await educationalVideoService.GetAllAsync();
            if (educationalVideos != null)
            {
                response.Data = educationalVideos;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.EducationalVideosFetchMessage;
                Logger.LogInformation("Education Videos - GetAllAsync success response", educationalVideos);
                return Ok(response);
            }
            else
            {
                response.Data = educationalVideos;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.EducationalVideosFetchFailureMessage;
            }
            Logger.LogInformation("Education Videos - GetAllAsync failure response", educationalVideos);
            return BadRequest(response);
        }

        /// <summary>
        /// Get all paginated educational videos
        /// </summary>
        /// <param name="educationalVideoParams"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllPaginated")]
        public async Task<IActionResult> GetAllPaginationAsync([FromQuery] PaginationParams educationalVideoParams)
        {
            Logger.LogInformation("Education Videos - GetAllPaginationAsync triggered.");
            BaseResponse response = new BaseResponse();
            var educationalVideos = await educationalVideoService.GetAllPaginationAsync(educationalVideoParams);

            Response.AddPaginationHeader(educationalVideos.CurrentPage, educationalVideos.PageSize,
               educationalVideos.TotalCount, educationalVideos.TotalPages);

            if (educationalVideos != null)
            {
                response.Data = educationalVideos;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.EducationalVideosFetchMessage;
                Logger.LogInformation("Education Videos - GetAllPaginationAsync success response", educationalVideos);
                return Ok(response);
            }
            else
            {
                response.Data = educationalVideos;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.EducationalVideoNotFoundMessage;
            }

            Logger.LogInformation("Education Videos - GetAllPaginationAsync failure response", educationalVideos);
            return BadRequest(response);
        }

        /// <summary>
        /// Add educational video information and upload the thumb image
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Add")]
        public IActionResult AddEducationalVideo([FromForm] EducationalVideoDto dto)
        {
            Logger.LogInformation("Add Educational Video triggered.");
            BaseResponse response = new BaseResponse();
            var imageUploadResult = educationalVideoService.UploadedThumbImage(dto.ThumbImage);
            if(!imageUploadResult.IsUploaded)
            {
                Logger.LogError("Add Educational Video - Upload image failed", imageUploadResult);
                response.Data = imageUploadResult;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = imageUploadResult.Message;
                return BadRequest(response);
            }
            dto.ThumbImagePath = imageUploadResult.FullImagePath;
            dto.IsDefaultThumb = imageUploadResult.IsDefaultThumb;
            dto.ImageName = imageUploadResult.ImageName;

            var educationalVideo = educationalVideoService.AddEducationalVideo(dto);

            if (educationalVideo == null)
            {
                var error = new Helpers.ErrorDto { Message = "Educational Video already exist!" };
                Logger.LogError("Add Educational Video failed", error);
                response.Data = educationalVideo;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = error.Message;
                return BadRequest(response);
            }
            else
            {
                Logger.LogInformation("Adding Educational Video response", educationalVideo);
                response.Data = educationalVideo;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.EducationalVideoAddedSuccessMessage;
                return Ok(response);
            }
        }

        /// <summary>
        /// Updates education video information
        /// </summary>
        /// <param name="id"></param>
        /// <param name="educationalVideoIn"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(string id, [FromForm] EducationalVideoDto educationalVideoIn)
        {
            Logger.LogInformation("Educational Video - Update triggered.");
            BaseResponse response = new BaseResponse();
            var educationalVideo = await educationalVideoService.GetEducationalVideoById(id);

            if (educationalVideo == null)
            {
                Logger.LogError("Educational Video - Update failed", educationalVideoIn);
                response.Data = educationalVideoIn;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.EducationalVideoNotFoundMessage;
                return NotFound(response);
            }
            if (educationalVideoIn.ThumbImage != null)
            {
                var imageUploadResult = educationalVideoService.UploadedThumbImage(educationalVideoIn.ThumbImage);
                if (!imageUploadResult.IsUploaded)
                {
                    Logger.LogError("Update Educational Video - Upload image failed", imageUploadResult);
                    response.Data = imageUploadResult;
                    response.ResponseCode = HTTPConstants.NOT_FOUND;
                    response.ResponseMessage = imageUploadResult.Message;
                    return BadRequest(response);
                }

                educationalVideoService.DeleteThumbImage(educationalVideoIn.ImageName, educationalVideoIn.IsDefaultThumb);

                educationalVideoIn.ThumbImagePath = imageUploadResult.FullImagePath;
                educationalVideoIn.IsDefaultThumb = imageUploadResult.IsDefaultThumb;
                educationalVideoIn.ImageName = imageUploadResult.ImageName;
            }

            educationalVideoIn.PostedOn = educationalVideo.PostedOn;
            educationalVideoIn.LastUpdatedOn = DateTime.UtcNow;
            educationalVideoService.UpdateAsync(id, educationalVideoIn);
            response.Data = educationalVideoIn;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.EducationalVideoUpdatedMessage;
            Logger.LogError("Educational Video - Update success", educationalVideoIn);
            return Ok(response);
        }

        /// <summary>
        /// Deletes educational video
        /// </summary>
        /// <param name="educationalVideoId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> DeleteEducationalVideo(string educationalVideoId)
        {
            Logger.LogInformation("EducationalVideo - Delete educational video triggered. educationalVideoId:", educationalVideoId);
            BaseResponse response = new BaseResponse();
            var educationalVideo = await educationalVideoService.GetEducationalVideoById(educationalVideoId);

            if (educationalVideo == null)
            {
                response.Data = educationalVideo;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.EducationalVideoDeleteFailureMessage;
                Logger.LogError("EducationalVideo - Delete educational video failed : educationalVideoId =", educationalVideoId);
                return NotFound(response);
            }
            else
            {
                educationalVideoService.DeleteThumbImage(educationalVideo.ImageName, educationalVideo.IsDefaultThumb);
                educationalVideoService.RemoveAsync(educationalVideo.Id);
                response.Data = "";
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.EducationalVideoDeleteSuccessMessage;
                Logger.LogError("EducationalVideo - Delete educational video success : educationalVideoId =", educationalVideoId);
                return Ok(response);
            }
        }

        /// <summary>
        /// Retrieves  educational video information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Details")]
        public async Task<IActionResult> GetEducationalVideoDetails(string id)
        {
            Logger.LogInformation("Educational Video Details-GetEducationalVideoDetails triggered.");

            BaseResponse response = new BaseResponse();
            var educationalVideoDetails = await educationalVideoService.GetEducationalVideoById(id);
            if (educationalVideoDetails != null)
            {
                response.Data = educationalVideoDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.EducationalVideoDetailsFetchSuccessMessage;
                Logger.LogInformation("Educational Video Details-GetEducationalVideoDetails success response", educationalVideoDetails);
                return Ok(response);
            }
            else
            {
                response.Data = educationalVideoDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.EducationalVideoDetailsFetchFailureMessage;
                Logger.LogInformation("Educational Video Details-GetEducationalVideoDetails failed response", educationalVideoDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Educational video unique check
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("UniqueCheck")]
        public async Task<IActionResult> EducationalVideoUniqueCheck(string id, string title, string url)
        {
            Logger.LogInformation("Educational video unique check-EducationalVideoUniqueCheck triggered.");

            BaseResponse response = new BaseResponse();
            var educationalVideoDetails = await educationalVideoService.CheckUniqueEducationalVideo(id, title, url);
            if (educationalVideoDetails)
            {
                response.Data = educationalVideoDetails;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.EducationalVideoUniqueCheckConflictMessage;
                Logger.LogInformation("Educational video unique check-EducationalVideoUniqueCheck already exists response", educationalVideoDetails);
                return BadRequest(response);
            }
            else
            {
                response.Data = educationalVideoDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.EducationalVideoUniqueCheckAvailableMessage;
            }
            Logger.LogInformation("Educational video unique check-EducationalVideoUniqueCheck available response", educationalVideoDetails);
            return Ok(response);
        }

        /// <summary>
        /// Upload custom slider image 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UploadSliderImage")]
        public async Task<IActionResult> AddImage([FromForm] ImageDto dto)
        {
            Logger.LogInformation("Add slider image triggered.");
            BaseResponse response = new BaseResponse();
            var imageUploadResult = await educationalVideoService.UploadImage(dto.FormImage);
            if (!imageUploadResult.IsUploaded)
            {
                Logger.LogError("Add slider image - Upload image failed", imageUploadResult);
                response.Data = imageUploadResult;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = imageUploadResult.Message;
                return BadRequest(response);
            }
            dto.Path = imageUploadResult.FullImagePath;
            dto.ImageName = imageUploadResult.ImageName;

            Logger.LogInformation("Add slider image response", imageUploadResult);
            response.Data = imageUploadResult;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = imageUploadResult.Message;
            return Ok(response);

        }
    }
}
