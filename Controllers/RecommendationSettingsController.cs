﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.RecommendationSettings;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RecommendationSettingsController : BaseController<RecommendationSettingsController>
    {
        private readonly IRecommendationSettingService recommendationSettingService;

        public RecommendationSettingsController(IRecommendationSettingService recommendationSettingService,ILogger<RecommendationSettingsController> logger) : base(logger)
        {
            this.recommendationSettingService = recommendationSettingService;
        }

        /// <summary>
        /// Add recommendation setting
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Add")]
        public IActionResult AddRecommendationSetting(List<RecommendationSettingDto> dto)
        {
            Logger.LogInformation("Add recommendation setting triggered.");
            BaseResponse response = new BaseResponse();
            var recommendationSettingDetails = recommendationSettingService.AddRecommendationSetting(dto);

            if (recommendationSettingDetails == null)
            {
                var error = new Helpers.ErrorDto { Message = "Recommendation setting already exist!" };
                Logger.LogError("Add recommendation setting failed", error);
                response.Data = recommendationSettingDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = error.Message;
                return BadRequest(response);
            }
            else
            {
                Logger.LogInformation("Adding recommendation setting response", recommendationSettingDetails);
                response.Data = recommendationSettingDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.RecommendationSettingAddedSuccessMessage;
                return Ok(response);
            }
        }

        /// <summary>
        /// Fetches all recommendation settings
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllAsync()
        {
            Logger.LogInformation("Recommendation settings - GetAllAsync triggered.");
            BaseResponse response = new BaseResponse();
            var recommendationSettings = await recommendationSettingService.GetAllAsync();
            if (recommendationSettings != null)
            {
                response.Data = recommendationSettings;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.RecommendationSettingFetchSuccessMessage;
                Logger.LogInformation("Recommendation settings - GetAllAsync success response", recommendationSettings);
                return Ok(response);
            }
            else
            {
                response.Data = recommendationSettings;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.RecommendationSettingFetchFailureMessage;
            }
            Logger.LogInformation("Recommendation settings - GetAllAsync failure response", recommendationSettings);
            return BadRequest(response);
        }

        /// <summary>
        /// Check uniqueness in recommendation settings
        /// </summary>
        /// <param name="recommendationSetting"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UniqueCheck")]
        public async Task<IActionResult> UniqueCheck(RecommendationSettingDto recommendationSetting)
        {
            Logger.LogInformation("Recommendation setting unique check-UniqueCheck triggered.");

            BaseResponse response = new BaseResponse();
            var recommendationSettingDetails = await recommendationSettingService.UniqueCheckRecommendationSetting(recommendationSetting);
            if (recommendationSettingDetails)
            {
                response.Data = recommendationSettingDetails;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.RecommendationSettingUniqueCheckConflictMessage;
                Logger.LogInformation("Recommendation setting unique check-UniqueCheck already exists response", recommendationSettingDetails);
                return BadRequest(response);
            }
            else
            {
                response.Data = recommendationSettingDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.RecommendationSettingUniqueCheckAvailableMessage;
            }
            Logger.LogInformation("Recommendation setting unique check-UniqueCheck available response", recommendationSettingDetails);
            return Ok(response);
        }

        /// <summary>
        /// Deletes recommendation setting
        /// </summary>
        /// <param name="recommendationSettingId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> DeleteRecommendationSetting(string recommendationSettingId)
        {
            Logger.LogInformation("Recommendation settings - Delete recommendation setting triggered. RecommendationSettingId:", recommendationSettingId);
            BaseResponse response = new BaseResponse();
            var recommendationSetting = await recommendationSettingService.GetRecommendationSettingById(recommendationSettingId);

            if (recommendationSetting == null)
            {
                response.Data = recommendationSetting;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.RecommendationSettingDeleteFailureMessage;
                Logger.LogError("Recommendation settings - Delete recommendation setting failed : RecommendationSettingId =", recommendationSettingId);
                return NotFound(response);
            }
            else
            {
                recommendationSettingService.RemoveAsync(recommendationSetting.Id);
                response.Data = "";
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.RecommendationSettingDeleteSuccessMessage;
                Logger.LogError("Recommendation settings - Delete recommendation setting success : RecommendationSettingId =", recommendationSettingId);
                return Ok(response);
            }
        }

       /// <summary>
       /// Get recommendation based on mood and sleep
       /// </summary>
       /// <param name="mood"></param>
       /// <param name="sleep"></param>
       /// <returns></returns>
        [HttpGet]
        [Route("RecommendationBasedOnLog")]
        public async Task<IActionResult> GetRecommendationBasedOnLogPerSubject(string mood, string sleep)
        {
            Logger.LogInformation("Recommendation for subject-GetRecommendationPerSubject triggered.");

            BaseResponse response = new BaseResponse();
            var recommendationForSubject = await recommendationSettingService.GetRecommendationBasedOnMoodAndSleep(mood, sleep);
            if (recommendationForSubject != null)
            {
                response.Data = recommendationForSubject;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.RecommendationBasedOnLogFetchSuccessMessage;
                Logger.LogInformation("Recommendation for subject-GetRecommendationPerSubject success response", recommendationForSubject);
                return Ok(response);
            }
            else
            {
                response.Data = recommendationForSubject;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.RecommendationBasedOnLogFetchFailureMessage;
                Logger.LogInformation("Recommendation for subject-GetRecommendationPerSubject failed response", recommendationForSubject);
                return BadRequest(response);
            }
        }
    }
}
