﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.BodyParts;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Models;
using VireoIntegrativeProgram.Constants;
using Microsoft.AspNetCore.Authorization;
using System;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Extensions;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BodyPartsController : BaseController<BodyPartsController>
    {
        private readonly IBodyPartsService bodyPartService;

        public BodyPartsController(IBodyPartsService bodyPartService, ILogger<BodyPartsController> logger) : base(logger)
        {
            this.bodyPartService = bodyPartService;
        }

        /// <summary>
        /// Get all body parts
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllBodyParts()
        {
            Logger.LogInformation("Body parts - GetAllBodyParts triggered.");
            BaseResponse response = new BaseResponse();
            var bodyPartDetails = await bodyPartService.GetAllBodyParts();
            if (bodyPartDetails != null)
            {
                response.Data = bodyPartDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.BodyPartsFetchMessage;
                Logger.LogInformation("Body parts - GetAllBodyParts success response", bodyPartDetails);
                return Ok(response);
            }
            else
            {
                response.Data = bodyPartDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.BodyPartsFetchFailureMessage;
            }
            Logger.LogInformation("Body parts - GetAllBodyParts failure response", bodyPartDetails);
            return BadRequest(response);
        }

        /// <summary>
        /// Add a body part
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> AddBodyParts([FromForm] BodyPartsDto dto)
        {
            Logger.LogInformation("Add body parts triggered.");
            BaseResponse response = new BaseResponse();
            var imageUploadResult = await bodyPartService.UploadAnImage(dto.BodyPartImage);
            if (!imageUploadResult.IsUploaded)
            {
                Logger.LogError("Add body parts - Upload image failed", imageUploadResult);
                response.Data = imageUploadResult;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = imageUploadResult.Message;
                return BadRequest(response);
            }
            dto.BodyPartImageName = imageUploadResult.ImageName;

            var bodyPartDetails = await bodyPartService.AddBodyParts(dto);

            if (bodyPartDetails == null)
            {
                var error = new Helpers.ErrorDto { Message = "Body part already exist!" };
                Logger.LogError("Add body part failed", error);
                response.Data = bodyPartDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = error.Message;
                return BadRequest(response);
            }
            else
            {
                Logger.LogInformation("Add body part response", bodyPartDetails);
                response.Data = bodyPartDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.BodyPartsAddedSuccessMessage;
                return Ok(response);
            }
        }

        /// <summary>
        /// Deletes body part
        /// </summary>
        /// <param name="bodyPartId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> DeleteBodyPart(string bodyPartId)
        {
            Logger.LogInformation("Body Part - Delete body part triggered. bodyPartId:", bodyPartId);
            BaseResponse response = new BaseResponse();
            var bodyPart = await bodyPartService.GetBodyPartById(bodyPartId);

            if (bodyPart == null)
            {
                response.Data = bodyPart;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.BodyPartDeleteFailureMessage;
                Logger.LogError("Body Part - Delete body part triggered failed : bodyPartId =", bodyPartId);
                return NotFound(response);
            }
            else
            {
                bodyPartService.DeleteBodyPartImage(bodyPart.BodyPartImageName);
                bodyPartService.RemoveAsync(bodyPart.Id);
                response.Data = "";
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.BodyPartDeleteSuccessMessage;
                Logger.LogError("Body Part - Delete body part success : bodyPartId =", bodyPartId);
                return Ok(response);
            }
        }

        /// <summary>
        /// Body part unique check
        /// </summary>
        /// <param name="id"></param>
        /// <param name="bodyPartName"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("UniqueCheck")]
        public async Task<IActionResult> BodyPartUniqueCheck(string id, string bodyPartName)
        {
            Logger.LogInformation("Body part unique check-BodyPartUniqueCheck triggered.");

            BaseResponse response = new BaseResponse();
            var bodyPartDetail = await bodyPartService.CheckUniqueBodyPart(id, bodyPartName);
            if (bodyPartDetail)
            {
                response.Data = bodyPartDetail;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.BodyPartUniqueCheckConflictMessage;
                Logger.LogInformation("Body part unique check-BodyPartUniqueCheck already exists response", bodyPartDetail);
                return BadRequest(response);
            }
            else
            {
                response.Data = bodyPartDetail;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.BodyPartUniqueCheckAvailableMessage;
            }
            Logger.LogInformation("Body part unique check-BodyPartUniqueCheck available response", bodyPartDetail);
            return Ok(response);
        }

        /// <summary>
        /// Get body part details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Details")]
        public async Task<IActionResult> GetBodyPartDetails(string id)
        {
            Logger.LogInformation("Body part details-GetBodyPartDetails triggered.");

            BaseResponse response = new BaseResponse();
            var bodyPartDetails = await bodyPartService.GetBodyPartById(id);
            if (bodyPartDetails != null)
            {
                response.Data = bodyPartDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.BodyPartDetailsFetchSuccessMessage;
                Logger.LogInformation("Body part details-GetBodyPartDetails success response", bodyPartDetails);
                return Ok(response);
            }
            else
            {
                response.Data = bodyPartDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.BodyPartDetailsFetchFailureMessage;
                Logger.LogInformation("Body part details-GetBodyPartDetails failed response", bodyPartDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Updates a body part
        /// </summary>
        /// <param name="id"></param>
        /// <param name="bodyPartsDto"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(string id, [FromForm] BodyPartUpsertDto bodyPartsDto)
        {
            Logger.LogInformation("Body part- Update triggered.");
            BaseResponse response = new BaseResponse();
            var bodyPartDetails = await bodyPartService.GetBodyPartById(id);

            if (bodyPartDetails == null)
            {
                Logger.LogError("Body part- Update failed", bodyPartsDto);
                response.Data = bodyPartsDto;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.BodyPartsUpdationFailureMessage;
                return NotFound(response);
            }
            if (bodyPartsDto.BodyPartImage != null)
            {
                var imageUploadResult = await bodyPartService.UploadAnImage(bodyPartsDto.BodyPartImage);
                if (!imageUploadResult.IsUploaded)
                {
                    Logger.LogError("Body part- Upload image failed", imageUploadResult);
                    response.Data = imageUploadResult;
                    response.ResponseCode = HTTPConstants.NOT_FOUND;
                    response.ResponseMessage = imageUploadResult.Message;
                    return BadRequest(response);
                }

                await bodyPartService.DeleteImage(bodyPartDetails.BodyPartImageName);

                bodyPartsDto.BodyPartImageName = imageUploadResult.ImageName;
            }

            bodyPartsDto.CreatedOn = bodyPartDetails.CreatedOn;
            bodyPartsDto.LastUpdatedOn = DateTime.UtcNow;
            bodyPartService.UpdateAsync(id, bodyPartsDto);

            response.Data = bodyPartsDto;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.BodyPartsUpdationSuccessMessage;
            Logger.LogError("Body part-Update success", bodyPartsDto);

            return Ok(response);
        }

        /// <summary>
        /// Get all body parts with pagination
        /// </summary>
        /// <param name="bodyPartDetails"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("PaginatedBodyParts")]
        public async Task<IActionResult> GetAllPaginationAsync([FromQuery] PaginationParams bodyPartDetails)
        {
            Logger.LogInformation("Body Parts - GetAllPaginationAsync triggered.");
            BaseResponse response = new BaseResponse();

            var bodyPartsList = await bodyPartService.GetAllPaginationAsync(bodyPartDetails);

            Response.AddPaginationHeader(bodyPartsList.CurrentPage, bodyPartsList.PageSize,
               bodyPartsList.TotalCount, bodyPartsList.TotalPages);

            if (bodyPartsList != null)
            {
                response.Data = bodyPartsList;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.BodyPartsPaginatedFetchMessage;
                Logger.LogInformation("Body Parts - GetAllPaginationAsync success response", bodyPartsList);
                return Ok(response);
            }
            else
            {
                response.Data = bodyPartsList;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.BodyPartsPaginatedNotFoundMessage;
            }

            Logger.LogInformation("Body Parts - GetAllPaginationAsync failure response", bodyPartsList);

            return BadRequest(response);
        }
    }
}

