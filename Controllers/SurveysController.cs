﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Dosing;
using VireoIntegrativeProgram.Business.Logs;
using VireoIntegrativeProgram.Business.MedicationJournal;
using VireoIntegrativeProgram.Business.Surveys;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Common.Enums;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class SurveysController : BaseController<SurveysController>
    {
        private readonly ISurveyService surveyService;
        private readonly ILogsService logService;
        private readonly IMedicationJournalService medicationJournalService;
        private readonly IDosingService dosingService;
        private readonly AppSettings appSettings;

        public SurveysController(ISurveyService surveyService, ILogsService logService, IMedicationJournalService medicationJournalService, IDosingService dosingService, ILogger<SurveysController> logger) : base(logger)
        {
            this.surveyService = surveyService;
            this.logService = logService;
            this.medicationJournalService = medicationJournalService;
            this.dosingService = dosingService;
        }
        /// <summary>
        /// Add survey
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Add")]
        public IActionResult AddSurvey(SurveyDto dto)
        {
            Logger.LogInformation("Add Survey triggered.");
            BaseResponse response = new BaseResponse();
            var survey = surveyService.AddSurvey(dto);           
            if (survey == null)
            {
                var error = new Helpers.ErrorDto { Message = "Survey already exist!" };
                Logger.LogError("Add Survey failed", error);
                response.Data = survey;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = error.Message;
                return BadRequest(response);
            }
            else
            {
                Logger.LogInformation("Adding Survey response", survey);
                response.Data = survey;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyAddedSuccessMessage;
                return Ok(response);
            }
        }

        /// <summary>
        /// Get All Paginated Surveys
        /// </summary>
        /// <param name="surveyParams"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("PaginatedSurveys")]
        public async Task<IActionResult> GetAllPaginationAsync([FromQuery] PaginationParams surveyParams)
        {
            Logger.LogInformation("Surveys - GetAllPaginationAsync triggered.");
            BaseResponse response = new BaseResponse();

            await surveyService.FindAndUpdateSurveysToCompleted();

            var surveys = await surveyService.GetAllPaginationAsync(surveyParams);

            Response.AddPaginationHeader(surveys.CurrentPage, surveys.PageSize,
               surveys.TotalCount, surveys.TotalPages);

            if (surveys != null)
            {
                response.Data = surveys;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveysFetchMessage;
                Logger.LogInformation("Surveys - GetAllPaginationAsync success response", surveys);
                return Ok(response);
            }
            else
            {
                response.Data = surveys;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyNotFoundMessage;
            }

            Logger.LogInformation("Surveys - GetAllPaginationAsync failure response", surveys);

            return BadRequest(response);
        }

        /// <summary>
        /// Updates survey information
        /// </summary>
        /// <param name="id">Survey id</param>
        /// <param name="surveyIn">Survey object</param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(string id, SurveyDto surveyIn)
        {
            Logger.LogInformation("Survey - Update triggered.");
            BaseResponse response = new BaseResponse();
            var survey = await surveyService.GetSurveyById(id);

            if (survey == null)
            {
                Logger.LogError("Survey - Update failed", surveyIn);
                response.Data = surveyIn;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyNotFoundMessage;
                return NotFound(response);
            }
            int responseTime = TimeConstants.AnswerTime;
            decimal minutesTaken = ((surveyIn.TotalInputQuestions * responseTime) / 60);
            int CompletionTime = (int)Math.Round(minutesTaken);
            surveyIn.CreatedOn = survey.CreatedOn;
            surveyIn.LastUpdatedOn = DateTime.UtcNow;
            surveyIn.TimeToComplete = CompletionTime;
            surveyService.UpdateAsync(id, surveyIn);
            response.Data = surveyIn;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.SurveyUpdatedMessage;
            Logger.LogError("Survey - Update success", surveyIn);
            return Ok(response);
        }

        /// <summary>
        /// Survey unique check
        /// </summary>
        /// <param name="id"></param>
        /// <param name="surveyName"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("UniqueCheck")]
        public async Task<IActionResult> SurveyUniqueCheck(string id, string surveyName)
        {
            Logger.LogInformation("Survey unique check-SurveyUniqueCheck triggered.");

            BaseResponse response = new BaseResponse();
            var surveyDetails = await surveyService.CheckUniqueSurvey(id, surveyName);
            if (surveyDetails)
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.SurveyUniqueCheckConflictMessage;
                Logger.LogInformation("Survey unique check-SurveyUniqueCheck already exists response", surveyDetails);
                return BadRequest(response);
            }
            else
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyUniqueCheckAvailableMessage;
            }
            Logger.LogInformation("Survey unique check-SurveyUniqueCheck available response", surveyDetails);
            return Ok(response);
        }

        /// <summary>
        /// Survey details by survey id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Details")]
        public async Task<IActionResult> GetSurveyDetails(string id)
        {
            Logger.LogInformation("Survey details-GetSurveyDetails triggered.");

            BaseResponse response = new BaseResponse();
            var surveyDetails = await surveyService.GetSurveyById(id);
            if (surveyDetails != null)
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyDetailsFetchSuccessMessage;
                Logger.LogInformation("Survey details-GetSurveyDetails success response", surveyDetails);
                return Ok(response);
            }
            else
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyDetailsFetchFailureMessage;
                Logger.LogInformation("Survey details-GetSurveyDetails failed response", surveyDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Delete a survey
        /// </summary>
        /// <param name="surveyId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> DeleteSurvey(string surveyId)
        {
            Logger.LogInformation("Survey-DeleteSurvey triggered. surveyId:", surveyId);
            BaseResponse response = new BaseResponse();
            var surveyDetails = await surveyService.GetSurveyById(surveyId);

            if (surveyDetails == null)
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyDeleteFailureMessage;
                Logger.LogError("Survey-DeleteSurvey failed : surveyId =", surveyId);
                return NotFound(response);
            }
            else
            {
                surveyService.RemoveAsync(surveyDetails.Id);
                response.Data = "";
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyDeleteSuccessMessage;
                Logger.LogError("Survey-DeleteSurvey success : surveyId =", surveyId);
                return Ok(response);
            }
        }

        /// <summary>
        /// Get surveys by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("SurveysById")]
        public async Task<IActionResult> GetUpcomingSurveys(string id)
        {
            Logger.LogInformation("Survey -GetSurveysById triggered.");

            BaseResponse response = new BaseResponse();
            var surveyDetails = await surveyService.UpcomingSurveys(id);
            if (surveyDetails.Count > 0)
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyListByIdFetchSuccessMessage;
                Logger.LogInformation("Survey -GetSurveysById success response", surveyDetails);
                return Ok(response);
            }
            else
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyListByIdFetchFailureMessage;
                Logger.LogInformation("Survey -GetSurveysById failed response", surveyDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get survey questions and options
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="surveyId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("QuestionsAndOptions")]
        public async Task<IActionResult> GetSurveysQuestionsAndOptions(string subjectId, string surveyId)
        {
            Logger.LogInformation("Survey -GetSurveysQuestionAndOptions triggered.");

            BaseResponse response = new BaseResponse();
            var surveyQuestionDetails = await surveyService.GetSurveyQuestionsAndAnswers(subjectId, surveyId);
            if (surveyQuestionDetails != null)
            {
                response.Data = surveyQuestionDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyQuestionsFetchSuccessMessage;
                Logger.LogInformation("Survey -GetSurveysQuestionAndOptions success response", surveyQuestionDetails);
                return Ok(response);
            }
            else
            {
                response.Data = surveyQuestionDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyQuestionsFetchFailureMessage;
                Logger.LogInformation("Survey -GetSurveysQuestionAndOptions failed response", surveyQuestionDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Updates survey questions
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateQuestions")]
        public IActionResult UpdateSurveyQuestions(SurveyAnswers dto)
        {
            Logger.LogInformation("Add Survey triggered.");
            BaseResponse response = new BaseResponse();
            var surveyQuestions = surveyService.UpdateSurveyQuestionsAndAnswersForSubject(dto);

            if (surveyQuestions == false)
            {
                var error = new Helpers.ErrorDto { Message = "Survey questions updation failed!" };
                Logger.LogError("Update survey questions failed", error);
                response.Data = surveyQuestions;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = error.Message;
                return BadRequest(response);
            }
            else
            {
                Logger.LogInformation("Update survey questions response", surveyQuestions);
                response.Data = surveyQuestions;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyQuestionsUpdatedSuccessMessage;
                return Ok(response);
            }
        }

        /// <summary>
        /// Get Surveys by Status
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("SurveysByStatus")]
        public async Task<IActionResult> GetSurveysByStatus(string subjectId, string status)
        {
            Logger.LogInformation("Survey -GetSurveysByStatus triggered.");

            BaseResponse response = new BaseResponse();
            var surveyDetails = await surveyService.GetSurveysBasedOnStatus(subjectId, status);
            if (surveyDetails.Count > 0)
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyListByStatusFetchSuccessMessage;
                Logger.LogInformation("Survey -GetSurveysByStatus success response", surveyDetails);
                return Ok(response);
            }
            else
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyListByStatusFetchFailureMessage;
                Logger.LogInformation("Survey -GetSurveysByStatus failed response", surveyDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Checks whether survey has started or not
        /// </summary>
        /// <param name="surveyId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("CheckSurveyStarted")]
        public async Task<IActionResult> CheckSurveyStarted(string surveyId)
        {
            Logger.LogInformation("Check survey started - CheckSurveyStarted triggered.");

            BaseResponse response = new BaseResponse();
            var surveyDetails = await surveyService.IsSurveyStarted(surveyId);
            if (surveyDetails)
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyStartedMessage;
                Logger.LogInformation("Check survey started - CheckSurveyStarted success response", surveyDetails);
                return NotFound(response);
            }
            else
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyNotStartedMessage;
            }
            Logger.LogInformation("Check survey started - CheckSurveyStarted not started response", surveyDetails);
            return Ok(response);
        }

        /// <summary>
        /// Updates with new patients in a survey
        /// </summary>
        /// <param name="surveyId"></param>
        /// <param name="patientInfos"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("UpdateNewPatients")]
        public async Task<IActionResult> UpdateNewPatients(string surveyId, List<PatientSurveyDetailsDto> patientInfos)
        {
            Logger.LogInformation("Update new patients to survey - UpdateNewPatients triggered.");
            BaseResponse response = new BaseResponse();
            var isUpdated = await surveyService.FindAndUpdatePatientsToSurvey(surveyId, patientInfos.ToEntities());

            if (!isUpdated)
            {
                Logger.LogError("Update new patients to survey - UpdateNewPatients failed", patientInfos);
                response.Data = isUpdated;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyNotFoundMessage;
                return NotFound(response);
            }

            response.Data = isUpdated;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.SurveyUpdatedMessage;
            Logger.LogError("Update new patients to survey - UpdateNewPatients success", patientInfos);
            return Ok(response);
        }

        /// <summary>
        /// Get ongoing surveys list
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("OngoingSurveys")]
        public async Task<IActionResult> GetOngoingSurveys(string subjectId)
        {
            Logger.LogInformation("Survey -GetOngoingSurveys triggered.");

            BaseResponse response = new BaseResponse();
            var surveyDetails = await surveyService.GetOngoingSurveys(subjectId);
            if (surveyDetails.Count > 0)
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyOngoingListFetchSuccessMessage;
                Logger.LogInformation("Survey -GetOngoingSurveys success response", surveyDetails);
                return Ok(response);
            }
            else
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyOngoingListFetchFailureMessage;
                Logger.LogInformation("Survey -GetOngoingSurveys failed response", surveyDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get other patients
        /// </summary>
        /// <param name="surveyId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetOtherPatients")]
        public async Task<IActionResult> GetOtherPatients(string surveyId)
        {
            Logger.LogInformation("Survey -GetOtherPatients triggered.");

            BaseResponse response = new BaseResponse();
            var patientDetails = await surveyService.GetOtherPatients(surveyId);
            if (patientDetails.Count > 0)
            {
                response.Data = patientDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyOtherPatientsListFetchSuccessMessage;
                Logger.LogInformation("Survey -GetOtherPatients success response", patientDetails);
                return Ok(response);
            }
            else
            {
                response.Data = patientDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyOtherPatientsListFetchFailureMessage;
                Logger.LogInformation("Survey -GetOtherPatients failed response", patientDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get missed survey submission dates 
        /// </summary>
        /// <param name="surveyId"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("UnSubmittedDates")]
        public async Task<IActionResult> GetMissedSurveySubmissions(string surveyId, string subjectId)
        {
            Logger.LogInformation("Survey -GetUnSubmittedDates triggered.");

            BaseResponse response = new BaseResponse();
            var submissionDetails = await surveyService.GetMissedSurveySubmissionDates(surveyId, subjectId);
            if (submissionDetails.Count > 0)
            {
                response.Data = submissionDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyUnsubmittedDatesListFetchSuccessMessage;
                Logger.LogInformation("Survey -GetUnSubmittedDates success response", submissionDetails);
                return Ok(response);
            }
            else
            {
                response.Data = submissionDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyUnsubmittedDatesListFetchFailureMessage;
                Logger.LogInformation("Survey -GetUnSubmittedDates failed response", submissionDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Survey medicine usage check for deletion
        /// </summary>
        /// <param name="medicineId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("MedicineExistCheck")]
        public async Task<IActionResult> SurveyMedicineCheck(string medicineId)
        {
            Logger.LogInformation("Survey medicine check-SurveyMedicineCheck triggered.");

            BaseResponse response = new BaseResponse();

            bool usageDetailsSurvey = await surveyService.GetMedicineUsageStatus(medicineId);
            bool usageDetailsLog = await logService.GetMedicineUsageStatus(medicineId);
            bool usageDetailsMedicationJournal = await medicationJournalService.GetMedicineUsageStatus(medicineId);
            bool usageDetailsDosing = await dosingService.GetMedicineUsageStatus(medicineId);

            if (usageDetailsSurvey == true || usageDetailsLog == true || usageDetailsMedicationJournal == true || usageDetailsDosing == true)
            {
                response.Data = usageDetailsSurvey;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.SurveyMedicineCheckConflictMessage;
                Logger.LogInformation("Survey medicine check-SurveyMedicineCheck already exists in survey response", usageDetailsSurvey);
                return BadRequest(response);
            }
            else
            {
                response.Data = usageDetailsSurvey;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyMedicineCheckNoConflictMessage;
            }
            Logger.LogInformation("Survey medicine check-SurveyMedicineCheck available response", usageDetailsSurvey);
            return Ok(response);
        }

        /// <summary>
        /// Get next date os survey submission date
        /// </summary>
        /// <param name="surveyId"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetNextSubmissionDate")]
        public async Task<IActionResult> GetSurveyNextSubmissionDate(string surveyId, string subjectId)
        {
            Logger.LogInformation("Survey -GetSurveyNextSubmissionDate triggered.");

            BaseResponse response = new BaseResponse();
            var surveyNextDateDetails = await surveyService.GetNextDateOfSubmission(surveyId, subjectId);
            if (surveyNextDateDetails != null)
            {
                response.Data = surveyNextDateDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyNextDateFetchSuccessMessage;
                Logger.LogInformation("Survey -GetSurveyNextSubmissionDate success response", surveyNextDateDetails);
                return Ok(response);
            }
            else
            {
                response.Data = surveyNextDateDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyNextDateFetchFailureMessage;
                Logger.LogInformation("Survey -GetSurveyNextSubmissionDate failed response", surveyNextDateDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Body parts usage status
        /// </summary>
        /// <param name="bodyPartsName"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("BodyPartsUsage")]
        public async Task<IActionResult> CheckBodyPartsUsageStatus(string bodyPartsName)
        {
            Logger.LogInformation("Survey-CheckBodyParts triggered.");

            BaseResponse response = new BaseResponse();
            var surveyDetails = await surveyService.GetBodyPartsUsageStatus(bodyPartsName);
            if (surveyDetails == false)
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.BodyPartAvailableForDeletionMessage;
                Logger.LogInformation("Survey-CheckBodyParts success response", surveyDetails);
                return Ok(response);
            }
            else
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.BodyPartNotAvailableForDeletionMessage;
                Logger.LogInformation("Survey-CheckBodyParts failed response", surveyDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Survey reports
        /// </summary>
        /// <param name="reportDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Reports")]
        public async Task<IActionResult> GetSurveyReports(SurveyReportParameter reportDetails)
        {
            Logger.LogInformation("Survey reports-GetSurveyReports triggered.");

            BaseResponse response = new BaseResponse();
            var surveyDetails = await surveyService.GetAnswersBySubjectSurveyAndQuestions(reportDetails);
            if (surveyDetails != null)
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveyReportsFetchSuccessMessage;
                Logger.LogInformation("Survey reports-GetSurveyReports success response", surveyDetails);
                return Ok(response);
            }
            else
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveyReportsFetchFailureMessage;
                Logger.LogInformation("Survey reports-GetSurveyReports failed response", surveyDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get all surveys
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            Logger.LogInformation("Survey-GetAll triggered.");

            BaseResponse response = new BaseResponse();
            var surveyDetails = await surveyService.GetAllSurveys();
            if (surveyDetails != null)
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveysListFetchedSuccessMessage;
                Logger.LogInformation("Survey-GetAll success response", surveyDetails);
                return Ok(response);
            }
            else
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveysListFetchedFailureMessage;
                Logger.LogInformation("Survey-GetAll failed response", surveyDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get all patients in survey
        /// </summary>
        /// <param name="surveyId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllPatientsInSurvey")]
        public async Task<IActionResult> GetAllPatientsInSurvey(string surveyId)
        {
            Logger.LogInformation("Survey-GetAllPatientsInSurvey triggered.");

            BaseResponse response = new BaseResponse();
            var surveyDetails = await surveyService.GetAllPatientsInSurvey(surveyId);
            if (surveyDetails != null)
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveysPatientsListFetchedSuccessMessage;
                Logger.LogInformation("Survey-GetAllPatientsInSurvey success response", surveyDetails);
                return Ok(response);
            }
            else
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveysPatientsListFetchedFailureMessage;
                Logger.LogInformation("Survey-GetAllPatientsInSurvey failed response", surveyDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Survey questions list
        /// </summary>
        /// <param name="surveyId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllQuestionsInSurvey")]
        public async Task<IActionResult> GetAllQuestionsInSurvey(string surveyId)
        {
            Logger.LogInformation("Survey-GetAllPatientsInSurvey triggered.");

            BaseResponse response = new BaseResponse();
            var surveyDetails = await surveyService.GetAllQuestionsInSurvey(surveyId);
            if (surveyDetails != null)
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.SurveysQuestionsListFetchedSuccessMessage;
                Logger.LogInformation("Survey-GetAllQuestionsInSurvey success response", surveyDetails);
                return Ok(response);
            }
            else
            {
                response.Data = surveyDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.SurveysQuestionsListFetchedFailureMessage;
                Logger.LogInformation("Survey-GetAllQuestionsInSurvey failed response", surveyDetails);
                return BadRequest(response);
            }
        }
    }
}
