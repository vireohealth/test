﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.FormControls;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FormControlsController : BaseController<FormControlsController>
    {
        private readonly IFormControlService formControlService;

        public FormControlsController(IFormControlService formControlService, ILogger<FormControlsController> logger) : base(logger)
        {
            this.formControlService = formControlService;
        }

        /// <summary>
        /// Get all form Controls
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllAsync()
        {
            Logger.LogInformation("Form control list-GetAllAsync triggered.");
            BaseResponse response = new BaseResponse();
            var formControlDetails = await formControlService.GetAllAsync();
            if (formControlDetails != null)
            {
                response.Data = formControlDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.FormControlFetchSuccessMessage;
                Logger.LogInformation("Form control list-GetAllAsync success response", formControlDetails);
                return Ok(response);
            }
            else
            {
                response.Data = formControlDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.FormControlFetchFailureMessage;
            }
            Logger.LogInformation("Form control list-GetAllAsync failure response", formControlDetails);
            return BadRequest(response);
        }
    }
}
