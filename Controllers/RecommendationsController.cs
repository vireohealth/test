﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Recommendations;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RecommendationsController : BaseController<RecommendationsController>
    {
        private readonly IRecommendationsService recommendationService;

        public RecommendationsController(IRecommendationsService recommendationService, ILogger<RecommendationsController> logger) : base(logger)
        {
            this.recommendationService = recommendationService;
        }
        /// <summary>
        /// Get all recommenddations
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllAsync()
        {
            Logger.LogInformation("Recommendations - GetAllAsync triggered.");
            BaseResponse response = new BaseResponse();
            var recommendationDetails = await recommendationService.GetAllAsync();
            if (recommendationDetails != null)
            {
                response.Data = recommendationDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.RecommendationsFetchSuccessMessage;
                Logger.LogInformation("Recommendations - GetAllAsync success response", recommendationDetails);
                return Ok(response);
            }
            else
            {
                response.Data = recommendationDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.RecommendationsFetchFailureMessage;
            }
            Logger.LogInformation("Recommendations - GetAllAsync failure response", recommendationDetails);
            return BadRequest(response);
        }

        /// <summary>
        /// Add recommendation
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Add")]
        public IActionResult AddRecommendation(RecommendationDto dto)
        {
            Logger.LogInformation("Add recommendation triggered.");
            BaseResponse response = new BaseResponse();
            var recommendation = recommendationService.AddRecommendation(dto);

            if (recommendation == null)
            {
                var error = new Helpers.ErrorDto { Message = "Recommendation already exist!" };
                Logger.LogError("Add recommendation failed", error);
                response.Data = recommendation;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = error.Message;
                return BadRequest(response);
            }
            else
            {
                Logger.LogInformation("Adding recommendation response", recommendation);
                response.Data = recommendation;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.RecommendationAddedSuccessMessage;
                return Ok(response);
            }
        }

        /// <summary>
        /// Recommendation unique check
        /// </summary>
        /// <param name="id"></param>
        /// <param name="recommendationName"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("UniqueCheck")]
        public async Task<IActionResult> RecommendationUniqueCheck(string id, string recommendationName)
        {
            Logger.LogInformation("Recommendation unique check-RecommendationUniqueCheck triggered.");

            BaseResponse response = new BaseResponse();
            var recommendationDetails = await recommendationService.CheckUniqueRecommendation(id, recommendationName);
            if (recommendationDetails)
            {
                response.Data = recommendationDetails;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.RecommendationUniqueCheckConflictMessage;
                Logger.LogInformation("Recommendation unique check-RecommendationUniqueCheck already exists response", recommendationDetails);
                return BadRequest(response);
            }
            else
            {
                response.Data = recommendationDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.RecommendationUniqueCheckAvailableMessage;
            }
            Logger.LogInformation("Recommendation unique check-RecommendationUniqueCheck available response", recommendationDetails);
            return Ok(response);
        }
       
        /// <summary>
        /// Get Recommendation by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Details")]
        public async Task<IActionResult> GetRecommendationDetails(string id)
        {
            Logger.LogInformation("Recommendation details-GetRecommendationDetails triggered.");

            BaseResponse response = new BaseResponse();
            var recommendationDetails = await recommendationService.GetRecommendationById(id);
            if (recommendationDetails != null)
            {
                response.Data = recommendationDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.RecommendationDetailsFetchSuccessMessage;
                Logger.LogInformation("Recommendation details-GetRecommendationDetails success response", recommendationDetails);
                return Ok(response);
            }
            else
            {
                response.Data = recommendationDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.RecommendationDetailsFetchFailureMessage;
                Logger.LogInformation("Recommendation details-GetRecommendationDetails failed response", recommendationDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Recommendation Update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="recommendation"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(string id, RecommendationDto recommendation)
        {
            Logger.LogInformation("Recommendation - Update triggered.");
            BaseResponse response = new BaseResponse();
            var recommendationDetails = await recommendationService.GetRecommendationById(id);

            if (recommendationDetails == null)
            {
                Logger.LogError("Recommendation - Update failed", recommendation);
                response.Data = recommendation;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.RecommendationNotFoundMessage;
                return NotFound(response);
            }

            recommendation.CreatedOn = recommendationDetails.CreatedOn;
            recommendation.LastUpdatedOn = DateTime.UtcNow;
            recommendationService.UpdateAsync(id, recommendation);
            response.Data = recommendation;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.RecommendationUpdatedMessage;
            Logger.LogError("Recommendation - Update success", recommendation);
            return Ok(response);
        }
       
        /// <summary>
        /// Delete recommendation
        /// </summary>
        /// <param name="recommendationId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> DeleteRecommendation(string recommendationId)
        {
            Logger.LogInformation("Recommendation-DeleteRecommendation triggered. RecommendationId:", recommendationId);
            BaseResponse response = new BaseResponse();
            var recommendationDetails = await recommendationService.GetRecommendationById(recommendationId);

            if (recommendationDetails == null)
            {
                response.Data = recommendationDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.RecommendationDeleteFailureMessage;
                Logger.LogError("Recommendation-DeleteRecommendation failed : RecommendationId =", recommendationId);
                return NotFound(response);
            }
            else
            {
                recommendationService.RemoveAsync(recommendationDetails.Id);
                response.Data = "";
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.RecommendationDeleteSuccessMessage;
                Logger.LogError("Recommendation-DeleteRecommendation success : RecommendationId =", recommendationId);
                return Ok(response);
            }
        }

        /// <summary>
        /// Recommendations paginated list
        /// </summary>
        /// <param name="recommendationParams"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("PaginatedRecommendations")]
        public async Task<IActionResult> GetAllPaginationAsync([FromQuery] PaginationParams recommendationParams)
        {
            Logger.LogInformation("Recommendations - GetAllPaginationAsync triggered.");
            BaseResponse response = new BaseResponse();
            var recommendationDetails = await recommendationService.GetAllPaginationAsync(recommendationParams);

            Response.AddPaginationHeader(recommendationDetails.CurrentPage, recommendationDetails.PageSize,
               recommendationDetails.TotalCount, recommendationDetails.TotalPages);

            if (recommendationDetails != null)
            {
                response.Data = recommendationDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.RecommendationsFetchMessage;
                Logger.LogInformation("Recommendations - GetAllPaginationAsync success response", recommendationDetails);
                return Ok(response);
            }
            else
            {
                response.Data = recommendationDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.RecommendationsNotFoundMessage;
            }

            Logger.LogInformation("Recommendations - GetAllPaginationAsync failure response", recommendationDetails);

            return BadRequest(response);
        }

        /// <summary>
        /// Get recommendations based on log
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("RecommendationBasedOnLog")]
        public async Task<IActionResult> GetRecommendationBasedOnLogPerSubject(string subjectId)
        {
            Logger.LogInformation("Recommendation for subject-GetRecommendationPerSubject triggered.");

            BaseResponse response = new BaseResponse();
            var recommendationForSubject = await recommendationService.GetRecommendationBasedOnSubject(subjectId);
            if (recommendationForSubject != null)
            {
                response.Data = recommendationForSubject;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.RecommendationBasedOnLogFetchSuccessMessage;
                Logger.LogInformation("Recommendation for subject-GetRecommendationPerSubject success response", recommendationForSubject);
                return Ok(response);
            }
            else
            {
                response.Data = recommendationForSubject;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.RecommendationBasedOnLogFetchFailureMessage;
                Logger.LogInformation("Recommendation for subject-GetRecommendationPerSubject failed response", recommendationForSubject);
                return BadRequest(response);
            }
        }
    }
}
