﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.JournalEntries;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class JournalEntriesController : BaseController<JournalEntriesController>
    {
        private readonly IJournalEntryService journalEntryService;
        private readonly AppSettings appSettings;

        public JournalEntriesController(IJournalEntryService journalEntryService, ILogger<JournalEntriesController> logger) : base(logger)
        {
            this.journalEntryService = journalEntryService;
        }

        /// <summary>
        /// Add journal entry
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Add")]
        public IActionResult AddJournalEntry(JournalEntryDto dto)
        {
            Logger.LogInformation("Add journal entry triggered.");
            BaseResponse response = new BaseResponse();
            var journalEntry = journalEntryService.AddJournalEntry(dto);

            if (journalEntry == null)
            {
                var error = new Helpers.ErrorDto { Message = "Journal entry already exist!" };
                Logger.LogError("Add journal entry failed", error);
                response.Data = journalEntry;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = error.Message;
                return BadRequest(response);
            }
            else
            {
                Logger.LogInformation("Adding journal entry response", journalEntry);
                response.Data = journalEntry;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.JournalEntryAddedSuccessMessage;
                return Ok(response);
            }
        }

        /// <summary>
        /// Get all paginated journal entries
        /// </summary>
        /// <param name="journalEntryParams"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("PaginatedJournalEntries")]
        public async Task<IActionResult> GetAllPaginationAsync([FromQuery] PaginationParams journalEntryParams,string subjectId)
        {
            Logger.LogInformation("Journal entries - GetAllPaginationAsync triggered.");
            BaseResponse response = new BaseResponse();
            var journalEntries = await journalEntryService.GetAllPaginationAsync(journalEntryParams,subjectId);

            Response.AddPaginationHeader(journalEntries.CurrentPage, journalEntries.PageSize,
               journalEntries.TotalCount, journalEntries.TotalPages);

            if (journalEntries != null)
            {
                response.Data = journalEntries;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.JournalEntriesFetchMessage;
                Logger.LogInformation("Journal entries - GetAllPaginationAsync success response", journalEntries);
                return Ok(response);
            }
            else
            {
                response.Data = journalEntries;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.JournalEntryNotFoundMessage;
            }

            Logger.LogInformation("Journal entries - GetAllPaginationAsync failure response", journalEntries);

            return BadRequest(response);
        }

        /// <summary>
        /// Updates journal entry
        /// </summary>
        /// <param name="id"></param>
        /// <param name="journalEntryIn"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(string id, JournalEntryDto journalEntryIn)
        {
            Logger.LogInformation("Journal entry - Update triggered.");
            BaseResponse response = new BaseResponse();
            var journalEntry = await journalEntryService.GetJournalEntryById(id);

            if (journalEntry == null)
            {
                Logger.LogError("Journal entry - Update failed", journalEntryIn);
                response.Data = journalEntryIn;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.JournalEntryNotFoundMessage;
                return NotFound(response);
            }

            journalEntryIn.CreatedOn = journalEntry.CreatedOn;
            journalEntryIn.LastUpdatedOn = DateTime.UtcNow;
            journalEntryService.UpdateAsync(id, journalEntryIn);
            response.Data = journalEntryIn;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.JournalEntryUpdatedMessage;
            Logger.LogError("Journal entry - Update success", journalEntryIn);
            return Ok(response);
        }

        /// <summary>
        /// Get details of journal entry
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Details")]
        public async Task<IActionResult> GetJournalEntryDetails(string id)
        {
            Logger.LogInformation("Journal entry details-GetJournalEntryDetails triggered.");

            BaseResponse response = new BaseResponse();
            var journalEntryDetails = await journalEntryService.GetJournalEntryById(id);
            if (journalEntryDetails != null)
            {
                response.Data = journalEntryDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.JournalEntryDetailsFetchSuccessMessage;
                Logger.LogInformation("JournalEntry details-GetJournalEntryDetails success response", journalEntryDetails);
                return Ok(response);
            }
            else
            {
                response.Data = journalEntryDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.JournalEntryDetailsFetchFailureMessage;
                Logger.LogInformation("Journal entry details-GetJournalEntryDetails failed response", journalEntryDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Unique check of journal entry
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        /// <param name="time"></param>
        /// <param name="journalDate"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("UniqueCheck")]
        public async Task<IActionResult> UniqueCheck(string id, string title, string time, DateTime journalDate, string subjectId)
        {
            Logger.LogInformation("Journal entry unique check-UniqueCheck triggered.");

            BaseResponse response = new BaseResponse();
            var journalEntryDetails = await journalEntryService.CheckUniqueJournalEntry(id, title,time,journalDate, subjectId);
            if (journalEntryDetails)
            {
                response.Data = journalEntryDetails;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.JournalEntryExistsMessage;
                Logger.LogInformation("Journal entry unique check-UniqueCheck already exists response", journalEntryDetails);
                return BadRequest(response);
            }
            else
            {
                response.Data = journalEntryDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.JournalEntryAvailableMessage;
            }
            Logger.LogInformation("Journal entry unique check-UniqueCheck available response", journalEntryDetails);
            return Ok(response);
        }

        /// <summary>
        /// Unique check of journal entry on a particular time
        /// </summary>
        /// <param name="id"></param>
        /// <param name="time"></param>
        /// <param name="journalDate"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("TimeUniqueCheck")]
        public async Task<IActionResult> TimeUniqueCheck(string id, string time, DateTime journalDate, string subjectId)
        {
            Logger.LogInformation("Journal entry unique check for time-TimeUniqueCheck triggered.");

            BaseResponse response = new BaseResponse();
            var journalEntryDetails = await journalEntryService.CheckUniqueJournalEntryTime(id, time, journalDate, subjectId);
            if (journalEntryDetails)
            {
                response.Data = journalEntryDetails;
                response.ResponseCode = HTTPConstants.CONFLICT;
                response.ResponseMessage = MessageConstants.JournalEntryExistsMessage;
                Logger.LogInformation("Journal entry unique check for time-TimeUniqueCheck already exists response", journalEntryDetails);
                return BadRequest(response);
            }
            else
            {
                response.Data = journalEntryDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.JournalEntryAvailableMessage;
            }
            Logger.LogInformation("Journal entry unique check for time-TimeUniqueCheck available response", journalEntryDetails);
            return Ok(response);
        }
        /// <summary>
        /// Gets journal entries
        /// </summary>
        /// <param name="journalDate"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAllAsync(DateTime journalDate,string subjectId)
        {
            Logger.LogInformation("Journal entries - GetAllAsync triggered.");
            BaseResponse response = new BaseResponse();
            var journalEntries = await journalEntryService.GetAllAsync(journalDate,subjectId);
            if (journalEntries != null)
            {
                response.Data = journalEntries;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.JournalEntriesFetchMessage;
                Logger.LogInformation("Journal entries - GetAllAsync success response", journalEntries);
                return Ok(response);
            }
            else
            {
                response.Data = journalEntries;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.JournalEntriesFetchFailureMessage;
            }
            Logger.LogInformation("Journal entries - GetAllAsync failure response", journalEntries);
            return BadRequest(response);
        }
    }
}
