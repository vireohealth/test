﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Subjects;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : BaseController<ISubjectService>
    {
        private readonly ISubjectService subjectService;

        public LoginController(ISubjectService subjectService,
            ILogger<ISubjectService> logger) : base(logger)
        {
            this.subjectService = subjectService;
        }

        /// <summary>
        /// Authenticate
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Authenticate")]
        public async Task<IActionResult> Authenticate(LoginDto login)
        {
            Logger.LogInformation("Login-Authenticate triggered.", login);
            BaseResponse response = new BaseResponse();
            var subjectDetails = await subjectService.Authenticate_jwt(login.UserName, login.Password, GetIPAddress(), login.UserType);

            if (subjectDetails == null)
            {
                var message = "user name or password is invalid.";
                Logger.LogError("Login-Authenticate error.", message);
                response.Data = subjectDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.LoginFailedMessage;
                Logger.LogInformation("Login-Authenticate error.", subjectDetails);
                return NotFound(response);
            }
            if(subjectDetails.IsMailExpired !=null && subjectDetails.IsMailExpired==true)
            {
                var message = "Password expired.";
                Logger.LogError("Login-Authenticate error.", message);
                response.Data = subjectDetails;
                response.ResponseCode = HTTPConstants.FORBIDDEN;
                response.ResponseMessage = MessageConstants.LoginPasswordExpiredMessage;
                Logger.LogInformation("Login-Authenticate error- Password expired.", subjectDetails);
                return Unauthorized(response);
            }
            SetTokenCookie(subjectDetails.RefreshToken);

            Logger.LogInformation("Login-Authenticate success.", subjectDetails);
            subjectDetails.RefreshTokens = null; //clear token list.
            response.Data = subjectDetails;
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.LoginSuccessMessage;
            return Ok(response);

        }

        /// <summary>
        /// Refresh Token
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("RefreshToken")]
        public IActionResult RefreshToken([FromBody] RevokeTokenRequestDto request)
        {
            Logger.LogInformation("Login-RefreshToken triggered.");
            BaseResponse responseBase = new BaseResponse();
            var refreshToken = request.Token ?? Request.Cookies["refreshToken"];

            Logger.LogInformation("Login-RefreshToken triggered.", refreshToken);
            if (string.IsNullOrEmpty(refreshToken))
            {
                Logger.LogError("Login-RefreshToken token not found.");
                responseBase.Data = refreshToken;
                responseBase.ResponseCode = HTTPConstants.NOT_FOUND;
                responseBase.ResponseMessage = MessageConstants.RefreshTokenInvalidMessage;
                return Unauthorized(responseBase);
            }

            var response = subjectService.RefreshToken(refreshToken, GetIPAddress());

            if (response == null)
            {
                Logger.LogError("Login-RefreshToken error.", response);
                responseBase.Data = response;
                responseBase.ResponseCode = HTTPConstants.NOT_FOUND;
                responseBase.ResponseMessage = MessageConstants.RefreshTokenInvalidMessage;
                return Unauthorized(responseBase);
            }

            SetTokenCookie(response.RefreshToken);
            Logger.LogInformation("Login-RefreshToken success.", response);
            responseBase.Data = response;
            responseBase.ResponseCode = HTTPConstants.OK;
            responseBase.ResponseMessage = MessageConstants.RefreshTokenSuccessMessage;
            return Ok(responseBase);
        }

        /// <summary>
        /// Revoke token
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("RevokeToken")]
        public async Task<IActionResult> RevokeToken([FromBody] RevokeTokenRequestDto request)
        {
            Logger.LogInformation("Login-RevokeToken triggered.", request);
            BaseResponse responseBase = new BaseResponse();
            var token = request.Token ?? Request.Cookies["refreshToken"];

            if (string.IsNullOrEmpty(token))
            {
                Logger.LogError("Login-RevokeToken is empty error.", token);
                responseBase.Data = token;
                responseBase.ResponseCode = HTTPConstants.NOT_FOUND;
                responseBase.ResponseMessage = MessageConstants.RevokeTokenInvalidMessage;
                return BadRequest(responseBase);
            }

            var response = await subjectService.RevokeToken(token, GetIPAddress());

            if (!response)
            {
                Logger.LogError("Login-RevokeToken error.", response);
                responseBase.Data = response;
                responseBase.ResponseCode = HTTPConstants.NOT_FOUND;
                responseBase.ResponseMessage = MessageConstants.RevokeTokenNotFoundMessage;
                return NotFound(responseBase);
            }

            Logger.LogInformation("Login-RevokeToken success.", response);
            responseBase.Data = response;
            responseBase.ResponseCode = HTTPConstants.OK;
            responseBase.ResponseMessage = MessageConstants.RevokeTokenSuccessMessage;
            return Ok(responseBase);
        }

        /// <summary>
        /// GetRefresh Tokens
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet("{id}/RefreshTokens")]
        public async Task<IActionResult> GetRefreshTokens(string subjectId)
        {
            Logger.LogInformation("Login-RefreshTokens triggered subjectid=", subjectId);
            BaseResponse responseBase = new BaseResponse();
            var subjectDetails = await subjectService.GetBySubjectId(subjectId);
            if (subjectDetails == null)
            {
                Logger.LogInformation("Login-RefreshTokens error subjectid=", subjectId);
                responseBase.Data = subjectDetails;
                responseBase.ResponseCode = HTTPConstants.OK;
                responseBase.ResponseMessage = MessageConstants.RefreshTokenUserNotFoundMessage;
                return NotFound(responseBase);
            }

            Logger.LogInformation("Login-RefreshTokens success subject=", subjectDetails);
            responseBase.Data = subjectDetails.RefreshTokens;
            responseBase.ResponseCode = HTTPConstants.OK;
            responseBase.ResponseMessage = MessageConstants.RefreshTokenUserSuccessMessage;
            return Ok(responseBase);
        }

        /// <summary>
        /// Set Token Cookie
        /// </summary>
        /// <param name="token"></param>
        private void SetTokenCookie(string token)
        {
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddDays(7)
            };
            Response.Cookies.Append("refreshToken", token, cookieOptions);
        }
    }
}
