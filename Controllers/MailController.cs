﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Mail;
using VireoIntegrativeProgram.Business.Subjects;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MailController : BaseController<IMailService>
    {
        private readonly IMailService mailService;
        private readonly EmailConfiguration settings;
        private readonly ISubjectService subjectService;
        private IHostEnvironment env;
        private const string Admin = "admin";
        private const string Patient = "patient";

        public MailController(IMailService mailService, EmailConfiguration settings,

           IHostEnvironment env, ILogger<IMailService> logger, ISubjectService subjectService) : base(logger)
        {
            this.settings = settings;
            this.mailService = mailService;
            this.env = env;
            this.subjectService = subjectService;
        }

        /// <summary>
        /// Verifies the mail for resetting password
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ResetMailVerification")]
        public async Task<IActionResult> ResetMailVerification(string email)
        {
            var path = env.ContentRootPath + Path.DirectorySeparatorChar.ToString()
              + "EmailTemplate" + Path.DirectorySeparatorChar.ToString()
              + "ResetVerificationMail.html";
            string body = string.Empty;
            BaseResponse response = new BaseResponse();
            var user = await subjectService.ProcessResetPasswordVerification(email);
            if (user == null)
            {
                Logger.LogError("Reset password mail verification error.", user);
                response.Data = user;
                response.ResponseCode = HTTPConstants.BAD_REQUEST;
                response.ResponseMessage = MessageConstants.MailVerificationFailureMessage;
                Logger.LogInformation("Reset Password Mail Verification error.");
                return BadRequest(response);
            }
            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }
            var url = user.UserType.ToLower() == Admin.ToLower() ? settings.AdminVerificationUrl : settings.PatientVerificationUrl;
            body = body.Replace("{URL}", url + "resetpassword?uuid=" + user.Id + "&email=" + user.Email);
            body = body.Replace("{UserName}", user.Name);

            Logger.LogInformation("Reset password mail verification triggered.", email);

            var validationResult = mailService.SendSimpleMail(email, string.Empty, "Reset Password Mail", body);
            if (!validationResult)
            {
                Logger.LogError("Reset password mail verification error.", validationResult);
                response.Data = validationResult;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.MailVerificationFailureMessage;
                Logger.LogInformation("Reset password mail verification error.");
                return Unauthorized(response);
            }

            Logger.LogInformation("Reset password mail verification success.", email);
            response.Data = "true";
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.MailVerificationSuccessMessage;
            return Ok(response);
        }

        /// <summary>
        /// Activation mail
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Activation")]
        public async Task<IActionResult> Activation(string email, string userId)
        {
            var path = env.ContentRootPath + Path.DirectorySeparatorChar.ToString()
                + "EmailTemplate" + Path.DirectorySeparatorChar.ToString()
                + "ActivationMail.html";
            string body = string.Empty;
            BaseResponse response = new BaseResponse();
            var user = await subjectService.GetBySubjectId(userId);
            if (user == null)
            {
                Logger.LogError("Email-Activation error.", user);
                response.Data = user;
                response.ResponseCode = HTTPConstants.BAD_REQUEST;
                response.ResponseMessage = MessageConstants.EMailVerificationFailureMessage;
                Logger.LogInformation("Email-Activation error.");
                return BadRequest(response);
            }
            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }
            var url = user.UserType.ToLower() == Admin.ToLower() ? settings.AdminVerificationUrl : settings.PatientVerificationUrl;
            body = body.Replace("{URL}", url + "activation?guid=" + userId);
            body = body.Replace("{UserName}", user.Name);

            Logger.LogInformation("Email-Activation triggered.", email);

            var validationResult = mailService.SendSimpleMail(email, string.Empty, "Activation Mail", body);
            if (!validationResult)
            {
                Logger.LogError("Email-Activation error.", validationResult);
                response.Data = validationResult;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.EMailVerificationFailureMessage;
                Logger.LogInformation("Email-Activation error.");
                return Unauthorized(response);
            }

            Logger.LogInformation("Email-Activation success.", email);
            response.Data = "true";
            response.ResponseCode = HTTPConstants.OK;
            response.ResponseMessage = MessageConstants.EMailVerificationSuccessMessage;
            return Ok(response);
        }

    }
}
