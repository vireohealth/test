﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Logs;
using VireoIntegrativeProgram.Business.PersonalInsights;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PersonalInsightsController : BaseController<PersonalInsightsController>
    {
        private readonly IPersonalInsightService personalInsightService;

        public PersonalInsightsController(IPersonalInsightService personalInsightService, ILogger<PersonalInsightsController> logger) : base(logger)
        {
            this.personalInsightService = personalInsightService;
        }

        /// <summary>
        /// Get count of logs and journals
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="frequency"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("LogAndJournalsCount")]
        public async Task<IActionResult> GetLogsAndJournalCounts(string subjectId, string frequency)
        {
            Logger.LogInformation("PersonalInsights-GetLogsAndJournalByFrequency triggered");
            BaseResponse response = new BaseResponse();
            var personalDetails = await personalInsightService.GetAllPersonalInsights(subjectId, frequency);
            if (personalDetails != null)
            {
                response.Data = personalDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.LogsAndJournalsCountFetchSuccessMessage;
                Logger.LogInformation("PersonalInsights-GetLogsAndJournalByFrequency success response", personalDetails);
                return Ok(response);
            }
            else
            {
                response.Data = personalDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.LogsAndJournalsCountFetchFailureMessage;
                Logger.LogInformation("PersonalInsights-GetLogsAndJournalByFrequency failure response", personalDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get effects of pain
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="frequency"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("EffectsOfPainInSleep")]
        public async Task<IActionResult> GetEffectsOfPainInSleep(string subjectId, string frequency)
        {
            Logger.LogInformation("PersonalInsights-GetEffectOfPain triggered");
            BaseResponse response = new BaseResponse();
            var painDetails = await personalInsightService.GetEffectOfPainPerSubject(subjectId, frequency);
            if (painDetails != null)
            {
                response.Data = painDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.EffectsOfPainInSleepFetchSuccessMessage;
                Logger.LogInformation("PersonalInsights-GetEffectOfPain success response", painDetails);
                return Ok(response);
            }
            else
            {
                response.Data = painDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.EffectsOfPainInSleepFetchFailureMessage;
                Logger.LogInformation("PersonalInsights-GetEffectOfPain failure response", painDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get effect of pain in mood
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="frequency"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("EffectsOfPainInMood")]
        public async Task<IActionResult> GetEffectsOfPainInMood(string subjectId, string frequency)
        {
            Logger.LogInformation("PersonalInsights-GetEffectsOfPainInMood triggered");
            BaseResponse response = new BaseResponse();
            var painDetails = await personalInsightService.GetPainMoods(subjectId, frequency);
            if (painDetails != null)
            {
                response.Data = painDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.EffectsOfPainInMoodFetchSuccessMessage;
                Logger.LogInformation("PersonalInsights-GetEffectsOfPainInMood success response", painDetails);
                return Ok(response);
            }
            else
            {
                response.Data = painDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.EffectsOfPainInMoodFetchFailureMessage;
                Logger.LogInformation("PersonalInsights-GetEffectsOfPainInMood failure response", painDetails);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get dates of pain in sleep
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="frequency"></param>
        /// <param name="sleepType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("DatesOfPainInSleep")]
        public async Task<IActionResult> GetDatesOfPainInSleep(string subjectId, string frequency, string sleepType)
        {
            Logger.LogInformation("PersonalInsights-GetDatesOfPainInSleep triggered");
            BaseResponse response = new BaseResponse();
            var painDetails = await personalInsightService.GetEffectOfPainDates(subjectId, frequency, sleepType);
            if (painDetails != null)
            {
                response.Data = painDetails;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.DatesOfPainInSleepFetchSuccessMessage;
                Logger.LogInformation("PersonalInsights-GetDatesOfPainInSleep success response", painDetails);
                return Ok(response);
            }
            else
            {
                response.Data = painDetails;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.DatesOfPainInSleepFetchFailureMessage;
                Logger.LogInformation("PersonalInsights-GetDatesOfPainInSleep failure response", painDetails);
                return BadRequest(response);
            }
        }
    }
}