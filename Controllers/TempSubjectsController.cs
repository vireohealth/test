﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Mail;
using VireoIntegrativeProgram.Business.TempSubjects;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TempSubjectsController : BaseController<TempSubjectsController>
    {
        private readonly ITempSubjectService tempSubjectService;
        private readonly IMailService mailService;
        private readonly EmailConfiguration settings;
        private IHostEnvironment env;

        public TempSubjectsController(ITempSubjectService tempSubjectService, IMailService mailService, EmailConfiguration settings,
           IHostEnvironment env, ILogger<TempSubjectsController> logger) : base(logger)
        {
            this.settings = settings;
            this.mailService = mailService;
            this.env = env;
            this.tempSubjectService = tempSubjectService;
        }

        /// <summary>
        /// Get temp subject
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Details")]
        public async Task<IActionResult> GetSubjectDetails(string id)
        {
            Logger.LogInformation("Temp Subject details-GetSubjectDetails triggered.");

            BaseResponse response = new BaseResponse();
            var tempSubject = await tempSubjectService.GetTempSubjectById(id);
            if (tempSubject != null)
            {
                response.Data = tempSubject;
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.TempSubjectDetailsFetchSuccessMessage;
                Logger.LogInformation("Temp Subject details-GetSubjectDetails success response", tempSubject);
                return Ok(response);
            }
            else
            {
                response.Data = tempSubject;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.TempSubjectDetailsFetchFailureMessage;
                Logger.LogInformation("Temp Subject details-GetSubjectDetails failed response", tempSubject);
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get activation for patient
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Activation")]
        public async Task<IActionResult> Activation(string email)
        {
            BaseResponse response = new BaseResponse();
            bool result = await tempSubjectService.CheckRegisteredSubjects(email);
            if (result == false)
            {
                var path = env.ContentRootPath + Path.DirectorySeparatorChar.ToString()
                + "EmailTemplate" + Path.DirectorySeparatorChar.ToString()
                + "PatientActivationMail.html";
                string body = string.Empty;
              
                var subjectDetails = await tempSubjectService.SignUpSubject(email);
                if (subjectDetails == null)
                {
                    Logger.LogError("Email-already verified error.", subjectDetails);
                    response.Data = subjectDetails;
                    response.ResponseCode = HTTPConstants.NOT_FOUND;
                    response.ResponseMessage = MessageConstants.EMailSignUpVerificationFailureMessage;
                    Logger.LogInformation("Email-already verified error.");
                    return Unauthorized(response);
                }
                using (StreamReader reader = new StreamReader(path))
                {
                    body = reader.ReadToEnd();
                }
                var url = settings.PatientVerificationUrl;
                body = body.Replace("{URL}", url + "createAccount?guid=" + subjectDetails.Id);

                Logger.LogInformation("Email-Activation triggered.", subjectDetails.Email);

                var validationResult = mailService.SendSimpleMail(subjectDetails.Email, string.Empty, "Activation Mail", body);
                if (!validationResult)
                {
                    Logger.LogError("Email-Activation error.", validationResult);
                    response.Data = validationResult;
                    response.ResponseCode = HTTPConstants.NOT_FOUND;
                    response.ResponseMessage = MessageConstants.EMailVerificationFailureMessage;
                    Logger.LogInformation("Email-Activation error.");
                    return Unauthorized(response);
                }

                Logger.LogInformation("Email-Activation success.", subjectDetails.Email);
                response.Data = "true";
                response.ResponseCode = HTTPConstants.OK;
                response.ResponseMessage = MessageConstants.EMailVerificationSuccessMessage;
                return Ok(response);
            }
            else
            {
                Logger.LogError("Email is already registered.", result);
                response.Data = result;
                response.ResponseCode = HTTPConstants.NOT_FOUND;
                response.ResponseMessage = MessageConstants.EMailVerificationRegisteredMessage;
                Logger.LogInformation("Email is already registered.");
                return Unauthorized(response);
            }            
        }
    }
}
