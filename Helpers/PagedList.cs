﻿
using System.Collections.Generic;
using System.Linq;

namespace VireoIntegrativeProgram.Helpers
{
    public class PagedList<T> : List<T>
    {

        public PagedList(IEnumerable<T> items, int count, int pageNumber, int pageSize)
        {
            CurrentPage = pageNumber;
            TotalPages = count;
            PageSize = pageSize;
            TotalCount = items.Count();
            AddRange(items);
        }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }


        public static PagedList<T> CreatePageList(List<T> source, int count, int pageNumber,
          int pageSize)
        {
            return new PagedList<T>(source, count, pageNumber, pageSize);

        }

    }
}
