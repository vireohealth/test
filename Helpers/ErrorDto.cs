﻿

namespace VireoIntegrativeProgram.Helpers
{
    public class ErrorDto
    {
        public string Message { get; set; }
    }
}
