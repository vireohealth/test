﻿

namespace VireoIntegrativeProgram.Common.Dtos
{
    public class PaginationParams
    {
        private const int MaxPageSize = 50;
        public int PageNumber { get; set; } = 1;
        private int _pageSize = 5;
        public string Search { get; set; }
        public string SortCol { get; set; }
        public string SortDir { get; set; }

        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }
    }
}
