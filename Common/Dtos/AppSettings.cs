﻿

namespace VireoIntegrativeProgram.Common.Dtos
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string ExpireTime { get; set; }
        public string ImageMaxSize { get; set; }
        public string ImageMaxWidth { get; set; }
        public string ImageMaxHeight { get; set; }
        public string APIPath { get; set; }
        public int QuestionSubmitTime { get; set; }
    }
}
