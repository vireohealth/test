﻿namespace VireoIntegrativeProgram.Common.Enums
{
    public class FeedbackType
    {
        public const string Effective = "Effective";
        public const string NotEffective = "Not Effective";
    }
}
