﻿namespace VireoIntegrativeProgram.Common.Enums
{
    public class PainInSleep
    {
        public const string Excellent = "Excellent";
        public const string Adequate = "Adequate";
        public const string Good = "Good";
        public const string Disturbed = "Disturbed";
        public const string NoSleep = "No sleep";
    }
}