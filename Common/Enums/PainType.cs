﻿namespace VireoIntegrativeProgram.Common.Enums
{
    public class PainType
    {
        public const string Burning = "Burning";
        public const string Shooting = "Shooting";
        public const string NoPain = "No pain";
        public const string Aching = "Aching";
        public const string Numbness = "Numbness";
        public const string Itching = "Itching";
        public const string Others = "Others";
    }
}
