﻿namespace VireoIntegrativeProgram.Common.Enums
{
    public class PatientType
    {
        public const string Admin = "Admin";
        public const string Patient = "Patient";
    }
}
