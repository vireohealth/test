﻿namespace VireoIntegrativeProgram.Common.Enums
{
    public class PainMoods
    {
        public const string Happy = "Happy and Energetic";
        public const string Frustrated = "Frustrated and Uncomfortable";
        public const string Angry = "Angry and Anxious";
        public const string Depressed = "Depressed and Tired";
        public const string Sad = "Sad and Stressed";
    }
}