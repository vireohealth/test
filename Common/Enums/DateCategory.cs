﻿namespace VireoIntegrativeProgram.Common.Enums
{
    public class DateCategory
    {
        public const string Day = "Day";
        public const string Week = "Week";
        public const string Month = "Month";
    }
}
