﻿
namespace VireoIntegrativeProgram.Common.Enums
{
    public class SubjectStatus
    {
        public const string Activated = "Activated";
        public const string NotActivated = "Not Activated";
        public const string Deleted = "Deleted";
    }
}
