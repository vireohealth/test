﻿namespace VireoIntegrativeProgram.Common.Enums
{
    public class DateFrequency
    {
        public const string Today = "Today";
        public const string Weekly = "Weekly";
        public const string Monthly = "Monthly";
        public const string Onetime = "One-Time";
        public const string Daily = "Daily";
        public const string Welcome = "Welcome";
        public const string EveryOtherDay = "Every-Other-Day";
    }
}