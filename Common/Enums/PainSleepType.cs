﻿namespace VireoIntegrativeProgram.Common.Enums
{
    public class PainSleepType
    {
        public const string Excellent = "Excellent";
        public const string Good = "Good";
        public const string Adequate = "Adequate";
        public const string Disturbed = "Disturbed";
        public const string NoSleep = "No sleep";
       
    }
}
