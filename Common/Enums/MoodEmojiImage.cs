﻿namespace VireoIntegrativeProgram.Common.Enums
{
    public class MoodEmojiImage
    {
        public const string HappyAndEnergetic = "0-happy-energetic.png";
        public const string FrustratedAndUncomfortable = "frustrated-uncomfortable.png";
        public const string AngryAndAnxious = "angry-anxious.png";
        public const string DepressedAndTired= "depressed-tired.png";
        public const string SadAndStressed = "sad-stressed.png";
    }
}
