﻿namespace VireoIntegrativeProgram.Common.Enums
{
    public class PainTreatment
    {
        public const string Acupressure = "Acupressure";
        public const string Acupuncture = "Acupuncture";
        public const string BedRest = "Bed Rest";
        public const string Cupping = "Cupping";
        public const string PainPump = "Pain Pump";
        public const string Cryotherapy = "Cryotherapy";
        public const string Orthotics = "Orthotics";
        public const string Heat = "Heat";
    }
    public class PainTreatmentIcon
    {
        public const string Acupressure = "acupressure.png";
        public const string Acupuncture = "acupuncture.png";
        public const string BedRest = "bed-rest.png";
        public const string Cupping = "cupping.png";
        public const string PainPump = "pain-pump.png";
        public const string Cryotherapy = "cryotherapy.png";
        public const string Orthotics = "orthotics.png";
        public const string Heat = "heat.png";
    }
}
