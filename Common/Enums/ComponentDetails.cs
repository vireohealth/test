﻿
namespace VireoIntegrativeProgram.Common.Enums
{
    public class ComponentDetails
    {
        public const string ActivityControl = "Activity"; 
        public const string BodyPartsControl = "Body Parts"; 
        public const string CheckBoxControl = "Check Box"; 
        public const string DateControl = "Date"; 
        public const string MedicationsControl = "Medications"; 
        public const string MoodControl = "Mood"; 
        public const string MultiSelectControl = "Multi select"; 
        public const string PainLevelControl = "Pain Level"; 
        public const string PainSymptomsControl = "Pain Symptoms"; 
        public const string PainTriggerControl = "Pain Trigger"; 
        public const string PainTypeControl = "Pain Type"; 
        public const string RadioButtonControl = "Radio Button"; 
        public const string SelectControl = "Select"; 
        public const string SleepControl = "Sleep"; 
        public const string TextAreaControl = "Text Area"; 
        public const string TextBoxControl = "Text Box"; 
        public const string TreatmentsControl= "Treatments"; 
    }
}
