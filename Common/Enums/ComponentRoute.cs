﻿
namespace VireoIntegrativeProgram.Common.Enums
{
    public class ComponentRoute
    {
        public const string ActivityRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected.value";
        public const string BodyPartsRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected.bodyPartName";
        public const string CheckBoxRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected.value";
        public const string DateRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.value";
        public const string MedicationsRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected.value";
        public const string MoodRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected.value";
        public const string MultiSelectRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected.value";
        public const string PainLevelRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected.value";
        public const string PainSymptomsRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected.value";
        public const string PainTriggerRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected.value";
        public const string PainTypeRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected.value";
        public const string RadioButtonRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected";
        public const string SelectRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected.value";
        public const string SleepRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected.value";
        public const string TextAreaRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.value";
        public const string TextBoxRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.value";
        public const string TreatmentsRoute = "Patients.DatesInfo.FormPropertyValues.components.components.right.selected.value";
        public const string LabelRoute = "Patients.DatesInfo.FormPropertyValues.components.components.left.text";
    }
}
