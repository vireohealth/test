﻿
namespace VireoIntegrativeProgram.Common.Enums
{
    public class SurveyStatus
    {
        public const string YetToStart = "Yet to start";
        public const string Active = "Active";
        public const string Completed = "Completed";
        public const string Today = "Today";
        public const string Ongoing = "Ongoing";
    }
}
