﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace VireoIntegrativeProgram.Common.Core
{
    public abstract class BaseController<TInstance> : ControllerBase
    {
        protected ILogger<TInstance> Logger { get; set; }

        public BaseController(ILogger<TInstance> logger)
        {
            this.Logger = logger;
        }

        protected string GetIPAddress()
        {
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }
    }
}
