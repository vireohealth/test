﻿using Microsoft.Extensions.Logging;

namespace VireoIntegrativeProgram.Common.Core
{
    public abstract class BusinessService<TInstance>
    {
        protected ILogger<TInstance> Logger { get; set; }

        protected BusinessService(ILogger<TInstance> logger)
        {
            this.Logger = logger;
        }
    }
}
