﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace VireoIntegrativeProgram.Models
{
    public class Survey
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public Product Product { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastUpdatedOn { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? StartDate { get; set; }
        public bool IsActive { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? EndDate { get; set; }
        public List<PatientSurveyDetails> Patients { get; set; }
        public BsonDocument FormProperties { get; set; }
        public int TotalInputQuestions { get; set; }
        public int TimeToComplete { get; set; }
        public SurveyType SurveyType { get; set; }
        public Frequency Frequency { get; set; }
        public string Status { get; set; }
        public bool IsSelectedAll { get; set; }
        public List<QuestionAndAnswer> QuestionsAndAnswers { get; set; }

    }
}
