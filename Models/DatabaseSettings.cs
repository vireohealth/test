﻿

namespace VireoIntegrativeProgram.Models
{
    public class DatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string SubjectCollectionName { get; set; }
        public string ProductCollectionName { get; set; }
        public string FormControlCollectionName { get; set; }
        public string SurveyTypeCollectionName { get; set; }
        public string SurveyCollectionName { get; set; }
        public string FrequencyCollectionName { get; set; }
        public string UnitOfMeasureCollectionName { get; set; }
        public string CountryCollectionName { get; set; }
        public string EducationalVideoCollectionName { get; set; }
        public string RecommendationCollectionName { get; set; }
        public string DosingCollectionName { get; set; }
        public string MenuCollectionName { get; set; }
        public string LogCollectionName { get; set; }
        public string JournalEntryCollectionName { get; set; }
        public string MedicationJournalCollectionName { get; set; }
        public string BodyPartsCollectionName { get; set; }
        public string TempSubjectsCollectionName { get; set; }
        public string RecommendationSettingCollectionName { get; set; }
        public string PasswordHistoryCollectionName { get; set; }
        public string FeedbackCollectionName { get; set; }
    }
}
