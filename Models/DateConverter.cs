﻿using System;

namespace VireoIntegrativeProgram.Models
{
    public class DateConverter
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}