﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace VireoIntegrativeProgram.Models
{
    public class RecommendationSettings
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string PainEffectMoodStatus { get; set; }
        public string PainEffectSleepStatus { get; set; }
        public Recommendation Recommendation { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastUpdatedOn { get; set; }
    }
}
