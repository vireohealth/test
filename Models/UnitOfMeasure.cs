﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace VireoIntegrativeProgram.Models
{
    public class UnitOfMeasure
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Unit { get; set; }
        public string Description { get; set; }
        public MeasurementType System { get; set; }
        public bool IsActive { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public List<ConversionUnit> ConversionUnits { get; set; }
    }
}
