﻿using System.Collections.Generic;

namespace VireoIntegrativeProgram.Models
{
    public class SurveyReportParameter
    {
        public string surveyId { get; set; }
        public List<string> subjectIds { get; set; }
        public List<SurveyQuestionsAndTypes> questionList { get; set; }
    }
}
