﻿
namespace VireoIntegrativeProgram.Models
{
    public class Answers
    {
        public string Answer { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
