﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace VireoIntegrativeProgram.Models
{
    public class PatientSurveyDetails
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public List<PatientDatesInfo> DatesInfo { get; set; }
    }
}
