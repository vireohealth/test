﻿using System;
using System.Collections.Generic;
using VireoIntegrativeProgram.Business.Surveys;

namespace VireoIntegrativeProgram.Models
{
    public class SurveyAnswers
    {
        public string SurveyId { get; set; }
        public string SubjectId { get; set; }
        public DateTime DateToUpdate { get; set; }
        public string SurveyResults { get; set; }
        public int FilledQuestions { get; set; }
        public decimal CompletionPercentage { get; set; }
        public List<QuestionAndAnswerDto> QuestionsAndAnswers { get; set; }
    }
}
