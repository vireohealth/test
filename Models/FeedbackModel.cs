﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace VireoIntegrativeProgram.Models
{
    public class FeedbackModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public string name { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string subject_id { get; set; }
        public string suggestions { get; set; }
        public int feedback { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime added_on { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime updated_on { get; set; }
    }
}
