﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace VireoIntegrativeProgram.Models
{
    public class Dosings
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string TSQMSideEffects { get; set; }
        public string TSQMSatisfaction { get; set; }
        public string TSQMSeverity { get; set; }
        public string PEGScore { get; set; }
        public bool IsActive { get; set; }
        public Product Product { get; set; }
        public Recommendation Recomendation { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastUpdatedOn { get; set; }
    }
}
