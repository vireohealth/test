﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Models
{
    public class Country
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("code2")]
        public string Code { get; set; }

        [BsonElement("code3")]
        public string Code3 { get; set; }

        [BsonElement("capital")]
        public string Capital { get; set; }

        [BsonElement("region")]
        public string Region { get; set; }

        [BsonElement("subregion")]
        public string SubRegion { get; set; }

        [BsonElement("states")]
        public List<State> States { get; set; }
    }
}
