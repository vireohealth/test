﻿

namespace VireoIntegrativeProgram.Models
{
    public class EmailConfiguration
    {
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public string SmtpServer { get; set; }
        public string Port { get; set; }
        public bool EnableSsl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AdminVerificationUrl { get; set; }
        public string PatientVerificationUrl { get; set; }

    }
}
