﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace VireoIntegrativeProgram.Models
{
    public class PasswordHistory
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string SubjectId { get; set; }
        public string Password { get; set; }
        public DateTime AddedOn { get; set; }
        public bool IsActive { get; set; }
    }
}
