﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Models
{
    public class FitBitAccessToken
    {
        [BsonElement("access_token")]
        public string Token { get; set; }
        [BsonElement("token_type")]
        public string TokenType { get; set; }
        [BsonElement("scope")]
        public string Scope { get; set; }
        [BsonElement("expires_in")]
        public int ExpiresIn { get; set; }
        [BsonElement("refresh_token")]
        public string RefreshToken { get; set; }
        [BsonElement("user_id")]
        public string UserId { get; set; }
        public DateTime UtcExpirationDate { get; set; }
    }
}
