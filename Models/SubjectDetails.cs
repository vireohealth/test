﻿

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace VireoIntegrativeProgram.Models
{
    public class SubjectDetails
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
