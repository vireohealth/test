﻿namespace VireoIntegrativeProgram.Models
{
    public class MedicationDosage
    {
        public Medications Medication { get; set; }
        public string Dosage { get; set; }
        public string Feedback { get; set; }
        public string FeedbackProductId { get; set; }
    }
}
