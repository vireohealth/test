﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Models
{
    public class FitBitSettings
    {
        public string FitbitClientId { get; set; }
        public string FitbitClientSecret { get; set; }
        public string RedirectURL { get; set; }
    }
}
