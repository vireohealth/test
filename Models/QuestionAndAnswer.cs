﻿using System.Collections.Generic;

namespace VireoIntegrativeProgram.Models
{
    public class QuestionAndAnswer
    {
        public string Question { get; set; }
        public List<Answers> Answers { get; set; }
    }
}
