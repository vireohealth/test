﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace VireoIntegrativeProgram.Models
{
    public class Subject
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ActivatedOn { get; set; }
        public bool IsActive { get; set; }
        public List<RefreshToken> RefreshTokens { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public string UserType { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public bool IsDeleted { get; set; }
        public string UserStatus { get; set; }
        public string ProfilePic { get; set; }
        public string Notes { get; set; }
        public string MyPreferedDispenseries { get; set; }
        public bool? IsMailExpired { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? LastPasswordUpdatedDate { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ResetMailSentDate { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? LastFeedbackDate { get; set; }
        public FitBitAccessToken FitBitAccessToken { get; set; }
        public Subject()
        {
            RefreshTokens = new List<RefreshToken>();
        }
    }
}
