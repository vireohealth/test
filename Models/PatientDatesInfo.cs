﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace VireoIntegrativeProgram.Models
{
    public class PatientDatesInfo
    {
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? SubmittedDate { get; set; }
        public BsonDocument FormPropertyValues { get; set; }
        public int TotalFilledInputQuestions { get; set; }
        public decimal CompletionPercent { get; set; }
        public bool IsCompleted { get; set; }
        public List<QuestionAndAnswer> QuestionsAndAnswers { get; set; }

    }
}
