﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace VireoIntegrativeProgram.Models
{
    public class MeasurementType
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
