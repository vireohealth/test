﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace VireoIntegrativeProgram.Models
{
    public class EducationalVideo
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ThumbImagePath { get; set; }
        public string VideoPath { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime PostedOn { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime LastUpdatedOn { get; set; }
        public bool IsEmailCampaign { get; set; }
        public bool IsScheduledBroadcastByEvent { get; set; }
        public bool IsScheduledBroadcastByDate { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? EventDate { get; set; }
        public string EventId { get; set; }
        public string EventName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDefaultThumb { get; set; }
        public string ImageName { get; set; }
        public List<SubjectDetails> SubjectList { get; set; }
    }
}
