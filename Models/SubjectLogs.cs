﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using VireoIntegrativeProgram.Business.Surveys;

namespace VireoIntegrativeProgram.Models
{
    public class SubjectLogs
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public List<string> Triggers { get; set; }
        public List<string> PainType { get; set; }
        public List<MedicationDosage> Medications { get; set; }
        public List<string> Sleep { get; set; }
        public List<string> Treatments { get; set; }
        public List<string> PainLocation { get; set; }
        public int? BodySide { get; set; } // 0 for Front and 1 for Back
        public List<string> LevelOfPain { get; set; }
        public string Notes { get; set; }
        public string SleepDisturbNotes { get; set; }
        public string PainTypeOthersNotes { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime DateOfLog { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime AddedOn { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime LastUpdatedOn { get; set; }
        public BaseSubject Subject { get; set; }
        public bool IsActive { get; set; }
    }
}
