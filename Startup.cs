using Fitbit.Api.Portable;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using VireoIntegrativeProgram.Business.BodyParts;
using VireoIntegrativeProgram.Business.Countries;
using VireoIntegrativeProgram.Business.Dosing;
using VireoIntegrativeProgram.Business.EducationalVideos;
using VireoIntegrativeProgram.Business.FitBitTokenManager;
using VireoIntegrativeProgram.Business.FormControls;
using VireoIntegrativeProgram.Business.Frequencies;
using VireoIntegrativeProgram.Business.JournalEntries;
using VireoIntegrativeProgram.Business.Logs;
using VireoIntegrativeProgram.Business.Mail;
using VireoIntegrativeProgram.Business.MedicationJournal;
using VireoIntegrativeProgram.Business.Menus;
using VireoIntegrativeProgram.Business.PersonalInsights;
using VireoIntegrativeProgram.Business.Products;
using VireoIntegrativeProgram.Business.Recommendations;
using VireoIntegrativeProgram.Business.RecommendationSettings;
using VireoIntegrativeProgram.Business.Subjects;
using VireoIntegrativeProgram.Business.Surveys;
using VireoIntegrativeProgram.Business.SurveyTypes;
using VireoIntegrativeProgram.Business.TempSubjects;
using VireoIntegrativeProgram.Business.UnitOfMeasurements;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Middleware;
using VireoIntegrativeProgram.Models;
using VireoIntegrativeProgram.Validators;

namespace VireoIntegrativeProgram
{
    public class Startup
    {
        private byte[] key;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private void RegisterConfigurations(IServiceCollection services)
        {
            services.Configure<DatabaseSettings>(
              Configuration.GetSection(nameof(DatabaseSettings)));


            services.AddSingleton(sp =>
                sp.GetRequiredService<IOptions<DatabaseSettings>>().Value);

            services.Configure<TwilioSettings>(
                Configuration.GetSection(nameof(TwilioSettings)));

            services.AddSingleton(sp =>
                sp.GetRequiredService<IOptions<TwilioSettings>>().Value);

            services.Configure<EmailConfiguration>(
                Configuration.GetSection(nameof(EmailConfiguration)));

            services.AddSingleton(sp =>
                sp.GetRequiredService<IOptions<EmailConfiguration>>().Value);

            services.Configure<FitBitSettings>(
               Configuration.GetSection(nameof(FitBitSettings)));

            services.AddSingleton(sp =>
                sp.GetRequiredService<IOptions<FitBitSettings>>().Value);

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            var appSettings = appSettingsSection.Get<AppSettings>();
            key = Encoding.ASCII.GetBytes(appSettings.Secret);
        }
        private void RegisterServices(IServiceCollection services)
        {
            services.AddApplicationInsightsTelemetry();
            services.AddScoped<ISubjectService, SubjectService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IFormControlService, FormControlService>();
            services.AddScoped<ISurveyTypesService, SurveyTypesService>();
            services.AddScoped<ISurveyService, SurveyService>();
            services.AddScoped<IFrequencyService, FrequencyService>();
            services.AddScoped<IUnitOfMeasurementService, UnitOfMeasurementService>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IEducationalVideoService, EducationalVideoService>();
            services.AddScoped<IRecommendationsService, RecommendationsService>();
            services.AddScoped<IDosingService, DosingService>();
            services.AddScoped<IRecommendationSettingService, RecommendationSettingService>();
            services.AddScoped<IMenuService, MenuService>();
            services.AddScoped<ILogsService, LogsService>();
            services.AddScoped<ITokenManager, TokenManager>();
            services.AddScoped<IRevokeTokenManager, RevokeTokenManager>();
            services.AddScoped<IJournalEntryService, JournalEntryService>();
            services.AddScoped<IMedicationJournalService, MedicationJournalService>();
            services.AddScoped<IPersonalInsightService, PersonalInsightService>();
            services.AddScoped<IBodyPartsService, BodyPartsService>();
            services.AddScoped<ITempSubjectService, TempSubjectService>();
            services.AddTransient<IMailService, MailService>();
            services.AddTransient<IValidator<SubjectDto>, SubjectValidator>();
            services.AddTransient<IValidator<SurveyDto>, SurveyValidator>();
            services.AddTransient<IValidator<PatientSurveyDetailsDto>, PatientSurveyDetailsValidator>();
            services.AddTransient<IValidator<ConfirmPasswordDto>, SetPasswordValidator>();
            services.AddTransient<IValidator<EmailConfirmPasswordDto>, SetConfirmPasswordValidator>();
            services.AddTransient<IValidator<EducationalVideoDto>, EducationalVideoValidator>();
            services.AddTransient<IValidator<ProductDto>, ProductValidators>();
            services.AddTransient<IValidator<JournalEntryDto>, JournalEntryValidators>();
            services.AddTransient<IValidator<RecommendationDto>, RecommendationValidator>();
            services.AddTransient<IValidator<MedicationJournalDto>, MedicationJournalValidator>();
            services.AddTransient<IValidator<BodyPartsDto>, BodyPartValidator>();
            services.AddTransient<IValidator<PatientDto>, PatientRegisterValidators>();
            services.AddTransient<IValidator<BodyPartUpsertDto>, BodyPartUpdateValidator>();
            services.AddTransient<IValidator<RecommendationSettingDto>, RecommendationSettingValidators>();
            services.AddTransient<IValidator<LogDto>, LogValidator>();

            services.AddMvc().ConfigureApiBehaviorOptions(options =>
            {
                options.InvalidModelStateResponseFactory = c =>
                {
                    var errors = string.Join(" , ", c.ModelState.Values.Where(v => v.Errors.Count > 0)
                      .SelectMany(v => v.Errors)
                      .Select(v => v.ErrorMessage));

                    return new BadRequestObjectResult(new BaseResponse
                    {
                        ResponseCode = HTTPConstants.BAD_REQUEST,
                        ResponseMessage = "Validation Error Occured",
                        Data = errors
                    });
                };
            });
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks();
            services.AddControllers();
            RegisterConfigurations(services);

            services.AddControllers().AddNewtonsoftJson(options =>
               options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddMvc().AddFluentValidation();
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
          .AddJwtBearer(x =>
          {
              x.RequireHttpsMetadata = false;
              x.SaveToken = true;
              x.TokenValidationParameters = new TokenValidationParameters
              {
                  ValidateIssuerSigningKey = true,
                  IssuerSigningKey = new SymmetricSecurityKey(key),
                  ValidateIssuer = false,
                  ValidateAudience = false,
                  ClockSkew = TimeSpan.Zero
              };
          });

            RegisterServices(services);

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                   builder => builder
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                        .SetIsOriginAllowed(origin => true)
               .AllowCredentials());
            });

            services.AddSwaggerGen(x =>
            {
                x.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Vireo API",
                    Version = "v1.1",
                    Contact = new OpenApiContact
                    {
                        Email = "jobin.kurian@cycloides.co.in",
                        Name = "Vireo Integrative program"
                    }
                });
                x.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                x.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                x.IncludeXmlComments(xmlPath);
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<ExceptionMiddleware>();
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                            Path.Combine(Directory.GetCurrentDirectory(), @"Images")),
                RequestPath = new PathString("/Images")
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                            Path.Combine(Directory.GetCurrentDirectory(), @"BodyPartsImages")),
                RequestPath = new PathString("/BodyPartsImages")
            });
            app.UseAuthentication();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors("CorsPolicy");

            app.UseSwagger();
            app.UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint("/swagger/v1/swagger.json", "Vireo API");
            });

            var option = new RewriteOptions();
            option.AddRedirect("^$", "swagger");
            app.UseRewriter(option);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health");
            });
        }
    }
}
