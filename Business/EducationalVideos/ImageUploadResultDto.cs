﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.EducationalVideos
{
    public class ImageUploadResultDto
    {
        public string Message { get; set; }
        public bool IsUploaded { get; set; }
        public string FullImagePath { get; set; }
        public string ImageName { get; set; }
        public bool IsDefaultThumb { get; set; }
    }
}
