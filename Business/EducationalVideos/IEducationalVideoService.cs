﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Helpers;

namespace VireoIntegrativeProgram.Business.EducationalVideos
{
    public interface IEducationalVideoService
    {
        Task<List<EducationalVideoDto>> GetAllAsync();
        Task<PagedList<EducationalVideoDto>> GetAllPaginationAsync(PaginationParams educationalVideoParams);
        EducationalVideoDto AddEducationalVideo(EducationalVideoDto dto);
        ImageUploadResultDto UploadedThumbImage(IFormFile thumbImage);
        Task<EducationalVideoDto> GetEducationalVideoById(string id);
        void UpdateAsync(string id, EducationalVideoDto educationalVideoDto);
        bool DeleteThumbImage(string imageName, bool isDefaultImage);
        void RemoveAsync(string id);
        Task<bool> CheckUniqueEducationalVideo(string id, string title, string url);
        Task<ImageUploadResultDto> UploadImage(IFormFile thumbImage);
    }
}
