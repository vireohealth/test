﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Helpers;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.EducationalVideos
{
    public class EducationalVideoService : BusinessService<IEducationalVideoService>, IEducationalVideoService
    {
        private readonly IMongoCollection<EducationalVideo> educationalVideoCollection;
        private readonly AppSettings appSettings;
        private IHostEnvironment env;
        private const string DefaultImage = "Default.jpg";
        private const string ImageFolder = "Images";
        Dictionary<string, string> MIMETypes = new Dictionary<string, string>
                                                    {
                                                        { "image/jpeg",".jpg"},
                                                        { "image/gif",".gif"},
                                                        {"image/png",".png"},

                                                    };
        public EducationalVideoService(DatabaseSettings databaseSettings,
            ILogger<IEducationalVideoService> logger,
            IOptions<AppSettings> appSettings, IHostEnvironment env) : base(logger)
        {
            this.appSettings = appSettings.Value;
            this.env = env;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);

            educationalVideoCollection = database.GetCollection<EducationalVideo>(databaseSettings.EducationalVideoCollectionName);
            educationalVideoCollection.Indexes.CreateOneAsync(new CreateIndexModel<EducationalVideo>(nameof(EducationalVideo.Title)));
        }

        public EducationalVideoDto AddEducationalVideo(EducationalVideoDto dto)
        {
            var educationalVideo = FindEducationalVideoByTitle(dto.Title);
            if (educationalVideo != null)
            {
                return null;
            }

            var entity = dto.ToEntity();
            entity.PostedOn = DateTime.UtcNow;
            entity.LastUpdatedOn = DateTime.UtcNow;
            entity.IsActive = true;

            educationalVideoCollection.InsertOne(entity);
            return FindEducationalVideoByTitle(dto.Title).ToDto();
           
        }
        private EducationalVideo FindEducationalVideoByTitle(string title)
        {
            return educationalVideoCollection.Find(x => x.Title.ToLower() == title.ToLower() && x.IsActive == true).FirstOrDefault();
        }
        public async Task<List<EducationalVideoDto>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return educationalVideoCollection.Find(video => true).ToDtos().OrderBy(a => a.Title).ToList();
            });
        }
        public async Task<PagedList<EducationalVideoDto>> GetAllPaginationAsync(PaginationParams educationalVideoParams)
        {
            return await Task.Run(async () =>
            {
                var filteredEducationVideos = Builders<EducationalVideo>.Filter.Empty;

                if (!string.IsNullOrEmpty(educationalVideoParams.Search))
                {
                    var search = educationalVideoParams.Search.ToLower();

                    filteredEducationVideos = Builders<EducationalVideo>.Filter
                    .Where(educational => educational.Title.ToLower().Contains(search)
                    || educational.Description.ToLower().Contains(search)
                    );
                }

                var SortColumn = Builders<EducationalVideo>.Sort.Descending(x => x.PostedOn);

                if (!string.IsNullOrEmpty(educationalVideoParams.SortCol))
                {
                    if ((!string.IsNullOrEmpty(educationalVideoParams.SortDir)) && (educationalVideoParams.SortDir == MessageConstants.AscendingSortDirection))
                    {
                        SortColumn = Builders<EducationalVideo>.Sort.Ascending(educationalVideoParams.SortCol);
                    }
                    else
                    {
                        SortColumn = Builders<EducationalVideo>.Sort.Descending(educationalVideoParams.SortCol);
                    }
                }
                var results = await educationalVideoCollection.AggregateByPage(filteredEducationVideos, SortColumn,
                educationalVideoParams.PageNumber, educationalVideoParams.PageSize);

                return PagedList<EducationalVideoDto>.CreatePageList(results.data.ToDtos(), results.totalPages,
                    educationalVideoParams.PageNumber, educationalVideoParams.PageSize);
            });
        }

        public ImageUploadResultDto UploadedThumbImage(IFormFile thumbImage)
        {
            ImageUploadResultDto result = new ImageUploadResultDto();
            
            string uploadsFolder = env.ContentRootPath + Path.DirectorySeparatorChar.ToString()
              + ImageFolder;
            string uploadUrl = appSettings.APIPath + Path.DirectorySeparatorChar.ToString()
             + ImageFolder;
            if (thumbImage != null)
            {
                string _doctype;
                MIMETypes.TryGetValue(thumbImage.ContentType.ToLower(), out _doctype);
                if (_doctype == null)
                    return new ImageUploadResultDto() { IsUploaded = false, Message = MessageConstants.ImageUploadedFormatIssueMessage };
                
                if(thumbImage.Length >= long.Parse(appSettings.ImageMaxSize))
                    return new ImageUploadResultDto() { IsUploaded = false, Message = MessageConstants.ImageUploadedSizeIssueMessage };

                using (var image = Image.FromStream(thumbImage.OpenReadStream()))
                {
                    // use image.Width and image.Height
                    if(image.Width > int.Parse(appSettings.ImageMaxWidth) || image.Height > int.Parse(appSettings.ImageMaxHeight))
                        return new ImageUploadResultDto() { IsUploaded = false, Message = MessageConstants.ImageUploadedWidthIssueMessage +" "+ appSettings.ImageMaxWidth + " x "+ appSettings.ImageMaxHeight };

                }

                string uniqueFileName = Guid.NewGuid().ToString() + thumbImage.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                string imagePath= Path.Combine(uploadUrl, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    thumbImage.CopyTo(fileStream);
                }
                result.ImageName = uniqueFileName;
                result.FullImagePath = imagePath;
                result.IsUploaded = true;
                result.IsDefaultThumb = false;
                result.Message = MessageConstants.ImageUploadedSuccessMessage;
            }
            else
            {
                string imagePath = Path.Combine(uploadUrl, DefaultImage);
                result.FullImagePath = imagePath;
                result.ImageName = DefaultImage;
                result.IsUploaded = true;
                result.IsDefaultThumb = true;
                result.Message = MessageConstants.ImageUploadedSuccessMessage;
            }
            return result;
        }
        public bool DeleteThumbImage(string imageName, bool isDefaultImage)
        {
            if (!isDefaultImage)
            {
                string uploadsFolder = env.ContentRootPath + Path.DirectorySeparatorChar.ToString()
             + ImageFolder;
                string imagePath = Path.Combine(uploadsFolder, imageName);
                File.Delete(imagePath);
            }
            return true;
        }
        public async Task<EducationalVideoDto> GetEducationalVideoById(string id)
        {
            return await Task.Run(() => FindEducationalVideoById(id).ToDto());
        }

        private EducationalVideo FindEducationalVideoById(string id)
        {
            return educationalVideoCollection.Find(x => x.Id == id).FirstOrDefault();
        }
        public async void UpdateAsync(string id, EducationalVideoDto educationalVideoDto)
        {
            await Task.Run(() =>
            {
                educationalVideoCollection.ReplaceOne(educationalVideo => educationalVideo.Id == id, educationalVideoDto.ToEntity());
            });
        }
        public async void RemoveAsync(string id)
        {
            await Task.Run(() =>
            {
                educationalVideoCollection.DeleteOne(educationalVideo => educationalVideo.Id == id);
            });
        }
        public async Task<bool> CheckUniqueEducationalVideo(string id, string title,string url)
        {
            var result = !string.IsNullOrEmpty(id)
                ? await Task.Run(() => CheckEducationalVideoWithIdExists(id, title, url).ToDto())
                : await Task.Run(() => CheckEducationalVideoExists(title, url).ToDto());
            return result != null;
        }

        private EducationalVideo CheckEducationalVideoWithIdExists(string id, string title,string url)
        {
            return educationalVideoCollection.Find(x => (x.Title.ToLower() == title.ToLower() || x.VideoPath.ToLower() == url.ToLower())
            && x.IsActive && x.Id != id).FirstOrDefault();
        }

        private EducationalVideo CheckEducationalVideoExists(string title,string url)
        {
            return educationalVideoCollection.Find(x => (x.Title.ToLower() == title.ToLower() || x.VideoPath.ToLower() == url.ToLower())
            && x.IsActive).FirstOrDefault();
        }

        public async Task<ImageUploadResultDto> UploadImage(IFormFile thumbImage)
        {
            try
            {
                return await Task.Run(() =>
                {
                    ImageUploadResultDto result = new ImageUploadResultDto();

                    string uploadsFolder = env.ContentRootPath + Path.DirectorySeparatorChar.ToString()
                      + ImageFolder;
                    string uploadUrl = appSettings.APIPath + Path.DirectorySeparatorChar.ToString()
                     + ImageFolder;
                    if (thumbImage != null)
                    {
                        string _doctype;
                        MIMETypes.TryGetValue(thumbImage.ContentType.ToLower(), out _doctype);
                        if (_doctype == null)
                            return new ImageUploadResultDto() { IsUploaded = false, Message = MessageConstants.ImageUploadedFormatIssueMessage };

                        if (thumbImage.Length >= long.Parse(appSettings.ImageMaxSize))
                            return new ImageUploadResultDto() { IsUploaded = false, Message = MessageConstants.ImageUploadedSizeIssueMessage };

                        using (var image = Image.FromStream(thumbImage.OpenReadStream()))
                        {
                            // use image.Width and image.Height
                            if (image.Width > int.Parse(appSettings.ImageMaxWidth) || image.Height > int.Parse(appSettings.ImageMaxHeight))
                                return new ImageUploadResultDto() { IsUploaded = false, Message = MessageConstants.ImageUploadedWidthIssueMessage + " " + appSettings.ImageMaxWidth + " x " + appSettings.ImageMaxHeight };

                        }

                        string uniqueFileName = Guid.NewGuid().ToString() + thumbImage.FileName;
                        string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                        string imagePath = Path.Combine(uploadUrl, uniqueFileName);
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            thumbImage.CopyTo(fileStream);
                        }
                        result.ImageName = uniqueFileName;
                        result.FullImagePath = imagePath;
                        result.IsUploaded = true;
                        result.IsDefaultThumb = false;
                        result.Message = MessageConstants.ImageUploadedSuccessMessage;
                    }
                    else
                    {
                        return new ImageUploadResultDto() { IsUploaded = false, Message = MessageConstants.NoImageUploadedMessage };

                    }
                    return result;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }
    }
}
