﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.Subjects
{
    public class ConfirmPasswordDto
    {
        [Required]
        public string Uuid { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [Required]
        public string ConfirmPassword { get; set; }
       
        public string PreviousPassword { get; set; }
    }
}
