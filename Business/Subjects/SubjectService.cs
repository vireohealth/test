﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.EducationalVideos;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Helpers;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.Subjects
{
    public class SubjectService : BusinessService<ISubjectService>, ISubjectService
    {
        private const string Admin = "Admin";

        private readonly IMongoCollection<Subject> subjects;
        private readonly IMongoCollection<FeedbackModel> feedback;
        private readonly IMongoCollection<PasswordHistory> passwordHistories;
        private readonly AppSettings appSettings;
        private IHostEnvironment env;

        private const string ImageFolder = "Images";
        Dictionary<string, string> MIMETypes = new Dictionary<string, string>
                                                    {
                                                        { "image/jpeg",".jpg"},
                                                        { "image/gif",".gif"},
                                                        {"image/png",".png"},

                                                    };

        public SubjectService(DatabaseSettings databaseSettings,
            ILogger<ISubjectService> logger,
            IOptions<AppSettings> appSettings, IHostEnvironment env) : base(logger)
        {
            this.appSettings = appSettings.Value;
            this.env = env;
            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);
            passwordHistories = database.GetCollection<PasswordHistory>(databaseSettings.PasswordHistoryCollectionName);
            passwordHistories.Indexes.CreateOneAsync(new CreateIndexModel<PasswordHistory>(nameof(PasswordHistory.SubjectId)));
            subjects = database.GetCollection<Subject>(databaseSettings.SubjectCollectionName);
            subjects.Indexes.CreateOneAsync(new CreateIndexModel<Subject>(nameof(Subject.Email)));
            feedback = database.GetCollection<FeedbackModel>(databaseSettings.FeedbackCollectionName);
            feedback.Indexes.CreateOneAsync(new CreateIndexModel<FeedbackModel>(nameof(FeedbackModel.name)));
        }
        public async Task<PagedList<SubjectDto>> GetAllPaginationAsync(PaginationParams subjectParams)
        {
            return await Task.Run(async () =>
            {
                var filteredSubjects = Builders<Subject>.Filter.Where(subject => subject.IsDeleted == false && subject.UserType != Admin);

                if (!string.IsNullOrEmpty(subjectParams.Search))
                {
                    var search = subjectParams.Search.ToLower();

                    filteredSubjects = Builders<Subject>.Filter
                    .Where(subject => subject.IsDeleted == false && subject.UserType != Admin && (subject.Name.ToLower().Contains(search)
                    || subject.Address.ToLower().Contains(search)));
                }

                var SortColumn = Builders<Subject>.Sort.Descending(x => x.AddedOn);

                if (!string.IsNullOrEmpty(subjectParams.SortCol))
                {
                    if ((!string.IsNullOrEmpty(subjectParams.SortDir)) && (subjectParams.SortDir == MessageConstants.AscendingSortDirection))
                    {
                        SortColumn = Builders<Subject>.Sort.Ascending(subjectParams.SortCol);
                    }
                    else
                    {
                        SortColumn = Builders<Subject>.Sort.Descending(subjectParams.SortCol);
                    }
                }
                var results = await subjects.AggregateByPage(filteredSubjects, SortColumn,
                subjectParams.PageNumber, subjectParams.PageSize);

                return PagedList<SubjectDto>.CreatePageList(results.data.ToDtos(), results.totalPages,
                    subjectParams.PageNumber, subjectParams.PageSize);
            });
        }

        public async Task<List<SubjectDto>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return subjects.Find(subject => subject.IsDeleted == false && subject.UserType != Admin).ToDtos().OrderBy(a => a.Name).ToList();
            });
        }

        public async Task<SubjectDto> ProcessResetPasswordVerification(string email)
        {
            return await Task.Run(() =>
            {
                try
                {
                    var subject = FindSubjectByMail(email);
                    if (subject == null)
                        return null;

                    var filter = Builders<Subject>.Filter.Eq(t => t.Email, email);
                    var updatedSubject = subjects.UpdateOne(
                         filter,
                         Builders<Subject>.Update.Set(
                            t => t.ResetMailSentDate,
                             DateTime.UtcNow)
                         );
                    subject.ResetMailSentDate = DateTime.UtcNow;

                    return subject.ToDto();
                }
                catch (Exception ex)
                {
                    return null;
                }

            });
        }
        private Subject FindSubjectByMail(string email)
        {
            return subjects.Find(x => x.Email == email.Trim() && x.IsDeleted == false).FirstOrDefault();
        }

        public async Task<bool> IsResetLinkExpired(string email)
        {
            return await Task.Run(() =>
            {
                var subject = FindSubjectByMail(email);
                if (DateTime.Now < subject.ResetMailSentDate?.AddMinutes(Convert.ToDouble(appSettings.ExpireTime)))
                    return false;

                return true;
            });
        }

        public async void UpdateAsync(string id, SubjectDto subjectIn)
        {
            await Task.Run(() =>
            {
                subjects.ReplaceOne(subject => subject.Id == id, subjectIn.ToEntity());
            });
        }

        public async void UpdateAsync(string id, Models.Subject subjectIn)
        {
            await Task.Run(() =>
            {
                subjects.ReplaceOne(subject => subject.Id == id, subjectIn);
            });
        }

        #region JWT Authentication Section
        public async Task<SubjectDto> Authenticate_jwt(string username, string password, string ipAddress, string userType)
        {
            return await Task.Run(() =>
            {
                var EncryptedPassword = string.IsNullOrEmpty(password) ? string.Empty : HashedPassword(password);
                var subjectEntity = subjects.Find(x => x.Email == username && x.Password == EncryptedPassword && x.IsActive == true && x.UserType == userType && x.IsDeleted == false).FirstOrDefault();

                if (subjectEntity?.UserType == null)
                {
                    Logger.LogError("Authenticate_jwt : User not found or role not assigned");
                    return null;
                }

                var jwtToken = GenerateJwtToken(subjectEntity);
                var refreshToken = GenerateRefreshToken(ipAddress);
                subjectEntity.RefreshTokens.Add(refreshToken);

                var user = subjectEntity.ToDto();

                UpdateAsync(subjectEntity.Id, user);

                var response = subjectEntity.ToDto();
                response.Token = jwtToken;
                response.RefreshToken = refreshToken.Token;
                response.Password = null;
                var lastFeedbackDate = GetLastFeedbackDate(response.Id);
                response.LastFeedbackDate = lastFeedbackDate.Result;
                return response;
            });
        }

        private string GenerateJwtToken(Subject subject)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, subject.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private RefreshToken GenerateRefreshToken(string ipAddress)
        {
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var randomBytes = new byte[64];
                rngCryptoServiceProvider.GetBytes(randomBytes);
                return new RefreshToken
                {
                    Token = Convert.ToBase64String(randomBytes),
                    Expires = DateTime.UtcNow.AddDays(7),
                    Created = DateTime.UtcNow,
                    CreatedByIp = ipAddress
                };
            }
        }

        public SubjectDto RefreshToken(string token, string ipAddress)
        {
            var subjectDetails = subjects.Find(x => x.RefreshTokens.Any(t => t.Token == token)).FirstOrDefault();

            if (subjectDetails == null) return null;
            var refreshToken = subjectDetails.RefreshTokens.Single(x => x.Token == token);
            if (!refreshToken.IsActive) return null;
            var newRefreshToken = GenerateRefreshToken(ipAddress);
            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;
            refreshToken.ReplacedByToken = newRefreshToken.Token;
            subjectDetails.RefreshTokens.Add(newRefreshToken);

            UpdateAsync(subjectDetails.Id, subjectDetails);

            var jwtToken = GenerateJwtToken(subjectDetails);
            var response = subjectDetails.ToDto();
            response.Token = jwtToken;
            response.RefreshTokens = null;
            response.RefreshToken = newRefreshToken.Token;
            response.Password = null;

            return response;
        }

        public async Task<bool> RevokeToken(string token, string ipAddress)
        {
            var subjectDetails = (await subjects.FindAsync(u => u.RefreshTokens.Any(t => t.Token == token))).FirstOrDefault();
            if (subjectDetails == null) return false;

            var refreshToken = subjectDetails.RefreshTokens.Single(x => x.Token == token);

            if (!refreshToken.IsActive) return false;

            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;

            UpdateAsync(subjectDetails.Id, subjectDetails);

            return true;
        }

        public bool Authenticate(string username, string password)
        {
            var subjectDetails = subjects.Find(x => x.Email == username && x.Password == password).FirstOrDefault();

            if (subjectDetails == null)
                return false;

            return true;
        }
        #endregion

        public async Task<SubjectDto> GetBySubjectId(string id)
        {
            return await Task.Run(() => FindSubject(id).ToDto());
        }

        public async Task<SubjectDto> GetBySubjectByMail(string mailId)
        {
            return await Task.Run(() => FindSubjectByMail(mailId).ToDto());
        }

        private Models.Subject FindSubject(string id)
        {
            return subjects.Find(x => x.Id == id).FirstOrDefault();
        }
        public async Task<bool> CheckFitBitAccountAlreadyIntegrated(string patientId, string fitbitUserId)
        {
            return await Task.Run(() => FindSubjectHasFitBitUserId(patientId, fitbitUserId) != null);
        }
        private Models.Subject FindSubjectHasFitBitUserId(string patientId, string fitbitUserId)
        {
            return subjects.Find(x => x.Id != patientId && x.FitBitAccessToken != null && x.FitBitAccessToken.UserId == fitbitUserId).FirstOrDefault();
        }

        #region Set Password Section
        public async Task<bool> SetPassword(ConfirmPasswordDto resetPasswordDto)
        {
            return await Task.Run(() =>
            {
                var filter = Builders<Subject>.Filter.Eq(t => t.Id, resetPasswordDto.Uuid);
                var EncryptedPassword = HashedPassword(resetPasswordDto.NewPassword);
                try
                {
                    subjects.UpdateOne(
                       filter,
                       Builders<Subject>.Update.Set(
                           t => t.Password,
                           EncryptedPassword)
                       );
                    subjects.UpdateOne(
                       filter,
                       Builders<Subject>.Update.Set(
                           t => t.LastPasswordUpdatedDate,
                           DateTime.UtcNow)
                       );
                    subjects.UpdateOne(
                   filter,
                   Builders<Models.Subject>.Update.Set(
                       t => t.IsMailExpired,
                       false));
                    AddPasswordHistory(resetPasswordDto.Uuid, EncryptedPassword);
                }
                catch (Exception ex)
                {
                    return false;
                }
                return true;
            });
        }
        /// <summary>
        /// This methos is used to validate password 
        /// </summary>
        /// <param name="confirmPasswordDto">confirmPasswordDto</param>
        /// <returns>bool</returns>
        public bool ValidatePasswordRules(ConfirmPasswordDto confirmPasswordDto)
        {           
            try
            {
                if (CheckPasswordContainsAccountName(confirmPasswordDto))
                {
                    return false;
                }
                return true;
            }
            catch(Exception e)
            {
                Logger.LogError(e.Message);
                return false;
            }
        }
        /// <summary>
        /// This method is used to verify password contains account name
        /// </summary>
        /// <param name="ConfirmPasswordDto">ConfirmPasswordDto</param>
        /// <returns>bool</returns>
        private bool CheckPasswordContainsAccountName(ConfirmPasswordDto ConfirmPasswordDto)
        {
            try
            {
                var userDetail= subjects.Find(x => x.Name.ToUpper() == ConfirmPasswordDto.NewPassword.ToUpper() && x.Id == ConfirmPasswordDto.Uuid).FirstOrDefault();                
                if (userDetail != null)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return false;
            }   
        }

        public bool CheckLastThreePasswordMatch(ConfirmPasswordDto resetPasswordDto)
        {
            var EncryptedPassword = HashedPassword(resetPasswordDto.NewPassword);
            try
            {
                var result = passwordHistories.Find(i => i.SubjectId == resetPasswordDto.Uuid && i.IsActive).ToDtos().OrderBy(a => a.AddedOn).ToList();
                if (result.Count > 0)
                {
                    var itemToCheck = result.Take(result.Count >= 3 ? 3 : result.Count).Where(i => i.Password == EncryptedPassword).FirstOrDefault();
                    return itemToCheck != null;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool CheckCurrentPasswordMatch(ConfirmPasswordDto resetPasswordDto)
        {
            var EncryptedPassword = HashedPassword(resetPasswordDto.PreviousPassword);
            try
            {
                var currentPassword = subjects.Find(a => a.Id == resetPasswordDto.Uuid && a.IsActive).ToDtos().ToList();
                var currentPasswordDetails = currentPassword != null ? currentPassword.Where(a => a.Password == EncryptedPassword).FirstOrDefault() : null;

                if (currentPasswordDetails != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return false;
            }
        }
        #endregion

        #region Hashing
        private string HashedPassword(string loggedInPassword)
        {
            try
            {
                var passwordhashedResult = CryptographyHelper.ToSHA256(loggedInPassword);
                return passwordhashedResult;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        #endregion

        public async Task<bool> CheckUniqueSubjectName(string id, string subjectUniqueName)
        {
            var result = !string.IsNullOrEmpty(id)
                ? await Task.Run(() => CheckSubjectExists(id, subjectUniqueName).ToDto())
                : await Task.Run(() => CheckSubjectExists(subjectUniqueName).ToDto());
            return result != null;
        }

        private Models.Subject CheckSubjectExists(string id, string subjectName)
        {
            return subjects.Find(x => x.Name.ToLower() == subjectName.ToLower()
            && x.IsActive && !x.IsDeleted && x.Id != id).FirstOrDefault();
        }

        private Models.Subject CheckSubjectExists(string subjectName)
        {
            return subjects.Find(x => x.Name.ToLower() == subjectName.ToLower()
            && x.IsActive && !x.IsDeleted).FirstOrDefault();
        }
        public async Task<ImageUploadResultDto> UploadProfilePic(IFormFile uploadedImage)
        {
            try
            {
                return await Task.Run(() =>
                {
                    ImageUploadResultDto result = new ImageUploadResultDto();

                    string uploadsFolder = env.ContentRootPath + Path.DirectorySeparatorChar.ToString()
                      + ImageFolder;
                    string uploadUrl = appSettings.APIPath + ImageFolder;
                    if (uploadedImage != null)
                    {
                        string _doctype;
                        MIMETypes.TryGetValue(uploadedImage.ContentType.ToLower(), out _doctype);
                        if (_doctype == null)
                            return new ImageUploadResultDto() { IsUploaded = false, Message = MessageConstants.ProfilePicUploadedFormatIssueMessage };

                        if (uploadedImage.Length >= long.Parse(appSettings.ImageMaxSize))
                            return new ImageUploadResultDto() { IsUploaded = false, Message = MessageConstants.ProfilePicUploadedSizeIssueMessage };

                        using (var image = Image.FromStream(uploadedImage.OpenReadStream()))
                        {
                            if (image.Width > int.Parse(appSettings.ImageMaxWidth) || image.Height > int.Parse(appSettings.ImageMaxHeight))
                                return new ImageUploadResultDto() { IsUploaded = false, Message = MessageConstants.ProfilePicUploadedWidthIssueMessage + " " + appSettings.ImageMaxWidth + " x " + appSettings.ImageMaxHeight };
                        }

                        string uniqueFileName = Guid.NewGuid().ToString() + uploadedImage.FileName;
                        string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                        string imagePath = Path.Combine(uploadUrl, uniqueFileName);
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            uploadedImage.CopyTo(fileStream);
                        }
                        result.ImageName = uniqueFileName;
                        result.FullImagePath = imagePath;
                        result.IsUploaded = true;
                        result.IsDefaultThumb = false;
                        result.Message = MessageConstants.ProfilePicUploadedSuccessMessage;
                    }
                    else
                    {
                        return null;
                    }
                    return result;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }

        public SubjectDto AddSubject(SubjectDto dto)
        {
            var subject = FindSubjectByMail(dto.Email);
            if (subject != null)
            {
                return null;
            }

            var entity = dto.ToEntity();
            entity.AddedOn = DateTime.UtcNow;
            entity.LastUpdatedOn = DateTime.UtcNow;
            entity.IsActive = false;
            entity.IsMailExpired = false;

            subjects.InsertOne(entity);
            return FindSubjectByMail(dto.Email).ToDto();
        }

        private bool AddPasswordHistory(string subjectId, string password)
        {
            try
            {
                PasswordHistory passwordHistory = new PasswordHistory
                {
                    SubjectId = subjectId,
                    Password = password,
                    AddedOn = DateTime.UtcNow,
                    IsActive = true
                };
                passwordHistories.InsertOne(passwordHistory);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SetPasswordForUser(ConfirmPasswordDto confirmPasswordDto)
        {
            var filter = Builders<Models.Subject>.Filter.Eq(t => t.Id, confirmPasswordDto.Uuid);
            var EncryptedPassword = HashedPassword(confirmPasswordDto.NewPassword);
            try
            {
                subjects.UpdateOne(
                    filter,
                    Builders<Models.Subject>.Update.Set(
                        t => t.IsActive,
                        true)
                    );
                subjects.UpdateOne(
                    filter,
                    Builders<Models.Subject>.Update.Set(
                        t => t.ActivatedOn,
                        DateTime.UtcNow)
                    );
                subjects.UpdateOne(
                   filter,
                   Builders<Models.Subject>.Update.Set(
                       t => t.Password,
                       EncryptedPassword)
                   );
                subjects.UpdateOne(
                    filter,
                    Builders<Models.Subject>.Update.Set(
                        t => t.LastPasswordUpdatedDate,
                        DateTime.UtcNow)
                    );
                subjects.UpdateOne(
                   filter,
                   Builders<Models.Subject>.Update.Set(
                       t => t.IsMailExpired,
                       false)
                   );
                AddPasswordHistory(confirmPasswordDto.Uuid, EncryptedPassword);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> CheckUniqueEmail(string id, string email)
        {
            var result = !string.IsNullOrEmpty(id)
                ? await Task.Run(() => CheckEmailIDExists(id, email))
                : await Task.Run(() => CheckEmailExists(email));
            return result;
            // return result != null;
        }

        private bool CheckEmailIDExists(string id, string email)
        {
            var result = subjects.Find(x => x.Email == email
             && x.IsActive == true && x.IsDeleted == false && x.Id == id).FirstOrDefault();
            return result != null ? true : false;
        }

        private bool CheckEmailExists(string email)
        {
            var result = subjects.Find(x => x.Email == email
            && x.IsActive == true && x.IsDeleted == false).FirstOrDefault();
            return result != null ? true : false;
        }

        public async Task<bool> DeleteUserById(string userId)
        {
            var result = await Task.Run(() => DeleteUser(userId).ToString());
            return result != null;
        }

        public bool DeleteUser(string userId)
        {
            var filter = Builders<Subject>.Filter.Eq(t => t.Id, userId);
            try
            {
                subjects.UpdateOne(
                    filter,
                    Builders<Subject>.Update.Set(
                        t => t.IsDeleted,
                        true)
                    );
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public bool DeleteProfilePic(string imageName)
        {
            string uploadsFolder = env.ContentRootPath + Path.DirectorySeparatorChar.ToString()
         + ImageFolder;
            string imagePath = Path.Combine(uploadsFolder, imageName);
            File.Delete(imagePath);

            return true;
        }
        public async Task<bool> RemoveProfileImage(string id)
        {
            return await Task.Run(() =>
            {
                try
                {
                    var filter = Builders<Models.Subject>.Filter.Eq(t => t.Id, id);
                    subjects.UpdateOne(
                  filter,
                  Builders<Models.Subject>.Update.Set(
                      t => t.ProfilePic,
                      string.Empty)
                  );
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.Message);
                    return false;
                }
                return true;
            });
        }
        public PatientDto RegisterPatient(PatientDto dto)
        {
            var subject = FindSubjectByMail(dto.Email);
            if (subject != null)
            {
                return null;
            }
            dto.Password = HashedPassword(dto.ConfirmPassword);
            var entity = dto.ToPatientEntity();
            entity.LastPasswordUpdatedDate = DateTime.UtcNow;
            entity.IsMailExpired = false;
            AddPasswordHistory(dto.Id, dto.Password);
            subjects.InsertOne(entity);
            dto.Password = dto.ConfirmPassword;

            return dto;
        }

        public async Task<DateTime> GetLastFeedbackDate(string subjectId)
        {
            return await Task.Run(() =>
            {
                try
                {
                    var result = feedback.Find(a => a.subject_id ==  subjectId).ToList();
                    var resultLast = result.OrderByDescending(a => a.added_on).FirstOrDefault();
                    var lastFeedbackdate = result.Count>0 ? resultLast.updated_on : DateTime.MinValue;
                    return lastFeedbackdate;
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.Message);
                    return DateTime.MinValue;
                }
            });
        }
    }
}
