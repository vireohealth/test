﻿using System.ComponentModel.DataAnnotations;

namespace VireoIntegrativeProgram.Business.Subjects
{
    public class EmailConfirmPasswordDto
    {
        [Required]
        public string Uuid { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [Required]
        public string ConfirmPassword { get; set; }
    }
}
