﻿using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using VireoIntegrativeProgram.Business.FitBitTokenManager;

namespace VireoIntegrativeProgram.Business.Subjects
{
    public class SubjectDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [Required(ErrorMessage = "The name is required.")]
        public string Name { get; set; }
       
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Address is required")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Phone/Cell is required")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Gender is required.")]
        public string Gender { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string Token { get; set; }
        public DateTime CreatedOn { get; set; }
        public List<RefreshTokenDto> RefreshTokens { get; set; }
        [JsonIgnore] // refresh token is returned in http only cookie
        public string RefreshToken { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public DateTime ActivatedOn { get; set; }
        public string UserType { get; set; }

        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }

        [Required(ErrorMessage = "Postal code is required")]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "State is required")]
        public string StateCode { get; set; }

        [Required(ErrorMessage = "State is required")]
        public string StateName { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ResetMailSentDate { get; set; }
        public bool? IsMailExpired { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? LastPasswordUpdatedDate { get; set; }

        public bool IsDeleted { get; set; }

        public string UserStatus { get; set; }
        public AccessTokenDto FitBitAccessToken { get; set; }

        public IFormFile ProfilePic { get; set; }
        public string ProfilePicName { get; set; }

        public string Notes { get; set; }
        public string MyPreferedDispenseries { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? LastFeedbackDate { get; set; }

        public SubjectDto()
        {
            IsActive = false;
            IsDeleted = false;
        }
    }
}
