﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.EducationalVideos;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Helpers;

namespace VireoIntegrativeProgram.Business.Subjects
{
    public interface ISubjectService
    {
        Task<PagedList<SubjectDto>> GetAllPaginationAsync(PaginationParams subjectParams);
        Task<List<SubjectDto>> GetAllAsync();
        void UpdateAsync(string id, SubjectDto subjectIn);
        void UpdateAsync(string id, Models.Subject subjectIn);
        Task<SubjectDto> Authenticate_jwt(string username, string password, string ipAddress, string userType);
        SubjectDto RefreshToken(string token, string ipAddress);
        Task<bool> RevokeToken(string token, string ipAddress);
        bool Authenticate(string username, string password);
        Task<SubjectDto> GetBySubjectId(string id);
        Task<bool> CheckUniqueSubjectName(string id, string subjectUniqueName);
        Task<SubjectDto> ProcessResetPasswordVerification(string email);
        Task<bool> IsResetLinkExpired(string email);
        Task<bool> SetPassword(ConfirmPasswordDto resetPasswordDto);
        SubjectDto AddSubject(SubjectDto dto);
        bool SetPasswordForUser(ConfirmPasswordDto confirmPasswordDto);
        Task<bool> CheckUniqueEmail(string id, string email);
        Task<bool> DeleteUserById(string userId);
        PatientDto RegisterPatient(PatientDto dto);
        Task<SubjectDto> GetBySubjectByMail(string mailId);
        Task<bool> CheckFitBitAccountAlreadyIntegrated(string patientId, string fitbitUserId);
        Task<ImageUploadResultDto> UploadProfilePic(IFormFile uploadedImage);
        bool DeleteProfilePic(string imageName);
        Task<bool> RemoveProfileImage(string id);
        bool CheckLastThreePasswordMatch(ConfirmPasswordDto resetPasswordDto);
        bool CheckCurrentPasswordMatch(ConfirmPasswordDto resetPasswordDto);
        bool ValidatePasswordRules(ConfirmPasswordDto resetPasswordDto);
       
    }
}
