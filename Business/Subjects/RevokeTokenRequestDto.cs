﻿
namespace VireoIntegrativeProgram.Business.Subjects
{
    public class RevokeTokenRequestDto
    {
        public string Token { get; set; }
    }
}
