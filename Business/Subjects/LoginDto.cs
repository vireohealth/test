﻿using System.ComponentModel.DataAnnotations;

namespace VireoIntegrativeProgram.Business.Subjects
{
    public class LoginDto
    {
        [Required(ErrorMessage = "Please enter user name")]
        [StringLength(100, ErrorMessage = "Name should not be greater than 100 characters")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter password")]
        public string Password { get; set; }

        public string UserType { get; set; }
    }
}
