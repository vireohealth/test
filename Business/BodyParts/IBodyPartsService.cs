﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Helpers;

namespace VireoIntegrativeProgram.Business.BodyParts
{
    public interface IBodyPartsService
    {
        Task<List<BodyPartsDto>> GetAllBodyParts();
        Task<BodyPartImageUploadDto> UploadAnImage(IFormFile thumbImage);
        Task<BodyPartsDto> AddBodyParts(BodyPartsDto dto);
        Task<BodyPartsDto> GetBodyPartById(string id);
        bool DeleteBodyPartImage(string imageName);
        void RemoveAsync(string id);
        Task<bool> CheckUniqueBodyPart(string id, string bodyPartName);
        Task<bool> DeleteImage(string imageName);
        void UpdateAsync(string id, BodyPartUpsertDto bodyPartDto);
        Task<PagedList<BodyPartsDto>> GetAllPaginationAsync(PaginationParams bodyPartsDetails);
    }
}
