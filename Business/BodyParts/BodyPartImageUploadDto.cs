﻿namespace VireoIntegrativeProgram.Business.BodyParts
{
    public class BodyPartImageUploadDto
    {
        public string Message { get; set; }
        public bool IsUploaded { get; set; }
        public string FullImagePath { get; set; }
        public string ImageName { get; set; }
        public bool IsDefaultImage { get; set; }
    }
}
