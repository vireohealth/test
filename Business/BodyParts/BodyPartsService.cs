﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;
using System.IO;
using VireoIntegrativeProgram.Business.EducationalVideos;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using VireoIntegrativeProgram.Constants;
using System.Drawing;
using System;
using VireoIntegrativeProgram.Helpers;

namespace VireoIntegrativeProgram.Business.BodyParts
{
    public class BodyPartsService : BusinessService<IBodyPartsService>, IBodyPartsService
    {
        private readonly IMongoCollection<BodyPartsDetails> bodyPartsCollection;

        private readonly AppSettings appSettings;
        private const string ImageFolder = "BodyPartsImages";
        private IHostEnvironment env;
        Dictionary<string, string> MIMETypes = new Dictionary<string, string>
                                                    {
                                                        { "image/jpeg",".jpg"},
                                                        { "image/gif",".gif"},
                                                        {"image/png",".png"},
                                                    };

        public BodyPartsService(DatabaseSettings databaseSettings,
            ILogger<IBodyPartsService> logger,
            IOptions<AppSettings> appSettings, IHostEnvironment env) : base(logger)
        {
            this.appSettings = appSettings.Value;
            this.env = env;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);
            bodyPartsCollection = database.GetCollection<BodyPartsDetails>(databaseSettings.BodyPartsCollectionName);
            bodyPartsCollection.Indexes.CreateOneAsync(new CreateIndexModel<BodyPartsDetails>(nameof(BodyPartsDetails.BodyPartName)));
        }

        public async Task<List<BodyPartsDto>> GetAllBodyParts()
        {
            try
            {
                return await Task.Run(() =>
                {
                    string imagePathUrl = appSettings.APIPath + Path.DirectorySeparatorChar.ToString() + ImageFolder;
                    var bodyPartList = bodyPartsCollection.Find(parts => true).ToDtos().OrderBy(a => a.BodyPartName).ToList();
                    foreach (var result in bodyPartList)
                    {
                        result.BodyPartImageName = Path.Combine(imagePathUrl, result.BodyPartImageName);
                    }
                    return bodyPartList;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<BodyPartImageUploadDto> UploadAnImage(IFormFile uploadedImage)
        {
            try
            {
                return await Task.Run(() =>
                {
                    BodyPartImageUploadDto result = new BodyPartImageUploadDto();

                    string uploadsFolder = env.ContentRootPath + Path.DirectorySeparatorChar.ToString()
                      + ImageFolder;
                    string uploadUrl = appSettings.APIPath + Path.DirectorySeparatorChar.ToString()
                     + ImageFolder;
                    if (uploadedImage != null)
                    {
                        string _doctype;
                        MIMETypes.TryGetValue(uploadedImage.ContentType.ToLower(), out _doctype);
                        if (_doctype == null)
                            return new BodyPartImageUploadDto() { IsUploaded = false, Message = MessageConstants.BodyPartsImageUploadedFormatIssueMessage };

                        if (uploadedImage.Length >= long.Parse(appSettings.ImageMaxSize))
                            return new BodyPartImageUploadDto() { IsUploaded = false, Message = MessageConstants.BodyPartsImageUploadedSizeIssueMessage };

                        using (var image = Image.FromStream(uploadedImage.OpenReadStream()))
                        {
                            if (image.Width > int.Parse(appSettings.ImageMaxWidth) || image.Height > int.Parse(appSettings.ImageMaxHeight))
                                return new BodyPartImageUploadDto() { IsUploaded = false, Message = MessageConstants.BodyPartsImageUploadedWidthIssueMessage + " " + appSettings.ImageMaxWidth + " x " + appSettings.ImageMaxHeight };
                        }

                        string uniqueFileName = Guid.NewGuid().ToString() + uploadedImage.FileName;
                        string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                        string imagePath = Path.Combine(uploadUrl, uniqueFileName);
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            uploadedImage.CopyTo(fileStream);
                        }
                        result.ImageName = uniqueFileName;
                        result.FullImagePath = imagePath;
                        result.IsUploaded = true;
                        result.IsDefaultImage = false;
                        result.Message = MessageConstants.BodyPartsImageUploadedSuccessMessage;
                    }
                    else
                    {
                        return null;
                    }
                    return result;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<BodyPartsDto> AddBodyParts(BodyPartsDto dto)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var bodyPartDetails = FindBodyPartByName(dto.BodyPartName);
                    if (bodyPartDetails != null)
                    {
                        return null;
                    }

                    var entity = dto.ToEntity();
                    entity.BodyPartName = entity.BodyPartName.ToLower();
                    entity.BodyPartType = entity.BodyPartType.ToLower();
                    entity.CreatedOn = DateTime.UtcNow;
                    entity.LastUpdatedOn = DateTime.UtcNow;
                    entity.IsActive = true;

                    bodyPartsCollection.InsertOne(entity);
                    return FindBodyPartByName(dto.BodyPartName).ToDto();
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }

        private BodyPartsDetails FindBodyPartByName(string BodyPartName)
        {
            try
            {
                return bodyPartsCollection.Find(x => x.BodyPartName.ToLower() == BodyPartName.ToLower() && x.IsActive == true).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<BodyPartsDto> GetBodyPartById(string id)
        {
            return await Task.Run(() => FindBodyPartById(id).ToDto());
        }

        private BodyPartsDetails FindBodyPartById(string id)
        {
            return bodyPartsCollection.Find(x => x.Id == id).FirstOrDefault();
        }

        public async void RemoveAsync(string id)
        {
            await Task.Run(() =>
            {
                bodyPartsCollection.DeleteOne(x => x.Id == id);
            });
        }

        public bool DeleteBodyPartImage(string imageName)
        {
            string uploadsFolder = env.ContentRootPath + Path.DirectorySeparatorChar.ToString()
         + ImageFolder;
            string imagePath = Path.Combine(uploadsFolder, imageName);
            File.Delete(imagePath);

            return true;
        }

        public async Task<bool> CheckUniqueBodyPart(string id, string bodyPartName)
        {
            var result = !string.IsNullOrEmpty(id)
                ? await Task.Run(() => CheckBodyPartNameWithIdExists(id, bodyPartName).ToDto())
                : await Task.Run(() => CheckBodyPartNameExists(bodyPartName).ToDto());
            return result != null;
        }

        private BodyPartsDetails CheckBodyPartNameWithIdExists(string id, string bodyPartName)
        {
            return bodyPartsCollection.Find(x => x.BodyPartName.ToLower() == bodyPartName.ToLower()
            && x.IsActive && x.Id != id).FirstOrDefault();
        }

        private BodyPartsDetails CheckBodyPartNameExists(string bodyPartName)
        {
            return bodyPartsCollection.Find(x => x.BodyPartName.ToLower() == bodyPartName.ToLower()
            && x.IsActive).FirstOrDefault();
        }

        public async Task<bool> DeleteImage(string imageName)
        {
            try
            {
                string uploadsFolder = env.ContentRootPath + Path.DirectorySeparatorChar.ToString()
                         + ImageFolder;
                string imagePath = Path.Combine(uploadsFolder, imageName);
                File.Delete(imagePath);
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return false;
            }
        }

        public async void UpdateAsync(string id, BodyPartUpsertDto bodyPartDto)
        {
            await Task.Run(() =>
            {
                bodyPartDto.BodyPartName = bodyPartDto.BodyPartName.ToLower();
                bodyPartDto.BodyPartType = bodyPartDto.BodyPartType.ToLower();
                bodyPartsCollection.ReplaceOne(bodyPart => bodyPart.Id == id, bodyPartDto.ToUpdateEntity());
            });
        }

        public async Task<PagedList<BodyPartsDto>> GetAllPaginationAsync(PaginationParams bodyPartsDetails)
        {
            return await Task.Run(async () =>
            {
                var filteredBodyParts = Builders<BodyPartsDetails>.Filter.Empty;

                if (!string.IsNullOrEmpty(bodyPartsDetails.Search))
                {
                    filteredBodyParts = Builders<BodyPartsDetails>.Filter
                    .Where(bodyDetails => bodyDetails.BodyPartName.ToLower().Contains(bodyPartsDetails.Search.ToLower()));
                }

                var SortColumn = Builders<BodyPartsDetails>.Sort.Descending(x => x.CreatedOn);

                if (!string.IsNullOrEmpty(bodyPartsDetails.SortCol))
                {
                    if ((!string.IsNullOrEmpty(bodyPartsDetails.SortDir)) && (bodyPartsDetails.SortDir == MessageConstants.AscendingSortDirection))
                    {
                        SortColumn = Builders<BodyPartsDetails>.Sort.Ascending(bodyPartsDetails.SortCol);
                    }
                    else
                    {
                        SortColumn = Builders<BodyPartsDetails>.Sort.Descending(bodyPartsDetails.SortCol);
                    }
                }
                var results = await bodyPartsCollection.AggregateByPage(filteredBodyParts, SortColumn,
                bodyPartsDetails.PageNumber, bodyPartsDetails.PageSize);

                return PagedList<BodyPartsDto>.CreatePageList(results.data.ToDtos(), results.totalPages,
                    bodyPartsDetails.PageNumber, bodyPartsDetails.PageSize);
            });
        }
    }
}
