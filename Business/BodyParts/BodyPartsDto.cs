﻿using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace VireoIntegrativeProgram.Business.BodyParts
{
    public class BodyPartsDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [Required(ErrorMessage = "The body part name is required.")]
        public string BodyPartName { get; set; }

        [Required(ErrorMessage = "The body part type is required.")]
        public string BodyPartType { get; set; }
        public string BodyPartImageName { get; set; }
        public bool IsActive { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CreatedOn { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime LastUpdatedOn { get; set; }
        [Required(ErrorMessage = "The body part image is required.")]
        public IFormFile BodyPartImage { get; set; }
        public BodyPartsDto()
        {
            IsActive = true;
        }
    }
}
