﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.SurveyTypes
{
    public interface ISurveyTypesService
    {
        Task<List<SurveyTypesDto>> GetAllAsync();
    }
}
