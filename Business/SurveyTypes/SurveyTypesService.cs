﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.SurveyTypes
{
    public class SurveyTypesService : BusinessService<ISurveyTypesService>, ISurveyTypesService
    {
        private readonly IMongoCollection<SurveyType> surveyTypes;
        private readonly AppSettings appSettings;

        public SurveyTypesService(DatabaseSettings databaseSettings,
          ILogger<ISurveyTypesService> logger,
          IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);

            surveyTypes = database.GetCollection<SurveyType>(databaseSettings.SurveyTypeCollectionName);
            surveyTypes.Indexes.CreateOneAsync(new CreateIndexModel<SurveyType>(nameof(SurveyType.Name)));
        }

        public async Task<List<SurveyTypesDto>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return surveyTypes.Find(survey => true).ToDtos().OrderBy(a => a.Name).ToList();
            });
        }
    }
}
