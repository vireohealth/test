﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace VireoIntegrativeProgram.Business.SurveyTypes
{
    public class SurveyTypesDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public SurveyTypesDto()
        {
            IsActive = true;
        }
    }
}
