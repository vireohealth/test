﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.FormControls
{
    public interface IFormControlService
    {
        Task<List<FormControlDto>> GetAllAsync();
    }
}
