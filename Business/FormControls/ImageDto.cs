﻿using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace VireoIntegrativeProgram.Business.FormControls
{
    public class ImageDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Path { get; set; }
        public string ImageName { get; set; }
        public bool IsActive { get; set; }
        [Required(ErrorMessage = "The form image is required.")]
        public IFormFile FormImage { get; set; }
    }
}
