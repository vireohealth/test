﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.FormControls
{
    public class FormControlService : BusinessService<IFormControlService>, IFormControlService
    {
        private readonly IMongoCollection<FormControl> formControls;
        private readonly AppSettings appSettings;

        public FormControlService(DatabaseSettings databaseSettings,
            ILogger<IFormControlService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);

            formControls = database.GetCollection<FormControl>(databaseSettings.FormControlCollectionName);
            formControls.Indexes.CreateOneAsync(new CreateIndexModel<FormControl>(nameof(FormControl.Name)));
        }

        public async Task<List<FormControlDto>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return formControls.Find(formDetails => true).ToDtos().OrderBy(a => a.Name).ToList();
            });
        }
    }
}
