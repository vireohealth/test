﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace VireoIntegrativeProgram.Business.FormControls
{
    public class FormControlDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Properties { get; set; }
        public bool IsActive { get; set; }

        public FormControlDto()
        {
            IsActive = true;
        }
    }
}
