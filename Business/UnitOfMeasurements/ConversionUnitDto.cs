﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace VireoIntegrativeProgram.Business.UnitOfMeasurements
{
    public class ConversionUnitDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Unit { get; set; }
        public decimal Value { get; set; }
    }
}
