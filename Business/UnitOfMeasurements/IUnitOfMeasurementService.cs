﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.UnitOfMeasurements
{
    public interface IUnitOfMeasurementService
    {
        Task<List<UnitOfMeasurementDTO>> GetAllAsync();
    }
}
