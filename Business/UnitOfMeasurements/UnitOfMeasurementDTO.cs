﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VireoIntegrativeProgram.Business.UnitOfMeasurements
{
    public class UnitOfMeasurementDTO 
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [Required(ErrorMessage = "The Unit is required.")]
        [MaxLength(32)]
        public string Unit { get; set; }
        [Required(ErrorMessage = "The Description is required.")]
        public string Description { get; set; }
        [Required(ErrorMessage = "The System is required.")]
        public MeasurementsTypeDto MeasurementSystem { get; set; }
        public bool IsActive { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public List<ConversionUnitDto> ConversionUnits { get; set; }
        public UnitOfMeasurementDTO()
        {
            IsActive = true;
        }
    }
}
