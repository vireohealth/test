﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.UnitOfMeasurements
{
    public class UnitOfMeasurementService : BusinessService<IUnitOfMeasurementService>, IUnitOfMeasurementService
    {
        private readonly IMongoCollection<UnitOfMeasure> unitOfMeasureCollection;
        private readonly AppSettings appSettings;

        public UnitOfMeasurementService(DatabaseSettings databaseSettings,
            ILogger<IUnitOfMeasurementService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;
            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);
            unitOfMeasureCollection = database.GetCollection<UnitOfMeasure>(databaseSettings.UnitOfMeasureCollectionName);
            unitOfMeasureCollection.Indexes.CreateOneAsync(new CreateIndexModel<UnitOfMeasure>(nameof(UnitOfMeasure.Unit)));
        }

        public async Task<List<UnitOfMeasurementDTO>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return unitOfMeasureCollection.Find(measure => true).ToDtos().OrderBy(a => a.Unit).ToList();
            });
        }
    }
}
