﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.Countries
{
    public class CountryService : BusinessService<ICountryService>, ICountryService
    {
        private readonly IMongoCollection<Country> countries;

        private readonly AppSettings appSettings;
        public CountryService(DatabaseSettings databaseSettings,
            ILogger<ICountryService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;
            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);
            countries = database.GetCollection<Country>(databaseSettings.CountryCollectionName);
            countries.Indexes.CreateOneAsync(new CreateIndexModel<Country>(nameof(Country.Name)));
        }

        public async Task<List<CountryDto>> GetStatesByCountryCodeAsync(string code)
        {
            return await Task.Run(() =>
            {
                return countries.Find(country => country.Code == code).ToDtos();
            });
        }
    }
}
