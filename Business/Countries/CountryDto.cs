﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace VireoIntegrativeProgram.Business.Countries
{
    public class CountryDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("code2")]
        public string Code { get; set; }

        [BsonElement("states")]
        public List<StateDto> States { get; set; }
        
    }
}
