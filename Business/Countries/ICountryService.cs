﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.Countries
{
    public interface ICountryService
    {
        Task<List<CountryDto>> GetStatesByCountryCodeAsync(string code);
    }
}
