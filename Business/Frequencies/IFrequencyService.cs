﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.Frequencies
{
    public interface IFrequencyService
    {
        Task<List<FrequencyDto>> GetAllAsync();
    }
}
