﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.Frequencies
{
    public class FrequencyService : BusinessService<IFrequencyService>, IFrequencyService
    {
        private readonly IMongoCollection<Frequency> frequencyCollection;
        private readonly AppSettings appSettings;

        public FrequencyService(DatabaseSettings databaseSettings,
            ILogger<IFrequencyService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);

            frequencyCollection = database.GetCollection<Frequency>(databaseSettings.FrequencyCollectionName);
            frequencyCollection.Indexes.CreateOneAsync(new CreateIndexModel<Frequency>(nameof(Frequency.Name)));
        }

        public async Task<List<FrequencyDto>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return frequencyCollection.Find(frequency => true).ToDtos().OrderBy(a => a.Name).ToList();
            });
        }
    }
}
