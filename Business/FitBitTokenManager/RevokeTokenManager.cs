﻿using Fitbit.Api.Portable;
using Fitbit.Api.Portable.OAuth2;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;

namespace VireoIntegrativeProgram.Business.FitBitTokenManager
{
    public class RevokeTokenManager : BusinessService<IRevokeTokenManager>, IRevokeTokenManager
    {
        public RevokeTokenManager(ILogger<IRevokeTokenManager> logger) : base(logger)
        {
        }
        private static string FitbitOauthRevokePostUrl => "https://api.fitbit.com/oauth2/revoke";
        public async Task<string> RevokeTokenAsync(FitbitClient client)
        {
            string postUrl = FitbitOauthRevokePostUrl;

            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("token", client.AccessToken.RefreshToken),
            });

            HttpClient httpClient;
            if (client.HttpClient == null)
            {
                httpClient = new HttpClient();
            }
            else
            {
                httpClient = client.HttpClient;
            }

            var clientIdConcatSecret = OAuth2Helper.Base64Encode(client.AppCredentials.ClientId + ":" + client.AppCredentials.ClientSecret);
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", clientIdConcatSecret);

            HttpResponseMessage response = await httpClient.PostAsync(postUrl, content);
            return await response.Content.ReadAsStringAsync();
        }
    }
}
