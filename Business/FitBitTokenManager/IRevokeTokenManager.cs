﻿using Fitbit.Api.Portable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.FitBitTokenManager
{
    public interface IRevokeTokenManager
    {
        Task<string> RevokeTokenAsync(FitbitClient client);
    }
}
