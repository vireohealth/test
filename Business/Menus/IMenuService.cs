﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.Menus
{
    public interface IMenuService
    {
        Task<List<MenuDto>> GetAllAsync(string userType);
    }
}
