﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.Menus
{
    public class MenuService : BusinessService<IMenuService>, IMenuService
    {
        private readonly IMongoCollection<Menu> menuCollection;

        private readonly AppSettings appSettings;
        public MenuService(DatabaseSettings databaseSettings,
            ILogger<IMenuService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);

            menuCollection = database.GetCollection<Menu>(databaseSettings.MenuCollectionName);

            menuCollection.Indexes.CreateOneAsync(new CreateIndexModel<Menu>(nameof(Menu.Name)));
        }

        public async Task<List<MenuDto>> GetAllAsync(string userType)
        {
            return await Task.Run(() =>
            {
                return menuCollection.Find(menu => menu.UserType.ToLower()==userType.ToLower() && menu.IsActive).SortBy(menu => menu.OrderNumber).ToDtos();
            });
        }
    }
}
