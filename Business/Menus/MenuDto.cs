﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace VireoIntegrativeProgram.Business.Menus
{
    public class MenuDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string UserType { get; set; }
        public int OrderNumber { get; set; }
        public string Action { get; set; }
        public string Icon { get; set; }
        public bool IsActive { get; set; }

        public MenuDto()
        {
            IsActive = true;
        }
    }
}
