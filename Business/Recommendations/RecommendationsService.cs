﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Helpers;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.Recommendations
{
    public class RecommendationsService : BusinessService<IRecommendationsService>, IRecommendationsService
    {
        private readonly IMongoCollection<Recommendation> recommendationCollection;
        private readonly AppSettings appSettings;

        public RecommendationsService(DatabaseSettings databaseSettings,
            ILogger<IRecommendationsService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);

            recommendationCollection = database.GetCollection<Recommendation>(databaseSettings.RecommendationCollectionName);
            recommendationCollection.Indexes.CreateOneAsync(new CreateIndexModel<Recommendation>(nameof(Recommendation.Name)));
        }

        public async Task<List<RecommendationDto>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return recommendationCollection.Find(recommment => true).ToDtos().OrderBy(a => a.Name).ToList();
            });
        }

        public RecommendationDto AddRecommendation(RecommendationDto RecommendationDto)
        {
            var Recommendation = FindRecommendationByName(RecommendationDto.Name);
            if (Recommendation != null)
            {
                return null;
            }

            var entity = RecommendationDto.ToEntity();
            entity.CreatedOn = entity.LastUpdatedOn = DateTime.UtcNow;
            entity.IsActive = true;

            recommendationCollection.InsertOne(entity);
            return FindRecommendationByName(RecommendationDto.Name).ToDto();
        }
        private Recommendation FindRecommendationByName(string name)
        {
            return recommendationCollection.Find(x => x.Name.ToLower() == name.ToLower() && x.IsActive == true).FirstOrDefault();
        }

        public async Task<bool> CheckUniqueRecommendation(string id, string recommendationName)
        {
            var result = !string.IsNullOrEmpty(id)
                ? await Task.Run(() => CheckRecommendationWithIdExists(id, recommendationName).ToDto())
                : await Task.Run(() => CheckRecommendationWithIdExists(recommendationName).ToDto());
            return result != null;
        }

        private Models.Recommendation CheckRecommendationWithIdExists(string id, string recommendationName)
        {
            return recommendationCollection.Find(x => x.Name.ToLower() == recommendationName.ToLower()
            && x.IsActive && x.Id != id).FirstOrDefault();
        }

        private Models.Recommendation CheckRecommendationWithIdExists(string recommendationName)
        {
            return recommendationCollection.Find(x => x.Name.ToLower() == recommendationName.ToLower()
            && x.IsActive).FirstOrDefault();
        }

        public async Task<RecommendationDto> GetRecommendationById(string id)
        {
            return await Task.Run(() => FindRecommendationById(id).ToDto());
        }
        private Recommendation FindRecommendationById(string id)
        {
            return recommendationCollection.Find(x => x.Id == id).FirstOrDefault();
        }
        public async void UpdateAsync(string id, RecommendationDto recommendationDetails)
        {
            await Task.Run(() =>
            {
                recommendationCollection.ReplaceOne(recommendation => recommendation.Id == id, recommendationDetails.ToEntity());
            });
        }
        public async void RemoveAsync(string id)
        {
            await Task.Run(() =>
            {
                recommendationCollection.DeleteOne(recommendation => recommendation.Id == id);
            });
        }

        public async Task<PagedList<RecommendationDto>> GetAllPaginationAsync(PaginationParams recommendationParams)
        {
            return await Task.Run(async () =>
            {
                var filteredRecommendation = Builders<Recommendation>.Filter.Where(recommend => recommend.IsActive == true);

                if (!string.IsNullOrEmpty(recommendationParams.Search))
                {
                    var search = recommendationParams.Search.ToLower();

                    filteredRecommendation = Builders<Recommendation>.Filter
                    .Where(recommend => recommend.IsActive == true  && (recommend.Name.ToLower().Contains(search)));
                }

                var SortColumn = Builders<Recommendation>.Sort.Descending(x => x.CreatedOn);

                if (!string.IsNullOrEmpty(recommendationParams.SortCol))
                {
                    if ((!string.IsNullOrEmpty(recommendationParams.SortDir)) && (recommendationParams.SortDir == MessageConstants.AscendingSortDirection))
                    {
                        SortColumn = Builders<Recommendation>.Sort.Ascending(recommendationParams.SortCol);
                    }
                    else
                    {
                        SortColumn = Builders<Recommendation>.Sort.Descending(recommendationParams.SortCol);
                    }
                }
                var results = await recommendationCollection.AggregateByPage(filteredRecommendation, SortColumn,
                recommendationParams.PageNumber, recommendationParams.PageSize);

                return PagedList<RecommendationDto>.CreatePageList(results.data.ToDtos(), results.totalPages,
                    recommendationParams.PageNumber, recommendationParams.PageSize);
            });
        }

        public async Task<string> GetRecommendationBasedOnSubject(string subjectId)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var result = recommendationCollection.Find(a => a.IsActive).FirstOrDefault();
                    if (result != null)
                    {
                        return result.Name;
                    }
                    return null;
                });                    
            }
            catch(Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }
    }
}
