﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Helpers;

namespace VireoIntegrativeProgram.Business.Recommendations
{
    public interface IRecommendationsService
    {
        Task<List<RecommendationDto>> GetAllAsync();
        RecommendationDto AddRecommendation(RecommendationDto surveyDto);
        Task<bool> CheckUniqueRecommendation(string id, string recommendationName);
        Task<RecommendationDto> GetRecommendationById(string id);
        void UpdateAsync(string id, RecommendationDto recommendationDetails);
        void RemoveAsync(string id);
        Task<PagedList<RecommendationDto>> GetAllPaginationAsync(PaginationParams recommendationParams);
        Task<string> GetRecommendationBasedOnSubject(string subjectId);
    }
}
