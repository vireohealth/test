﻿namespace VireoIntegrativeProgram.Business.PersonalInsights
{
    public class PersonalInsightDto
    {
        public string LogCount { get; set; }
        public string JournalEntryCount { get; set; }
    }
}