﻿namespace VireoIntegrativeProgram.Business.PersonalInsights
{
    public class EffectOfSleepPainDto
    {
        public string Excellent { get; set; }
        public string Adequate { get; set; }
        public string Good { get; set; }
        public string Disturbed { get; set; }
        public string NoSleep { get; set; }

    }
}
