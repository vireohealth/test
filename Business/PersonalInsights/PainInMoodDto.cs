﻿
namespace VireoIntegrativeProgram.Business.PersonalInsights
{
    public class PainInMoodDto
    {
        public string ImagePath { get; set; }
        public string MoodName { get; set; }
    }
}