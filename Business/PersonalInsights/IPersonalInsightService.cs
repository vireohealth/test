﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.PersonalInsights
{
    public interface IPersonalInsightService
    {
        Task<PersonalInsightDto> GetAllPersonalInsights(string subjectId, string frequency);
        Task<List<KeyValuePair<string, int>>> GetEffectOfPainPerSubject(string subjectId, string frequency);
        Task<PainInMoodDto> GetPainMoods(string subjectId, string frequency);
        Task<List<string>> GetEffectOfPainDates(string subjectId, string frequency, string sleepType);
    }
}