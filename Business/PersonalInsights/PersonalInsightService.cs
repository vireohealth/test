﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Common.Enums;
using VireoIntegrativeProgram.Models;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace VireoIntegrativeProgram.Business.PersonalInsights
{
    public class PersonalInsightService : BusinessService<IPersonalInsightService>, IPersonalInsightService
    {
        private readonly IMongoCollection<Models.SubjectLogs> logCollection;
        private readonly IMongoCollection<JournalEntry> journalEntryCollection;
        private readonly IMongoCollection<MedicationJournalEntry> medicationJournalCollection;

        private const string ImageFolder = "Images";

        private readonly AppSettings appSettings;

        public PersonalInsightService(DatabaseSettings databaseSettings,
            ILogger<IPersonalInsightService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);
            logCollection = database.GetCollection<Models.SubjectLogs>(databaseSettings.LogCollectionName);
            logCollection.Indexes.CreateOneAsync(new CreateIndexModel<Models.SubjectLogs>(nameof(Models.SubjectLogs.Id)));
            journalEntryCollection = database.GetCollection<JournalEntry>(databaseSettings.JournalEntryCollectionName);
            journalEntryCollection.Indexes.CreateOneAsync(new CreateIndexModel<JournalEntry>(nameof(JournalEntry.Title)));
            medicationJournalCollection = database.GetCollection<MedicationJournalEntry>(databaseSettings.MedicationJournalCollectionName);
            medicationJournalCollection.Indexes.CreateOneAsync(new CreateIndexModel<MedicationJournalEntry>(nameof(MedicationJournalEntry.Time)));
        }

        private DateConverter ConvertDateByFrequency(string frequency)
        {
            try
            {
                DateConverter dateConverter = new DateConverter();
                if (frequency == DateFrequency.Today)
                {
                    dateConverter.StartDate = DateTime.Today.Date;
                    dateConverter.EndDate = DateTime.Today.Date;
                }
                else if (frequency == DateFrequency.Weekly)
                {
                    dateConverter.StartDate = DateTime.Today.AddDays(-6);
                    dateConverter.EndDate = DateTime.Today.Date; 
                }
                else if (frequency == DateFrequency.Monthly)
                {
                    dateConverter.StartDate = DateTime.Today.AddDays(-29);
                    dateConverter.EndDate = DateTime.Today.Date; 
                }
                return dateConverter;
            }
            catch (Exception ex)
            {
                Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<PersonalInsightDto> GetAllPersonalInsights(string subjectId, string frequency)
        {
            try
            {
                return await Task.Run(() =>
                {
                    PersonalInsightDto personalDetails = new PersonalInsightDto();
                    var result = ConvertDateByFrequency(frequency);
                    if (result.StartDate != DateTime.MinValue && result.EndDate != DateTime.MinValue)
                    {
                        personalDetails.LogCount = logCollection.Find(a => a.IsActive && a.Subject.Id == subjectId && (a.DateOfLog >= result.StartDate && a.DateOfLog <= result.EndDate)).CountDocuments().ToString();
                        var JournalEntryCount = journalEntryCollection.Find(a => a.IsActive && a.Patient.Id == subjectId && (a.JournalDate >= result.StartDate && a.JournalDate <= result.EndDate)).CountDocuments();
                        var MedicationJournalEntryCount = medicationJournalCollection.Find(a => a.IsActive && a.Patient.Id == subjectId && (a.MedicationJournalDate >= result.StartDate && a.MedicationJournalDate <= result.EndDate)).CountDocuments();
                        personalDetails.JournalEntryCount =Convert.ToString(JournalEntryCount + MedicationJournalEntryCount);
                    }
                    return personalDetails;
                });
            }
            catch (Exception ex)
            {
                Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<List<KeyValuePair<string, int>>> GetEffectOfPainPerSubject(string subjectId, string frequency)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var result = ConvertDateByFrequency(frequency);
                    if (result.StartDate != DateTime.MinValue && result.EndDate != DateTime.MinValue)
                    {
                        List<KeyValuePair<string, int>> keyValues = new List<KeyValuePair<string, int>>();
                        var logList = logCollection.Find(a => a.IsActive == true && a.Subject.Id == subjectId && (a.DateOfLog >= result.StartDate && a.DateOfLog <= result.EndDate)).ToList();
                        if (logList.Count > 0)
                        {
                            keyValues.Add(new KeyValuePair<string, int>(PainInSleep.Adequate, logList.Where(a => a.Sleep.Contains(PainInSleep.Adequate)).Count()));
                            keyValues.Add(new KeyValuePair<string, int>(PainInSleep.Disturbed, logList.Where(a => a.Sleep.Contains(PainInSleep.Disturbed)).Count()));
                            keyValues.Add(new KeyValuePair<string, int>(PainInSleep.Excellent, logList.Where(a => a.Sleep.Contains(PainInSleep.Excellent)).Count()));
                            keyValues.Add(new KeyValuePair<string, int>(PainInSleep.Good, logList.Where(a => a.Sleep.Contains(PainInSleep.Good)).Count()));
                            keyValues.Add(new KeyValuePair<string, int>(PainInSleep.NoSleep, logList.Where(a => a.Sleep.Contains(PainInSleep.NoSleep)).Count()));

                            var pairResult = keyValues.OrderByDescending(a => a.Value).ToList();
                            return pairResult;
                        }
                    }
                    return null;
                });
            }
            catch (Exception ex)
            {
                Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<PainInMoodDto> GetPainMoods(string subjectId, string frequency)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var frequencyResult = ConvertDateByFrequency(frequency);
                    if (frequencyResult.StartDate != DateTime.MinValue && frequencyResult.EndDate != DateTime.MinValue)
                    {
                        PainInMoodDto painDetails = new PainInMoodDto();
                        string imagePathUrl = appSettings.APIPath + Path.DirectorySeparatorChar.ToString() + ImageFolder;
                        var moodsList = logCollection.Find(a => a.IsActive && a.Subject.Id == subjectId && (a.DateOfLog >= frequencyResult.StartDate && a.DateOfLog <= frequencyResult.EndDate)).ToList();
                        if (moodsList.Count > 0)
                        {
                            decimal totalCount = moodsList.Count();
                            int totalMoodValue = 0;
                            foreach(var moodValue in moodsList)
                            {
                                totalMoodValue= totalMoodValue+Convert.ToInt32(moodValue.LevelOfPain.OrderByDescending(t=>t).FirstOrDefault());
                            }
                           // decimal sumOfMoodValues = moodsList.Sum(a => Convert.ToInt32(a.LevelOfPain.FirstOrDefault()));
                            decimal sumOfMoodValues = totalMoodValue;
                            decimal result = Convert.ToInt32(Math.Ceiling(sumOfMoodValues / totalCount));
                            if (result == 0)
                            {
                                painDetails.MoodName = PainMoods.Happy;
                                painDetails.ImagePath = Path.Combine(imagePathUrl, MoodEmojiImage.HappyAndEnergetic);
                            }
                            else if (result > 0 && result < 4)
                            {
                                painDetails.MoodName = PainMoods.Frustrated;
                                painDetails.ImagePath = Path.Combine(imagePathUrl, MoodEmojiImage.FrustratedAndUncomfortable);
                            }
                            else if (result >= 4 && result < 7)
                            {
                                painDetails.MoodName = PainMoods.Angry;
                                painDetails.ImagePath = Path.Combine(imagePathUrl, MoodEmojiImage.AngryAndAnxious);
                            }
                            else if (result >= 7 && result < 10)
                            {
                                painDetails.MoodName = PainMoods.Depressed;
                                painDetails.ImagePath = Path.Combine(imagePathUrl, MoodEmojiImage.DepressedAndTired);
                            }
                            else if (result == 10)
                            {
                                painDetails.MoodName = PainMoods.Sad;
                                painDetails.ImagePath = Path.Combine(imagePathUrl, MoodEmojiImage.SadAndStressed);
                            }
                        }
                        return painDetails;
                    }
                    return null;
                });
            }
            catch (Exception ex)
            {
                Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<List<string>> GetEffectOfPainDates(string subjectId, string frequency, string sleepType)
        {
            try
            {
                return await Task.Run(() =>
                {
                    List<string> valueDate = new List<string>();
                    var result = ConvertDateByFrequency(frequency);
                    if (result.StartDate != DateTime.MinValue && result.EndDate != DateTime.MinValue)
                    {
                        var logList = logCollection.Find(a => a.IsActive == true && a.Sleep.Contains(sleepType) && a.Subject.Id == subjectId && (a.DateOfLog >= result.StartDate && a.DateOfLog <= result.EndDate)).ToList();
                        if (logList.Count > 0)
                        {
                            foreach (var logData in logList)
                            {
                                var dateLogged = logData.DateOfLog.ToString();
                                valueDate.Add(dateLogged);
                            }
                            var orderedList = valueDate.OrderBy(x => DateTime.Parse(x)).ToList();
                            return orderedList;
                        }
                    }
                    return null;
                });
            }
            catch (Exception ex)
            {
                Logger.LogInformation(ex.Message);
                return null;
            }
        }
    }
}