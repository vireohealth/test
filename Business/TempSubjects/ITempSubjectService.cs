﻿
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.TempSubjects
{
    public interface ITempSubjectService
    {
        Task<TempSubjectDto> SignUpSubject(string email);
        Task<TempSubjectDto> GetTempSubjectById(string id);
        bool UpdateTempUser(string email);
        Task<bool> CheckRegisteredSubjects(string email);
    }
}
