﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.TempSubjects
{
    public class TempSubjectService : BusinessService<ITempSubjectService>, ITempSubjectService
    {
        private readonly IMongoCollection<Models.TempSubjects> tempSubjectsCollection;
        private readonly IMongoCollection<Subject> subjectCollection;
        private readonly AppSettings appSettings;

        public TempSubjectService(DatabaseSettings databaseSettings,
            ILogger<ITempSubjectService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);

            tempSubjectsCollection = database.GetCollection<Models.TempSubjects>(databaseSettings.TempSubjectsCollectionName);
            tempSubjectsCollection.Indexes.CreateOneAsync(new CreateIndexModel<Models.TempSubjects>(nameof(Models.TempSubjects.Email)));
            subjectCollection = database.GetCollection<Subject>(databaseSettings.SubjectCollectionName);
            subjectCollection.Indexes.CreateOneAsync(new CreateIndexModel<Subject>(nameof(Subject.Email)));
        }

        public async Task<TempSubjectDto> SignUpSubject(string email)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var signUpDetails = FindSignUpByEmail(email);
                    if (signUpDetails != null && signUpDetails.IsActive == false)
                    {
                        return null;
                    }
                    else if (signUpDetails != null && signUpDetails.IsActive == true)
                    {
                        return FindSignUpByEmail(email).ToDto();
                    }
                    else
                    {
                        TempSubjectDto details = new TempSubjectDto();
                        details.Email = email;
                        details.CreatedOn = DateTime.UtcNow;
                        details.LastUpdatedOn = DateTime.UtcNow;
                        details.IsActive = true;
                        var entity = details.ToEntity();

                        tempSubjectsCollection.InsertOne(entity);
                        return FindSignUpByEmail(email).ToDto();
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }
        public bool UpdateTempUser(string email)
        {
            var filter = Builders<Models.TempSubjects>.Filter.Eq(t => t.Email, email.Trim());
            filter &= Builders<Models.TempSubjects>.Filter.Eq(t => t.IsActive, true);

            try
            {
                tempSubjectsCollection.UpdateOne(
                    filter,
                    Builders<Models.TempSubjects>.Update.Set(
                        t => t.IsActive,
                        false)
                    );
            }
            catch (Exception ex)
            {
                Logger.LogInformation(ex.Message);
                return false;
            }
            return true;
        }

        public async Task<TempSubjectDto> GetTempSubjectById(string id)
        {
            return await Task.Run(() => FindTempSubjectById(id).ToDto());
        }

        private Models.TempSubjects FindTempSubjectById(string id)
        {
            var result = tempSubjectsCollection.Find(x => x.Id == id && x.IsActive == true).FirstOrDefault();
            if (result != null)
            {
                var emailExist = FindSignUpByEmailForSubject(result.Email, result.Id);
                return emailExist != null ? null : result;
            }
            return result;
        }
        private Models.TempSubjects FindSignUpByEmailForSubject(string email, string Id)
        {
            return tempSubjectsCollection.Find(x => x.Email.ToLower() == email.ToLower() && x.IsActive == true && x.Id != Id).FirstOrDefault();
        }
        private Models.TempSubjects FindSignUpByEmail(string email)
        {
            return tempSubjectsCollection.Find(x => x.Email.ToLower() == email.ToLower() && x.IsActive == true).FirstOrDefault();
        }
        public async Task<bool> CheckRegisteredSubjects(string email)
        {
            try
            {
                return await Task.Run(() =>
                {
                    bool subjectResult = subjectCollection.Find(a => a.Email.ToLower() == email.ToLower() && a.IsDeleted == false).Any();
                    bool tempSubjectResult = tempSubjectsCollection.Find(x => x.Email.ToLower() == email.ToLower() && x.IsActive == false).Any();
                    if (!subjectResult && !tempSubjectResult)
                    {
                        return false;
                    }
                    return true;

                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return false;
            }
        }
    }
}
