﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.RecommendationSettings
{
    public class RecommendationSettingService : BusinessService<IRecommendationSettingService>, IRecommendationSettingService
    {
        private readonly IMongoCollection<Models.RecommendationSettings> recommendationSettingCollection;
        private readonly AppSettings appSettings;
        public RecommendationSettingService(DatabaseSettings databaseSettings, ILogger<IRecommendationSettingService> logger, IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);

            recommendationSettingCollection = database.GetCollection<Models.RecommendationSettings>(databaseSettings.RecommendationSettingCollectionName);
            recommendationSettingCollection.Indexes.CreateOneAsync(new CreateIndexModel<Models.RecommendationSettings>(nameof(Models.RecommendationSettings.PainEffectMoodStatus)));
        }

        public List<RecommendationSettingDto> AddRecommendationSetting(List<RecommendationSettingDto> recommendationSettingDto)
        {
            foreach (var result in recommendationSettingDto)
            {
                var entity = result.ToEntity();
                entity.CreatedOn = DateTime.UtcNow;
                entity.LastUpdatedOn = DateTime.UtcNow;
                entity.IsActive = true;
                if (!string.IsNullOrEmpty(result.Id))
                {
                    recommendationSettingCollection.DeleteOne(setting => setting.Id == result.Id);
                }
                recommendationSettingCollection.InsertOne(entity);
            }
            return recommendationSettingDto;
        }

        public async Task<List<RecommendationSettingDto>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return recommendationSettingCollection.Find(setting => true).ToDtos().OrderBy(a => a.CreatedOn).ToList();
            });
        }

        public async Task<bool> UniqueCheckRecommendationSetting(RecommendationSettingDto recommendationSettingDetails)
        {
            return await Task.Run(() =>
            {
                var result = recommendationSettingCollection.
                Find(setting => setting.IsActive &&
                setting.PainEffectSleepStatus == recommendationSettingDetails.PainEffectSleepStatus &&
                setting.PainEffectMoodStatus == recommendationSettingDetails.PainEffectMoodStatus)
                .ToDtos().OrderBy(a => a.CreatedOn).ToList();
                return result.Count > 0 ? true : false;
            });
        }

        public async Task<RecommendationSettingDto> GetRecommendationSettingById(string id)
        {
            return await Task.Run(() => FindRecommendationSettingById(id).ToDto());
        }

        private Models.RecommendationSettings FindRecommendationSettingById(string id)
        {
            return recommendationSettingCollection.Find(x => x.Id == id).FirstOrDefault();
        }

        public async void RemoveAsync(string id)
        {
            await Task.Run(() =>
            {
                recommendationSettingCollection.DeleteOne(setting => setting.Id == id);
            });
        }

        public async Task<string> GetRecommendationBasedOnMoodAndSleep(string mood, string sleep)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var result = recommendationSettingCollection.Find(a => a.IsActive && a.PainEffectMoodStatus.ToLower() == mood.ToLower() && a.PainEffectSleepStatus.ToLower() == sleep.ToLower()).FirstOrDefault();
                    if (result != null)
                    {
                        return result.Recommendation.Name;
                    }
                    return null;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }
    }
}
