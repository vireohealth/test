﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.RecommendationSettings
{
    public interface IRecommendationSettingService
    {
        List<RecommendationSettingDto> AddRecommendationSetting(List<RecommendationSettingDto> recommendationSettingDto);
        Task<List<RecommendationSettingDto>> GetAllAsync();
        Task<bool> UniqueCheckRecommendationSetting(RecommendationSettingDto recommendationSettingDetails);
        Task<RecommendationSettingDto> GetRecommendationSettingById(string id);
        void RemoveAsync(string id);
        Task<string> GetRecommendationBasedOnMoodAndSleep(string mood, string sleep);
    }
}
