﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using VireoIntegrativeProgram.Business.Recommendations;

namespace VireoIntegrativeProgram.Business.RecommendationSettings
{
    public class RecommendationSettingDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string PainEffectMoodStatus { get; set; }
        public string PainEffectSleepStatus { get; set; }
        public RecommendationDto Recommendation { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public RecommendationSettingDto()
        {
            IsActive = true;
        }
    }
}
