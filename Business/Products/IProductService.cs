﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Helpers;

namespace VireoIntegrativeProgram.Business.Products
{
    public interface IProductService
    {
        Task<List<ProductDto>> GetAllAsync();
        ProductDto AddProduct(ProductDto dto);
        Task<bool> CheckUniqueName(string id, string name);
        Task<ProductDto> GetProductById(string id);
        void UpdateAsync(string id, ProductDto productIn);
        void RemoveAsync(string id);
        Task<PagedList<ProductDto>> GetAllPaginationAsync(PaginationParams productParams);
    }
}
