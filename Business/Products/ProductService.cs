﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Helpers;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.Products
{
    public class ProductService : BusinessService<IProductService>, IProductService
    {
        private readonly IMongoCollection<Product> productCollection;
        private readonly AppSettings appSettings;

        public ProductService(DatabaseSettings databaseSettings,
            ILogger<IProductService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);

            productCollection = database.GetCollection<Product>(databaseSettings.ProductCollectionName);
            productCollection.Indexes.CreateOneAsync(new CreateIndexModel<Product>(nameof(Product.Name)));
        }

        public async Task<List<ProductDto>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return productCollection.Find(product => true).ToDtos().OrderBy(a => a.Name).ToList();
            });
        }

        public ProductDto AddProduct(ProductDto dto)
        {
            var productDetails = FindProductByName(dto.Name);
            if (productDetails != null)
            {
                return null;
            }

            var entity = dto.ToEntity();
            entity.CreatedOn = DateTime.UtcNow;
            entity.LastUpdatedOn = DateTime.UtcNow;
            entity.IsActive = true;

            productCollection.InsertOne(entity);
            return FindProductByName(dto.Name).ToDto();

        }

        private Product FindProductByName(string name)
        {
            return productCollection.Find(x => x.Name.ToLower() == name.ToLower() && x.IsActive == true).FirstOrDefault();
        }

        public async Task<bool> CheckUniqueName(string id, string name)
        {
            var result = !string.IsNullOrEmpty(id)
                ? await Task.Run(() => CheckNameExists(id, name).ToDto())
                : await Task.Run(() => CheckNameExists(name).ToDto());
            return result != null;
        }

        private Product CheckNameExists(string id, string name)
        {
            return productCollection.Find(x => x.Name.ToLower() == name.ToLower()
            && x.IsActive && x.Id != id).FirstOrDefault();
        }

        private Product CheckNameExists(string name)
        {
            return productCollection.Find(x => x.Name.ToLower() == name.ToLower()
            && x.IsActive).FirstOrDefault();
        }
        public async Task<ProductDto> GetProductById(string id)
        {
            return await Task.Run(() => FindProductById(id).ToDto());
        }

        private Product FindProductById(string id)
        {
            return productCollection.Find(x => x.Id == id).FirstOrDefault();
        }
        public async void UpdateAsync(string id, ProductDto productIn)
        {
            await Task.Run(() =>
            {
                productCollection.ReplaceOne(product => product.Id == id, productIn.ToEntity());
            });
        }
        public async void RemoveAsync(string id)
        {
            await Task.Run(() =>
            {
                productCollection.DeleteOne(productDetails => productDetails.Id == id);
            });
        }
        public async Task<PagedList<ProductDto>> GetAllPaginationAsync(PaginationParams productParams)
        {
            return await Task.Run(async () =>
            {
                var filteredProduct = Builders<Product>.Filter.Where(product => product.IsActive == true);

                if (!string.IsNullOrEmpty(productParams.Search))
                {
                    var search = productParams.Search.ToLower();

                    filteredProduct = Builders<Product>.Filter
                    .Where(product => product.IsActive == true && product.Name.ToLower().Contains(search));
                }

                var SortColumn = Builders<Product>.Sort.Descending(x => x.CreatedOn);

                if (!string.IsNullOrEmpty(productParams.SortCol))
                {
                    if ((!string.IsNullOrEmpty(productParams.SortDir)) && (productParams.SortDir == MessageConstants.AscendingSortDirection))
                    {
                        SortColumn = Builders<Product>.Sort.Ascending(productParams.SortCol);
                    }
                    else
                    {
                        SortColumn = Builders<Product>.Sort.Descending(productParams.SortCol);
                    }
                }
                var results = await productCollection.AggregateByPage(filteredProduct, SortColumn,
                productParams.PageNumber, productParams.PageSize);

                return PagedList<ProductDto>.CreatePageList(results.data.ToDtos(), results.totalPages,
                    productParams.PageNumber, productParams.PageSize);
            });
        }
    }
}
