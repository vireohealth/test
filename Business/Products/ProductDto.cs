﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace VireoIntegrativeProgram.Business.Products
{
    public class ProductDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [Required(ErrorMessage = "The  name is required.")]
        public string Name { get; set; }
        public int Amount { get; set; }
        public string AmountMesurement { get; set; }
        public string Frequency { get; set; }
        public string FrequencyMeasurement { get; set; }
        public bool IsActive { get; set; }
        public bool IsVireoProduct { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public ProductDto()
        {
            IsActive = true;
        }
    }
}
