﻿
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace VireoIntegrativeProgram.Business.Products
{
    public class BaseProductDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
