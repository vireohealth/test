﻿
namespace VireoIntegrativeProgram.Business.Dosing
{
    public class DosingUniqueDto
    {       
        public string TSQMSideEffects { get; set; }
        public string TSQMSatisfaction { get; set; }
        public string TSQMSeverity { get; set; }
        public string PEGScore { get; set; }
        public string ProductName { get; set; }
    }
}
