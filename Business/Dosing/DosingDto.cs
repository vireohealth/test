﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using VireoIntegrativeProgram.Business.Products;
using VireoIntegrativeProgram.Business.Recommendations;

namespace VireoIntegrativeProgram.Business.Dosing
{
    public class DosingDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string TSQMSideEffects { get; set; }
        public string TSQMSatisfaction { get; set; }
        public string TSQMSeverity { get; set; }
        public string PEGScore { get; set; }
        public ProductDto Product { get; set; }
        public RecommendationDto Recomendation { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public DosingDto()
        {
            IsActive = true;
        }
    }
}
