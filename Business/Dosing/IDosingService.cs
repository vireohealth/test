﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.Dosing
{
    public interface IDosingService
    {
        List<DosingDto> AddDosing(List<DosingDto> dosingDto);
        Task<List<DosingDto>> GetAllAsync();
        Task<bool> UniqueCheckDosing(DosingUniqueDto dosingDetails);
        Task<DosingDto> GetDosingById(string id);
        void RemoveAsync(string id);
        Task<bool> GetMedicineUsageStatus(string medicineId);
    }
}
