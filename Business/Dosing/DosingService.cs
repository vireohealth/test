﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.Dosing
{
    public class DosingService : BusinessService<IDosingService>, IDosingService
    {
        private readonly IMongoCollection<Dosings> dosingCollection;
        private readonly AppSettings appSettings;

        public DosingService(DatabaseSettings databaseSettings,
            ILogger<IDosingService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);

            dosingCollection = database.GetCollection<Dosings>(databaseSettings.DosingCollectionName);
            dosingCollection.Indexes.CreateOneAsync(new CreateIndexModel<Dosings>(nameof(Dosings.TSQMSideEffects)));
        }

        public List<DosingDto> AddDosing(List<DosingDto> dosingDto)
        {           
            foreach (var result in dosingDto)
            {   
                var entity = result.ToEntity();
                entity.CreatedOn = DateTime.UtcNow;
                entity.LastUpdatedOn = DateTime.UtcNow;
                entity.IsActive = true;
                if (!string.IsNullOrEmpty(result.Id))
                {
                    dosingCollection.DeleteOne(dose => dose.Id == result.Id);
                }
                dosingCollection.InsertOne(entity);
            }
            return dosingDto;
        }

        public async Task<List<DosingDto>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return dosingCollection.Find(dosing => true).ToDtos().OrderBy(a => a.CreatedOn).ToList();
            });
        }

        public async Task<bool> UniqueCheckDosing(DosingUniqueDto dosingDetails)
        {
            return await Task.Run(() =>
            {
                var result = dosingCollection.
                Find(dose => dose.IsActive &&
                dose.PEGScore == dosingDetails.PEGScore &&
                dose.TSQMSatisfaction == dosingDetails.TSQMSatisfaction &&
                dose.TSQMSeverity == dosingDetails.TSQMSeverity &&
                dose.TSQMSideEffects == dosingDetails.TSQMSideEffects &&
                dose.Product.Name == dosingDetails.ProductName)
                .ToDtos().OrderBy(a => a.CreatedOn).ToList();
                return result.Count > 0 ? true : false;
            });
        }

        public async Task<DosingDto> GetDosingById(string id)
        {
            return await Task.Run(() => FindDosingById(id).ToDto());
        }

        private Dosings FindDosingById(string id)
        {
            return dosingCollection.Find(x => x.Id == id).FirstOrDefault();
        }

        public async void RemoveAsync(string id)
        {
            await Task.Run(() =>
            {
                dosingCollection.DeleteOne(dosing => dosing.Id == id);
            });
        }

        public async Task<bool> GetMedicineUsageStatus(string medicineId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var medicineDetails = dosingCollection.Find(a => a.IsActive && a.Product.Id == medicineId).FirstOrDefault();
                    if (medicineDetails != null)
                    {
                        return true;
                    }
                    return false;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return false;
            }
        }
    }
}
