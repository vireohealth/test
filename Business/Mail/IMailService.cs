﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.Mail
{
    public interface IMailService
    {
        bool SendSimpleMail(string to, string from, string subject, string body);
        bool SendSimpleMail(string to, string from, string cc, string subject, string body);
        bool SendSimpleMailWithAttachment(string to, string from, string cc, string subject, string body, string filepPath, string contentType);
        bool SendSimpleMailWithAttachmentv2(string to, string from, string cc, string subject, string body, string filepPath, string contentType, string fileName);

    }
}
