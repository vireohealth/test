﻿using FluentEmail.Core;
using FluentEmail.Smtp;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net.Mail;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.Mail
{
    public class MailService : BusinessService<IMailService>, IMailService
    {

        private readonly EmailConfiguration settings;

        public MailService(EmailConfiguration settings,

            ILogger<IMailService> logger) : base(logger)
        {
            this.settings = settings;

        }
        /// <summary>
        /// Send mail
        /// </summary>
        /// <param name="to"></param>
        /// <param name="from"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public bool SendSimpleMail(string to, string from, string subject, string body)
        {

            int port;
            if (!int.TryParse(settings.Port, out port))
                port = 25;
            var smtpClient = new SmtpClient(settings.SmtpServer, port);
            if (port != 25) smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = false;
            if (!string.IsNullOrEmpty(settings.UserName) && !string.IsNullOrEmpty(settings.Password))
            {
                smtpClient.Credentials = new System.Net.NetworkCredential(settings.UserName, settings.Password);
            }
            if (string.IsNullOrEmpty(from))
            {
                from = settings.From;
            }
            try
            {
                var sender = new SmtpSender(smtpClient);
                Email.DefaultSender = sender;
                var email = Email
                    .From(from)
                    .To(to)
                    .Subject(subject)
                    .Body(body, true)
                    .Send();
                return email.Successful;
            }
            catch (Exception ex) { return false; }

        }
        /// <summary>
        /// Sends mail with CC
        /// </summary>
        /// <param name="to"></param>
        /// <param name="from"></param>
        /// <param name="cc"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public bool SendSimpleMail(string to, string from, string cc, string subject, string body)
        {
            int port;
            if (!int.TryParse(settings.Port, out port))
                port = 25;
            var smtpClient = new SmtpClient(settings.SmtpServer, port);
            if (port != 25) smtpClient.EnableSsl = true;
            if (!string.IsNullOrEmpty(settings.UserName) && !string.IsNullOrEmpty(settings.Password))
            {
                smtpClient.Credentials = new System.Net.NetworkCredential(settings.UserName, settings.Password);
            }
            if (string.IsNullOrEmpty(from))
            {
                from = settings.From;
            }
            try
            {
                var sender = new SmtpSender(smtpClient);
                Email.DefaultSender = sender;
                var email = Email
                    .From(from)
                    .To(to)
                    .CC(cc)
                    .Subject(subject)
                    .Body(body, true)
                    .Send();
                return email.Successful;
            }
            catch (Exception ex) { return false; }
        }

        /// <summary>
        /// Send mail with attachement
        /// </summary>
        /// <param name="to"></param>
        /// <param name="from"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="fileName"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public bool SendSimpleMailWithAttachment(string to, string from, string cc, string subject, string body, string filepPath, string contentType)
        {

            int port;
            if (!int.TryParse(settings.Port, out port))
                port = 25;
            var smtpClient = new SmtpClient(settings.SmtpServer, port);
            if (port != 25) smtpClient.EnableSsl = true;
            if (!string.IsNullOrEmpty(settings.UserName) && !string.IsNullOrEmpty(settings.Password))
            {
                smtpClient.Credentials = new System.Net.NetworkCredential(settings.UserName, settings.Password);
            }
            if (string.IsNullOrEmpty(from))
            {
                from = settings.From;
            }
            var sender = new SmtpSender(smtpClient);
            Email.DefaultSender = sender;
            using (Stream stream = new FileStream(filepPath, FileMode.Open, FileAccess.Read))
            {
                stream.Flush();
                stream.Seek(0, SeekOrigin.Begin);
                var attachment = new FluentEmail.Core.Models.Attachment()
                {
                    Data = stream,
                    ContentType = contentType,
                    Filename = filepPath,
                    IsInline = true
                };
                try
                {
                    var email = Email
                    .From(from)
                    .To(to)
                    .CC(cc)
                    .Subject(subject)
                    .Body(body)
                    .Attach(attachment)
                    .Send();
                    return email.Successful;
                }
                catch (Exception ex) { return false; }
            }

        }


        public bool SendSimpleMailWithAttachmentv2(string to, string from, string cc, string subject, string body, string filepPath, string contentType, string fileName)
        {
            int port;
            if (!int.TryParse(settings.Port, out port))
                port = 25;
            var smtpClient = new SmtpClient(settings.SmtpServer, port);
            if (port != 25) smtpClient.EnableSsl = true;
            if (!string.IsNullOrEmpty(settings.UserName) && !string.IsNullOrEmpty(settings.Password))
            {
                smtpClient.Credentials = new System.Net.NetworkCredential(settings.UserName, settings.Password);
            }
            if (string.IsNullOrEmpty(from))
            {
                from = settings.From;
            }
            var sender = new SmtpSender(smtpClient);
            Email.DefaultSender = sender;
            using (Stream stream = new FileStream(filepPath, FileMode.Open, FileAccess.Read))
            {
                stream.Flush();
                stream.Seek(0, SeekOrigin.Begin);
                var attachment = new FluentEmail.Core.Models.Attachment()
                {
                    Data = stream,
                    ContentType = contentType,
                    Filename = fileName,
                    IsInline = true
                };
                try
                {
                    var email = Email
                    .From(from)
                    .To(to)
                    .CC(cc)
                    .Subject(subject)
                    .Body(body)
                    .Attach(attachment)
                    .Send();
                    return email.Successful;
                }
                catch (Exception ex) { return false; }
            }

        }
    }
}
