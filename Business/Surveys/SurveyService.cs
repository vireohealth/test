﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Common.Enums;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Helpers;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.Surveys
{
    public class SurveyService : BusinessService<ISurveyService>, ISurveyService
    {
        private readonly IMongoCollection<Survey> surveyCollection;

        private readonly IMongoCollection<Subject> subjectCollection;

        private readonly AppSettings appSettings;
        public SurveyService(DatabaseSettings databaseSettings,
            ILogger<ISurveyService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);
            surveyCollection = database.GetCollection<Survey>(databaseSettings.SurveyCollectionName);
            surveyCollection.Indexes.CreateOneAsync(new CreateIndexModel<Survey>(nameof(Survey.Name)));
            subjectCollection = database.GetCollection<Subject>(databaseSettings.SubjectCollectionName);
            subjectCollection.Indexes.CreateOneAsync(new CreateIndexModel<Subject>(nameof(Subject.Email)));
        }

        public SurveyDto AddSurvey(SurveyDto surveyDto)
        {
            var survey = FindSurveyByName(surveyDto.Name);
            if (survey != null)
            {
                return null;
            }
            var entity = surveyDto.ToEntity();
            int responseTime = appSettings.QuestionSubmitTime;
            decimal minutesTaken = ((entity.TotalInputQuestions * responseTime) / 60);
            int CompletionTime = (int)Math.Round(minutesTaken);
            entity.CreatedOn = DateTime.UtcNow;
            entity.LastUpdatedOn = DateTime.UtcNow;
            entity.Status = SurveyStatus.YetToStart;
            entity.IsActive = true;
            entity.TimeToComplete = CompletionTime;
            surveyCollection.InsertOne(entity);
            return FindSurveyByName(surveyDto.Name).ToDto();
        }
        private Survey FindSurveyByName(string name)
        {
            return surveyCollection.Find(x => x.Name.ToLower() == name.ToLower() && x.IsActive == true).FirstOrDefault();
        }
        public async Task<PagedList<SurveyDto>> GetAllPaginationAsync(PaginationParams surveyParams)
        {
            return await Task.Run(async () =>
            {
                var filteredSurveys = Builders<Survey>.Filter.Empty;

                if (!string.IsNullOrEmpty(surveyParams.Search))
                {
                    filteredSurveys = Builders<Survey>.Filter
                    .Where(survey => survey.Name.ToLower().Contains(surveyParams.Search.ToLower()));
                }

                var SortColumn = Builders<Survey>.Sort.Descending(x => x.CreatedOn);

                if (!string.IsNullOrEmpty(surveyParams.SortCol))
                {
                    if ((!string.IsNullOrEmpty(surveyParams.SortDir)) && (surveyParams.SortDir == MessageConstants.AscendingSortDirection))
                    {
                        SortColumn = Builders<Survey>.Sort.Ascending(surveyParams.SortCol);
                    }
                    else
                    {
                        SortColumn = Builders<Survey>.Sort.Descending(surveyParams.SortCol);
                    }
                }
                var results = await surveyCollection.AggregateByPage(filteredSurveys, SortColumn,
                surveyParams.PageNumber, surveyParams.PageSize);

                return PagedList<SurveyDto>.CreatePageList(results.data.ToDtos(), results.totalPages,
                    surveyParams.PageNumber, surveyParams.PageSize);

            });
        }

        public async Task<SurveyDto> GetSurveyById(string id)
        {
            return await Task.Run(() => FindSurveyById(id).ToDto());
        }

        private Survey FindSurveyById(string id)
        {
            return surveyCollection.Find(x => x.Id == id).FirstOrDefault();
        }
        public async void UpdateAsync(string id, SurveyDto surveyIn)
        {
            await Task.Run(() =>
            {
                surveyCollection.ReplaceOne(survey => survey.Id == id, surveyIn.ToEntity());
            });
        }

        public async Task<bool> CheckUniqueSurvey(string id, string surveyName)
        {
            var result = !string.IsNullOrEmpty(id)
                ? await Task.Run(() => CheckSurveyWithIdExists(id, surveyName).ToDto())
                : await Task.Run(() => CheckSurveyWithIdExists(surveyName).ToDto());
            return result != null;
        }

        private Models.Survey CheckSurveyWithIdExists(string id, string surveyName)
        {
            return surveyCollection.Find(x => x.Name.ToLower() == surveyName.ToLower()
            && x.IsActive && x.Id != id).FirstOrDefault();
        }

        private Models.Survey CheckSurveyWithIdExists(string surveyName)
        {
            return surveyCollection.Find(x => x.Name.ToLower() == surveyName.ToLower()
            && x.IsActive).FirstOrDefault();
        }
        public async Task<bool> IsSurveyStarted(string surveyId)
        {
            var result = await Task.Run(() => CheckSurveyStarted(surveyId).ToDto());
            return result != null;
        }

        public Survey CheckSurveyStarted(string surveyId)
        {
            return surveyCollection.Find(x => x.IsActive && x.Id == surveyId && x.Patients.Any(i => i.DatesInfo.Any(i => i.IsCompleted))).FirstOrDefault();
        }

        public async void RemoveAsync(string id)
        {
            await Task.Run(() =>
            {
                surveyCollection.DeleteOne(surveyDetails => surveyDetails.Id == id);
            });
        }

        public async Task<List<SurveyDto>> UpcomingSurveys(string subjectId)
        {
            return await Task.Run(() =>
            {
                var result = surveyCollection.Find(a => a.IsActive == true && a.Patients.Any(p => p.Id == subjectId)
                 && a.StartDate != null && a.StartDate > DateTime.Today)
                .ToList()
                .OrderBy(a => a.StartDate)
                .ToList();

                return result.ToDtos(subjectId);
            });
        }

        public async Task<string> GetSurveyQuestionsAndAnswers(string subjectId, string surveyId)
        {
            return await Task.Run(async () =>
            {
                bool surveyExists = surveyCollection.Find(a => a.IsActive && a.Id == surveyId && a.Patients.Any(a => a.Id == subjectId)).Any();
                var result = "";
                if (surveyExists)
                {
                    result = surveyCollection.Find(a => a.IsActive && a.Id == surveyId && a.Patients.Any(a => a.Id == subjectId)).FirstOrDefault().FormProperties.ToString();
                }
                return result == "" ? null : result;
            });
        }

        public bool UpdateSurveyQuestionsAndAnswersForSubject(SurveyAnswers surveyanswers)
        {
            var filter = Builders<Survey>.Filter.Where(t => t.Id == surveyanswers.SurveyId && t.IsActive);
            try
            {
                var surveyList = surveyCollection.Find(x => x.Id == surveyanswers.SurveyId);
                var surveyDetails = surveyList.FirstOrDefault().ToDto();
                bool datesPerSubject = surveyDetails.Patients.Find(i => i.Id == surveyanswers.SubjectId).DatesInfo.Any(a => a.SubmittedDate?.Date == surveyanswers.DateToUpdate.Date);
                if (datesPerSubject)
                {
                    var form = surveyDetails.Patients.Find(i => i.Id == surveyanswers.SubjectId).DatesInfo.Find(i => i.SubmittedDate?.Date == surveyanswers.DateToUpdate.Date);
                    form.SubmittedDate = DateTime.UtcNow;
                    form.TotalFilledInputQuestions = surveyanswers.FilledQuestions;
                    form.FormPropertyValues = surveyanswers.SurveyResults;
                    form.CompletionPercent = surveyanswers.CompletionPercentage;
                    form.IsCompleted = surveyanswers.CompletionPercentage == 100 ? true : false;
                    form.QuestionsAndAnswers = surveyanswers.QuestionsAndAnswers;
                    surveyCollection.ReplaceOne(x => x.Id == surveyanswers.SurveyId, surveyDetails.ToEntity());
                }
                else
                {
                    var form = surveyDetails.Patients.Find(i => i.Id == surveyanswers.SubjectId);
                    PatientDatesInfoDto dateInfo = new PatientDatesInfoDto();
                    dateInfo.SubmittedDate = surveyanswers.DateToUpdate;
                    dateInfo.FormPropertyValues = surveyanswers.SurveyResults;
                    dateInfo.CompletionPercent = surveyanswers.CompletionPercentage;
                    dateInfo.TotalFilledInputQuestions = surveyanswers.FilledQuestions;
                    dateInfo.IsCompleted = surveyanswers.CompletionPercentage == 100 ? true : false;
                    dateInfo.QuestionsAndAnswers = surveyanswers.QuestionsAndAnswers;
                    form.DatesInfo.Add(dateInfo);
                    dateInfo.IsCompleted = surveyanswers.CompletionPercentage == 100 ? true : false;
                    surveyCollection.ReplaceOne(x => x.Id == surveyanswers.SurveyId, surveyDetails.ToEntity());
                }

                surveyCollection.UpdateOne(
                    filter,
                    Builders<Models.Survey>.Update.Set(
                        t => t.LastUpdatedOn,
                        DateTime.UtcNow)
                    );

                surveyCollection.UpdateOne(
                   filter,
                   Builders<Models.Survey>.Update.Set(
                       t => t.Status,
                       SurveyStatus.Active)
                   );
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<List<SurveyDto>> GetSurveysBasedOnStatus(string subjectId, string status)
        {
            try
            {
                List<SurveyDto> surveyList = new List<SurveyDto>();
                return await Task.Run(async () =>
                {
                    var result = await surveyCollection.Find(a => a.IsActive && a.Patients.Any(a => a.Id == subjectId)).ToListAsync();
                    if (status == SurveyStatus.Completed)
                    {
                        foreach (var survey in result)
                        {
                            foreach (var datesurvey in survey.Patients.Where(a => a.Id == subjectId))
                            {
                                if ((survey.Frequency.Name == DateFrequency.Onetime && survey.SurveyType.Name == SurveyTypeEnum.Welcome.ToString()
                                && survey.Patients.Where(a => a.Id == subjectId).Any(a => a.DatesInfo.Any(a => a.IsCompleted)))
                                ||
                                (survey.Frequency.Name == DateFrequency.Onetime && survey.SurveyType.Name != SurveyTypeEnum.Welcome.ToString()
                                && datesurvey.DatesInfo.Any(i => i.IsCompleted))
                                ||
                                survey.EndDate < DateTime.Today)
                                {
                                    surveyList.Add(survey.ToDto(subjectId));
                                }
                            }
                        }
                        return surveyList;
                    }
                    else
                    {
                        foreach (var survey in result)
                        {
                            if (survey.Frequency.Name == DateFrequency.Daily)
                            {
                                if ((survey.StartDate <= DateTime.Now.Date && survey.EndDate >= DateTime.Now.Date) && !survey.Patients.Where(a => a.Id == subjectId)
                                .Any(a => a.DatesInfo.Any(a => a.SubmittedDate?.Date == DateTime.Today.Date)))
                                {
                                    surveyList.Add(survey.ToDto(subjectId));
                                }
                            }
                            else if (survey.Frequency.Name == DateFrequency.Weekly)
                            {
                                bool IfExists = GetAllDatesForWeeklySurvey(survey.StartDate.HasValue ? survey.StartDate.Value : new DateTime(), survey.EndDate.HasValue ? survey.EndDate.Value : new DateTime());
                                if (IfExists == true && !survey.Patients.Where(a => a.Id == subjectId).Any(a => a.DatesInfo.Any(a => a.SubmittedDate?.Date == DateTime.Today.Date)))
                                {
                                    surveyList.Add(survey.ToDto(subjectId));
                                }
                            }
                            else if (survey.Frequency.Name == DateFrequency.EveryOtherDay)
                            {
                                bool IfExists = GetAllDatesForEveryOtherDaySurvey(survey.StartDate.HasValue ? survey.StartDate.Value : new DateTime(), survey.EndDate.HasValue ? survey.EndDate.Value : new DateTime());
                                if (IfExists == true && !survey.Patients.Where(a => a.Id == subjectId).Any(a => a.DatesInfo.Any(a => a.SubmittedDate?.Date == DateTime.Today.Date)))
                                {
                                    surveyList.Add(survey.ToDto(subjectId));
                                }
                            }
                            else if (survey.Frequency.Name == DateFrequency.Onetime && survey.SurveyType.Name != SurveyTypeEnum.Welcome.ToString())
                            {
                                if ((survey.StartDate <= DateTime.Now.Date && survey.EndDate >= DateTime.Now.Date) && !survey.Patients.Where(a => a.Id == subjectId).Any(a => a.DatesInfo.Any(a => a.IsCompleted)))
                                {
                                    surveyList.Add(survey.ToDto(subjectId));
                                }
                            }
                            else if (survey.Frequency.Name == DateFrequency.Onetime && survey.SurveyType.Name == SurveyTypeEnum.Welcome.ToString())
                            {
                                if (!survey.Patients.Where(a => a.Id == subjectId).Any(a => a.DatesInfo.Any(a => a.IsCompleted)))
                                {
                                    surveyList.Add(survey.ToDto(subjectId));
                                }
                            }
                        }
                        return surveyList;
                    }
                });
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private bool GetAllDatesForWeeklySurvey(DateTime startingDate, DateTime endingDate)
        {
            List<DateTime> allDates = new List<DateTime>();

            for (DateTime date = startingDate; date <= endingDate; date = date.AddDays(7))
            {
                allDates.Add(date);
            }
            if (allDates.Any(a => a.Date == DateTime.Today.Date))
            {
                return true;
            }
            return false;
        }

        private bool GetAllDatesForEveryOtherDaySurvey(DateTime startingDate, DateTime endingDate)
        {
            List<DateTime> allDates = new List<DateTime>();

            for (DateTime date = startingDate; date <= endingDate; date = date.AddDays(2))
            {
                allDates.Add(date);
            }
            if (allDates.Any(a => a.Date == DateTime.Today.Date))
            {
                return true;
            }
            return false;
        }

        public async Task<bool> FindAndUpdateToSelectAllSurveys(PatientSurveyDetails patientInfo)
        {
            return await Task.Run(() =>
            {
                try
                {
                    var result = surveyCollection.Find(i => i.IsActive && i.IsSelectedAll && ((i.EndDate == null) || (i.EndDate != null && i.EndDate >= DateTime.Today))).ToList();

                    result.ForEach(survey =>
                    {
                        AddSurveyForNewPatient(patientInfo, survey.Id, survey.Patients);
                    });
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            });
        }

        private bool AddNewPatientsToSurvey(List<PatientSurveyDetails> patientInfos, string surveyId, List<PatientSurveyDetails> patients)
        {
            var filter = Builders<Survey>.Filter.Eq(t => t.Id, surveyId);
            foreach (var patientInfo in patientInfos)
            {
                if (!patients.Where(x => x.Id == patientInfo.Id).Any())
                {
                    patients.Add(patientInfo);
                }
            }
            surveyCollection.UpdateOne(filter,
            Builders<Survey>.Update.Set("Patients",
                patients)
            );
            return true;
        }

        private bool AddSurveyForNewPatient(PatientSurveyDetails patientInfo, string surveyId, List<PatientSurveyDetails> patients)
        {
            var filter = Builders<Survey>.Filter.Eq(t => t.Id, surveyId);
            List<QuestionAndAnswer> questionDetailsList = new List<QuestionAndAnswer>();
            QuestionAndAnswer questionDetails = new QuestionAndAnswer();
            var qtns= surveyCollection.Find(a => a.Id == surveyId).FirstOrDefault();
            if(qtns!=null)
            {
                questionDetails = qtns.Patients.FirstOrDefault().DatesInfo.FirstOrDefault().QuestionsAndAnswers.FirstOrDefault();
                questionDetailsList.Add(questionDetails);
            }
            foreach(var result in patientInfo.DatesInfo)
            {
                result.QuestionsAndAnswers = questionDetailsList;
            }
         
            if (!patients.Where(x => x.Id == patientInfo.Id).Any())
            {
                patients.Add(patientInfo);
                surveyCollection.UpdateOne(filter,
                Builders<Survey>.Update.Set("Patients",
                    patients)
                );
            }
            return true;
        }

        public async Task<bool> FindAndUpdatePatientsToSurvey(string surveyId, List<PatientSurveyDetails> patientInfos)
        {
            return await Task.Run(() =>
            {
                try
                {
                    var survey = surveyCollection.Find(i => i.IsActive && i.Id == surveyId).FirstOrDefault();
                    AddNewPatientsToSurvey(patientInfos, survey.Id, survey.Patients);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            });
        }

        public async Task<List<SurveyDto>> GetOngoingSurveys(string subjectId)
        {
            try
            {
                List<SurveyDto> surveyList = new List<SurveyDto>();
                return await Task.Run(() =>
                {
                    var result = surveyCollection.Find(a => a.IsActive == true && a.Patients.Any(p => p.Id == subjectId)
                 && ((a.StartDate <= DateTime.Now.Date && a.EndDate >= DateTime.Now.Date)
                 && a.SurveyType.Name != SurveyTypeEnum.Welcome && a.Frequency.Name != DateFrequency.Onetime))
                 .ToList()
                 .OrderBy(a => a.StartDate)
                 .ToList();

                    return result.ToDtos(subjectId);
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<List<BaseSubjectDto>> GetOtherPatients(string surveyId)
        {
            try
            {
                List<BaseSubjectDto> subjectList = new List<BaseSubjectDto>();
                return await Task.Run(async () =>
                {
                    var surveySubjectList = surveyCollection.Find(a => a.IsActive == true && a.Id == surveyId).ToList().FirstOrDefault();
                    if (surveySubjectList != null)
                    {
                        List<BaseSubjectDto> surveyWithSubjectList = surveySubjectList.Patients.Select(a => new BaseSubjectDto { Id = a.Id.ToString(), Name = a.Name }).ToList();
                        var totalSubjectList = subjectCollection.Find(a => a.IsActive == true && a.UserType != PatientType.Admin).ToList();
                        List<BaseSubjectDto> fullSubjectList = totalSubjectList.Select(a => new BaseSubjectDto { Id = a.Id.ToString(), Name = a.Name }).ToList();
                        var subjectsAvailable = fullSubjectList.ToList();
                        foreach (var item in fullSubjectList)
                        {
                            if (surveyWithSubjectList.Any(a => a.Id == item.Id))
                            {
                                subjectsAvailable.Remove(item);
                            }
                        }

                        return subjectsAvailable;
                    }
                    return null;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }

        private DateConverter ConvertDateByFrequency(string frequency)
        {
            try
            {
                DateConverter dateConverter = new DateConverter();
                if (frequency == DateFrequency.Today)
                {
                    dateConverter.StartDate = DateTime.Today.Date;
                    dateConverter.EndDate = DateTime.Today.Date;
                }
                else if (frequency == DateFrequency.Weekly)
                {
                    dateConverter.StartDate = DateTime.Today.AddDays(-6);
                    dateConverter.EndDate = DateTime.Today.Date;
                }
                else if (frequency == DateFrequency.Monthly)
                {
                    dateConverter.StartDate = DateTime.Today.AddDays(-29);
                    dateConverter.EndDate = DateTime.Today.Date;
                }
                return dateConverter;
            }
            catch (Exception ex)
            {
                Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<List<DateTime>> GetMissedSurveySubmissionDates(string surveyId, string subjectId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    List<DateTime> allListDates = new List<DateTime>();
                    List<DateTime> expectedFilledDates = new List<DateTime>();
                    List<DateTime> FilledDates = new List<DateTime>();
                    var surveyDetails = surveyCollection.Find(a => a.Id == surveyId).FirstOrDefault();
                    if (surveyDetails != null && surveyDetails.Frequency.Name != DateFrequency.Onetime && surveyDetails.SurveyType.Name != DateFrequency.Welcome)
                    {
                        var surveyFrequency = surveyDetails.Frequency.Name;
                        for (DateTime date = surveyDetails.StartDate.Value.Date; date <= surveyDetails.EndDate.Value.Date; date = date.AddDays(surveyFrequency == DateFrequency.Daily ? 1 : surveyFrequency == DateFrequency.Weekly ? 7 : surveyFrequency == DateFrequency.EveryOtherDay ? 2 : 30))
                        {
                            if (DateTime.Today.Date >= date)
                            {
                                expectedFilledDates.Add(date);
                            }
                        }

                        var subjectInsertedDetails = surveyDetails.Patients.Where(a => a.Id == subjectId).ToList();
                        if (subjectInsertedDetails != null)
                        {
                            foreach (var insertedDate in subjectInsertedDetails.Select(a => a.DatesInfo))
                            {
                                foreach (var innerDate in insertedDate.Where(a => a.SubmittedDate != null))
                                {
                                    FilledDates.Add(innerDate.SubmittedDate.Value);
                                }
                            }
                        }
                        else
                        {
                            return null;
                        }
                        allListDates = expectedFilledDates.ToList();
                        foreach (var item in allListDates)
                        {
                            if (FilledDates.Any(a => a.Date == item.Date))
                            {
                                expectedFilledDates.Remove(item);
                            }
                        }
                        return expectedFilledDates;
                    }
                    return null;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<bool> GetMedicineUsageStatus(string medicineId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var medicineDetails = surveyCollection.Find(a => a.IsActive && a.Product.Id == medicineId).FirstOrDefault();
                    if (medicineDetails != null)
                    {
                        return true;
                    }
                    return false;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return false;
            }
        }

        public async Task<bool> FindAndUpdateSurveysToCompleted()
        {
            try
            {
                return await Task.Run(() =>
                {
                    var surveys = FindSurveysToBeCompleted();
                    foreach (var survey in surveys.Result)
                    {
                        var filter = Builders<Survey>.Filter.Eq(t => t.Id, survey.Id);
                        var updatedSubject = surveyCollection.UpdateOne(
                             filter,
                             Builders<Survey>.Update.Set(
                                t => t.Status,
                                 SurveyStatus.Completed));
                    }
                    return true;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return false;
            }
        }

        private async Task<List<Survey>> FindSurveysToBeCompleted()
        {
            return await surveyCollection.Find(i => i.EndDate < DateTime.Today && i.Status != SurveyStatus.Completed).ToListAsync();
        }

        public async Task<string> GetNextDateOfSubmission(string surveyId, string subjectId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var surveyDetails = surveyCollection.Find(a => a.IsActive == true && a.Id == surveyId && a.SurveyType.Name != DateFrequency.Welcome && a.Patients.Any(a => a.Id == subjectId)).FirstOrDefault();
                    if (surveyDetails != null)
                    {
                        DateTime lastSubmittedDate = DateTime.MinValue;
                        var lastSubmittedDateDetails = surveyDetails.Patients.Where(a => a.Id == subjectId).Select(a => a.DatesInfo.ToList());
                        foreach (var result in lastSubmittedDateDetails.LastOrDefault())
                        {
                            lastSubmittedDate = result.SubmittedDate.HasValue ? result.SubmittedDate.Value : DateTime.MinValue;
                        }
                        if (surveyDetails.Frequency.Name == DateFrequency.Daily)
                        {
                            return (DateTime.Today.Date.AddDays(1) <= surveyDetails.EndDate.Value.Date) ? DateTime.Today.Date.AddDays(1).ToString() : null;
                        }
                        else if (surveyDetails.Frequency.Name == DateFrequency.Weekly)
                        {
                            lastSubmittedDate = (lastSubmittedDate == DateTime.MinValue) ? DateTime.Today.Date.AddDays(7) : lastSubmittedDate.Date.AddDays(7);
                            return (lastSubmittedDate <= surveyDetails.EndDate.Value.Date) ? lastSubmittedDate.ToString() : null;
                        }
                        else if (surveyDetails.Frequency.Name == DateFrequency.EveryOtherDay)
                        {
                            lastSubmittedDate = (lastSubmittedDate == DateTime.MinValue) ? DateTime.Today.Date.AddDays(2) : lastSubmittedDate.Date.AddDays(2);
                            return (lastSubmittedDate <= surveyDetails.EndDate.Value.Date) ? lastSubmittedDate.ToString() : null;
                        }
                        else if (surveyDetails.Frequency.Name == DateFrequency.Onetime)
                        {
                            return null;
                        }

                    }
                    return null;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<bool> GetBodyPartsUsageStatus(string bodyPartName)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var filter = Builders<Survey>.Filter.Eq("FormProperties.components.components.right.values.bodyPartName", bodyPartName.ToLower());

                    var result = surveyCollection.Find(filter).ToList();
                    return result.Count > 0 ? true : false;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return false;
            }
        }

        public async Task<List<SurveyFilterDto>> GetAnswersBySubjectSurveyAndQuestions(SurveyReportParameter reportDetails)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    List<SurveyFilterDto> SurveyQuestionReponses = new List<SurveyFilterDto>();
                    foreach (var questionDetails in reportDetails.questionList)
                    {
                        foreach (var subjectId in reportDetails.subjectIds)
                        {
                            var surveyDetails = surveyCollection.Find(a => a.IsActive && a.Id == reportDetails.surveyId).FirstOrDefault();
                            if (surveyDetails != null)
                            {
                                var subjectDetails = surveyDetails.Patients.Where(a => a.Id == subjectId).ToList();
                                if (subjectDetails != null)
                                {
                                    foreach (var insertedDate in subjectDetails.Select(a => a.DatesInfo))
                                    {
                                        foreach (var innerDate in insertedDate.Where(a => a.SubmittedDate != null))
                                        {
                                            var combinedAnswer = "";
                                            var combinedTitle = "";
                                            var combinedDescription = "";
                                            var combinedImage = "";

                                            SurveyFilterDto surveyDateDetails = new SurveyFilterDto();
                                            surveyDateDetails.Question = questionDetails.Question;
                                            surveyDateDetails.SubjectName = surveyDetails.Patients.FirstOrDefault(a => a.Id == subjectId).Name;
                                            surveyDateDetails.SubmittedDate = innerDate.SubmittedDate?.Date.ToString();
                                            var answerDetails = innerDate.QuestionsAndAnswers.Where(a => a.Question == questionDetails.Question).Select(a => a.Answers).ToList();
                                            foreach (var answersGroup in answerDetails)
                                            {
                                                foreach (var answersGroupDetails in answersGroup)
                                                {
                                                    combinedAnswer = combinedAnswer == "" ? answersGroupDetails.Answer : combinedAnswer + ";" + answersGroupDetails.Answer;
                                                    combinedTitle = combinedTitle == "" ? answersGroupDetails.Title : combinedTitle + ";" + answersGroupDetails.Title;
                                                    combinedDescription = combinedDescription == "" ? answersGroupDetails.Description : combinedDescription + ";" + answersGroupDetails.Description;
                                                    combinedImage = combinedImage == "" ? answersGroupDetails.Image : combinedImage + ";" + answersGroupDetails.Image;
                                                }
                                            }
                                            surveyDateDetails.Answer = combinedAnswer;
                                            surveyDateDetails.Description = combinedDescription;
                                            surveyDateDetails.Title = combinedTitle;
                                            surveyDateDetails.ImagePath = combinedImage;
                                            SurveyQuestionReponses.Add(surveyDateDetails);
                                        }
                                    }
                                }
                                else
                                {
                                    return null;
                                }
                            }
                        }
                    }
                    return SurveyQuestionReponses.OrderBy(a => a.SubmittedDate).ThenBy(a => a.Question).ToList();
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }
        public async Task<List<BaseSurveyDto>> GetAllSurveys()
        {
            try
            {
                return await Task.Run(() =>
                {
                    List<BaseSurveyDto> SurveyFullList = new List<BaseSurveyDto>();
                    var surveylist = surveyCollection.Find(a => a.IsActive == true).ToList().Select(a => new { Id = a.Id, Name = a.Name, Frequency = a.Frequency.Name }).ToList();
                    foreach (var result in surveylist)
                    {
                        BaseSurveyDto surveyDetails = new BaseSurveyDto();
                        surveyDetails.Id = result.Id;
                        surveyDetails.Name = result.Name;
                        surveyDetails.Frequency = result.Frequency;
                        SurveyFullList.Add(surveyDetails);
                    }
                    return SurveyFullList.OrderBy(a => a.Name).ToList();
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null; ;
            }
        }

        public async Task<List<BasePatientDto>> GetAllPatientsInSurvey(string surveyId)
        {
            try
            {
                return await Task.Run(() =>
                {
                    List<BasePatientDto> patientFullList = new List<BasePatientDto>();
                    var patientList = surveyCollection.Find(a => a.IsActive == true && a.Id == surveyId).ToList().Select(a => a.Patients).ToList();
                    foreach (var result in patientList)
                    {
                        foreach (var patientResult in result)
                        {
                            BasePatientDto patientDetails = new BasePatientDto();
                            patientDetails.Id = patientResult.Id;
                            patientDetails.Name = patientResult.Name;
                            patientFullList.Add(patientDetails);
                        }
                    }
                    return patientFullList;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null; ;
            }
        }
        public async Task<List<QuestionDto>> GetAllQuestionsInSurvey(string surveyId)
        {
            try
            {
                return await Task.Run(() =>
                {
                    List<QuestionDto> questionsFullList = new List<QuestionDto>();
                    var questionDetailsFromSurvey = surveyCollection.Find(a => a.IsActive == true && a.Id == surveyId).ToList().Select(a => a.Patients.FirstOrDefault().DatesInfo.LastOrDefault().QuestionsAndAnswers).ToList();
                    foreach (var result in questionDetailsFromSurvey)
                    {
                        foreach (var questionResult in result)
                        {
                            QuestionDto questionDetails = new QuestionDto();
                            questionDetails.Question = questionResult.Question;
                            questionsFullList.Add(questionDetails);
                        }
                    }
                    return questionsFullList;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null; ;
            }
        }

        public async Task<List<SurveyFilterDto>> GetAnswersBySubjectSurveyAndQuestionsReports(SurveyReportParameter reportDetails)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    List<SurveyFilterDto> SurveyQuestionReponses = new List<SurveyFilterDto>();
                    foreach (var questionDetails in reportDetails.questionList)
                    {
                        int totalSubjectCount = reportDetails.subjectIds.Count();
                        int Skip = 0;
                        var subjectListIds = reportDetails.subjectIds.Skip(Skip).Take(5);
                        int i = 5;
                        if (i <= totalSubjectCount)
                        {
                            for (i = 5; i <= totalSubjectCount; i++)
                            {
                                if (subjectListIds.Count() == 5)
                                {
                                    var subjectId1 = "";
                                    var subjectId2 = "";
                                    var subjectId3 = "";
                                    var subjectId4 = "";
                                    var subjectId5 = "";
                                    int listIDCount = 1;
                                    foreach (var listId in subjectListIds)
                                    {
                                        subjectId1 = listIDCount == 1 ? listId : subjectId1;
                                        subjectId2 = listIDCount == 2 ? listId : subjectId2;
                                        subjectId3 = listIDCount == 3 ? listId : subjectId3;
                                        subjectId4 = listIDCount == 4 ? listId : subjectId4;
                                        subjectId5 = listIDCount == 5 ? listId : subjectId5;
                                        listIDCount = listIDCount + 1;
                                    }

                                    var subject1 = GetSurveyResponseFromSubject1(subjectId1, questionDetails.Question, reportDetails.surveyId);
                                    var subject2 = GetSurveyResponseFromSubject2(subjectId2, questionDetails.Question, reportDetails.surveyId);
                                    var subject3 = GetSurveyResponseFromSubject3(subjectId3, questionDetails.Question, reportDetails.surveyId);
                                    var subject4 = GetSurveyResponseFromSubject4(subjectId4, questionDetails.Question, reportDetails.surveyId);
                                    var subject5 = GetSurveyResponseFromSubject5(subjectId5, questionDetails.Question, reportDetails.surveyId);
                                    await Task.WhenAll(subject1, subject2, subject3, subject4, subject5);
                                    if (subject1 != null)
                                    {
                                        var resultSubject1 = await subject1;
                                        SurveyQuestionReponses.AddRange(resultSubject1);
                                    }
                                    if (subject2 != null)
                                    {
                                        var resultSubject2 = await subject2;
                                        SurveyQuestionReponses.AddRange(resultSubject2);
                                    }
                                    if (subject3 != null)
                                    {
                                        var resultSubject3 = await subject3;
                                        SurveyQuestionReponses.AddRange(resultSubject3);
                                    }
                                    if (subject4 != null)
                                    {
                                        var resultSubject4 = await subject4;
                                        SurveyQuestionReponses.AddRange(resultSubject4);
                                    }
                                    if (subject5 != null)
                                    {
                                        var resultSubject5 = await subject5;
                                        SurveyQuestionReponses.AddRange(resultSubject5);
                                    }
                                }
                                else
                                {
                                    foreach (var subjectId in subjectListIds)
                                    {
                                        List<SurveyFilterDto> questionResponseFromSubject1 = await GetSurveyResponseFromSubject1(subjectId, questionDetails.Question, reportDetails.surveyId);
                                        if (questionResponseFromSubject1 != null)
                                        {
                                            SurveyQuestionReponses.AddRange(questionResponseFromSubject1);
                                        }
                                    }
                                }

                                Skip = Skip + 5;
                                i = i + 4;
                            }
                            var pendingSubjectIDs = reportDetails.subjectIds.Skip(i - 5);
                            foreach (var subjectId in pendingSubjectIDs)
                            {
                                List<SurveyFilterDto> questionResponseFromSubject1 = await GetSurveyResponseFromSubject1(subjectId, questionDetails.Question, reportDetails.surveyId);
                                if (questionResponseFromSubject1 != null)
                                {
                                    SurveyQuestionReponses.AddRange(questionResponseFromSubject1);
                                }
                            }
                        }
                        else
                        {
                            foreach (var subjectId in subjectListIds)
                            {
                                List<SurveyFilterDto> questionResponseFromSubject1 = await GetSurveyResponseFromSubject1(subjectId, questionDetails.Question, reportDetails.surveyId);
                                if (questionResponseFromSubject1 != null)
                                {
                                    SurveyQuestionReponses.AddRange(questionResponseFromSubject1);
                                }
                            }
                        }



                    }
                    return SurveyQuestionReponses.OrderBy(a => a.SubmittedDate).ThenBy(a => a.Question).ToList();
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<List<SurveyFilterDto>> GetSurveyResponseFromSubject1(string subjectId, string question, string surveyId)
        {
            try
            {
                return await Task.Run(() =>
                {
                    List<SurveyFilterDto> SurveyQuestionReponses = new List<SurveyFilterDto>();
                    var surveyDetails = surveyCollection.Find(a => a.IsActive && a.Id == surveyId).FirstOrDefault();
                    if (surveyDetails != null)
                    {
                        var subjectDetails = surveyDetails.Patients.Where(a => a.Id == subjectId).ToList();
                        if (subjectDetails != null)
                        {
                            foreach (var insertedDate in subjectDetails.Select(a => a.DatesInfo))
                            {
                                foreach (var innerDate in insertedDate.Where(a => a.SubmittedDate != null))
                                {
                                    var combinedAnswer = "";
                                    var combinedTitle = "";
                                    var combinedDescription = "";
                                    var combinedImage = "";

                                    SurveyFilterDto surveyDateDetails = new SurveyFilterDto();
                                    surveyDateDetails.Question = question;
                                    surveyDateDetails.SubjectName = surveyDetails.Patients.FirstOrDefault(a => a.Id == subjectId).Name;
                                    surveyDateDetails.SubmittedDate = innerDate.SubmittedDate?.Date.ToString();
                                    var answerDetails = innerDate.QuestionsAndAnswers.Where(a => a.Question == question).Select(a => a.Answers).ToList();
                                    foreach (var answersGroup in answerDetails)
                                    {
                                        foreach (var answersGroupDetails in answersGroup)
                                        {
                                            combinedAnswer = combinedAnswer == "" ? answersGroupDetails.Answer : combinedAnswer + ";" + answersGroupDetails.Answer;
                                            combinedTitle = combinedTitle == "" ? answersGroupDetails.Title : combinedTitle + ";" + answersGroupDetails.Title;
                                            combinedDescription = combinedDescription == "" ? answersGroupDetails.Description : combinedDescription + ";" + answersGroupDetails.Description;
                                            combinedImage = combinedImage == "" ? answersGroupDetails.Image : combinedImage + ";" + answersGroupDetails.Image;
                                        }
                                    }
                                    surveyDateDetails.Answer = combinedAnswer;
                                    surveyDateDetails.Description = combinedDescription;
                                    surveyDateDetails.Title = combinedTitle;
                                    surveyDateDetails.ImagePath = combinedImage;
                                    SurveyQuestionReponses.Add(surveyDateDetails);
                                }
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    return SurveyQuestionReponses;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null; ;
            }
        }
        public async Task<List<SurveyFilterDto>> GetSurveyResponseFromSubject2(string subjectId, string question, string surveyId)
        {
            try
            {
                return await Task.Run(() =>
                {
                    List<SurveyFilterDto> SurveyQuestionReponses = new List<SurveyFilterDto>();
                    var surveyDetails = surveyCollection.Find(a => a.IsActive && a.Id == surveyId).FirstOrDefault();
                    if (surveyDetails != null)
                    {
                        var subjectDetails = surveyDetails.Patients.Where(a => a.Id == subjectId).ToList();
                        if (subjectDetails != null)
                        {
                            foreach (var insertedDate in subjectDetails.Select(a => a.DatesInfo))
                            {
                                foreach (var innerDate in insertedDate.Where(a => a.SubmittedDate != null))
                                {
                                    var combinedAnswer = "";
                                    var combinedTitle = "";
                                    var combinedDescription = "";
                                    var combinedImage = "";

                                    SurveyFilterDto surveyDateDetails = new SurveyFilterDto();
                                    surveyDateDetails.Question = question;
                                    surveyDateDetails.SubjectName = surveyDetails.Patients.FirstOrDefault(a => a.Id == subjectId).Name;
                                    surveyDateDetails.SubmittedDate = innerDate.SubmittedDate?.Date.ToString();
                                    var answerDetails = innerDate.QuestionsAndAnswers.Where(a => a.Question == question).Select(a => a.Answers).ToList();
                                    foreach (var answersGroup in answerDetails)
                                    {
                                        foreach (var answersGroupDetails in answersGroup)
                                        {
                                            combinedAnswer = combinedAnswer == "" ? answersGroupDetails.Answer : combinedAnswer + ";" + answersGroupDetails.Answer;
                                            combinedTitle = combinedTitle == "" ? answersGroupDetails.Title : combinedTitle + ";" + answersGroupDetails.Title;
                                            combinedDescription = combinedDescription == "" ? answersGroupDetails.Description : combinedDescription + ";" + answersGroupDetails.Description;
                                            combinedImage = combinedImage == "" ? answersGroupDetails.Image : combinedImage + ";" + answersGroupDetails.Image;
                                        }
                                    }
                                    surveyDateDetails.Answer = combinedAnswer;
                                    surveyDateDetails.Description = combinedDescription;
                                    surveyDateDetails.Title = combinedTitle;
                                    surveyDateDetails.ImagePath = combinedImage;
                                    SurveyQuestionReponses.Add(surveyDateDetails);
                                }
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    return SurveyQuestionReponses;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null; ;
            }
        }
        public async Task<List<SurveyFilterDto>> GetSurveyResponseFromSubject3(string subjectId, string question, string surveyId)
        {
            try
            {
                return await Task.Run(() =>
                {
                    List<SurveyFilterDto> SurveyQuestionReponses = new List<SurveyFilterDto>();
                    var surveyDetails = surveyCollection.Find(a => a.IsActive && a.Id == surveyId).FirstOrDefault();
                    if (surveyDetails != null)
                    {
                        var subjectDetails = surveyDetails.Patients.Where(a => a.Id == subjectId).ToList();
                        if (subjectDetails != null)
                        {
                            foreach (var insertedDate in subjectDetails.Select(a => a.DatesInfo))
                            {
                                foreach (var innerDate in insertedDate.Where(a => a.SubmittedDate != null))
                                {
                                    var combinedAnswer = "";
                                    var combinedTitle = "";
                                    var combinedDescription = "";
                                    var combinedImage = "";

                                    SurveyFilterDto surveyDateDetails = new SurveyFilterDto();
                                    surveyDateDetails.Question = question;
                                    surveyDateDetails.SubjectName = surveyDetails.Patients.FirstOrDefault(a => a.Id == subjectId).Name;
                                    surveyDateDetails.SubmittedDate = innerDate.SubmittedDate?.Date.ToString();
                                    var answerDetails = innerDate.QuestionsAndAnswers.Where(a => a.Question == question).Select(a => a.Answers).ToList();
                                    foreach (var answersGroup in answerDetails)
                                    {
                                        foreach (var answersGroupDetails in answersGroup)
                                        {
                                            combinedAnswer = combinedAnswer == "" ? answersGroupDetails.Answer : combinedAnswer + ";" + answersGroupDetails.Answer;
                                            combinedTitle = combinedTitle == "" ? answersGroupDetails.Title : combinedTitle + ";" + answersGroupDetails.Title;
                                            combinedDescription = combinedDescription == "" ? answersGroupDetails.Description : combinedDescription + ";" + answersGroupDetails.Description;
                                            combinedImage = combinedImage == "" ? answersGroupDetails.Image : combinedImage + ";" + answersGroupDetails.Image;
                                        }
                                    }
                                    surveyDateDetails.Answer = combinedAnswer;
                                    surveyDateDetails.Description = combinedDescription;
                                    surveyDateDetails.Title = combinedTitle;
                                    surveyDateDetails.ImagePath = combinedImage;
                                    SurveyQuestionReponses.Add(surveyDateDetails);
                                }
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    return SurveyQuestionReponses;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null; ;
            }
        }
        public async Task<List<SurveyFilterDto>> GetSurveyResponseFromSubject4(string subjectId, string question, string surveyId)
        {
            try
            {
                return await Task.Run(() =>
                {
                    List<SurveyFilterDto> SurveyQuestionReponses = new List<SurveyFilterDto>();
                    var surveyDetails = surveyCollection.Find(a => a.IsActive && a.Id == surveyId).FirstOrDefault();
                    if (surveyDetails != null)
                    {
                        var subjectDetails = surveyDetails.Patients.Where(a => a.Id == subjectId).ToList();
                        if (subjectDetails != null)
                        {
                            foreach (var insertedDate in subjectDetails.Select(a => a.DatesInfo))
                            {
                                foreach (var innerDate in insertedDate.Where(a => a.SubmittedDate != null))
                                {
                                    var combinedAnswer = "";
                                    var combinedTitle = "";
                                    var combinedDescription = "";
                                    var combinedImage = "";

                                    SurveyFilterDto surveyDateDetails = new SurveyFilterDto();
                                    surveyDateDetails.Question = question;
                                    surveyDateDetails.SubjectName = surveyDetails.Patients.FirstOrDefault(a => a.Id == subjectId).Name;
                                    surveyDateDetails.SubmittedDate = innerDate.SubmittedDate?.Date.ToString();
                                    var answerDetails = innerDate.QuestionsAndAnswers.Where(a => a.Question == question).Select(a => a.Answers).ToList();
                                    foreach (var answersGroup in answerDetails)
                                    {
                                        foreach (var answersGroupDetails in answersGroup)
                                        {
                                            combinedAnswer = combinedAnswer == "" ? answersGroupDetails.Answer : combinedAnswer + ";" + answersGroupDetails.Answer;
                                            combinedTitle = combinedTitle == "" ? answersGroupDetails.Title : combinedTitle + ";" + answersGroupDetails.Title;
                                            combinedDescription = combinedDescription == "" ? answersGroupDetails.Description : combinedDescription + ";" + answersGroupDetails.Description;
                                            combinedImage = combinedImage == "" ? answersGroupDetails.Image : combinedImage + ";" + answersGroupDetails.Image;
                                        }
                                    }
                                    surveyDateDetails.Answer = combinedAnswer;
                                    surveyDateDetails.Description = combinedDescription;
                                    surveyDateDetails.Title = combinedTitle;
                                    surveyDateDetails.ImagePath = combinedImage;
                                    SurveyQuestionReponses.Add(surveyDateDetails);
                                }
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    return SurveyQuestionReponses;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null; ;
            }
        }
        public async Task<List<SurveyFilterDto>> GetSurveyResponseFromSubject5(string subjectId, string question, string surveyId)
        {
            try
            {
                return await Task.Run(() =>
                {
                    List<SurveyFilterDto> SurveyQuestionReponses = new List<SurveyFilterDto>();
                    var surveyDetails = surveyCollection.Find(a => a.IsActive && a.Id == surveyId).FirstOrDefault();
                    if (surveyDetails != null)
                    {
                        var subjectDetails = surveyDetails.Patients.Where(a => a.Id == subjectId).ToList();
                        if (subjectDetails != null)
                        {
                            foreach (var insertedDate in subjectDetails.Select(a => a.DatesInfo))
                            {
                                foreach (var innerDate in insertedDate.Where(a => a.SubmittedDate != null))
                                {
                                    var combinedAnswer = "";
                                    var combinedTitle = "";
                                    var combinedDescription = "";
                                    var combinedImage = "";

                                    SurveyFilterDto surveyDateDetails = new SurveyFilterDto();
                                    surveyDateDetails.Question = question;
                                    surveyDateDetails.SubjectName = surveyDetails.Patients.FirstOrDefault(a => a.Id == subjectId).Name;
                                    surveyDateDetails.SubmittedDate = innerDate.SubmittedDate?.Date.ToString();
                                    var answerDetails = innerDate.QuestionsAndAnswers.Where(a => a.Question == question).Select(a => a.Answers).ToList();
                                    foreach (var answersGroup in answerDetails)
                                    {
                                        foreach (var answersGroupDetails in answersGroup)
                                        {
                                            combinedAnswer = combinedAnswer == "" ? answersGroupDetails.Answer : combinedAnswer + ";" + answersGroupDetails.Answer;
                                            combinedTitle = combinedTitle == "" ? answersGroupDetails.Title : combinedTitle + ";" + answersGroupDetails.Title;
                                            combinedDescription = combinedDescription == "" ? answersGroupDetails.Description : combinedDescription + ";" + answersGroupDetails.Description;
                                            combinedImage = combinedImage == "" ? answersGroupDetails.Image : combinedImage + ";" + answersGroupDetails.Image;
                                        }
                                    }
                                    surveyDateDetails.Answer = combinedAnswer;
                                    surveyDateDetails.Description = combinedDescription;
                                    surveyDateDetails.Title = combinedTitle;
                                    surveyDateDetails.ImagePath = combinedImage;
                                    SurveyQuestionReponses.Add(surveyDateDetails);
                                }
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    return SurveyQuestionReponses;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null; ;
            }
        }
    }
}
