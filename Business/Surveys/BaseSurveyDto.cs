﻿
namespace VireoIntegrativeProgram.Business.Surveys
{
    public class BaseSurveyDto
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string Frequency { get; set; }
    }
}
