﻿
namespace VireoIntegrativeProgram.Business.Surveys
{
    public class SurveyFilterDto
    {
        public string Question { get; set; }
        public string SubjectName { get; set; }
        public string SubmittedDate { get; set; }
        public string Answer { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
    }
}
