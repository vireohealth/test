﻿using System.Collections.Generic;

namespace VireoIntegrativeProgram.Business.Surveys
{
    public class QuestionAndAnswerDto
    {
        public string Questions { get; set; }
        public List<AnswersDto> Answers { get; set; }
    }
}