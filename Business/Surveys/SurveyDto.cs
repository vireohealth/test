﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using VireoIntegrativeProgram.Business.Frequencies;
using VireoIntegrativeProgram.Business.Products;
using VireoIntegrativeProgram.Business.Subjects;
using VireoIntegrativeProgram.Business.SurveyTypes;

namespace VireoIntegrativeProgram.Business.Surveys
{
    public class SurveyDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public ProductDto Product { get; set; }
        public FrequencyDto Frequency { get; set; }
        public SurveyTypesDto SurveyType { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastUpdatedOn { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? StartDate { get; set; }
        public bool IsActive { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? EndDate { get; set; }
        public List<PatientSurveyDetailsDto> Patients { get; set; }
        public string FormProperties { get; set; }
        public int TotalInputQuestions { get; set; }
        public string Status { get; set; }
        public int TimeToComplete { get; set; }
        public bool IsSelectedAll { get; set; }
        public List<QuestionAndAnswerDto> QuestionsAndAnswers { get; set; }
    }
}
