﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Helpers;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.Surveys
{
    public interface ISurveyService
    {
        SurveyDto AddSurvey(SurveyDto surveyDto);
        Task<PagedList<SurveyDto>> GetAllPaginationAsync(PaginationParams surveyParams);
        Task<SurveyDto> GetSurveyById(string id);
        void UpdateAsync(string id, SurveyDto surveyIn);
        Task<bool> CheckUniqueSurvey(string id, string surveyName);      
        void RemoveAsync(string id);
        Task<List<SurveyDto>> UpcomingSurveys(string subjectId);
        Task<string> GetSurveyQuestionsAndAnswers(string subjectId, string surveyId);
        bool UpdateSurveyQuestionsAndAnswersForSubject(SurveyAnswers surveyanswers);
        Task<List<SurveyDto>> GetSurveysBasedOnStatus(string subjectId, string status);
        Task<bool> FindAndUpdateToSelectAllSurveys(PatientSurveyDetails patientInfo);
        Task<List<SurveyDto>> GetOngoingSurveys(string subjectId);
        Task<List<BaseSubjectDto>> GetOtherPatients(string surveyId);
        Task<List<DateTime>> GetMissedSurveySubmissionDates(string surveyId, string subjectId);
        Task<bool> GetMedicineUsageStatus(string medicineId);
        Task<bool> IsSurveyStarted(string surveyId);
        Task<bool> FindAndUpdatePatientsToSurvey(string surveyId, List<PatientSurveyDetails> patientInfos);
        Task<bool> FindAndUpdateSurveysToCompleted();
        Task<string> GetNextDateOfSubmission(string surveyId, string subjectId);
        Task<bool> GetBodyPartsUsageStatus(string bodyPartName);
        Task<List<SurveyFilterDto>> GetAnswersBySubjectSurveyAndQuestions(SurveyReportParameter reportDetails);
        Task<List<BaseSurveyDto>> GetAllSurveys();
        Task<List<BasePatientDto>> GetAllPatientsInSurvey(string surveyId);
        Task<List<QuestionDto>> GetAllQuestionsInSurvey(string surveyId);
    }
}
