﻿
namespace VireoIntegrativeProgram.Business.Surveys
{
    public class BasePatientDto
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }
}   