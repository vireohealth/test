﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace VireoIntegrativeProgram.Business.Surveys
{
    public class PatientDatesInfoDto
    {
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? SubmittedDate { get; set; }
        public string FormPropertyValues { get; set; }
        public int TotalFilledInputQuestions { get; set; }
        public decimal CompletionPercent { get; set; }
        public bool IsCompleted { get; set; }
        public List<QuestionAndAnswerDto> QuestionsAndAnswers { get; set; }

        public PatientDatesInfoDto()
        {
            IsCompleted = false;
            SubmittedDate = DateTime.MinValue;
        }
    }
}
