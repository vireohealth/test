﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Helpers;

namespace VireoIntegrativeProgram.Business.JournalEntries
{
    public interface IJournalEntryService
    {
        JournalEntryDto AddJournalEntry(JournalEntryDto journalEntryDto);
        Task<PagedList<JournalEntryDto>> GetAllPaginationAsync(PaginationParams journalEntryParams,string subjectId);
        Task<JournalEntryDto> GetJournalEntryById(string id);
        void UpdateAsync(string id, JournalEntryDto journalEntryIn);
        Task<List<JournalEntryDto>> GetAllAsync(DateTime journalDate,string subjectId);
        Task<bool> CheckUniqueJournalEntry(string id, string title, string time, DateTime journalDate, string subjectId);
        Task<bool> CheckUniqueJournalEntryTime(string id, string time, DateTime journalDate, string subjectId);
    }
}
