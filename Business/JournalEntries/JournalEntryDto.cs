﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using VireoIntegrativeProgram.Business.Surveys;

namespace VireoIntegrativeProgram.Business.JournalEntries
{
    public class JournalEntryDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Title { get; set; }
        public string Time { get; set; }
        public string Notes { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime JournalDate { get; set; }
        public bool IsActive { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CreatedOn { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime LastUpdatedOn { get; set; }
        public BaseSubjectDto Patient { get; set; }
        public JournalEntryDto()
        {
            IsActive = true;
        }
    }
}
