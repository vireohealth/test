﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Helpers;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.JournalEntries
{
    public class JournalEntryService : BusinessService<IJournalEntryService>, IJournalEntryService
    {
        private readonly IMongoCollection<JournalEntry> journalEntryCollection;
        private readonly AppSettings appSettings;

        public JournalEntryService(DatabaseSettings databaseSettings,
            ILogger<IJournalEntryService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);

            journalEntryCollection = database.GetCollection<JournalEntry>(databaseSettings.JournalEntryCollectionName);
            journalEntryCollection.Indexes.CreateOneAsync(new CreateIndexModel<JournalEntry>(nameof(JournalEntry.Title)));
        }

        public JournalEntryDto AddJournalEntry(JournalEntryDto journalEntryDto)
        {
            var journalEntryDetails = FindJournalEntryByTitleTime(journalEntryDto.Title, journalEntryDto.Time, journalEntryDto.JournalDate, journalEntryDto.Patient.Id);
            if (journalEntryDetails != null)
            {
                return null;
            }

            var entity = journalEntryDto.ToEntity();
            entity.CreatedOn = DateTime.UtcNow;
            entity.LastUpdatedOn = DateTime.UtcNow;
            entity.IsActive = true;

            journalEntryCollection.InsertOne(entity);
            return FindJournalEntryByTitleTime(journalEntryDto.Title, journalEntryDto.Time, journalEntryDto.JournalDate, journalEntryDto.Patient.Id).ToDto();
        }
        private JournalEntry FindJournalEntryByTitleTime(string title, string time, DateTime journalDate, string subjectId)
        {
            return journalEntryCollection.Find(x => x.Title.ToLower() == title.ToLower()
            && x.Time.ToLower() == time.ToLower()
            && x.JournalDate == journalDate
            && x.IsActive == true
            && x.Patient.Id == subjectId)
                .FirstOrDefault();
        }

        public async Task<PagedList<JournalEntryDto>> GetAllPaginationAsync(PaginationParams journalEntryParams, string subjectId)
        {
            return await Task.Run(async () =>
            {
                var filteredJournalEntries = Builders<JournalEntry>.Filter.Where(journal => journal.Patient.Id == subjectId);

                if (!string.IsNullOrEmpty(journalEntryParams.Search))
                {
                    DateTime.TryParse(journalEntryParams.Search, out DateTime search);

                    filteredJournalEntries = Builders<JournalEntry>.Filter
                    .Where(JournalEntry => JournalEntry.JournalDate == search && JournalEntry.Patient.Id == subjectId);
                }

                var SortColumn = Builders<JournalEntry>.Sort.Descending(x => x.CreatedOn);

                if (!string.IsNullOrEmpty(journalEntryParams.SortCol))
                {
                    if ((!string.IsNullOrEmpty(journalEntryParams.SortDir)) && (journalEntryParams.SortDir == MessageConstants.AscendingSortDirection))
                    {
                        SortColumn = Builders<JournalEntry>.Sort.Ascending(journalEntryParams.SortCol);
                    }
                    else
                    {
                        SortColumn = Builders<JournalEntry>.Sort.Descending(journalEntryParams.SortCol);
                    }
                }
                var results = await journalEntryCollection.AggregateByPage(filteredJournalEntries, SortColumn,
                journalEntryParams.PageNumber, journalEntryParams.PageSize);

                return PagedList<JournalEntryDto>.CreatePageList(results.data.ToDtos(), results.totalPages,
                    journalEntryParams.PageNumber, journalEntryParams.PageSize);
            });
        }

        public async Task<JournalEntryDto> GetJournalEntryById(string id)
        {
            return await Task.Run(() => FindJournalEntryById(id).ToDto());
        }
        private JournalEntry FindJournalEntryById(string id)
        {
            return journalEntryCollection.Find(x => x.Id == id).FirstOrDefault();
        }
        public async void UpdateAsync(string id, JournalEntryDto journalEntryIn)
        {
            await Task.Run(() =>
            {
                journalEntryCollection.ReplaceOne(journalEntry => journalEntry.Id == id, journalEntryIn.ToEntity());
            });
        }
        public async Task<List<JournalEntryDto>> GetAllAsync(DateTime journalDate, string subjectId)
        {
            return await Task.Run(() =>
            {
                return journalEntryCollection.Find(journalEntry => journalEntry.JournalDate == journalDate && journalEntry.Patient.Id == subjectId).ToDtos().OrderBy(a => DateTime.ParseExact(a.Time,
                                         "hh:mm tt", CultureInfo.InvariantCulture).TimeOfDay).ToList();
            });
        }
        public async Task<bool> CheckUniqueJournalEntry(string id, string title, string time, DateTime journalDate, string subjectId)
        {
            var result = !string.IsNullOrEmpty(id)
                ? await Task.Run(() => CheckJournalEntryExists(id, title, time, journalDate, subjectId).ToDto())
                : await Task.Run(() => CheckJournalEntryExists(title, time, journalDate, subjectId).ToDto());
            return result != null;
        }

        private JournalEntry CheckJournalEntryExists(string id, string title, string time, DateTime journalDate, string subjectId)
        {
            return journalEntryCollection.Find(x => x.Title.ToLower() == title.ToLower() && x.Time.ToLower() == time.ToLower()
            && x.JournalDate == journalDate
            && x.IsActive && x.Id != id && x.Patient.Id == subjectId).FirstOrDefault();
        }

        private JournalEntry CheckJournalEntryExists(string title, string time, DateTime journalDate, string subjectId)
        {
            return journalEntryCollection.Find(x => x.Title.ToLower() == title.ToLower() && x.Time.ToLower() == time.ToLower()
            && x.JournalDate == journalDate
            && x.IsActive && x.Patient.Id == subjectId).FirstOrDefault();
        }
        public async Task<bool> CheckUniqueJournalEntryTime(string id, string time, DateTime journalDate, string subjectId)
        {
            var result = !string.IsNullOrEmpty(id)
                ? await Task.Run(() => CheckJournalEntryTimeExists(id, time, journalDate, subjectId).ToDto())
                : await Task.Run(() => CheckJournalEntryTimeExists( time, journalDate, subjectId).ToDto());
            return result != null;
        }

        private JournalEntry CheckJournalEntryTimeExists(string id, string time, DateTime journalDate, string subjectId)
        {
            return journalEntryCollection.Find(x => x.Time.ToLower() == time.ToLower()
            && x.JournalDate == journalDate
            && x.IsActive && x.Id != id && x.Patient.Id == subjectId).FirstOrDefault();
        }

        private JournalEntry CheckJournalEntryTimeExists(string time, DateTime journalDate, string subjectId)
        {
            return journalEntryCollection.Find(x => x.Time.ToLower() == time.ToLower()
            && x.JournalDate == journalDate
            && x.IsActive && x.Patient.Id == subjectId).FirstOrDefault();
        }
    }
}
