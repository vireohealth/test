﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Helpers;

namespace VireoIntegrativeProgram.Business.MedicationJournal
{
    public interface IMedicationJournalService
    {
        MedicationJournalDto AddMedicationJournal(MedicationJournalDto medicationJournalDto);
        Task<PagedList<MedicationJournalDto>> GetAllPaginationAsync(PaginationParams medicationJournals, string subjectId);
        Task<List<MedicationJournalDto>> GetAllAsync(DateTime medicationJournalDate, string subjectId);
        Task<MedicationJournalDto> GetMedicationJournalById(string id);
        void UpdateMedicationJournal(string id, MedicationJournalDto medicationJournal);
        Task<bool> CheckUniqueMedicationJournal(string id, string productName, string time, DateTime medicationJournalDate, string subjectId);
        Task<bool> CheckUniqueMedicationJournalTime(string id, string time, DateTime medicationJournalDate, string subjectId);
        Task<bool> GetMedicineUsageStatus(string medicineId);
    }
}
