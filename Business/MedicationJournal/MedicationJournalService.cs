﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Constants;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Helpers;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.MedicationJournal
{
    public class MedicationJournalService : BusinessService<IMedicationJournalService>, IMedicationJournalService
    {
        private readonly IMongoCollection<MedicationJournalEntry> medicationJournalCollection;
        private readonly AppSettings appSettings;

        public MedicationJournalService(DatabaseSettings databaseSettings,
            ILogger<IMedicationJournalService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);

            medicationJournalCollection = database.GetCollection<MedicationJournalEntry>(databaseSettings.MedicationJournalCollectionName);
            medicationJournalCollection.Indexes.CreateOneAsync(new CreateIndexModel<MedicationJournalEntry>(nameof(MedicationJournalEntry.Time)));
        }

        public MedicationJournalDto AddMedicationJournal(MedicationJournalDto medicationJournalDto)
        {
            var journalEntryDetails = FindMedicationJournalEntryByProductAndTime(medicationJournalDto.Product.Name, medicationJournalDto.Time, medicationJournalDto.MedicationJournalDate, medicationJournalDto.Patient.Id);
            if (journalEntryDetails != null)
            {
                return null;
            }

            var entity = medicationJournalDto.ToEntity();
            entity.CreatedOn = DateTime.UtcNow;
            entity.LastUpdatedOn = DateTime.UtcNow;
            entity.IsActive = true;

            medicationJournalCollection.InsertOne(entity);
            return FindMedicationJournalEntryByProductAndTime(medicationJournalDto.Product.Name, medicationJournalDto.Time, medicationJournalDto.MedicationJournalDate, medicationJournalDto.Patient.Id).ToDto();
        }

        private MedicationJournalEntry FindMedicationJournalEntryByProductAndTime(string productName, string time, DateTime medicationJournalDate, string subjectId)
        {
            return medicationJournalCollection.Find(x => x.Product.Name.ToLower() == productName.ToLower()
            && x.Time.ToLower() == time.ToLower()
            && x.MedicationJournalDate == medicationJournalDate
            && x.IsActive == true
            && x.Patient.Id == subjectId)
                .FirstOrDefault();
        }

        public async Task<PagedList<MedicationJournalDto>> GetAllPaginationAsync(PaginationParams medicationJournals, string subjectId)
        {
            return await Task.Run(async () =>
            {
                var filteredJournalEntries = Builders<MedicationJournalEntry>.Filter.Where(a => a.Patient.Id == subjectId);

                if (!string.IsNullOrEmpty(medicationJournals.Search))
                {
                    DateTime.TryParse(medicationJournals.Search, out DateTime search);

                    filteredJournalEntries = Builders<MedicationJournalEntry>.Filter
                    .Where(JournalEntry => JournalEntry.MedicationJournalDate == search && JournalEntry.Patient.Id == subjectId);
                }

                var SortColumn = Builders<MedicationJournalEntry>.Sort.Descending(x => x.CreatedOn);

                if (!string.IsNullOrEmpty(medicationJournals.SortCol))
                {
                    if ((!string.IsNullOrEmpty(medicationJournals.SortDir)) && (medicationJournals.SortDir == MessageConstants.AscendingSortDirection))
                    {
                        SortColumn = Builders<MedicationJournalEntry>.Sort.Ascending(medicationJournals.SortCol);
                    }
                    else
                    {
                        SortColumn = Builders<MedicationJournalEntry>.Sort.Descending(medicationJournals.SortCol);
                    }
                }
                var results = await medicationJournalCollection.AggregateByPage(filteredJournalEntries, SortColumn,
                medicationJournals.PageNumber, medicationJournals.PageSize);

                return PagedList<MedicationJournalDto>.CreatePageList(results.data.ToDtos(), results.totalPages,
                    medicationJournals.PageNumber, medicationJournals.PageSize);
            });
        }

        public async Task<List<MedicationJournalDto>> GetAllAsync(DateTime medicationJournalDate, string subjectId)
        {
            return await Task.Run(() =>
            {
                return medicationJournalCollection.Find(medicationJournal => medicationJournal.MedicationJournalDate == medicationJournalDate && medicationJournal.Patient.Id == subjectId).ToDtos().OrderBy(a => DateTime.ParseExact(a.Time,
                                         "hh:mm tt", CultureInfo.InvariantCulture).TimeOfDay).ToList();
            });
        }

        public async Task<MedicationJournalDto> GetMedicationJournalById(string id)
        {
            return await Task.Run(() => FindMedicationJournalById(id).ToDto());
        }

        private MedicationJournalEntry FindMedicationJournalById(string id)
        {
            return medicationJournalCollection.Find(x => x.Id == id).FirstOrDefault();
        }

        public async void UpdateMedicationJournal(string id, MedicationJournalDto medicationJournal)
        {
            await Task.Run(() =>
            {
                medicationJournalCollection.ReplaceOne(medicationJournal => medicationJournal.Id == id, medicationJournal.ToEntity());
            });
        }

        public async Task<bool> CheckUniqueMedicationJournal(string id, string productName, string time, DateTime medicationJournalDate, string subjectId)
        {
            var result = !string.IsNullOrEmpty(id)
                ? await Task.Run(() => CheckMedicationJournalExists(id, productName, time, medicationJournalDate, subjectId).ToDto())
                : await Task.Run(() => CheckMedicationJournalExists(productName, time, medicationJournalDate, subjectId).ToDto());
            return result != null;
        }

        private MedicationJournalEntry CheckMedicationJournalExists(string id, string productName, string time, DateTime medicationJournalDate, string subjectId)
        {
            return medicationJournalCollection.Find(x => x.Product.Name.ToLower() == productName.ToLower() && x.Time.ToLower() == time.ToLower()
            && x.MedicationJournalDate == medicationJournalDate
            && x.IsActive && x.Id != id && x.Patient.Id == subjectId).FirstOrDefault();
        }

        private MedicationJournalEntry CheckMedicationJournalExists(string productName, string time, DateTime medicationJournalDate, string subjectId)
        {
            return medicationJournalCollection.Find(x => x.Product.Name.ToLower() == productName.ToLower() && x.Time.ToLower() == time.ToLower()
            && x.MedicationJournalDate == medicationJournalDate
            && x.IsActive && x.Patient.Id == subjectId).FirstOrDefault();
        }

        public async Task<bool> CheckUniqueMedicationJournalTime(string id, string time, DateTime medicationJournalDate, string subjectId)
        {
            var result = !string.IsNullOrEmpty(id)
                ? await Task.Run(() => CheckMedicationJournalTimeExists(id, time, medicationJournalDate, subjectId).ToDto())
                : await Task.Run(() => CheckMedicationJournalTimeExists(time, medicationJournalDate, subjectId).ToDto());
            return result != null;
        }

        private MedicationJournalEntry CheckMedicationJournalTimeExists(string id, string time, DateTime medicationJournalDate, string subjectId)
        {
            return medicationJournalCollection.Find(x => x.Time.ToLower() == time.ToLower()
            && x.MedicationJournalDate == medicationJournalDate
            && x.IsActive && x.Id != id && x.Patient.Id == subjectId).FirstOrDefault();
        }

        private MedicationJournalEntry CheckMedicationJournalTimeExists(string time, DateTime medicationJournalDate, string subjectId)
        {
            return medicationJournalCollection.Find(x => x.Time.ToLower() == time.ToLower()
            && x.MedicationJournalDate == medicationJournalDate
            && x.IsActive && x.Patient.Id == subjectId).FirstOrDefault();
        }

        public async Task<bool> GetMedicineUsageStatus(string medicineId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var medicineDetails = medicationJournalCollection.Find(a => a.IsActive && a.Product.Id == medicineId).FirstOrDefault();
                    if (medicineDetails != null)
                    {
                        return true;
                    }
                    return false;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return false;
            }
        }
    }
}
