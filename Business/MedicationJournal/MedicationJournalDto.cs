﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using VireoIntegrativeProgram.Business.Products;
using VireoIntegrativeProgram.Business.Surveys;

namespace VireoIntegrativeProgram.Business.MedicationJournal
{
    public class MedicationJournalDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Time { get; set; }
        public string Notes { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime MedicationJournalDate { get; set; }
        public bool IsActive { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CreatedOn { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime LastUpdatedOn { get; set; }
        public BaseSubjectDto Patient { get; set; }
        public ProductDto Product { get; set; }
        public MedicationJournalDto()
        {
            IsActive = true;
        }
    }
}
