﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.SubjectLogs;
using VireoIntegrativeProgram.Business.Subjects;
using VireoIntegrativeProgram.Common.Core;
using VireoIntegrativeProgram.Common.Dtos;
using VireoIntegrativeProgram.Common.Enums;
using VireoIntegrativeProgram.Extensions;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Business.Logs
{
    public class LogsService : BusinessService<ILogsService>, ILogsService
    {
        private readonly IMongoCollection<Models.SubjectLogs> logCollection;
        private const int RoundValue = 2;
        private const int TotalWeekDays = 7;
        private const int TotalMonthDays = 30;
        private const string ImageFolder = "Images";
        private readonly AppSettings appSettings;
        public LogsService(DatabaseSettings databaseSettings,
            ILogger<ILogsService> logger,
            IOptions<AppSettings> appSettings) : base(logger)
        {
            this.appSettings = appSettings.Value;

            var client = new MongoClient(databaseSettings.ConnectionString);
            var database = client.GetDatabase(databaseSettings.DatabaseName);
            logCollection = database.GetCollection<Models.SubjectLogs>(databaseSettings.LogCollectionName);
            logCollection.Indexes.CreateOneAsync(new CreateIndexModel<Models.SubjectLogs>(nameof(Models.SubjectLogs.Id)));
        }

        public async Task<LogDto> GetLogByDateAndSubjectId(DateTime dateSelected, string subjectId)
        {
            return await Task.Run(() =>
            {
                // var result = logCollection.Find(x => x.Subject.Id == subjectId && x.DateOfLog == dateSelected.Date).FirstOrDefault().ToDto();
                var result = logCollection.Find(x => x.Subject.Id == subjectId).ToList();
                foreach (var logDetail in result)
                {
                    DateTime dateOfLog = logDetail.DateOfLog;
                    if (dateOfLog.Date == dateSelected.Date)
                    {
                        return logDetail.ToDto();
                    }
                }
                // return result != null ? result : null;
                return null;
            });
        }

        public LogDto UpsertLog(LogDto logDto)
        {
            var entity = logDto.ToEntity();

            entity.LastUpdatedOn = DateTime.UtcNow;
            entity.IsActive = true;
            var logDetail = GetLogByDateAndSubjectId(logDto.DateOfLog, logDto.Subject.Id);

            if (logDetail.Result == null)
            {
                entity.AddedOn = DateTime.UtcNow;
                logCollection.InsertOne(entity);
            }
            else
            {
                entity.Id = logDetail.Result.Id;
                entity.AddedOn = logDetail.Result.AddedOn;
                logCollection.ReplaceOne(logdetail => logdetail.Id == entity.Id, entity);
            }

            return logDto;
        }

        public async Task<LogDto> GetLogById(string id)
        {
            return await Task.Run(() => FindLogById(id).ToDto());
        }

        private Models.SubjectLogs FindLogById(string id)
        {
            return logCollection.Find(x => x.Id == id).FirstOrDefault();
        }
        public async Task<List<CommunityInsightDto>> GetFeedbackOtherUsers(string dateCategory, string patientId, List<SubjectDto> patients)
        {
            return await Task.Run(() =>
            {
                List<CommunityInsightDto> communityDto = new List<CommunityInsightDto>();

                if (DateCategory.Day == dateCategory)
                {
                    var logs = GetLogsByDateForOtherUsersFeedback(DateTime.Today, patientId);
                    var feedbackInfos = GetFeedbackDetails(logs.Result);

                    var totalVireoProducts = feedbackInfos.Count;
                    var totalEffectivePercentage = (totalVireoProducts > 0) ? feedbackInfos.Sum(i => i.EffectivePercent) : 0;
                    var totalNonEffectivePercentage = (totalVireoProducts > 0) ? feedbackInfos.Sum(i => i.NonEffectivePercent) : 0;

                    communityDto.Add(new CommunityInsightDto
                    {
                        Name = FeedbackType.Effective,
                        Percentage = (totalVireoProducts > 0) ?
                        Math.Round(totalEffectivePercentage / (decimal)totalVireoProducts, RoundValue) : 0
                    });

                    communityDto.Add(new CommunityInsightDto
                    {
                        Name = FeedbackType.NotEffective,
                        Percentage = (totalVireoProducts > 0) ?
                        Math.Round(totalNonEffectivePercentage / (decimal)totalVireoProducts, RoundValue) : 0
                    });
                }
                else if (DateCategory.Week == dateCategory)
                {
                    DateTime endDate = DateTime.Today;
                    DateTime startDate = DateTime.Today.AddDays(-TotalWeekDays);
                    CalculateFeedbackPercentage(patientId, endDate, startDate, out int totalLoggedDays, out decimal totalEffectivePercentage, out decimal totalNonEffectivePercentage);

                    communityDto.Add(new CommunityInsightDto
                    {
                        Name = FeedbackType.Effective,
                        Percentage = (totalLoggedDays > 0) ?
                        Math.Round(totalEffectivePercentage / (decimal)totalLoggedDays, RoundValue) : 0
                    });
                    communityDto.Add(new CommunityInsightDto
                    {
                        Name = FeedbackType.NotEffective,
                        Percentage = (totalLoggedDays > 0) ?
                        Math.Round(totalNonEffectivePercentage / (decimal)totalLoggedDays, RoundValue) : 0
                    });

                }
                else if (DateCategory.Month == dateCategory)
                {
                    DateTime endDate = DateTime.Today;
                    DateTime startDate = DateTime.Today.AddDays(-TotalMonthDays);

                    CalculateFeedbackPercentage(patientId, endDate, startDate, out int totalLoggedDays, out decimal totalEffectivePercentage, out decimal totalNonEffectivePercentage);

                    communityDto.Add(new CommunityInsightDto
                    {
                        Name = FeedbackType.Effective,
                        Percentage = (totalLoggedDays > 0) ?
                        Math.Round(totalEffectivePercentage / (decimal)totalLoggedDays, RoundValue) : 0
                    });
                    communityDto.Add(new CommunityInsightDto
                    {
                        Name = FeedbackType.NotEffective,
                        Percentage = (totalLoggedDays > 0) ?
                        Math.Round(totalNonEffectivePercentage / (decimal)totalLoggedDays, RoundValue) : 0
                    });
                }
                communityDto = communityDto.OrderByDescending(i => i.Percentage).ToList();
                return communityDto;
            });
        }

        private void CalculateFeedbackPercentage(string patientId, DateTime endDate, DateTime startDate, out int totalLoggedDays, out decimal totalEffectivePercentage, out decimal totalNonEffectivePercentage)
        {
            totalLoggedDays = 0;
            var dates = GetDates(startDate, endDate);

            totalEffectivePercentage = 0;
            totalNonEffectivePercentage = 0;
            foreach (var date in dates)
            {
                var logs = GetLogsByDateForOtherUsersFeedback(date, patientId);
                var feedbackInfos = GetFeedbackDetails(logs.Result);

                var totalVireoProducts = feedbackInfos.Count;
                if (totalVireoProducts > 0)
                    totalLoggedDays += 1;

                totalEffectivePercentage += (totalVireoProducts > 0) ? Math.Round(feedbackInfos.Sum(i => i.EffectivePercent) / (decimal)totalVireoProducts, RoundValue) : 0;
                totalNonEffectivePercentage += (totalVireoProducts > 0) ? Math.Round(feedbackInfos.Sum(i => i.NonEffectivePercent) / (decimal)totalVireoProducts, RoundValue) : 0;
            }
        }

        public async Task<List<CommunityInsightDto>> GetPainEffectSleepForOtherUsers(string dateCategory, string patientId, List<SubjectDto> patients)
        {
            return await Task.Run(() =>
            {
                List<CommunityInsightDto> communityDto = new List<CommunityInsightDto>();

                // Find total patients
                var totalPatients = patients.Count;
                if (DateCategory.Day == dateCategory)
                {

                    // Find total logged users in particular date
                    var logs = GetLogsByDateForOtherUsers(DateTime.Today, patientId);
                    var totalLoggedPatients = logs.Result.Count;
                    // Find Pain sleep type count
                    var excellentCount = GetPainSleepTypeCount(PainSleepType.Excellent, logs.Result);
                    var goodCount = GetPainSleepTypeCount(PainSleepType.Good, logs.Result);
                    var adequateCount = GetPainSleepTypeCount(PainSleepType.Adequate, logs.Result);
                    var disturbedCount = GetPainSleepTypeCount(PainSleepType.Disturbed, logs.Result);
                    var noSleepCount = GetPainSleepTypeCount(PainSleepType.NoSleep, logs.Result);


                    // Find pain sleep type percent and set in model
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.Excellent, Percentage = GetPercent(excellentCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.Good, Percentage = GetPercent(goodCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.Adequate, Percentage = GetPercent(adequateCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.Disturbed, Percentage = GetPercent(disturbedCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.NoSleep, Percentage = GetPercent(noSleepCount, totalLoggedPatients) });

                }
                else if (DateCategory.Week == dateCategory)
                {
                    DateTime endDate = DateTime.Today;
                    DateTime startDate = DateTime.Today.AddDays(-TotalWeekDays);
                    decimal excellentTotalPercent, goodTotalPercent, adequateTotalPercent, disturbedTotalPercent, noSleepTotalPercent;

                    GetPainSleepTypeTotalPercentageByDates(patientId, totalPatients, endDate, startDate, out excellentTotalPercent,
                        out goodTotalPercent, out adequateTotalPercent, out disturbedTotalPercent, out noSleepTotalPercent);

                    // Find pain sleep type percent and set in model
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.Excellent, Percentage = excellentTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.Good, Percentage = goodTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.Adequate, Percentage = adequateTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.Disturbed, Percentage = disturbedTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.NoSleep, Percentage = noSleepTotalPercent });
                }
                else if (DateCategory.Month == dateCategory)
                {
                    DateTime endDate = DateTime.Today;
                    DateTime startDate = DateTime.Today.AddDays(-TotalMonthDays);
                    decimal excellentTotalPercent, goodTotalPercent, adequateTotalPercent, disturbedTotalPercent, noSleepTotalPercent;

                    GetPainSleepTypeTotalPercentageByDates(patientId, totalPatients, endDate, startDate, out excellentTotalPercent,
                        out goodTotalPercent, out adequateTotalPercent, out disturbedTotalPercent, out noSleepTotalPercent);

                    // Find pain sleep type percent and set in model
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.Excellent, Percentage = excellentTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.Good, Percentage = goodTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.Adequate, Percentage = adequateTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.Disturbed, Percentage = disturbedTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainSleepType.NoSleep, Percentage = noSleepTotalPercent });
                }
                communityDto = communityDto.OrderByDescending(i => i.Percentage).ToList();
                return communityDto;
            });
        }
        private void GetPainSleepTypeTotalPercentageByDates(string patientId, int totalPatients, DateTime endDate, DateTime startDate, out decimal excellentTotalPercent, out decimal goodTotalPercent,
            out decimal adequateTotalPercent, out decimal disturbedTotalPercent, out decimal noSleepTotalPercent)
        {
            var dates = GetDates(startDate, endDate);
            excellentTotalPercent = 0;
            goodTotalPercent = 0;
            adequateTotalPercent = 0;
            disturbedTotalPercent = 0;
            noSleepTotalPercent = 0;
            int totalLoggedDays = 0;
            foreach (var date in dates)
            {
                var logs = GetLogsByDateForOtherUsers(date, patientId);
                var totalLoggedPatients = logs.Result.Count;
                if (totalLoggedPatients > 0)
                {
                    totalLoggedDays += 1;
                    // Find Pain sleep type count
                    var excellentCount = GetPainSleepTypeCount(PainSleepType.Excellent, logs.Result);
                    var goodCount = GetPainSleepTypeCount(PainSleepType.Good, logs.Result);
                    var adequateCount = GetPainSleepTypeCount(PainSleepType.Adequate, logs.Result);
                    var disturbedCount = GetPainSleepTypeCount(PainSleepType.Disturbed, logs.Result);
                    var noSleepCount = GetPainSleepTypeCount(PainSleepType.NoSleep, logs.Result);


                    var excellentPercent = GetPercent(excellentCount, totalLoggedPatients);
                    var goodPercent = GetPercent(goodCount, totalLoggedPatients);
                    var adequatePercent = GetPercent(adequateCount, totalLoggedPatients);
                    var disturbedPercent = GetPercent(disturbedCount, totalLoggedPatients);
                    var noSleepPercent = GetPercent(noSleepCount, totalLoggedPatients);

                    excellentTotalPercent += excellentPercent;
                    goodTotalPercent += goodPercent;
                    adequateTotalPercent += adequatePercent;
                    disturbedTotalPercent += disturbedPercent;
                    noSleepTotalPercent += noSleepPercent;
                }
            }

            noSleepTotalPercent = Math.Round(noSleepTotalPercent / (decimal)totalLoggedDays, RoundValue);
            excellentTotalPercent = Math.Round(excellentTotalPercent / (decimal)totalLoggedDays, RoundValue);
            goodTotalPercent = Math.Round(goodTotalPercent / (decimal)totalLoggedDays, RoundValue);
            adequateTotalPercent = Math.Round(adequateTotalPercent / (decimal)totalLoggedDays, RoundValue);
            disturbedTotalPercent = Math.Round(disturbedTotalPercent / (decimal)totalLoggedDays, RoundValue);
        }

        public async Task<List<CommunityInsightDto>> GetTopPainLoggedOtherUsers(string dateCategory, string patientId, List<SubjectDto> patients)
        {
            return await Task.Run(() =>
            {
                List<CommunityInsightDto> communityDto = new List<CommunityInsightDto>();

                // Find total patients
                var totalPatients = patients.Count;
                if (DateCategory.Day == dateCategory)
                {

                    // Find total logged users in particular date
                    var logs = GetLogsByDateForOtherUsers(DateTime.Today, patientId);
                    var totalLoggedPatients = logs.Result.Count;
                    // Find Pain type count
                    var burningCount = GetPainTypeCount(PainType.Burning, logs.Result);
                    var achingCount = GetPainTypeCount(PainType.Aching, logs.Result);
                    var itchingCount = GetPainTypeCount(PainType.Itching, logs.Result);
                    var noPainCount = GetPainTypeCount(PainType.NoPain, logs.Result);
                    var numbnessCount = GetPainTypeCount(PainType.Numbness, logs.Result);
                    var shootingCount = GetPainTypeCount(PainType.Shooting, logs.Result);
                    var othersCount = GetPainTypeCount(PainType.Others, logs.Result);

                    // Find pain type percent and set in model
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Burning, Percentage = GetPercent(burningCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Aching, Percentage = GetPercent(achingCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Itching, Percentage = GetPercent(itchingCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.NoPain, Percentage = GetPercent(noPainCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Numbness, Percentage = GetPercent(numbnessCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Shooting, Percentage = GetPercent(shootingCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Others, Percentage = GetPercent(othersCount, totalLoggedPatients) });


                }
                else if (DateCategory.Week == dateCategory)
                {
                    DateTime endDate = DateTime.Today;
                    DateTime startDate = DateTime.Today.AddDays(-TotalWeekDays);
                    decimal burningTotalPercent, achingTotalPercent, itchingTotalPercent, noPainTotalPercent,
                    shootingTotalPercent, numbnessTotalPercent, othersTotalPercent;

                    GetPainTypeTotalPercentageByDates(patientId, endDate, startDate, out burningTotalPercent,
                        out achingTotalPercent, out itchingTotalPercent, out noPainTotalPercent,
                        out shootingTotalPercent, out numbnessTotalPercent, out othersTotalPercent);

                    // Find pain type percent and set in model
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Burning, Percentage = burningTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Aching, Percentage = achingTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Itching, Percentage = itchingTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.NoPain, Percentage = noPainTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Numbness, Percentage = numbnessTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Shooting, Percentage = shootingTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Others, Percentage = othersTotalPercent });

                }
                else if (DateCategory.Month == dateCategory)
                {
                    DateTime endDate = DateTime.Today;
                    DateTime startDate = DateTime.Today.AddDays(-TotalMonthDays);
                    decimal burningTotalPercent, achingTotalPercent, itchingTotalPercent,
                    noPainTotalPercent, shootingTotalPercent, numbnessTotalPercent, othersTotalPercent;

                    GetPainTypeTotalPercentageByDates(patientId, endDate, startDate,
                        out burningTotalPercent, out achingTotalPercent, out itchingTotalPercent,
                        out noPainTotalPercent, out shootingTotalPercent, out numbnessTotalPercent, out othersTotalPercent);

                    // Find pain type percent and set in model
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Burning, Percentage = burningTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Aching, Percentage = achingTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Itching, Percentage = itchingTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.NoPain, Percentage = noPainTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Numbness, Percentage = numbnessTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Shooting, Percentage = shootingTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Name = PainType.Others, Percentage = othersTotalPercent });

                }
                communityDto = communityDto.OrderByDescending(i => i.Percentage).Take(3).ToList();
                return communityDto;
            });
        }

        public async Task<List<CommunityInsightDto>> GetTopTreatmentsForOtherUsers(string dateCategory, string patientId, List<SubjectDto> patients)
        {
            return await Task.Run(() =>
            {
                List<CommunityInsightDto> communityDto = new List<CommunityInsightDto>();
                string uploadUrl = appSettings.APIPath + Path.DirectorySeparatorChar.ToString()
             + ImageFolder;
                // Find total patients
                var totalPatients = patients.Count;
                if (DateCategory.Day == dateCategory)
                {

                    // Find total logged users in particular date
                    var logs = GetLogsByDateForOtherUsers(DateTime.Today, patientId);
                    var totalLoggedPatients = logs.Result.Count;
                    // Find treatment count
                    var acupressureCount = GetTreatmentCount(PainTreatment.Acupressure, logs.Result);
                    var acupunctureCount = GetTreatmentCount(PainTreatment.Acupuncture, logs.Result);
                    var bedRestCount = GetTreatmentCount(PainTreatment.BedRest, logs.Result);
                    var cryotherapyCount = GetTreatmentCount(PainTreatment.Cryotherapy, logs.Result);
                    var cuppingCount = GetTreatmentCount(PainTreatment.Cupping, logs.Result);
                    var orthoticsCount = GetTreatmentCount(PainTreatment.Orthotics, logs.Result);
                    var painPumpCount = GetTreatmentCount(PainTreatment.PainPump, logs.Result);
                    var heatCount = GetTreatmentCount(PainTreatment.Heat, logs.Result);

                    // Find treatment percent and set in model
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Acupressure), Name = PainTreatment.Acupressure, Percentage = GetPercent(acupressureCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Acupuncture), Name = PainTreatment.Acupuncture, Percentage = GetPercent(acupunctureCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.BedRest), Name = PainTreatment.BedRest, Percentage = GetPercent(bedRestCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Cryotherapy), Name = PainTreatment.Cryotherapy, Percentage = GetPercent(cryotherapyCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Cupping), Name = PainTreatment.Cupping, Percentage = GetPercent(cuppingCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Orthotics), Name = PainTreatment.Orthotics, Percentage = GetPercent(orthoticsCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.PainPump), Name = PainTreatment.PainPump, Percentage = GetPercent(painPumpCount, totalLoggedPatients) });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Heat), Name = PainTreatment.Heat, Percentage = GetPercent(heatCount, totalLoggedPatients) });
                }
                else if (DateCategory.Week == dateCategory)
                {
                    DateTime endDate = DateTime.Today;
                    DateTime startDate = DateTime.Today.AddDays(-TotalWeekDays);
                    decimal acupressureTotalPercent, acupunctureTotalPercent, bedRestTotalPercent, cryotherapyTotalPercent, cuppingTotalPercent, orthoticsTotalPercent, painPumpTotalPercent, heatTotalPercent;

                    GetPainTreatmentTotalPercentageByDates(patientId, endDate, startDate, out acupressureTotalPercent
, out acupunctureTotalPercent, out bedRestTotalPercent, out cryotherapyTotalPercent, out cuppingTotalPercent, out orthoticsTotalPercent, out painPumpTotalPercent, out heatTotalPercent);

                    // Find treatment percent and set in model
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Acupressure), Name = PainTreatment.Acupressure, Percentage = acupressureTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Acupuncture), Name = PainTreatment.Acupuncture, Percentage = acupunctureTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.BedRest), Name = PainTreatment.BedRest, Percentage = bedRestTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Cryotherapy), Name = PainTreatment.Cryotherapy, Percentage = cryotherapyTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Cupping), Name = PainTreatment.Cupping, Percentage = cuppingTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Orthotics), Name = PainTreatment.Orthotics, Percentage = orthoticsTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.PainPump), Name = PainTreatment.PainPump, Percentage = painPumpTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Heat), Name = PainTreatment.Heat, Percentage = heatTotalPercent });

                }
                else if (DateCategory.Month == dateCategory)
                {
                    DateTime endDate = DateTime.Today;
                    DateTime startDate = DateTime.Today.AddDays(-TotalMonthDays);
                    decimal acupressureTotalPercent, acupunctureTotalPercent, bedRestTotalPercent, cryotherapyTotalPercent, cuppingTotalPercent,
                    orthoticsTotalPercent, painPumpTotalPercent, heatTotalPercent;

                    GetPainTreatmentTotalPercentageByDates(patientId, endDate, startDate, out acupressureTotalPercent, out acupunctureTotalPercent, out bedRestTotalPercent,
                        out cryotherapyTotalPercent, out cuppingTotalPercent, out orthoticsTotalPercent, out painPumpTotalPercent, out heatTotalPercent);

                    // Find treatment percent and set in model
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Acupressure), Name = PainTreatment.Acupressure, Percentage = acupressureTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Acupuncture), Name = PainTreatment.Acupuncture, Percentage = acupunctureTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.BedRest), Name = PainTreatment.BedRest, Percentage = bedRestTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Cryotherapy), Name = PainTreatment.Cryotherapy, Percentage = cryotherapyTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Cupping), Name = PainTreatment.Cupping, Percentage = cuppingTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Orthotics), Name = PainTreatment.Orthotics, Percentage = orthoticsTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.PainPump), Name = PainTreatment.PainPump, Percentage = painPumpTotalPercent });
                    communityDto.Add(new CommunityInsightDto { Icon = Path.Combine(uploadUrl, PainTreatmentIcon.Heat), Name = PainTreatment.Heat, Percentage = heatTotalPercent });
                }
                communityDto = communityDto.OrderByDescending(i => i.Percentage).Take(3).ToList();
                return communityDto;
            });
        }
        private void GetPainTreatmentTotalPercentageByDates(string patientId, DateTime endDate, DateTime startDate, out decimal acupressureTotalPercent, out decimal acupunctureTotalPercent,
            out decimal bedRestTotalPercent, out decimal cryotherapyTotalPercent, out decimal cuppingTotalPercent, out decimal orthoticsTotalPercent,
            out decimal painPumpTotalPercent, out decimal heatTotalPercent)
        {
            var dates = GetDates(startDate, endDate);
            acupressureTotalPercent = 0;
            acupunctureTotalPercent = 0;
            bedRestTotalPercent = 0;
            cryotherapyTotalPercent = 0;
            cuppingTotalPercent = 0;
            orthoticsTotalPercent = 0;
            painPumpTotalPercent = 0;
            heatTotalPercent = 0;
            int totalLoggedDays = 0;
            foreach (var date in dates)
            {
                var logs = GetLogsByDateForOtherUsers(date, patientId);
                var totalLoggedPatients = logs.Result.Count;
                if (totalLoggedPatients > 0)
                {
                    totalLoggedDays += 1;
                    // Find Pain type count
                    var acupressureCount = GetTreatmentCount(PainTreatment.Acupressure, logs.Result);
                    var acupunctureCount = GetTreatmentCount(PainTreatment.Acupuncture, logs.Result);
                    var bedRestCount = GetTreatmentCount(PainTreatment.BedRest, logs.Result);
                    var cryotherapyCount = GetTreatmentCount(PainTreatment.Cryotherapy, logs.Result);
                    var cuppingCount = GetTreatmentCount(PainTreatment.Cupping, logs.Result);
                    var orthoticsCount = GetTreatmentCount(PainTreatment.Orthotics, logs.Result);
                    var painPumpCount = GetTreatmentCount(PainTreatment.PainPump, logs.Result);
                    var heatCount = GetTreatmentCount(PainTreatment.Heat, logs.Result);

                    var acupressurePercent = GetPercent(acupressureCount, totalLoggedPatients);
                    var acupuncturePercent = GetPercent(acupunctureCount, totalLoggedPatients);
                    var bedRestPercent = GetPercent(bedRestCount, totalLoggedPatients);
                    var cryotherapyPercent = GetPercent(cryotherapyCount, totalLoggedPatients);
                    var cuppingPercent = GetPercent(cuppingCount, totalLoggedPatients);
                    var orthoticsPercent = GetPercent(orthoticsCount, totalLoggedPatients);
                    var painPumpPercent = GetPercent(painPumpCount, totalLoggedPatients);
                    var heatPercent = GetPercent(heatCount, totalLoggedPatients);

                    orthoticsTotalPercent += orthoticsPercent;
                    cuppingTotalPercent += cuppingPercent;
                    acupressureTotalPercent += acupressurePercent;
                    acupunctureTotalPercent += acupuncturePercent;
                    bedRestTotalPercent += bedRestPercent;
                    cryotherapyTotalPercent += cryotherapyPercent;
                    painPumpTotalPercent += painPumpPercent;
                    heatTotalPercent += heatPercent;
                }
            }

            orthoticsTotalPercent = Math.Round(orthoticsTotalPercent / (decimal)totalLoggedDays, RoundValue);
            cuppingTotalPercent = Math.Round(cuppingTotalPercent / (decimal)totalLoggedDays, RoundValue);
            acupressureTotalPercent = Math.Round(acupressureTotalPercent / (decimal)totalLoggedDays, RoundValue);
            acupunctureTotalPercent = Math.Round(acupunctureTotalPercent / (decimal)totalLoggedDays, RoundValue);
            bedRestTotalPercent = Math.Round(bedRestTotalPercent / (decimal)totalLoggedDays, RoundValue);
            cryotherapyTotalPercent = Math.Round(cryotherapyTotalPercent / (decimal)totalLoggedDays, RoundValue);
            painPumpTotalPercent = Math.Round(painPumpTotalPercent / (decimal)totalLoggedDays, RoundValue);
            heatTotalPercent = Math.Round(heatTotalPercent / (decimal)totalLoggedDays, RoundValue);
        }


        private void GetPainTypeTotalPercentageByDates(string patientId, DateTime endDate, DateTime startDate,
            out decimal burningTotalPercent, out decimal achingTotalPercent, out decimal itchingTotalPercent,
            out decimal noPainTotalPercent, out decimal shootingTotalPercent, out decimal numbnessTotalPercent, out decimal othersTotalPercent)
        {
            var dates = GetDates(startDate, endDate);
            burningTotalPercent = 0;
            achingTotalPercent = 0;
            itchingTotalPercent = 0;
            noPainTotalPercent = 0;
            shootingTotalPercent = 0;
            numbnessTotalPercent = 0;
            othersTotalPercent = 0;
            int totalLoggedDays = 0;
            foreach (var date in dates)
            {
                var logs = GetLogsByDateForOtherUsers(date, patientId);
                var totalLoggedPatients = logs.Result.Count;
                if (totalLoggedPatients > 0)
                {
                    totalLoggedDays += 1;
                    // Find Pain type count
                    var burningCount = GetPainTypeCount(PainType.Burning, logs.Result);
                    var achingCount = GetPainTypeCount(PainType.Aching, logs.Result);
                    var itchingCount = GetPainTypeCount(PainType.Itching, logs.Result);
                    var noPainCount = GetPainTypeCount(PainType.NoPain, logs.Result);
                    var numbnessCount = GetPainTypeCount(PainType.Numbness, logs.Result);
                    var shootingCount = GetPainTypeCount(PainType.Shooting, logs.Result);
                    var othersCount = GetPainTypeCount(PainType.Others, logs.Result);

                    var burningPercent = GetPercent(burningCount, totalLoggedPatients);
                    var achingPercent = GetPercent(achingCount, totalLoggedPatients);
                    var itchingPercent = GetPercent(itchingCount, totalLoggedPatients);
                    var noPainPercent = GetPercent(noPainCount, totalLoggedPatients);
                    var numbnessPercent = GetPercent(numbnessCount, totalLoggedPatients);
                    var shootingPercent = GetPercent(shootingCount, totalLoggedPatients);
                    var othersPercent = GetPercent(othersCount, totalLoggedPatients);

                    numbnessTotalPercent += numbnessPercent;
                    shootingTotalPercent += shootingPercent;
                    burningTotalPercent += burningPercent;
                    achingTotalPercent += achingPercent;
                    itchingTotalPercent += itchingPercent;
                    noPainTotalPercent += noPainPercent;
                    othersTotalPercent += othersPercent;
                }
            }

            numbnessTotalPercent = Math.Round(numbnessTotalPercent / (decimal)totalLoggedDays, RoundValue);
            shootingTotalPercent = Math.Round(shootingTotalPercent / (decimal)totalLoggedDays, RoundValue);
            burningTotalPercent = Math.Round(burningTotalPercent / (decimal)totalLoggedDays, RoundValue);
            achingTotalPercent = Math.Round(achingTotalPercent / (decimal)totalLoggedDays, RoundValue);
            itchingTotalPercent = Math.Round(itchingTotalPercent / (decimal)totalLoggedDays, RoundValue);
            noPainTotalPercent = Math.Round(noPainTotalPercent / (decimal)totalLoggedDays, RoundValue);
            othersTotalPercent = Math.Round(othersTotalPercent / (decimal)totalLoggedDays, RoundValue);
        }

        private List<DateTime> GetDates(DateTime startDate, DateTime endDate)
        {
            var dates = new List<DateTime>();
            for (var dt = endDate; dt > startDate; dt = dt.AddDays(-1))
            {
                dates.Add(dt);
            }
            return dates;
        }

        private async Task<List<LogDto>> GetLogsByDateForOtherUsers(DateTime date, string patientId)
        {
            return await Task.Run(() => GetSubjectLogsByDateForOtherUsers(patientId).ToDtos().Where(x => x.DateOfLog.Date == date.Date).ToList());
        }

        private List<Models.SubjectLogs> GetSubjectLogsByDateForOtherUsers(string patientId)
        {
            return logCollection.Find(x => x.Subject.Id != patientId).ToList();
        }

        private async Task<List<LogDto>> GetLogsByDateForOtherUsersFeedback(DateTime date, string patientId)
        {
            return await Task.Run(() => GetSubjectLogsByDateForOtherUsersFeedback(patientId).ToDtos().Where(x => x.DateOfLog.Date == date.Date).ToList());
        }

        private List<Models.SubjectLogs> GetSubjectLogsByDateForOtherUsersFeedback(string patientId)
        {
            return logCollection.Find(x => x.Subject.Id != patientId && x.Medications.Any(i => !string.IsNullOrEmpty(i.Feedback))).ToList();
        }
        private List<FeedBackInfoDto> GetFeedbackDetails(List<LogDto> logs)
        {
            List<FeedBackInfoDto> feedbackInfos = new List<FeedBackInfoDto>();
            List<FeedBackInfoDto> allFeedbackInfos = new List<FeedBackInfoDto>();
            List<FeedBackInfoDto> infos;

            foreach (var log in logs)
            {
                infos = new List<FeedBackInfoDto>();
                foreach (var product in log.Medications)
                {
                    infos.Add(new FeedBackInfoDto
                    {
                        ProductId = string.IsNullOrEmpty(product.FeedbackProductId) ? "" : product.FeedbackProductId,
                        EffectiveCount = string.IsNullOrEmpty(product.Feedback) ? 0 : product.Feedback.ToLower() == FeedbackType.Effective.ToString().ToLower() ? 1 : 0,
                        NonEffectiveCount = string.IsNullOrEmpty(product.Feedback) ? 0 : product.Feedback.ToLower() == FeedbackType.NotEffective.ToString().ToLower() ? 1 : 0
                    });
                }
                feedbackInfos.AddRange(infos);
            }

            var productFeedbackGroup = feedbackInfos.GroupBy(i => i.ProductId);
            foreach (var feedbackGroup in productFeedbackGroup)
            {
                var effectiveCount = feedbackGroup.Sum(i => i.EffectiveCount);
                var notEffectiveCount = feedbackGroup.Sum(i => i.NonEffectiveCount);
                var totalLoggedUsers = effectiveCount + notEffectiveCount;
                allFeedbackInfos.Add(new FeedBackInfoDto
                {
                    ProductId = feedbackGroup.Key,
                    EffectiveCount = effectiveCount,
                    NonEffectiveCount = notEffectiveCount,
                    TotalUsersLogged = totalLoggedUsers,
                    EffectivePercent = GetPercent(effectiveCount, totalLoggedUsers),
                    NonEffectivePercent = GetPercent(notEffectiveCount, totalLoggedUsers)
                });
            }
            return allFeedbackInfos;
        }

        private int GetTreatmentCount(string treatment, List<LogDto> logs)
        {
            int count = 0;
            logs.ForEach(log =>
            {

                if (log.Treatments.Contains(treatment))
                {
                    count += 1;
                }
            });
            return count;
        }
        private int GetPainTypeCount(string painType, List<LogDto> logs)
        {
            int count = 0;
            logs.ForEach(log =>
            {

                if (log.PainType.Contains(painType))
                {
                    count += 1;
                }
            });
            return count;
        }
        private int GetPainSleepTypeCount(string painSleepType, List<LogDto> logs)
        {
            int count = 0;
            logs.ForEach(log =>
            {

                if (log.Sleep.Contains(painSleepType))
                {
                    count += 1;
                }
            });
            return count;
        }
        private decimal GetPercent(int count, int totalLoggedPatients)
        {
            decimal percentage = 0;
            //  percentage = (((decimal)(painTypeCount * totalLoggedPatients) / (decimal)(totalPatients * totalPatients)) * 100);
            if (totalLoggedPatients > 0)
            {
                percentage = (((decimal)(count) / (decimal)(totalLoggedPatients)) * 100);
            }

            return Math.Round(percentage, RoundValue);
        }
        public async Task<bool> GetMedicineUsageStatus(string medicineId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var medicineDetails = logCollection.Find(a => a.IsActive && a.Medications.Any(a => a.Medication.Id == medicineId)).FirstOrDefault();
                    if (medicineDetails != null)
                    {
                        return true;
                    }
                    return false;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return false;
            }
        }

        public async Task<bool> CheckFeedbackProvidedEarlier(string subjectId, string productId)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var result = logCollection.Find(a => a.IsActive && a.Medications.Any(a => a.FeedbackProductId == productId)).ToList();
                    bool feedbackProvided = result != null ? result.Any(a => a.Subject.Id == subjectId) : false;
                    return feedbackProvided;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return false;
            }
        }
        public async Task<bool> CheckAlreadyProductLogged(DateTime dateSelected, string subjectId, string productId)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var result = logCollection.Find(x => x.Subject.Id == subjectId && x.Medications.Any(i => i.Medication.Id == productId) && x.DateOfLog < dateSelected)
                    .FirstOrDefault().ToDto();
                    return result != null;
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return false;
            }
        }
    }
}
