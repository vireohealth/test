﻿namespace VireoIntegrativeProgram.Business.Logs
{
    public class CommunityInsightDto
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public decimal Percentage { get; set; }
    }
}
