﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VireoIntegrativeProgram.Business.SubjectLogs
{
    public class FeedBackInfoDto
    {
        public string ProductId { get; set; }
        public int EffectiveCount { get; set; }
        public int NonEffectiveCount { get; set; }
        public int TotalUsersLogged { get; set; }
        public decimal EffectivePercent { get; set; }
        public decimal NonEffectivePercent { get; set; }
    }
}
