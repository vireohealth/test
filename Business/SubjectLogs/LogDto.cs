﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VireoIntegrativeProgram.Business.Surveys;

namespace VireoIntegrativeProgram.Business.Logs
{
    public class LogDto 
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public List<string> Triggers { get; set; }
        [Required]
        public List<string> PainType { get; set; }
        public List<MedicationDosageDto> Medications { get; set; }
        [Required]
        public List<string> Sleep { get; set; }
        [Required]
        public List<string> Treatments { get; set; }
        public List<string> PainLocation { get; set; }
        public int? BodySide { get; set; } // 0 for Front and 1 for Back
      
        public List<string> LevelOfPain { get; set; }
        public string Notes { get; set; }
        public string SleepDisturbNotes { get; set; }
        public string PainTypeOthersNotes { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime DateOfLog { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime AddedOn { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime LastUpdatedOn { get; set; }
        public BaseSubjectDto Subject { get; set; }
        public bool IsActive { get; set; }
        public LogDto()
        {
            IsActive = true;
        }
    }
}
