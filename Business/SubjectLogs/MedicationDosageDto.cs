﻿using VireoIntegrativeProgram.Business.Medications;

namespace VireoIntegrativeProgram.Business.Logs
{
    public class MedicationDosageDto 
    {
        public MedicationDto Medication { get; set; }
        public string Dosage { get; set; }
        public string Feedback { get; set; }
        public string FeedbackProductId { get; set; }
    }
}
