﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Subjects;

namespace VireoIntegrativeProgram.Business.Logs
{
    public interface ILogsService
    {
        Task<LogDto> GetLogByDateAndSubjectId(DateTime dateSelected, string subjectId);
        LogDto UpsertLog(LogDto logDto);
        Task<List<CommunityInsightDto>> GetTopPainLoggedOtherUsers(string dateCategory, string patientId, List<SubjectDto> patients);
        Task<List<CommunityInsightDto>> GetTopTreatmentsForOtherUsers(string dateCategory, string patientId, List<SubjectDto> patients);
        Task<List<CommunityInsightDto>> GetPainEffectSleepForOtherUsers(string dateCategory, string patientId, List<SubjectDto> patients);
        Task<List<CommunityInsightDto>> GetFeedbackOtherUsers(string dateCategory, string patientId, List<SubjectDto> patients);
        Task<bool> GetMedicineUsageStatus(string medicineId);
        Task<bool> CheckFeedbackProvidedEarlier(string subjectId, string productId);
        Task<bool> CheckAlreadyProductLogged(DateTime dateSelected, string subjectId, string productId);
    }
}
