﻿

namespace VireoIntegrativeProgram.Constants
{
    public static class MessageConstants
    {
        public static readonly string SuccessMessage = "Success";
        public static readonly string AscendingSortDirection = "asc";
        public static readonly string DecendingSortDirection = "desc";
        public static readonly string SubjectsFetchMessage = "Subjects fetched successfully";
        public static readonly string SubjectNotFoundMessage = "Subject not found";
        public static readonly string SubjectListFetchFailureMessage = "Subject list fetched failed";

        public static readonly string ProductsFetchMessage = "Products fetched successfully";
        public static readonly string ProductsFetchFailureMessage = "Products fetched failed";

        public static readonly string FormControlFetchSuccessMessage = "Form controls fetched successfully";
        public static readonly string FormControlFetchFailureMessage = "Form controls fetching failed";

        public static readonly string SurveyTypesFetchSuccessMessage = "Survey types fetched successfully";
        public static readonly string SurveyTypesFetchFailureMessage = "Survey types fetching failed";

        public static readonly string SurveyAddedSuccessMessage = "Survey added successfully";
        public static readonly string FrequenciesFetchMessage = "Frequencies fetched successfully";
        public static readonly string FrequenciesFetchFailureMessage = "Frequencies fetching failed";

        public static readonly string SurveysFetchMessage = "Surveys fetched successfully";
        public static readonly string SurveyNotFoundMessage = "Survey not found";
        public static readonly string SurveyUpdatedMessage = "Survey updated successfully";

        public static readonly string SurveyUniqueCheckAvailableMessage = "Survey name available";
        public static readonly string SurveyUniqueCheckConflictMessage = "Survey name not available";

        public static readonly string SurveyDetailsFetchSuccessMessage = "Survey details fetched successfully";
        public static readonly string SurveyDetailsFetchFailureMessage = "Survey details fetching failed";

        public static readonly string SurveyDeleteSuccessMessage = "Survey deleted successfully";
        public static readonly string SurveyDeleteFailureMessage = "Survey deletion failed";

        public static readonly string UnitOfMeasureSuccessMessage = "Unit of measure fetched successfully";
        public static readonly string UnitOfMeasureFailureMessage = "Unit of measure fetching failed";

        public static readonly string LoginFailedMessage = "user name or password is invalid";
        public static readonly string LoginPasswordExpiredMessage = "Password expired";
        public static readonly string LoginSuccessMessage = "User-authenticated successfully";
        public static readonly string RefreshTokenInvalidMessage = "Refresh token is invalid";
        public static readonly string RefreshTokenSuccessMessage = "Refresh token created successfully";
        public static readonly string RevokeTokenInvalidMessage = "Revoke token is required";
        public static readonly string RevokeTokenNotFoundMessage = "Revoke token not found";
        public static readonly string RevokeTokenSuccessMessage = "Revoke token created successfully";
        public static readonly string RefreshTokenUserNotFoundMessage = "Revoke token not found for user";
        public static readonly string RefreshTokenUserSuccessMessage = "Revoke token created successfully for user";

        public static readonly string MailVerificationSuccessMessage = "Mail verification done successfully";
        public static readonly string MailVerificationFailureMessage = "Mail verification failed";

        public static readonly string CheckResetLinkExpiredMessage = "Reset link has been expired";
        public static readonly string CheckResetLinkAllowedMessage = "Reset link not expired";

        public static readonly string SetPasswordFailureMessage = "Setting password encountered some issues";
        public static readonly string SetPasswordSuccessMessage = "Password updated successfully";
        public static readonly string CheckLastThreePasswordMatchMessage = "Last three password has been matched";
        public static readonly string SubjectNameExistsMessage = "Subject name exists";
        public static readonly string SubjectNameAvailableMessage = "Subject name available";

        public static readonly string StateNotFoundMessage = "States not found";
        public static readonly string StateSuccessMessage = "States fetched successfully";

        public static readonly string EMailVerificationSuccessMessage = "Email verification done successfully";
        public static readonly string EMailVerificationFailureMessage = "Email verification failed";

        public static readonly string SubjectFailureMessage = "Unable to add subject";
        public static readonly string SubjectCreatedMessage = "Subject added successfully";

        public static readonly string NotAccessibleMessage = "User cannot access";
        public static readonly string UserCanAccessMessage = "User can access";

        public static readonly string SubjectUpdatedMessage = "Subject updated successfully";
        public static readonly string EducationalVideosFetchMessage = "Educational videos fetched successfully";
        public static readonly string EducationalVideosFetchFailureMessage = "Educational videos fetching failed";
        public static readonly string EducationalVideoNotFoundMessage = "Educational video not found";

        public static readonly string SubjectDetailsFetchSuccessMessage = "Subject details fetched successfully";
        public static readonly string SubjectDetailsFetchFailureMessage = "Subject details fetching failed";

        public static readonly string SubjectEmailExistsMessage = "Subject email exists";
        public static readonly string SubjectEmailAvailableMessage = "Subject email available";

        public static readonly string SubjectDeleteSuccessMessage = "Subject deleted successfully";
        public static readonly string SubjectDeleteFailureMessage = "Subject deletion failed";

        public static readonly string ImageUploadedSuccessMessage = "Thumb image is uploaded successfully";
        public static readonly string ImageUploadedFormatIssueMessage = "Thumb image must be of formats such as JPG, GIF, or PNG";
        public static readonly string ImageUploadedSizeIssueMessage = "Thumb image is larger than 2 MB";
        public static readonly string ImageUploadedWidthIssueMessage = "Thumb image's must be";
        public static readonly string NoImageUploadedMessage = "No image uploaded";
        public static readonly string EducationalVideoAddedSuccessMessage = "Educational video added successfully";
        public static readonly string EducationalVideoUpdatedMessage = "Educational video updated successfully";
        public static readonly string EducationalVideoDeleteSuccessMessage = "Educational video deleted successfully";
        public static readonly string EducationalVideoDeleteFailureMessage = "Educational video deletion failed";
        public static readonly string EducationalVideoDetailsFetchSuccessMessage = "Educational video details fetched successfully";
        public static readonly string EducationalVideoDetailsFetchFailureMessage = "Educational video details fetching failed";
        public static readonly string EducationalVideoUniqueCheckAvailableMessage = "Educational video title/video path available";
        public static readonly string EducationalVideoUniqueCheckConflictMessage = "Educational video title/video path not available";

        public static readonly string RecommendationsFetchSuccessMessage = "Recommendations fetched successfully";
        public static readonly string RecommendationsFetchFailureMessage = "Recommendations fetching failed";

        public static readonly string DosingAddedSuccessMessage = "Dosing added successfully";
        public static readonly string RecommendationAddedSuccessMessage = "Recommendation added successfully";
        public static readonly string RecommendationUniqueCheckAvailableMessage = "Recommendation is available";
        public static readonly string RecommendationUniqueCheckConflictMessage = "Recommendation is not available";

        public static readonly string DosingFetchSuccessMessage = "Dosing fetched successfully";
        public static readonly string DosingFetchFailureMessage = "Dosing fetching failed";

        public static readonly string DosingUniqueCheckAvailableMessage = "Dosing is available";
        public static readonly string DosingUniqueCheckConflictMessage = "Dosing is not available";
        public static readonly string DosingDeleteSuccessMessage = "Dosing deleted successfully";
        public static readonly string DosingDeleteFailureMessage = "Dosing deletion failed";
        public static readonly string MenuGetSuccessMessage = "Menu fetched successfully";
        public static readonly string MenuGetFailureMessage = "Menu fetched failed";

        public static readonly string SurveyListByIdFetchSuccessMessage = "Survey list fetched successfully";
        public static readonly string SurveyListByIdFetchFailureMessage = "Survey list fetching failed";

        public static readonly string LogsFetchSuccessMessage = "Logs fetched successfully";
        public static readonly string LogsFetchFailureMessage = "logs fetching failed";
        public static readonly string LogsSavedSuccessMessage = "Logs saved successfully";

        public static readonly string ProductNameExistsMessage = "Product name exists";
        public static readonly string ProductNameAvailableMessage = "Product name available";
        public static readonly string ProductNotFoundMessage = "Product not found";
        public static readonly string ProductUpdatedMessage = "Product updated successfully";
        public static readonly string ProductDetailsFetchSuccessMessage = "Product details fetched successfully";
        public static readonly string ProductDetailsFetchFailureMessage = "Product details fetching failed";
        public static readonly string ProductDeleteSuccessMessage = "Product deleted successfully";
        public static readonly string ProductDeleteFailureMessage = "Product deletion failed";

        public static readonly string JournalEntryAddedSuccessMessage = "Journal entry added successfully";
        public static readonly string JournalEntriesFetchMessage = "Journal entries fetched successfully";
        public static readonly string JournalEntryNotFoundMessage = "Journal entry not found";
        public static readonly string JournalEntryUpdatedMessage = "Journal entry updated successfully";
        public static readonly string JournalEntryDetailsFetchSuccessMessage = "Journal entry details fetched successfully";
        public static readonly string JournalEntryDetailsFetchFailureMessage = "Journal entry details fetching failed";
        public static readonly string JournalEntryExistsMessage = "Journal entry exists";
        public static readonly string JournalEntryAvailableMessage = "Journal entry available";
        public static readonly string JournalEntriesFetchFailureMessage = "Journal entries fetching failed";

        public static readonly string RecommendationDetailsFetchSuccessMessage = "Recommendation details fetched successfully";
        public static readonly string RecommendationDetailsFetchFailureMessage = "Recommendation details fetching failed";
        public static readonly string RecommendationNotFoundMessage = "Recommendation not found";
        public static readonly string RecommendationUpdatedMessage = "Recommendation updated successfully";
        public static readonly string RecommendationDeleteSuccessMessage = "Recommendation deleted successfully";
        public static readonly string RecommendationDeleteFailureMessage = "Recommendation deletion failed";

        public static readonly string RecommendationsFetchMessage = "Recommendations fetched successfully";
        public static readonly string RecommendationsNotFoundMessage = "Recommendations not found";

        public static readonly string MedicationJournalEntriesFetchMessage = "Medication journal entries fetched successfully";
        public static readonly string MedicationJournalEntryNotFoundMessage = "Medication journal entry not found";
        public static readonly string MedicationJournalAddedSuccessMessage = "Medication journal added successfully";
        public static readonly string MedicationJournalDetailsFetchSuccessMessage = "Medication journal details fetched successfully";
        public static readonly string MedicationJournalDetailsFetchFailureMessage = "Medication journal details fetching failed";
        public static readonly string MedicationJournalIdNotFoundMessage = "Medication journal not found";
        public static readonly string MedicationJournalUpdatedMessage = "Medication journal updated successfully";
        public static readonly string MedicationJournalExistsMessage = "Medication journal exists";
        public static readonly string MedicationJournalAvailableMessage = "Medication journal available";

        public static readonly string SurveyQuestionsFetchSuccessMessage = "Survey questions fetched successfully";
        public static readonly string SurveyQuestionsFetchFailureMessage = "Survey questions fetched failed";

        public static readonly string SurveyQuestionsUpdatedSuccessMessage = "Survey questions updated successfully";

        public static readonly string SurveyListByStatusFetchSuccessMessage = "Survey list fetched successfully";
        public static readonly string SurveyListByStatusFetchFailureMessage = "Survey list fetching failed";

        public static readonly string TopPainForOtherUsersFetchSuccessMessage = "Top pain logged for other users fetched successfully";
        public static readonly string TopPainForOtherUsersFetchFailureMessage = "Top pain logged for other users fetching failed";
        public static readonly string TopTreatmentsForOtherUsersFetchSuccessMessage = "Top treatments for other users fetched successfully";
        public static readonly string TopTreatmentsForOtherUsersFetchFailureMessage = "Top treatments for other users fetching failed";
        public static readonly string PainEffectSleepForOtherUsersFetchSuccessMessage = "Effect of pain in sleep for other users fetched successfully";
        public static readonly string PainEffectSleepForOtherUsersFetchFailureMessage = "Effect of pain in sleep for other users fetching failed";
        public static readonly string FeedbackOtherUsersFetchSuccessMessage = "Soft-Gel capsule feedback from other users fetched successfully";
        public static readonly string FeedbackOtherUsersFetchFailureMessage = "Soft-Gel capsule feedback from other users fetching failed";

        public static readonly string LogsAndJournalsCountFetchSuccessMessage = "Logs and journals fetched successfully";
        public static readonly string LogsAndJournalsCountFetchFailureMessage = "Logs and journals fetching failed";

        public static readonly string EffectsOfPainInSleepFetchSuccessMessage = "Effect of pain in sleep fetched successfully";
        public static readonly string EffectsOfPainInSleepFetchFailureMessage = "Effect of pain in sleep fetching failed";

        public static readonly string EffectsOfPainInMoodFetchSuccessMessage = "Effect of pain in mood fetched successfully";
        public static readonly string EffectsOfPainInMoodFetchFailureMessage = "Effect of pain in mood fetching failed";

        public static readonly string RecommendationBasedOnLogFetchSuccessMessage = "Recommendation for subject fetched successfully";
        public static readonly string RecommendationBasedOnLogFetchFailureMessage = "Recommendation for subject fetching failed";

        public static readonly string DatesOfPainInSleepFetchSuccessMessage = "Dates of pain in sleep fetched successfully";
        public static readonly string DatesOfPainInSleepFetchFailureMessage = "Dates of pain in sleep fetching failed";

        public static readonly string SurveyOngoingListFetchSuccessMessage = "Ongoing survey list fetched successfully";
        public static readonly string SurveyOngoingListFetchFailureMessage = "Ongoing survey list fetching failed";

        public static readonly string SurveyOtherPatientsListFetchSuccessMessage = "Other Patients list fetched successfully";
        public static readonly string SurveyOtherPatientsListFetchFailureMessage = "Other Patients list fetching failed";

        public static readonly string SurveyUnsubmittedDatesListFetchSuccessMessage = "Unsubmitted survey list fetched successfully";
        public static readonly string SurveyUnsubmittedDatesListFetchFailureMessage = "Unsubmitted survey list fetching failed";

        public static readonly string SurveyMedicineCheckConflictMessage = "Medicine used in surveys/Logs/Dosage/Journals are not available for deletion";
        public static readonly string SurveyMedicineCheckNoConflictMessage = "Medicine  available for deletion";
        public static readonly string SurveyStartedMessage = "Survey has started, so cannot edit";
        public static readonly string SurveyNotStartedMessage = "Survey has not started yet, so proceed with updation";

        public static readonly string BodyPartsFetchMessage = "Body parts fetched successfully";
        public static readonly string BodyPartsFetchFailureMessage = "Body parts fetched failed";

        public static readonly string BodyPartsImageUploadedSuccessMessage = "Body parts image is uploaded successfully";
        public static readonly string BodyPartsImageUploadedFormatIssueMessage = "Body parts image must be of formats such as JPG, GIF, or PNG";
        public static readonly string BodyPartsImageUploadedSizeIssueMessage = "Body parts image is larger than 2 MB";
        public static readonly string BodyPartsImageUploadedWidthIssueMessage = "Body parts image's must be";

        public static readonly string BodyPartsAddedSuccessMessage = "Body parts added successfully";

        public static readonly string PatientRegisterFailureMessage = "Unable to register patient";
        public static readonly string PatientRegisterCreatedMessage = "Patient registered successfully";

        public static readonly string TempSubjectDetailsFetchSuccessMessage = "Temp subject fetched successfully";
        public static readonly string TempSubjectDetailsFetchFailureMessage = "Subject already registered";

        public static readonly string EMailSignUpVerificationFailureMessage = "Email already activated";

        public static readonly string SubjectDetailsByIdFetchSuccessMessage = "Temp subject details fetched successfully";
        public static readonly string SubjectDetailsByIdFetchFailureMessage = "Email not available and registered";

        public static readonly string SurveyNextDateFetchSuccessMessage = "Survey next date fetched successfully";
        public static readonly string SurveyNextDateFetchFailureMessage = "Survey next date fetched failed";

        public static readonly string EMailVerificationRegisteredMessage = "Email is registered";
        public static readonly string BodyPartDeleteSuccessMessage = "Body part deleted successfully";
        public static readonly string BodyPartDeleteFailureMessage = "Body part deletion failed";
        public static readonly string BodyPartUniqueCheckAvailableMessage = "Body part name is available";
        public static readonly string BodyPartUniqueCheckConflictMessage = "Body part is not available";

        public static readonly string BodyPartDetailsFetchSuccessMessage = "Body part details fetched successfully";
        public static readonly string BodyPartDetailsFetchFailureMessage = "Body part details fetching failed";

        public static readonly string BodyPartsUpdationSuccessMessage = "Body part updated successfully";
        public static readonly string BodyPartsUpdationFailureMessage = "Body part updation failed";

        public static readonly string BodyPartAvailableForDeletionMessage = "Body part available for deletion";
        public static readonly string BodyPartNotAvailableForDeletionMessage = "Body part used in surveys not available for deletion";

        public static readonly string BodyPartsPaginatedFetchMessage = "Body part list fetched successfully";
        public static readonly string BodyPartsPaginatedNotFoundMessage = "Body part list not found";
        public static readonly string FitbitAccessTokenMessage = "Access token information generated successfully";
        public static readonly string FitbitRefreshAccessTokenMessage = "Refresh token information generated successfully";
        public static readonly string FitBitIntegratedExpired = "FitBit either expired/not integrated";
        public static readonly string FitBitActivityStatus = "Activity info fetched successfully";
        public static readonly string FitBitSleepStatus = "Sleep info fetched successfully";
        public static readonly string FitBitHeartRateStatus = "Heart rate info fetched successfully";

        public static readonly string RecommendationSettingAddedSuccessMessage = "Recommendation setting added successfully";
        public static readonly string RecommendationSettingFetchSuccessMessage = "Recommendation settings fetched successfully";
        public static readonly string RecommendationSettingFetchFailureMessage = "Recommendation settings fetching failed";
        public static readonly string RecommendationSettingUniqueCheckAvailableMessage = "Recommendation setting is available";
        public static readonly string RecommendationSettingUniqueCheckConflictMessage = "Recommendation setting is not available";
        public static readonly string RecommendationSettingDeleteSuccessMessage = "Recommendation setting deleted successfully";
        public static readonly string RecommendationSettingDeleteFailureMessage = "Recommendation setting deletion failed";
        public static readonly string FitbitRevokeAccessTokenMessage = "Revoke token triggered successfully";
        public static readonly string FitbitAlreadyIntegratedMessage = "This fitbit account is already integrated with other user";

        public static readonly string SurveyReportsFetchSuccessMessage = "Survey reports fetched successfully";
        public static readonly string SurveyReportsFetchFailureMessage = "Survey reports fetching failed";

        public static readonly string SurveysListFetchedSuccessMessage = "Survey fetched successfully";
        public static readonly string SurveysListFetchedFailureMessage = "Survey  fetched failed";

        public static readonly string SurveysPatientsListFetchedSuccessMessage = "Survey patients fetched successfully";
        public static readonly string SurveysPatientsListFetchedFailureMessage = "Survey patients  fetched failed";

        public static readonly string SurveysQuestionsListFetchedSuccessMessage = "Survey questions fetched successfully";
        public static readonly string SurveysQuestionsListFetchedFailureMessage = "Survey questions  fetched failed";

        public static readonly string ProductFeedbackAllowedMessage = "Feedback can be provided for the medicine selected";
        public static readonly string ProductFeedbackNotAllowedMessage = "Feedback not allowed for the product selected";

        public static readonly string ProfilePicUploadedSuccessMessage = "Profile picture is uploaded successfully";
        public static readonly string ProfilePicUploadedFormatIssueMessage = "Profile picture must be of formats such as JPG, GIF, or PNG";
        public static readonly string ProfilePicUploadedSizeIssueMessage = "Profile picture is larger than 2 MB";
        public static readonly string ProfilePicUploadedWidthIssueMessage = "Profile picture must be";
        public static readonly string ProfilePicDeleteSuccessMessage = "Profile picture deleted successfully";
        public static readonly string ProfilePicDeleteFailureMessage = "Profile picture deletion failed";

        public static readonly string CheckCurrentPasswordMatchMessage = "Current password is not valid";       
        public static readonly string PasswordRequiredCharactersMessage = "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character";
        public static readonly string ConfirmPasswordMessage = "Your new password and confirm password do not match";
    }
}
