﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.UnitOfMeasurements;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class UnitOfMeasurementExtensions
    {
        public static UnitOfMeasurementDTO ToDto(this UnitOfMeasure entity)
        {
            if (entity == null)
                return null;

            return new UnitOfMeasurementDTO
            {
                Id = entity.Id,
                Unit = entity.Unit,
                Description = entity.Description,
                MeasurementSystem = entity.System.ToMeasurementTypeDto(),
                Created = entity.Created,
                LastUpdatedOn = entity.LastUpdatedOn,
                IsActive = entity.IsActive,
                ConversionUnits = entity.ConversionUnits.ToConversionUnitDtos()
            };
        }
        public static ConversionUnitDto ToConversionUnitDto(this ConversionUnit entity)
        {
            if (entity == null)
                return null;

            return new ConversionUnitDto
            {
                Id = entity.Id,
                Unit = entity.Unit,
                Value = entity.Value
            };
        }
        public static MeasurementsTypeDto ToMeasurementTypeDto(this MeasurementType entity)
        {
            if (entity == null)
                return null;

            return new MeasurementsTypeDto
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }
        public static List<ConversionUnitDto> ToConversionUnitDtos(this IReadOnlyList<ConversionUnit> conversionUnitEntity)
        {
            return conversionUnitEntity.ToList().Select(x => x.ToConversionUnitDto()).ToList();
        }

        public static MeasurementType ToMeasurementTypeEntity(this MeasurementsTypeDto dto)
        {
            if (dto == null)
                return null;

            return new MeasurementType
            {
                Id = dto.Id,
                Name = dto.Name
            };
        }

        public static UnitOfMeasure ToEntity(this UnitOfMeasurementDTO dto)
        {
            if (dto == null)
                return null;

            return new UnitOfMeasure
            {
                Id = dto.Id,
                Description = dto.Description,
                System = dto.MeasurementSystem.ToMeasurementTypeEntity(),
                Unit = dto.Unit,
                IsActive = dto.IsActive,
                Created = dto.Created,
                LastUpdatedOn = dto.LastUpdatedOn,
                ConversionUnits = dto.ConversionUnits.ToConversionUnitEntities()
            };
        }
        public static List<ConversionUnit> ToConversionUnitEntities(this IEnumerable<ConversionUnitDto> dtos)
        {
            return dtos.ToList().Select(x => x.ToConversionUnitEntity()).ToList();
        }
        public static List<UnitOfMeasurementDTO> ToDtos(this IReadOnlyList<UnitOfMeasure> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static ConversionUnit ToConversionUnitEntity(this ConversionUnitDto dto)
        {
            if (dto == null)
                return null;

            return new ConversionUnit
            {
                Id = dto.Id,
                Unit = dto.Unit,
                Value = dto.Value
            };
        }
        public static List<UnitOfMeasurementDTO> ToDtos(this IFindFluent<UnitOfMeasure, UnitOfMeasure> unitOfMeasureEntityList)
        {
            return unitOfMeasureEntityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}
