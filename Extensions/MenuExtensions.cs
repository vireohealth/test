﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.Menus;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class MenuExtensions
    {
        public static MenuDto ToDto(this Menu entity)
        {
            if (entity == null)
                return null;

            return new MenuDto
            {
                Id = entity.Id,
                Name = entity.Name,
                IsActive = entity.IsActive,
                Action=entity.Action,
                Icon=entity.Icon,
                Title=entity.Title,
                Key=entity.Key,
                OrderNumber=entity.OrderNumber,
                UserType=entity.UserType
            };
        }
        public static List<MenuDto> ToDtos(this IReadOnlyList<Menu> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static List<MenuDto> ToDtos(this IFindFluent<Menu, Menu> menuEntityList)
        {
            return menuEntityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}
