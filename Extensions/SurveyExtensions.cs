﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.Surveys;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class SurveyExtensions
    {
        public static Survey ToEntity(this SurveyDto dto)
        {
            if (dto == null)
                return null;

            return new Survey
            {
                Id = dto.Id,
                Name = dto.Name,
                IsActive = dto.IsActive,
                CreatedOn = dto.CreatedOn,
                Product = dto.Product.ToEntity(),
                Frequency=dto.Frequency.ToEntity(),
                SurveyType=dto.SurveyType.ToEntity(),
                EndDate = dto.EndDate,
                LastUpdatedOn = dto.LastUpdatedOn,
                Patients = dto.Patients.ToEntities(),
                StartDate = dto.StartDate,
                Status = dto.Status,
                TimeToComplete=dto.TimeToComplete,
                TotalInputQuestions=dto.TotalInputQuestions,
                FormProperties = (!string.IsNullOrEmpty(dto.FormProperties))
                ? MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(dto.FormProperties) : new BsonDocument(),
                IsSelectedAll=dto.IsSelectedAll,
                QuestionsAndAnswers=dto.QuestionsAndAnswers.ToQuestionAndAnswerEntities()
            };
        }
        public static PatientDatesInfoDto ToDto(this PatientDatesInfo entity)
        {
            if (entity == null)
                return null;

            return new PatientDatesInfoDto
            {
                SubmittedDate = entity.SubmittedDate,
                CompletionPercent = entity.CompletionPercent,
                FormPropertyValues = (entity.FormPropertyValues != null) ? entity.FormPropertyValues.ToJson() : "",
                IsCompleted = entity.IsCompleted,
                TotalFilledInputQuestions = entity.TotalFilledInputQuestions,
                QuestionsAndAnswers=entity.QuestionsAndAnswers.ToQuestionAndAnswerDtos()
            };
        }

        public static PatientSurveyDetailsDto ToDto(this PatientSurveyDetails entity)
        {
            if (entity == null)
                return null;

            return new PatientSurveyDetailsDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Gender=entity.Gender,
                DatesInfo = entity.DatesInfo.ToDtos()
            };
        }

        public static BaseSubjectDto ToDto(this BasicDetails entity)
        {
            if (entity == null)
                return null;

            return new BaseSubjectDto
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }
        public static BaseSubjectDto ToSubjectDto(this BaseSubject entity)
        {
            if (entity == null)
                return null;

            return new BaseSubjectDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Gender=entity.Gender
            };
        }

        public static List<BasicDetails> ToEntities(this IEnumerable<BaseSubjectDto> dtos)
        {
            return dtos.ToList().Select(x => x.ToEntity()).ToList();
        }
        public static BasicDetails ToEntity(this BaseSubjectDto dto)
        {
            if (dto == null)
                return null;

            return new BasicDetails
            {
                Id = dto.Id,
                Name = dto.Name
            };
        }
        public static BaseSubject ToSubjectEntity(this BaseSubjectDto dto)
        {
            if (dto == null)
                return null;

            return new BaseSubject
            {
                Id = dto.Id,
                Name = dto.Name,
                Gender=dto.Gender
            };
        }
        public static PatientSurveyDetails ToEntity(this PatientSurveyDetailsDto dto)
        {
            if (dto == null)
                return null;

            return new PatientSurveyDetails
            {
                Id = dto.Id,
                Name = dto.Name,
                Gender=dto.Gender,
                DatesInfo = dto.DatesInfo.ToEntities()
            };
        }
        public static List<PatientSurveyDetails> ToEntities(this IEnumerable<PatientSurveyDetailsDto> dtos)
        {
            return dtos.ToList().Select(x => x.ToEntity()).ToList();
        }
        public static PatientDatesInfo ToEntity(this PatientDatesInfoDto dto)
        {
            if (dto == null)
                return null;

            return new PatientDatesInfo
            {
                SubmittedDate = dto.SubmittedDate,
                CompletionPercent = dto.CompletionPercent,
                FormPropertyValues= (!string.IsNullOrEmpty(dto.FormPropertyValues))
                ? MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(dto.FormPropertyValues) : new BsonDocument(),
                IsCompleted =dto.IsCompleted,
                TotalFilledInputQuestions=dto.TotalFilledInputQuestions,
                QuestionsAndAnswers=dto.QuestionsAndAnswers.ToQuestionAndAnswerEntities()
            };
        }
        public static List<PatientDatesInfo> ToEntities(this IEnumerable<PatientDatesInfoDto> dtos)
        {
            return dtos.ToList().Select(x => x.ToEntity()).ToList();
        }

        public static SurveyDto ToDto(this Survey entity)
        {
            if (entity == null)
                return null;

            return new SurveyDto
            {
                Id = entity.Id,
                Name = entity.Name,
                IsActive = entity.IsActive,
                CreatedOn = entity.CreatedOn,
                Product = entity.Product.ToDto(),
                Frequency=entity.Frequency.ToDto(),
                SurveyType=entity.SurveyType.ToDto(),
                EndDate = entity.EndDate,
                LastUpdatedOn = entity.LastUpdatedOn,
                Patients = entity.Patients.ToDtos(),
                StartDate = entity.StartDate,
                Status=entity.Status,
                TimeToComplete=entity.TimeToComplete,
                TotalInputQuestions=entity.TotalInputQuestions,
                FormProperties = (entity.FormProperties != null) ? entity.FormProperties.ToJson() : "",
                IsSelectedAll=entity.IsSelectedAll,
                QuestionsAndAnswers=entity.QuestionsAndAnswers.ToQuestionAndAnswerDtos()
            };
        }
        public static SurveyDto ToDto(this Survey entity, string patientId)
        {
            if (entity == null)
                return null;

            return new SurveyDto
            {
                Id = entity.Id,
                Name = entity.Name,
                IsActive = entity.IsActive,
                CreatedOn = entity.CreatedOn,
                Product = entity.Product.ToDto(),
                Frequency = entity.Frequency.ToDto(),
                SurveyType = entity.SurveyType.ToDto(),
                EndDate = entity.EndDate,
                LastUpdatedOn = entity.LastUpdatedOn,
                Patients = entity.Patients.Where(patient=>patient.Id == patientId).ToList().ToDtos(),
                StartDate = entity.StartDate,
                Status = entity.Status,
                TimeToComplete = entity.TimeToComplete,
                TotalInputQuestions = entity.TotalInputQuestions,
                FormProperties = (entity.FormProperties != null) ? entity.FormProperties.ToJson() : "",
                IsSelectedAll=entity.IsSelectedAll,
                QuestionsAndAnswers=entity.QuestionsAndAnswers.ToQuestionAndAnswerDtos()
            };
        }
        public static List<SurveyDto> ToDtos(this IReadOnlyList<Survey> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static List<SurveyDto> ToDtos(this IReadOnlyList<Survey> entityList,string patientId)
        {
            return entityList.ToList().Select(x => x.ToDto(patientId)).ToList();
        }
        public static List<BaseSubjectDto> ToDtos(this IReadOnlyList<BaseSubject> entityList)
        {
            return entityList.ToList().Select(x => x.ToSubjectDto()).ToList();
        }
        public static List<BaseSubjectDto> ToDtos(this IFindFluent<BaseSubject, BaseSubject> entityList)
        {
            return entityList.ToList().Select(x => x.ToSubjectDto()).ToList();
        }
        public static List<PatientDatesInfoDto> ToDtos(this IReadOnlyList<PatientDatesInfo> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static List<PatientDatesInfoDto> ToDtos(this IFindFluent<PatientDatesInfo, PatientDatesInfo> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static List<PatientSurveyDetailsDto> ToDtos(this IReadOnlyList<PatientSurveyDetails> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static List<PatientSurveyDetailsDto> ToDtos(this IFindFluent<PatientSurveyDetails, PatientSurveyDetails> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }


        public static AnswersDto ToAnswerDto(this Answers entity)
        {
            if (entity == null)
                return null;

            return new AnswersDto
            {
                Answer = entity.Answer,
                Description = entity.Description,
                Image = entity.Image,
                Title = entity.Title
            };
        }
        public static List<AnswersDto> ToAnswerDtos(this IReadOnlyList<Answers> entityList)
        {
            return entityList.ToList().Select(x => x.ToAnswerDto()).ToList();
        }
        public static List<AnswersDto> ToAnswerDtos(this IFindFluent<Answers, Answers> entityList)
        {
            return entityList.ToList().Select(x => x.ToAnswerDto()).ToList();
        }
        public static Answers ToAnswerEntity(this AnswersDto dto)
        {
            if (dto == null)
                return null;

            return new Answers
            {
                Answer = dto.Answer,
                Description = dto.Description,
                Image = dto.Image,
                Title = dto.Title
            };
        }
        public static List<Answers> ToAnswerEntities(this IEnumerable<AnswersDto> dtos)
        {
            return dtos.ToList().Select(x => x.ToAnswerEntity()).ToList();
        }
        public static QuestionAndAnswerDto ToQuestionAndAnswerDto(this QuestionAndAnswer entity)
        {
            if (entity == null)
                return null;

            return new QuestionAndAnswerDto
            {
                Questions = entity.Question,
                Answers = entity.Answers.ToAnswerDtos()
            };
        }
        public static List<QuestionAndAnswerDto> ToQuestionAndAnswerDtos(this IReadOnlyList<QuestionAndAnswer> entityList)
        {
            return entityList.ToList().Select(x => x.ToQuestionAndAnswerDto()).ToList();
        }
        public static List<QuestionAndAnswerDto> ToQuestionAndAnswerDtos(this IFindFluent<QuestionAndAnswer, QuestionAndAnswer> entityList)
        {
            return entityList.ToList().Select(x => x.ToQuestionAndAnswerDto()).ToList();
        }
        public static QuestionAndAnswer ToQuestionAndAnswerEntity(this QuestionAndAnswerDto dto)
        {
            if (dto == null)
                return null;

            return new QuestionAndAnswer
            {
                Question = dto.Questions,
                Answers = dto.Answers.ToAnswerEntities()
            };
        }
        public static List<QuestionAndAnswer> ToQuestionAndAnswerEntities(this IEnumerable<QuestionAndAnswerDto> dtos)
        {
            return dtos.ToList().Select(x => x.ToQuestionAndAnswerEntity()).ToList();
        }


    }
}
