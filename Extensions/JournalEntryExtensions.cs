﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.JournalEntries;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class JournalEntryExtensions
    {
        public static List<JournalEntryDto> ToDtos(this IFindFluent<JournalEntry, JournalEntry> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static JournalEntryDto ToDto(this JournalEntry entity)
        {
            if (entity == null)
                return null;

            return new JournalEntryDto
            {
                Id = entity.Id,
                Title = entity.Title,
                Notes = entity.Notes,
                JournalDate = entity.JournalDate,
                Time = entity.Time,
                CreatedOn = entity.CreatedOn,
                LastUpdatedOn = entity.LastUpdatedOn,
                IsActive = entity.IsActive,
                Patient = entity.Patient.ToSubjectDto()
            };
        }
        public static JournalEntry ToEntity(this JournalEntryDto dto)
        {
            if (dto == null)
                return null;

            return new JournalEntry
            {
                Id = dto.Id,
                Title = dto.Title,
                Notes = dto.Notes,
                JournalDate = dto.JournalDate,
                Time = dto.Time,
                IsActive = dto.IsActive,
                CreatedOn = dto.CreatedOn,
                LastUpdatedOn = dto.LastUpdatedOn,
                Patient = dto.Patient.ToSubjectEntity()
            };
        }
        public static List<JournalEntryDto> ToDtos(this IReadOnlyList<JournalEntry> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}
