﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Products;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class ProductExtensions
    {
        public static List<ProductDto> ToDtos(this IFindFluent<Product, Product> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static ProductDto ToDto(this Product entity)
        {
            if (entity == null)
                return null;

            return new ProductDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Amount=entity.Amount,
                AmountMesurement=entity.AmountMesurement,
                Frequency=entity.Frequency,
                CreatedOn=entity.CreatedOn,
                IsVireoProduct = entity.IsVireoProduct,
                LastUpdatedOn =entity.LastUpdatedOn,
                IsActive = entity.IsActive,
                FrequencyMeasurement=entity.FrequencyMeasurement
            };
        }
        public static Product ToEntity(this ProductDto dto)
        {
            if (dto == null)
                return null; 

            return new Product
            {
                Id = dto.Id,
                Name = dto.Name,
                Amount=dto.Amount,
                AmountMesurement=dto.AmountMesurement,
                Frequency=dto.Frequency,
                IsActive = dto.IsActive,
                IsVireoProduct = dto.IsVireoProduct,
                CreatedOn =dto.CreatedOn,
                LastUpdatedOn=dto.LastUpdatedOn,
                FrequencyMeasurement=dto.FrequencyMeasurement
            };
        }
        public static List<ProductDto> ToDtos(this IReadOnlyList<Product> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}
