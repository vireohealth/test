﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.MedicationJournal;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class MedicationJournalExtensions
    {
        public static List<MedicationJournalDto> ToDtos(this IFindFluent<MedicationJournalEntry, MedicationJournalEntry> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static MedicationJournalDto ToDto(this MedicationJournalEntry entity)
        {
            if (entity == null)
                return null;

            return new MedicationJournalDto
            {
                Id = entity.Id,
                Product = entity.Product.ToDto(),
                Patient = entity.Patient.ToSubjectDto(),
                MedicationJournalDate = entity.MedicationJournalDate,
                Time = entity.Time,
                Notes=entity.Notes,
                CreatedOn = entity.CreatedOn,
                LastUpdatedOn = entity.LastUpdatedOn,
                IsActive = entity.IsActive
            };
        }
        public static MedicationJournalEntry ToEntity(this MedicationJournalDto dto)
        {
            if (dto == null)
                return null;

            return new MedicationJournalEntry
            {
                Id = dto.Id,
                Product = dto.Product.ToEntity(),
                Patient = dto.Patient.ToSubjectEntity(),
                MedicationJournalDate = dto.MedicationJournalDate,
                Time = dto.Time,
                Notes=dto.Notes,
                IsActive = dto.IsActive,
                CreatedOn = dto.CreatedOn,
                LastUpdatedOn = dto.LastUpdatedOn
            };
        }
        public static List<MedicationJournalDto> ToDtos(this IReadOnlyList<MedicationJournalEntry> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}
