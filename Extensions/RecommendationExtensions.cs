﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.Recommendations;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class RecommendationExtensions
    {
        public static List<RecommendationDto> ToDtos(this IFindFluent<Recommendation, Recommendation> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static RecommendationDto ToDto(this Recommendation entity)
        {
            if (entity == null)
                return null;

            return new RecommendationDto
            {
                Id = entity.Id,
                Name = entity.Name,
                IsActive = entity.IsActive,
                CreatedOn = entity.CreatedOn,
                LastUpdatedOn = entity.LastUpdatedOn
            };
        }
        public static Recommendation ToEntity(this RecommendationDto dto)
        {
            if (dto == null)
                return null;

            return new Recommendation
            {
                Id = dto.Id,
                Name = dto.Name,
                IsActive = dto.IsActive,
                CreatedOn = dto.CreatedOn,
                LastUpdatedOn = dto.LastUpdatedOn
            };
        }
        public static List<RecommendationDto> ToDtos(this IReadOnlyList<Recommendation> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}