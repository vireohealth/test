﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.TempSubjects;

namespace VireoIntegrativeProgram.Extensions
{
    public static class TempSubjectExtensions
    {
        public static List<TempSubjectDto> ToDtos(this IFindFluent<Models.TempSubjects, Models.TempSubjects> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static TempSubjectDto ToDto(this Models.TempSubjects entity)
        {
            if (entity == null)
                return null;

            return new TempSubjectDto
            {
                Id = entity.Id,
                Email = entity.Email,
                CreatedOn = entity.CreatedOn,
                LastUpdatedOn = entity.LastUpdatedOn,
                IsActive = entity.IsActive
            };
        }
        public static Models.TempSubjects ToEntity(this TempSubjectDto dto)
        {
            if (dto == null)
                return null;

            return new Models.TempSubjects
            {
                Id = dto.Id,
                Email = dto.Email,
                IsActive = dto.IsActive,
                CreatedOn = dto.CreatedOn,
                LastUpdatedOn = dto.LastUpdatedOn
            };
        }
        public static List<TempSubjectDto> ToDtos(this IReadOnlyList<Models.TempSubjects> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}
