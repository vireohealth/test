﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.EducationalVideos;
using VireoIntegrativeProgram.Business.Subjects;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class EducationalVideoExtensions
    {
        public static EducationalVideoDto ToDto(this EducationalVideo entity)
        {
            if (entity == null)
                return null;

            return new EducationalVideoDto
            {

                Id = entity.Id,
                Description = entity.Description,
                EventDate = entity.EventDate,
                EventId = entity.EventId,
                EventName = entity.EventName,
                IsActive = entity.IsActive,
                IsEmailCampaign = entity.IsEmailCampaign,
                IsScheduledBroadcastByDate = entity.IsScheduledBroadcastByDate,
                IsScheduledBroadcastByEvent = entity.IsScheduledBroadcastByEvent,
                PostedOn = entity.PostedOn,
                LastUpdatedOn=entity.LastUpdatedOn,
                ThumbImagePath = entity.ThumbImagePath,
                Title = entity.Title,
                VideoPath = entity.VideoPath,
                IsDefaultThumb=entity.IsDefaultThumb,
                ImageName=entity.ImageName,
                SubjectList=entity.SubjectList.ToSubjectDtos()
            };
        }
        public static EducationalVideo ToEntity(this EducationalVideoDto dto)
        {
            if (dto == null)
                return null;

            return new EducationalVideo
            {

                Id = dto.Id,
                Description = dto.Description,
                EventDate = dto.EventDate,
                EventId = dto.EventId,
                EventName = dto.EventName,
                IsActive = dto.IsActive,
                IsEmailCampaign = dto.IsEmailCampaign,
                IsScheduledBroadcastByDate = dto.IsScheduledBroadcastByDate,
                IsScheduledBroadcastByEvent = dto.IsScheduledBroadcastByEvent,
                PostedOn = dto.PostedOn,
                LastUpdatedOn=dto.LastUpdatedOn,
                ThumbImagePath = dto.ThumbImagePath,
                Title = dto.Title,
                VideoPath = dto.VideoPath,
                IsDefaultThumb = dto.IsDefaultThumb,
                ImageName=dto.ImageName,
                SubjectList=dto.SubjectList.ToSubjectEntities()
            };
        }
        public static List<EducationalVideoDto> ToDtos(this IReadOnlyList<EducationalVideo> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static List<EducationalVideoDto> ToDtos(this IFindFluent<EducationalVideo, EducationalVideo> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static SubjectDetailsDto ToSubjectDto(this SubjectDetails entity)
        {
            if (entity == null)
                return null;

            return new SubjectDetailsDto
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }
        public static SubjectDetails ToSubjectEntity(this SubjectDetailsDto dto)
        {
            if (dto == null)
                return null;

            return new SubjectDetails
            {
                Id = dto.Id,
                Name = dto.Name
            };
        }
        public static List<SubjectDetailsDto> ToSubjectDtos(this IReadOnlyList<SubjectDetails> entityList)
        {
            return entityList.ToList().Select(x => x.ToSubjectDto()).ToList();
        }
        public static List<SubjectDetailsDto> ToSubjectDtos(this IFindFluent<SubjectDetails, SubjectDetails> entityList)
        {
            return entityList.ToList().Select(x => x.ToSubjectDto()).ToList();
        }
        public static List<SubjectDetails> ToSubjectEntities(this IEnumerable<SubjectDetailsDto> dtos)
        {
            return dtos.ToList().Select(x => x.ToSubjectEntity()).ToList();
        }
    }
}
