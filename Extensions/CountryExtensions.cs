﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Countries;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class CountryExtensions
    {
        public static StateDto ToDto(this State entity)
        {
            if (entity == null)
                return null;

            return new StateDto
            {
                Code = entity.Code,
                Name = entity.Name,
                Division = entity.Division
            };
        }

        public static List<StateDto> ToDtos(this IOrderedEnumerable<State> entities)
        {
            return entities.ToList().Select(x => x.ToDto()).ToList();
        }

        public static CountryDto ToDto(this Country entity)
        {
            if (entity == null)
                return null;

            return new CountryDto
            {
                Code = entity.Code,
                Name = entity.Name,
                Id = entity.Id,
                States=entity.States.OrderBy(i => i.Name).ToDtos()
            };
        }

        public static List<CountryDto> ToDtos(this IFindFluent<Country, Country> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}
