﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.Dosing;
using VireoIntegrativeProgram.Business.Products;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class DosingExtensions
    {
        public static List<DosingDto> ToDtos(this IFindFluent<Dosings, Dosings> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static DosingDto ToDto(this Dosings entity)
        {
            if (entity == null)
                return null;

            return new DosingDto
            {
                Id = entity.Id,
                CreatedOn = entity.CreatedOn,
                IsActive = entity.IsActive,
                LastUpdatedOn=entity.LastUpdatedOn,
                PEGScore=entity.PEGScore,
                TSQMSatisfaction=entity.TSQMSatisfaction,
                TSQMSeverity=entity.TSQMSeverity,
                TSQMSideEffects=entity.TSQMSideEffects,
                Product=entity.Product.ToDto(),
                Recomendation=entity.Recomendation.ToDto()
            };
        }
        public static Dosings ToEntity(this DosingDto dto)
        {
            if (dto == null)
                return null;

            return new Dosings
            {
                Id = dto.Id,
                IsActive = dto.IsActive,
                CreatedOn=dto.CreatedOn,
                LastUpdatedOn=dto.LastUpdatedOn,
                PEGScore=dto.PEGScore,
                TSQMSatisfaction=dto.TSQMSatisfaction,
                TSQMSeverity=dto.TSQMSeverity,
                TSQMSideEffects=dto.TSQMSideEffects,
                Product=dto.Product.ToEntity(),
                Recomendation=dto.Recomendation.ToEntity()
            };
        }

        
    }
}
