﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.Frequencies;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class FrequencyExtensions
    {
        public static List<FrequencyDto> ToDtos(this IFindFluent<Frequency, Frequency> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static FrequencyDto ToDto(this Frequency entity)
        {
            if (entity == null)
                return null;

            return new FrequencyDto
            {
                Id = entity.Id,
                Name = entity.Name,
                IsActive = entity.IsActive
            };
        }
        public static Frequency ToEntity(this FrequencyDto dto)
        {
            if (dto == null)
                return null;

            return new Frequency
            {
                Id = dto.Id,
                Name = dto.Name,
                IsActive = dto.IsActive
            };
        }
    }
}
