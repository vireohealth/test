﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.FormControls;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class FormControlExtensions
    {
        public static FormControlDto ToDto(this FormControl entity)
        {
            if (entity == null)
                return null;

            return new FormControlDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Properties = (entity.Properties != null) ? entity.Properties.ToJson() : "",
                IsActive = entity.IsActive,
            };
        }
        public static FormControl ToEntity(this FormControlDto dto)
        {
            if (dto == null)
                return null;

            return new FormControl
            {
                Id = dto.Id,
                Name = dto.Name,
                Properties = (!string.IsNullOrEmpty(dto.Properties))
                ? MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(dto.Properties) : new BsonDocument(),
                IsActive = dto.IsActive,
            };
        }
        public static List<FormControlDto> ToDtos(this IFindFluent<FormControl, FormControl> formControlEntityList)
        {
            return formControlEntityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}
