﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.PasswordHistories;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class PasswordHistoryExtensions
    {
        public static PasswordHistoryDto ToDto(this PasswordHistory entity)
        {
            if (entity == null)
                return null;

            return new PasswordHistoryDto
            {
                Id= entity.Id,
                AddedOn=entity.AddedOn,
                IsActive=entity.IsActive,
                Password=entity.Password,
                SubjectId=entity.SubjectId
            };
        }
        public static List<PasswordHistoryDto> ToDtos(this IFindFluent<PasswordHistory, PasswordHistory> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}
