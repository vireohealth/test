﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.Logs;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class SubjectLogExtensions
    {
        public static SubjectLogs ToEntity(this LogDto dto)
        {
            if (dto == null)
                return null;

            return new SubjectLogs
            {
                Id = dto.Id,
                IsActive = dto.IsActive,
                AddedOn = dto.AddedOn,
                DateOfLog = dto.DateOfLog,
                Subject = dto.Subject.ToSubjectEntity(),
                LevelOfPain = dto.LevelOfPain,
                BodySide=dto.BodySide,
                LastUpdatedOn =dto.LastUpdatedOn,
                Medications=dto.Medications.ToEntities(),
                Notes=dto.Notes,
                PainLocation=dto.PainLocation,
                PainType=dto.PainType,
                Sleep=dto.Sleep,
                Treatments=dto.Treatments,
                Triggers=dto.Triggers,
                SleepDisturbNotes=dto.SleepDisturbNotes,
                PainTypeOthersNotes=dto.PainTypeOthersNotes
            };
        }
        public static LogDto ToDto(this SubjectLogs  entity)
        {
            if (entity == null)
                return null;

            return new LogDto
            {
                Id = entity.Id,
                IsActive = entity.IsActive,
                AddedOn = entity.AddedOn,
                DateOfLog = entity.DateOfLog,
                Subject = entity.Subject.ToSubjectDto(),
                LevelOfPain = entity.LevelOfPain,
                BodySide = entity.BodySide,
                LastUpdatedOn = entity.LastUpdatedOn,
                Medications = entity.Medications.ToDtos(),
                Notes = entity.Notes,
                PainLocation = entity.PainLocation,
                PainType = entity.PainType,
                Sleep = entity.Sleep,
                Treatments = entity.Treatments,
                Triggers = entity.Triggers,
                SleepDisturbNotes=entity.SleepDisturbNotes,
                PainTypeOthersNotes=entity.PainTypeOthersNotes
            };
        }
        public static List<LogDto> ToDtos(this IFindFluent<SubjectLogs, SubjectLogs> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static List<LogDto> ToDtos(this IReadOnlyList<SubjectLogs> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}
