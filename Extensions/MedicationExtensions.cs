﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.Logs;
using VireoIntegrativeProgram.Business.Medications;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class MedicationExtensions
    {
        public static MedicationDto ToDto(this Medications entity)
        {
            if (entity == null)
                return null;

            return new MedicationDto
            {
                Name = entity.Name,
                Id=entity.Id,
                IsVireoProduct=entity.IsVireoProduct,
                IsActive = entity.IsActive
            };
        }
        public static Medications ToEntity(this MedicationDto dto)
        {
            if (dto == null)
                return null;

            return new Medications
            {
                Name = dto.Name,
                Id = dto.Id,
                IsVireoProduct = dto.IsVireoProduct,
                IsActive = dto.IsActive
            };
        }

        public static MedicationDosageDto ToDto(this MedicationDosage entity)
        {
            if (entity == null)
                return null;

            return new MedicationDosageDto
            {
              Medication=entity.Medication.ToDto(),
              Dosage=entity.Dosage,
              Feedback=entity.Feedback,
              FeedbackProductId=entity.FeedbackProductId
            };
        }
        public static MedicationDosage ToEntity(this MedicationDosageDto dto)
        {
            if (dto == null)
                return null;

            return new MedicationDosage
            {
                Medication = dto.Medication.ToEntity(),
                Dosage = dto.Dosage,
                Feedback=dto.Feedback,
                FeedbackProductId=dto.FeedbackProductId
            };
        }
        public static List<MedicationDto> ToDtos(this IFindFluent<Medications, Medications> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static List<MedicationDosageDto> ToDtos(this IFindFluent<MedicationDosage, MedicationDosage> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static List<MedicationDosage> ToEntities(this IEnumerable<MedicationDosageDto> dtos)
        {
            return dtos.ToList().Select(x => x.ToEntity()).ToList();
        }
        public static List<Medications> ToEntities(this IEnumerable<MedicationDto> dtos)
        {
            return dtos.ToList().Select(x => x.ToEntity()).ToList();
        }
        public static List<MedicationDosageDto> ToDtos(this IReadOnlyList<MedicationDosage> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}
