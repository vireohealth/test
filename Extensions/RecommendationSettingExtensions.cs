﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.RecommendationSettings;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class RecommendationSettingExtensions
    {
        public static List<RecommendationSettingDto> ToDtos(this IFindFluent<RecommendationSettings, RecommendationSettings> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static RecommendationSettingDto ToDto(this RecommendationSettings entity)
        {
            if (entity == null)
                return null;

            return new RecommendationSettingDto
            {
                Id = entity.Id,
                CreatedOn = entity.CreatedOn,
                IsActive = entity.IsActive,
                LastUpdatedOn = entity.LastUpdatedOn,
                PainEffectMoodStatus = entity.PainEffectMoodStatus,
                PainEffectSleepStatus = entity.PainEffectSleepStatus,
                Recommendation = entity.Recommendation.ToDto()
            };
        }

        public static RecommendationSettings ToEntity(this RecommendationSettingDto dto)
        {
            if (dto == null)
                return null;

            return new RecommendationSettings
            {
                Id = dto.Id,
                IsActive = dto.IsActive,
                CreatedOn = dto.CreatedOn,
                LastUpdatedOn = dto.LastUpdatedOn,
                PainEffectMoodStatus = dto.PainEffectMoodStatus,
                PainEffectSleepStatus = dto.PainEffectSleepStatus,
                Recommendation = dto.Recommendation.ToEntity()
            };
        }
    }
}
