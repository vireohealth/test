﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.BodyParts;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class BodyPartUpdateExtensions
    {
        public static List<BodyPartUpsertDto> ToUpdateDtos(this IFindFluent<BodyPartsDetails, BodyPartsDetails> entityList)
        {
            return entityList.ToList().Select(x => x.ToUpdateDto()).ToList();
        }
        public static BodyPartUpsertDto ToUpdateDto(this BodyPartsDetails entity)
        {
            if (entity == null)
                return null;

            return new BodyPartUpsertDto
            {
                Id = entity.Id,
                BodyPartName = entity.BodyPartName,
                BodyPartImageName = entity.BodyPartImageName,
                BodyPartType = entity.BodyPartType,
                CreatedOn = entity.CreatedOn,
                LastUpdatedOn = entity.LastUpdatedOn,
                IsActive = entity.IsActive
            };
        }
        public static BodyPartsDetails ToUpdateEntity(this BodyPartUpsertDto dto)
        {
            if (dto == null)
                return null;

            return new BodyPartsDetails
            {
                Id = dto.Id,
                BodyPartName = dto.BodyPartName,
                BodyPartImageName = dto.BodyPartImageName,
                BodyPartType = dto.BodyPartType,
                IsActive = dto.IsActive,
                CreatedOn = dto.CreatedOn,
                LastUpdatedOn = dto.LastUpdatedOn
            };
        }
        public static List<BodyPartUpsertDto> ToUpdateDtos(this IReadOnlyList<BodyPartsDetails> entityList)
        {
            return entityList.ToList().Select(x => x.ToUpdateDto()).ToList();
        }
    }
}
