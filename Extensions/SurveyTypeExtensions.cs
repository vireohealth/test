﻿
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.SurveyTypes;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class SurveyTypeExtensions
    {
        public static SurveyTypesDto ToDto(this SurveyType entity)
        {
            if (entity == null)
                return null;

            return new SurveyTypesDto
            {
                Id = entity.Id,
                Name = entity.Name,
                IsActive = entity.IsActive,
            };
        }
        public static SurveyType ToEntity(this SurveyTypesDto dto)
        {
            if (dto == null)
                return null;

            return new SurveyType
            {
                Id = dto.Id,
                Name = dto.Name,
                IsActive = dto.IsActive,
            };
        }
        public static List<SurveyTypesDto> ToDtos(this IFindFluent<SurveyType, SurveyType> surveyTypeEntityList)
        {
            return surveyTypeEntityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}
