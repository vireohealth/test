﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using VireoIntegrativeProgram.Business.FitBitTokenManager;
using VireoIntegrativeProgram.Business.Subjects;
using VireoIntegrativeProgram.Common.Enums;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class SubjectExtensions
    {
        private const string Patient = "Patient";
        public static RefreshTokenDto ToDto(this RefreshToken dto)
        {
            if (dto == null)
                return null;

            return new RefreshTokenDto
            {
                Created = dto.Created,
                CreatedByIp = dto.CreatedByIp,
                Expires = dto.Expires,
                Id = dto.Id,
                ReplacedByToken = dto.ReplacedByToken,
                Revoked = dto.Revoked,
                RevokedByIp = dto.RevokedByIp,
                Token = dto.Token
            };
        }
        public static PatientSurveyDetails ToPatientSurvey(this SubjectDto dto)
        {
            if (dto == null)
                return null;
            return new PatientSurveyDetails
            {
                Id = dto.Id,
                Name = dto.Name,
                DatesInfo = new List<PatientDatesInfo>() {
                    new PatientDatesInfo {
                        SubmittedDate=null, CompletionPercent=0, FormPropertyValues = new BsonDocument(), IsCompleted=false, TotalFilledInputQuestions=0
                    }
                }
            };
        }
        public static RefreshToken ToEntity(this RefreshTokenDto entity)
        {
            if (entity == null)
                return null;

            return new RefreshToken
            {
                Created = entity.Created,
                CreatedByIp = entity.CreatedByIp,
                Expires = entity.Expires,
                Id = entity.Id,
                ReplacedByToken = entity.ReplacedByToken,
                Revoked = entity.Revoked,
                RevokedByIp = entity.RevokedByIp,
                Token = entity.Token
            };
        }

        public static List<RefreshToken> ToEntities(this IEnumerable<RefreshTokenDto> dtos)
        {
            return dtos.ToList().Select(x => x.ToEntity()).ToList();
        }

        public static List<RefreshTokenDto> ToDtos(this IEnumerable<RefreshToken> entities)
        {
            return entities.ToList().Select(x => x.ToDto()).ToList();
        }

        public static Subject ToEntity(this SubjectDto dto)
        {
            if (dto == null)
                return null;

            return new Subject
            {
                Id = dto.Id,
                Name = dto.Name,
                AddedOn = dto.CreatedOn,
                Password = dto.Password,
                Email = dto.Email,
                Address = dto.Address,
                Gender = dto.Gender,
                Phone = dto.Phone,
                IsActive = dto.IsActive,
                RefreshTokens = dto?.RefreshTokens?.ToEntities(),
                LastUpdatedOn = dto.LastUpdatedOn,
                ActivatedOn = dto.ActivatedOn,
                UserType = dto.UserType,
                ResetMailSentDate = dto.ResetMailSentDate,
                City = dto.City,
                PostalCode = dto.PostalCode,
                ProfilePic = dto.ProfilePicName,
                StateCode = dto.StateCode,
                StateName = dto.StateName,
                IsDeleted = dto.IsDeleted,
                IsMailExpired = (dto.IsMailExpired == null) ? false : dto.IsMailExpired,
                LastPasswordUpdatedDate = dto.LastPasswordUpdatedDate,
                Notes = dto.Notes,
                FitBitAccessToken = dto.FitBitAccessToken?.ToFitBitAccessTokenEntity(),
                MyPreferedDispenseries = dto.MyPreferedDispenseries,
                UserStatus = dto.IsDeleted ? SubjectStatus.Deleted : dto.IsActive ? SubjectStatus.Activated : SubjectStatus.NotActivated,
                LastFeedbackDate=dto.LastFeedbackDate
            };
        }
        public static FitBitAccessToken ToFitBitAccessTokenEntity(this AccessTokenDto dto)
        {
            if (dto == null)
                return null;

            return new FitBitAccessToken
            {
                ExpiresIn = dto.ExpiresIn,
                RefreshToken = dto.RefreshToken,
                Scope = dto.Scope,
                Token = dto.Token,
                TokenType = dto.TokenType,
                UserId = dto.UserId,
                UtcExpirationDate = dto.UtcExpirationDate
            };
        }
        public static AccessTokenDto ToFitBitAccessTokenDto(this FitBitAccessToken dto)
        {
            if (dto == null)
                return null;

            return new AccessTokenDto
            {
                ExpiresIn = dto.ExpiresIn,
                RefreshToken = dto.RefreshToken,
                Scope = dto.Scope,
                Token = dto.Token,
                TokenType = dto.TokenType,
                UserId = dto.UserId,
                UtcExpirationDate = dto.UtcExpirationDate
            };
        }
        public static Subject ToPatientEntity(this PatientDto dto)
        {
            if (dto == null)
                return null;

            return new Subject
            {
                Id = dto.Id,
                Name = dto.Name,
                AddedOn = System.DateTime.UtcNow,
                Password = dto.Password,
                Email = dto.Email,
                Address = dto.Address,
                Gender = dto.Gender,
                Phone = dto.Phone,
                IsActive = true,
                LastUpdatedOn = System.DateTime.UtcNow,
                ActivatedOn = System.DateTime.UtcNow,
                UserType = Patient,
                ResetMailSentDate = null,
                City = dto.City,
                PostalCode = "",
                StateCode = "",
                StateName = "",
                IsDeleted = false,
                MyPreferedDispenseries = dto.MyPreferedDispenseries,
                UserStatus = SubjectStatus.Activated,
                RefreshTokens = new List<RefreshToken>(),
                LastFeedbackDate = dto.LastFeedbackdDate

            };
        }

        public static SubjectDto ToDto(this Subject entity)
        {
            if (entity == null)
                return null;

            return new SubjectDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Address = entity.Address,
                Gender = entity.Gender,
                Phone = entity.Phone,
                Email = entity.Email,
                IsActive = entity.IsActive,
                RefreshTokens = entity?.RefreshTokens?.ToDtos(),
                CreatedOn = entity.AddedOn,
                LastUpdatedOn = entity.LastUpdatedOn,
                ActivatedOn = entity.ActivatedOn,
                UserType = entity.UserType,
                Password = entity.Password,
                ResetMailSentDate = entity.ResetMailSentDate,
                City = entity.City,
                PostalCode = entity.PostalCode,
                StateCode = entity.StateCode,
                StateName = entity.StateName,
                IsDeleted = entity.IsDeleted,
                ProfilePicName = entity.ProfilePic,
                Notes = entity.Notes,
                IsMailExpired = entity.IsMailExpired == null ? false : entity.IsMailExpired,
                LastPasswordUpdatedDate = entity.LastPasswordUpdatedDate,
                MyPreferedDispenseries = entity.MyPreferedDispenseries,
                FitBitAccessToken = entity.FitBitAccessToken.ToFitBitAccessTokenDto(),
                UserStatus = entity.IsDeleted ? SubjectStatus.Deleted : entity.IsActive ? SubjectStatus.Activated : SubjectStatus.NotActivated,
                LastFeedbackDate = entity.LastFeedbackDate
            };
        }
        public static List<SubjectDto> ToDtos(this IReadOnlyList<Subject> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static List<SubjectDto> ToDtos(this IFindFluent<Subject, Subject> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static List<Subject> ToEntities(this IEnumerable<SubjectDto> dtos)
        {
            return dtos.ToList().Select(x => x.ToEntity()).ToList();
        }
    }
}
