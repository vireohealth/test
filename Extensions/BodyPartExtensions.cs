﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VireoIntegrativeProgram.Business.BodyParts;
using VireoIntegrativeProgram.Models;

namespace VireoIntegrativeProgram.Extensions
{
    public static class BodyPartExtensions
    {
        public static List<BodyPartsDto> ToDtos(this IFindFluent<BodyPartsDetails, BodyPartsDetails> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
        public static BodyPartsDto ToDto(this BodyPartsDetails entity)
        {
            if (entity == null)
                return null;

            return new BodyPartsDto
            {
                Id = entity.Id,
                BodyPartName = entity.BodyPartName,
                BodyPartImageName=entity.BodyPartImageName,
                BodyPartType=entity.BodyPartType,
                CreatedOn = entity.CreatedOn,
                LastUpdatedOn = entity.LastUpdatedOn,
                IsActive = entity.IsActive
            };
        }
        public static BodyPartsDetails ToEntity(this BodyPartsDto dto)
        {
            if (dto == null)
                return null;

            return new BodyPartsDetails
            {
                Id = dto.Id,
                BodyPartName = dto.BodyPartName,
                BodyPartImageName=dto.BodyPartImageName,
                BodyPartType=dto.BodyPartType,
                IsActive = dto.IsActive,
                CreatedOn = dto.CreatedOn,
                LastUpdatedOn = dto.LastUpdatedOn
            };
        }
        public static List<BodyPartsDto> ToDtos(this IReadOnlyList<BodyPartsDetails> entityList)
        {
            return entityList.ToList().Select(x => x.ToDto()).ToList();
        }
    }
}
